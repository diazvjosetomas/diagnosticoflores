

CREATE TABLE IF NOT EXISTS `asistencias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `personale_id` int(11) NOT NULL,
  `fecha_asistencia` date DEFAULT NULL,
  `tipoentrada_id` int(11) NOT NULL,
  `hora` varchar(25) DEFAULT NULL,
  `created` date DEFAULT NULL,
  `modified` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `personale_id` (`personale_id`),
  KEY `tipoentrada_id` (`tipoentrada_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `asistencias`
--

INSERT INTO `asistencias` (`id`, `personale_id`, `fecha_asistencia`, `tipoentrada_id`, `hora`, `created`, `modified`) VALUES
(1, 1, '2016-10-24', 1, '09:58:00 PM', '2016-10-24', '2016-10-24');

-- --------------------------------------------------------

--
-- Table structure for table `avisoprivas`
--

CREATE TABLE IF NOT EXISTS `avisoprivas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(300) NOT NULL,
  `nombre_archivo` varchar(500) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `avisoprivas`
--

INSERT INTO `avisoprivas` (`id`, `titulo`, `nombre_archivo`, `created`, `modified`) VALUES
(1, 'Aviso de privacidad', 'upload/lotes_2016-10-30-11-27-00_validar.pdf', '2016-10-30 11:27:00', '2016-10-30 11:27:00');

-- --------------------------------------------------------

--
-- Table structure for table `blogcategorias`
--

CREATE TABLE IF NOT EXISTS `blogcategorias` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `denominacion` varchar(300) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `blogcategorias`
--

INSERT INTO `blogcategorias` (`id`, `denominacion`, `created`, `modified`) VALUES
(1, 'InformaciÃ³n', '2016-10-31 08:05:39', '2016-10-31 08:05:39');

-- --------------------------------------------------------

--
-- Table structure for table `blogentradas`
--

CREATE TABLE IF NOT EXISTS `blogentradas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(300) NOT NULL,
  `contenido` text NOT NULL,
  `blogcategoria_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `nombre_imagen` varchar(45) NOT NULL,
  `ruta_imagen` varchar(45) NOT NULL,
  `tipo_imagen` varchar(45) NOT NULL,
  `carpeta_imagen` varchar(45) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `blogentradas`
--

INSERT INTO `blogentradas` (`id`, `titulo`, `contenido`, `blogcategoria_id`, `user_id`, `nombre_imagen`, `ruta_imagen`, `tipo_imagen`, `carpeta_imagen`, `created`, `modified`) VALUES
(1, 'Lorem Ipsum', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce commodo gravida rutrum. Curabitur dignissim mauris ac libero posuere aliquam. Cras sed ligula et justo rhoncus pretium eget nec dolor. Proin eu sagittis mi, ut fringilla nulla. Vivamus tempus tempus magna eu malesuada. Quisque ac convallis mauris. Quisque pretium tortor sed dui molestie auctor. Quisque nisl mauris, aliquet id libero tempor, elementum auctor eros. Donec pretium purus eget felis hendrerit, vel egestas augue scelerisque. Aenean sit amet nunc in nisi vulputate lacinia sed in nibh. Suspendisse semper enim at urna lacinia, vel vestibulum leo pretium. Etiam sit amet nibh dictum, faucibus lorem at, faucibus ex. Aliquam sem turpis, commodo malesuada mi quis, suscipit dapibus tortor. Aenean scelerisque non leo faucibus malesuada.</p>\r\n', 1, 1, 'a_02112016_102115.jpeg', 'upload/a_02112016_102115.jpeg', 'image/jpeg', 'upload', '2016-10-31 08:23:07', '2016-11-02 22:21:15'),
(2, 'Las Caries', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce commodo gravida rutrum. Curabitur dignissim mauris ac libero posuere aliquam. Cras sed ligula et justo rhoncus pretium eget nec dolorLorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce commodo gravida rutrum. Curabitur dignissim mauris ac libero posuere aliquam. Cras sed ligula et justo rhoncus pretium eget nec dolor</p>\r\n', 1, 1, 'a_02112016_111610.jpeg', 'upload/a_02112016_111610.jpeg', 'image/jpeg', 'upload', '2016-11-02 23:16:10', '2016-11-02 23:16:10');

-- --------------------------------------------------------

--
-- Table structure for table `blogpublicidades`
--

CREATE TABLE IF NOT EXISTS `blogpublicidades` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `carpeta_imagen` varchar(100) NOT NULL,
  `nombre_imagen` varchar(100) NOT NULL,
  `tipo_imagen` varchar(100) NOT NULL,
  `ruta_imagen` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `blogpublicidades`
--

INSERT INTO `blogpublicidades` (`id`, `titulo`, `carpeta_imagen`, `nombre_imagen`, `tipo_imagen`, `ruta_imagen`, `created`, `modified`) VALUES
(1, '', 'upload', 'a_06112016_081916.jpeg', 'image/jpeg', 'upload/a_06112016_081916.jpeg', '2016-11-06 08:19:16', '2016-11-06 08:19:16');

-- --------------------------------------------------------

--
-- Table structure for table `categorias`
--

CREATE TABLE IF NOT EXISTS `categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria` varchar(512) NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `categorias`
--

INSERT INTO `categorias` (`id`, `categoria`, `created`, `modified`) VALUES
(6, 'Horario Suc 1', '2016-10-19', '2016-10-26'),
(7, 'Servicios', '2016-10-19', '2016-10-19'),
(8, 'Nosotros', '2016-10-21', '2016-10-21'),
(9, 'Direccion 1', '2016-10-21', '2016-10-31'),
(10, 'Horario Suc 2', '2016-10-26', '2016-10-26'),
(11, 'Horario Suc 3', '2016-10-26', '2016-10-26'),
(12, 'Direccion 2', '2016-10-31', '2016-10-31'),
(13, 'Direccion 3 ', '2016-10-31', '2016-10-31');

-- --------------------------------------------------------

--
-- Table structure for table `citas`
--

CREATE TABLE IF NOT EXISTS `citas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_cita_id` int(11) NOT NULL,
  `nombres_apellidos` varchar(250) NOT NULL,
  `email` varchar(250) DEFAULT NULL,
  `mensaje` varchar(2050) NOT NULL,
  `telefono` varchar(60) NOT NULL,
  `fecha` date DEFAULT NULL,
  `hora` varchar(9) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo_cita_id` (`tipo_cita_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `clientes`
--

CREATE TABLE IF NOT EXISTS `clientes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nombres` varchar(200) NOT NULL,
  `apellidos` varchar(200) NOT NULL,
  `telefono` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `clientes`
--

INSERT INTO `clientes` (`id`, `nombres`, `apellidos`, `telefono`, `email`, `created`, `modified`) VALUES
(1, 'Juan Antonio', 'Pachec Lugo', '04146404394', 'juanpacheco1567@gmail.com', '2016-11-24 17:08:07', '2016-11-24 17:08:07');

-- --------------------------------------------------------

--
-- Table structure for table `conceptos`
--

CREATE TABLE IF NOT EXISTS `conceptos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `denominacion` varchar(90) NOT NULL,
  `descripcion` text NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `conceptos`
--

INSERT INTO `conceptos` (`id`, `denominacion`, `descripcion`, `created`, `modified`) VALUES
(1, 'Quincena', '', '2016-10-23', '2016-10-23');

-- --------------------------------------------------------

--
-- Table structure for table `configurations`
--

CREATE TABLE IF NOT EXISTS `configurations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_system` varchar(256) NOT NULL,
  `icon` varchar(90) DEFAULT NULL,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `money_simbol_id` int(11) NOT NULL,
  `separation_miles` varchar(1) DEFAULT NULL,
  `separation_decimals` varchar(1) NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `language_id` (`language_id`),
  KEY `money_simbol_id` (`money_simbol_id`),
  KEY `country_id` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contactos`
--

CREATE TABLE IF NOT EXISTS `contactos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(512) NOT NULL,
  `email` varchar(512) DEFAULT NULL,
  `asunto` varchar(256) NOT NULL,
  `mensaje` varchar(1024) DEFAULT NULL,
  `created` date DEFAULT NULL,
  `modified` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `contactos`
--

INSERT INTO `contactos` (`id`, `nombres`, `email`, `asunto`, `mensaje`, `created`, `modified`) VALUES
(1, '', 'diazvjosetomas@gmail.com', 'asd', 'asd', '2016-10-21', '2016-10-21'),
(2, '', 'diazvjosetomas@gmail.com', 'Saludo', 'Hola Saludos', '2016-10-21', '2016-10-21'),
(3, 'jose tomas', 'diazvjosetomas@gmail.com', 'Saludos', 'Saludos', '2016-10-21', '2016-10-21'),
(4, 'asdsad', 'diazvjosetomas@gmail.com', 'dsaasd', 'asdasd', '2016-10-21', '2016-10-21'),
(5, 'jose', 'diazvjosetomas@gmail.com', 'esta es una prueba', 'pruueba', '2016-10-21', '2016-10-21'),
(6, 'asd', 'asd@gmail.com', 'asd', 'asd', '2016-10-21', '2016-10-21');

-- --------------------------------------------------------

--
-- Table structure for table `contenidos`
--

CREATE TABLE IF NOT EXISTS `contenidos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria_id` int(11) NOT NULL,
  `contenido` mediumtext NOT NULL,
  `estatus` int(11) NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `contenidos`
--

INSERT INTO `contenidos` (`id`, `categoria_id`, `contenido`, `estatus`, `created`, `modified`) VALUES
(3, 6, '<h3>Horarios de Trabajo:</h3>\r\n\r\n<p>Lunes - Viernes</p>\r\n\r\n<p>8:00 a.m. - 5:00 p.m..</p>\r\n\r\n<p>Llamanos al (999) 9.24.23.75</p>\r\n\r\n<p><a href="#">M&aacute;s informaci&oacute;n...</a></p>\r\n', 1, '2016-10-19', '2016-10-19'),
(4, 8, '<div style="background:transparent; border:0px; padding:0px">\r\n<p><img alt="" src="http://www.clivadent.es/wp-content/uploads/2015/11/radiografia_dental_xativa.jpg" style="border-style:solid; border-width:1px; float:left; height:120px; margin:0px 10px; width:120px" /></p>\r\n\r\n<p>Quantum</p>\r\n\r\n<p><strong>NUESTRA&nbsp;</strong>EMPRESA</p>\r\n\r\n<p><strong>Objetivo:</strong>&nbsp;Contribuir a la detecci&oacute;n temprana de las diversas patolog&iacute;as bucales para ofrecer un plan de tratamiento oportuno y certero. Ademas de ser una empresa que contribuya a la capacitaci&oacute;n y actualizaci&oacute;n del odont&oacute;logo en temas de diagn&oacute;stico y de esta forma participar en la mejora de la calidad de vida de las personas</p>\r\n</div>\r\n\r\n<p>â€‹<strong>Misi&oacute;n:</strong>&nbsp;Ser aliados del profesionista de salud bucal en la primera etapa de la atenci&ograve;n odont&ograve;logica;&nbsp;<strong>&ldquo;EL DIAGN&Oacute;STICO&rdquo;</strong>, brindandole la mejor tecnolog&igrave;a asi como su asesor&iacute;a, capacitaci&oacute;n y actualizaci&oacute;n, adem&aacute;s de adecuadas herramientas que le faciliten este proceso y garantice un diagn&oacute;stico preciso y certero.</p>\r\n\r\n<p><strong>Visi&oacute;n:</strong>&nbsp;Posicionarnos como el centro imagenol&ograve;gico l&iacute;der del sureste impulsando el desarrollo de nuevas tecnolog&iacute;as, asi como fomentar la educaci&oacute;n cont&iacute;nua y la investigaci&oacute;n, contando con un excelente equipo de trabajo que compartan nuestra meta y valores.&nbsp;</p>\r\n', 1, '2016-10-21', '2016-10-22'),
(5, 9, '<p>CALLE 69 X 46 Y 48 #435-A COL. CENTRO MERIDA YUCAT&Aacute;N. CP. 97000</p>\r\n\r\n<p><strong>Email:</strong>&nbsp;<a href="mailto:quantumcentro@gmail.com">quantumcentro@gmail.com</a><br />\r\n<strong>Tel:&nbsp;</strong>&nbsp;(999) 9.24.23.75</p>\r\n\r\n<p>&nbsp;</p>\r\n', 1, '2016-10-21', '2016-10-31'),
(6, 10, '<h3>Horarios de Trabajo:</h3>\r\n\r\n<p>Lunes - Viernes</p>\r\n\r\n<p>8:00 a.m. - 5:00 p.m..</p>\r\n\r\n<p>Llamanos al (999) 9.24.23.75</p>\r\n\r\n<p><a href="#">M&aacute;s informaci&oacute;n...</a></p>\r\n', 1, '2016-10-26', '2016-10-26'),
(7, 11, '<h3>Horarios de Trabajo:</h3>\r\n\r\n<p>Lunes - Viernes</p>\r\n\r\n<p>8:00 a.m. - 5:00 p.m..</p>\r\n\r\n<p>Llamanos al (999) 9.24.23.75</p>\r\n\r\n<p><a href="#">M&aacute;s informaci&oacute;n...</a></p>\r\n', 1, '2016-10-26', '2016-10-26'),
(8, 12, '<p>CALLE 42&nbsp;X 55&nbsp;Y 55-A&nbsp;#355&nbsp;FRACC. FRANCISCO DE MONTEJO,</p>\r\n\r\n<p>MERIDA YUCAT&Aacute;N. CP. 97203</p>\r\n\r\n<p><strong>Email:</strong>&nbsp;<a href="mailto:quantummontejo@gmail.com">quantummontejo@gmail.com</a><br />\r\n<strong>Tel:&nbsp;</strong>&nbsp;(999) 1.95.22.98</p>\r\n', 1, '2016-10-31', '2016-10-31');

-- --------------------------------------------------------

--
-- Table structure for table `countrys`
--

CREATE TABLE IF NOT EXISTS `countrys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `countrys`
--

INSERT INTO `countrys` (`id`, `country`) VALUES
(1, 'venezuela');

-- --------------------------------------------------------

--
-- Table structure for table `dashboards`
--

CREATE TABLE IF NOT EXISTS `dashboards` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(2056) NOT NULL,
  `font-awesome` varchar(90) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `dashboards`
--

INSERT INTO `dashboards` (`id`, `name`, `font-awesome`) VALUES
(1, 'Admin', 'fa-gears');

-- --------------------------------------------------------

--
-- Table structure for table `doctores`
--

CREATE TABLE IF NOT EXISTS `doctores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres_apellidos` varchar(90) NOT NULL,
  `correo` varchar(90) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `tipo_cliente` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `doctores`
--

INSERT INTO `doctores` (`id`, `nombres_apellidos`, `correo`, `telefono`, `tipo_cliente`) VALUES
(1, 'jose tomas', 'diazv@gmail.com', '', 0),
(2, 'as', 'as', '', 0),
(3, 'asdasd', 'asd', 'asd', 0),
(4, 'jose', 'diazv@gmail.com', '1', 0),
(5, '', '', '', 1),
(6, '', '', '', 1),
(7, '', '', '', 1),
(8, '', '', '', 1),
(9, '', '', '', 1),
(10, '', '', '', 1),
(11, '', '', '', 1),
(12, '', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `egresos`
--

CREATE TABLE IF NOT EXISTS `egresos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `egresotipo_id` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `descripcion` text NOT NULL,
  `monto` float(26,2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `egresotipos`
--

CREATE TABLE IF NOT EXISTS `egresotipos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `denominacion` varchar(200) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `empresas`
--

CREATE TABLE IF NOT EXISTS `empresas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(300) NOT NULL,
  `email1` varchar(200) NOT NULL,
  `email2` varchar(200) NOT NULL,
  `telefono1` varchar(100) NOT NULL,
  `telefono2` varchar(100) NOT NULL,
  `identificacionfiscal` varchar(200) NOT NULL,
  `direccion` varchar(300) NOT NULL,
  `facebook` varchar(300) NOT NULL,
  `twitter` varchar(300) NOT NULL,
  `youtube` varchar(300) NOT NULL,
  `snapchat` varchar(300) NOT NULL,
  `carpeta_imagen` varchar(45) NOT NULL,
  `nombre_imagen` varchar(45) NOT NULL,
  `tipo_imagen` varchar(45) NOT NULL,
  `ruta_imagen` varchar(45) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `empresas`
--

INSERT INTO `empresas` (`id`, `nombre`, `email1`, `email2`, `telefono1`, `telefono2`, `identificacionfiscal`, `direccion`, `facebook`, `twitter`, `youtube`, `snapchat`, `carpeta_imagen`, `nombre_imagen`, `tipo_imagen`, `ruta_imagen`, `created`, `modified`) VALUES
(3, 'Diagnostico flores', 'quantummontejo@gmail.com', '', '(999) 1.95.22.98', '', '010101', 'CALLE 42 X 55 Y 55-A #355 FRACC. FRANCISCO DE MONTEJO,  MERIDA YUCATÃN. CP. 97203', 'https://www.facebook.com/Diagn%C3%B3stico-Flores-705717822777738/', '', 'https://www.youtube.com/channel/UCDhpBXyOBOFV5WZEaA54W4w', '', 'upload', 'a_06112016_085533.png', 'image/png', 'upload/a_06112016_085533.png', '2016-10-28 11:05:21', '2016-11-06 08:55:33');

-- --------------------------------------------------------

--
-- Table structure for table `facturaciones`
--

CREATE TABLE IF NOT EXISTS `facturaciones` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `razon_social` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `rfc` varchar(100) NOT NULL,
  `direccion` varchar(200) NOT NULL,
  `folio` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `facturaciones`
--

INSERT INTO `facturaciones` (`id`, `razon_social`, `email`, `rfc`, `direccion`, `folio`, `status`, `created`, `modified`) VALUES
(3, 'asd', 'asd@gmail.com', 'asd', 'asad', 'asd', 2, '2016-10-31 16:29:53', '2016-11-17 15:26:33');

-- --------------------------------------------------------

--
-- Table structure for table `imghorarios`
--

CREATE TABLE IF NOT EXISTS `imghorarios` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(300) NOT NULL,
  `carpeta_imagen` varchar(45) NOT NULL,
  `nombre_imagen` varchar(45) NOT NULL,
  `tipo_imagen` varchar(45) NOT NULL,
  `ruta_imagen` varchar(45) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `imghorarios`
--

INSERT INTO `imghorarios` (`id`, `titulo`, `carpeta_imagen`, `nombre_imagen`, `tipo_imagen`, `ruta_imagen`, `created`, `modified`) VALUES
(1, 'Imagen de horario 1', 'upload', 'a_30102016_111411.jpeg', 'image/jpeg', 'upload/a_30102016_111411.jpeg', '2016-10-30 11:08:20', '2016-10-30 11:14:11');

-- --------------------------------------------------------

--
-- Table structure for table `ingresoivas`
--

CREATE TABLE IF NOT EXISTS `ingresoivas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `monto` float(26,2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ingresoivas`
--

INSERT INTO `ingresoivas` (`id`, `monto`, `created`, `modified`) VALUES
(1, 16.00, '2016-11-24 19:22:56', '2016-11-24 19:22:56');

-- --------------------------------------------------------

--
-- Table structure for table `ingresos`
--

CREATE TABLE IF NOT EXISTS `ingresos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `personale_id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `folio` varchar(100) NOT NULL,
  `n_estudio` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ingresotipomontos`
--

CREATE TABLE IF NOT EXISTS `ingresotipomontos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ingresotipo_id` int(11) NOT NULL,
  `ingresotipopago_id` int(11) NOT NULL,
  `monto` float(26,2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ingresotipopagos`
--

CREATE TABLE IF NOT EXISTS `ingresotipopagos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `denominacion` varchar(200) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ingresotipos`
--

CREATE TABLE IF NOT EXISTS `ingresotipos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `denominacion` varchar(200) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(2) DEFAULT 'es',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `language`) VALUES
(2, 'es'),
(3, 'en');

-- --------------------------------------------------------

--
-- Table structure for table `money_simbols`
--

CREATE TABLE IF NOT EXISTS `money_simbols` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coins` varchar(90) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pagos`
--

CREATE TABLE IF NOT EXISTS `pagos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `concepto_id` int(11) NOT NULL,
  `personale_id` int(11) NOT NULL,
  `proveedor_pago_id` int(11) NOT NULL,
  `monto` float(26,2) NOT NULL,
  `fecha_pago` date NOT NULL,
  `nombre_archivo` varchar(500) NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `concepto_id` (`concepto_id`),
  KEY `personale_id` (`personale_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `pagos`
--

INSERT INTO `pagos` (`id`, `concepto_id`, `personale_id`, `proveedor_pago_id`, `monto`, `fecha_pago`, `nombre_archivo`, `created`, `modified`) VALUES
(1, 1, 1, 1, 100.00, '2016-10-23', 'upload/lotes_2016-10-28-12-59-57_validar.pdf', '2016-10-23', '2016-10-28'),
(2, 1, 1, 1, 75000.00, '2016-10-23', '', '2016-10-23', '2016-10-23'),
(6, 1, 1, 1, 1200.00, '2016-10-26', '', '2016-10-26', '2016-10-26');

-- --------------------------------------------------------

--
-- Table structure for table `permisos`
--

CREATE TABLE IF NOT EXISTS `permisos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipopermiso_id` int(11) NOT NULL,
  `personale_id` int(11) NOT NULL,
  `desde` date NOT NULL,
  `hasta` date NOT NULL,
  `motivo` mediumtext NOT NULL,
  `nombre_archivo` varchar(500) NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tipopermiso_id` (`tipopermiso_id`),
  KEY `personale_id` (`personale_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='permisos otorgados al personal de trabajo' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `permisos`
--

INSERT INTO `permisos` (`id`, `tipopermiso_id`, `personale_id`, `desde`, `hasta`, `motivo`, `nombre_archivo`, `created`, `modified`) VALUES
(1, 1, 1, '2016-10-23', '2016-10-23', 'Ausencia en el trabajo', 'upload/lotes_2016-10-28-01-00-42_validar.pdf', '2016-10-23', '2016-10-28');

-- --------------------------------------------------------

--
-- Table structure for table `personales`
--

CREATE TABLE IF NOT EXISTS `personales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rfc` varchar(60) NOT NULL,
  `sucursale_id` int(11) NOT NULL,
  `nombres` varchar(90) NOT NULL,
  `apellidos` varchar(90) NOT NULL,
  `telefono` varchar(25) NOT NULL,
  `email` varchar(90) NOT NULL,
  `direccion` varchar(512) NOT NULL,
  `fecha_ingreso` date NOT NULL,
  `nombre_archivo` varchar(500) NOT NULL,
  `created` date DEFAULT NULL,
  `modified` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `personales`
--

INSERT INTO `personales` (`id`, `rfc`, `sucursale_id`, `nombres`, `apellidos`, `telefono`, `email`, `direccion`, `fecha_ingreso`, `nombre_archivo`, `created`, `modified`) VALUES
(1, '18643687', 1, 'jose', 'diaz', '04124616235', 'diazvjosetomas@gmail.com', 'maracay', '2016-10-23', '', '2016-10-23', '2016-10-23'),
(2, '81526432', 1, 'pedro', 'perez', '04124616235', 'asd@gmail.com', 'maracay', '2016-12-12', '', '2016-10-24', '2016-10-24'),
(3, '18643687', 1, 'jose', 'diaz', '04124616235', 'diazvjosetomas@gmail.com', 'San Juan de los Morros', '2016-10-23', '', '2016-10-24', '2016-10-24'),
(4, '18521', 1, 'Carlos', 'PÃ©rez', '04124561252', 'p@gmail.com', 'San Juan ', '2016-10-25', '', '2016-10-25', '2016-10-25'),
(5, '81526432', 1, 'pedro', 'perez', '04124616235', 'asd@gmail.com', 'maracay', '2016-12-12', '', '2016-10-25', '2016-10-25'),
(6, '18521', 1, 'Carlos', 'PÃ©rez', '04124561252', 'p@gmail.com', 'San Juan ', '2016-10-25', 'upload/lotes_2016-10-28-01-00-20_validar.pdf', '2016-10-28', '2016-10-28');

-- --------------------------------------------------------

--
-- Table structure for table `proveedor_pagos`
--

CREATE TABLE IF NOT EXISTS `proveedor_pagos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `denominacion` varchar(256) NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `proveedor_pagos`
--

INSERT INTO `proveedor_pagos` (`id`, `denominacion`, `created`, `modified`) VALUES
(1, 'Caja chica', '2016-10-26', '2016-10-26');

-- --------------------------------------------------------

--
-- Table structure for table `servicios`
--

CREATE TABLE IF NOT EXISTS `servicios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(512) DEFAULT NULL,
  `denominacion` mediumtext NOT NULL,
  `estatus` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `servicios`
--

INSERT INTO `servicios` (`id`, `titulo`, `denominacion`, `estatus`) VALUES
(1, 'RADIOGRAFÃAS DIGITALES', 'Unicas en el sureste, ofreciÃ©ndonos la mejor calidad y visiÃ³n de las estructuras anatÃ³micas y patologÃ­as.', 1),
(2, 'FOTOGRAFÃA CLÃNICA', 'Complemento profesional para el diagnÃ³stico.', 1),
(3, 'ASESORÃ­A Y CAPACITACIÃ“N', 'Del uso de los diversos software para visualizar estudios tomogrÃ¡ficos de acuerdo a cada especialidad.', 1),
(4, 'ESTUDIOS TOMOGRÃFICOS', 'Con la Ãºltima tecnologÃ­a, que nos brinda menor radiaciÃ³n y mayor exactitud y precisiÃ³n en el diagnÃ³stico', 1),
(5, 'INTERPRETACIÃ“N RADIOGRÃFICA', 'Elaborada por especialista, y para apoyo diagnÃ³stico', 1),
(6, 'ACTUALIZACIÃ“N', 'Creamos eventos de interÃ¨s cientÃ­fico que nos permite aprender y ademÃ¡s interactuar.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE IF NOT EXISTS `sliders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `carpeta_imagen` varchar(256) DEFAULT NULL,
  `contenido` text,
  `nombre_imagen` varchar(512) NOT NULL,
  `tipo_imagen` varchar(90) NOT NULL,
  `ruta_imagen` varchar(256) NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `carpeta_imagen`, `contenido`, `nombre_imagen`, `tipo_imagen`, `ruta_imagen`, `created`, `modified`) VALUES
(17, 'upload', '<h4>Proceso Seguro<br />\r\n<span>\r\nindicado para todas las edades\r\n</span>\r\n</h4>\r\n', 'a_03112016_105330.png', 'image/png', 'upload/a_03112016_105330.png', '2016-11-03', '2016-11-03'),
(19, 'upload', '<h4>Proceso Seguro<br />\r\n<span>\r\nindicado para todas las edades\r\n</span>\r\n</h4>\r\n', 'a_03112016_110045.jpeg', 'image/jpeg', 'upload/a_03112016_110045.jpeg', '2016-11-03', '2016-11-03');

-- --------------------------------------------------------

--
-- Table structure for table `sucursales`
--

CREATE TABLE IF NOT EXISTS `sucursales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `denominacion` varchar(60) NOT NULL,
  `direccion` varchar(256) NOT NULL,
  `telefono` varchar(25) NOT NULL,
  `email` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sucursales`
--

INSERT INTO `sucursales` (`id`, `denominacion`, `direccion`, `telefono`, `email`) VALUES
(1, 'Sucursal 1', 'Calle1', '0243-43132352', 'sucursal1@quamtum.com');

-- --------------------------------------------------------

--
-- Table structure for table `tipoclientes`
--

CREATE TABLE IF NOT EXISTS `tipoclientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `denominacion` varchar(90) NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tipoclientes`
--

INSERT INTO `tipoclientes` (`id`, `denominacion`, `created`, `modified`) VALUES
(1, 'MÃ©dico', '2016-10-26', '2016-10-26');

-- --------------------------------------------------------

--
-- Table structure for table `tipoentradas`
--

CREATE TABLE IF NOT EXISTS `tipoentradas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `denominacion` varchar(90) NOT NULL,
  `descripcion` text NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tipoentradas`
--

INSERT INTO `tipoentradas` (`id`, `denominacion`, `descripcion`, `created`, `modified`) VALUES
(1, 'Entrada', '', '2016-10-23', '2016-10-23'),
(2, 'Salida', '', '2016-10-28', '2016-10-28');

-- --------------------------------------------------------

--
-- Table structure for table `tipopermisos`
--

CREATE TABLE IF NOT EXISTS `tipopermisos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `denominacion` varchar(256) NOT NULL,
  `descripcion` text NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tipopermisos`
--

INSERT INTO `tipopermisos` (`id`, `denominacion`, `descripcion`, `created`, `modified`) VALUES
(1, 'Provisional', '', '2016-10-23', '2016-10-23'),
(2, 'Familiar', '', '2016-10-24', '2016-10-24');

-- --------------------------------------------------------

--
-- Table structure for table `tipo_cita`
--

CREATE TABLE IF NOT EXISTS `tipo_cita` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria_id` int(11) NOT NULL,
  `titulo_cita` varchar(120) NOT NULL,
  `tiempo_estudio` varchar(60) DEFAULT NULL,
  `costo` varchar(60) DEFAULT NULL,
  `detalles_cita` varchar(512) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `categoria_id` (`categoria_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(100) NOT NULL,
  `password` varchar(300) NOT NULL,
  `email` varchar(300) NOT NULL,
  `nombre` varchar(300) NOT NULL,
  `apellidos` varchar(300) NOT NULL,
  `telefono` varchar(90) NOT NULL,
  `statu` int(11) NOT NULL,
  `tipocliente_id` int(11) NOT NULL,
  `nivel` int(11) NOT NULL DEFAULT '3',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user`, `password`, `email`, `nombre`, `apellidos`, `telefono`, `statu`, `tipocliente_id`, `nivel`) VALUES
(1, 'pongopa', '827ccb0eea8a706c4c34a16891f84e7b', 'juanpacheco1567@gmail.com', 'Juan Antonio', 'Pacheco Lugo', '', 1, 0, 1),
(5, 'juan', '81dc9bdb52d04dc20036dbd8313ed055', 'juanpacheco1567@gmail.com', 'juan antonio', 'Pacheco Lugo', '', 1, 0, 1),
(6, 'jose', '81dc9bdb52d04dc20036dbd8313ed055', 'diazvjosetomas@gmail.com', 'jose', 'diaz', '04124616235', 1, 1, 3);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `asistencias`
--
ALTER TABLE `asistencias`
  ADD CONSTRAINT `asistencias_ibfk_1` FOREIGN KEY (`personale_id`) REFERENCES `personales` (`id`),
  ADD CONSTRAINT `asistencias_ibfk_2` FOREIGN KEY (`tipoentrada_id`) REFERENCES `tipoentradas` (`id`);

--
-- Constraints for table `pagos`
--
ALTER TABLE `pagos`
  ADD CONSTRAINT `pagos_ibfk_1` FOREIGN KEY (`concepto_id`) REFERENCES `conceptos` (`id`),
  ADD CONSTRAINT `pagos_ibfk_2` FOREIGN KEY (`personale_id`) REFERENCES `personales` (`id`);

--
-- Constraints for table `permisos`
--
ALTER TABLE `permisos`
  ADD CONSTRAINT `permisos_ibfk_1` FOREIGN KEY (`tipopermiso_id`) REFERENCES `tipopermisos` (`id`),
  ADD CONSTRAINT `permisos_ibfk_2` FOREIGN KEY (`personale_id`) REFERENCES `personales` (`id`);


