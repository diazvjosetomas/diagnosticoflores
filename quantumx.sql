-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 08, 2007 at 06:46 PM
-- Server version: 5.5.54-0+deb8u1
-- PHP Version: 5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quantumx`
--

-- --------------------------------------------------------

--
-- Table structure for table `adjuntos`
--

CREATE TABLE IF NOT EXISTS `adjuntos` (
`id` int(11) NOT NULL,
  `denominacion` varchar(30) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `ingresotipo_id` int(11) DEFAULT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adjuntos`
--

INSERT INTO `adjuntos` (`id`, `denominacion`, `user_id`, `cliente_id`, `ingresotipo_id`, `created`, `modified`) VALUES
(6, '', 10, 2, 11, '2017-08-10', '2017-08-10');

-- --------------------------------------------------------

--
-- Table structure for table `asignacitas`
--

CREATE TABLE IF NOT EXISTS `asignacitas` (
`id` int(11) NOT NULL,
  `cita_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT '0',
  `created` date NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asignacitas`
--

INSERT INTO `asignacitas` (`id`, `cita_id`, `user_id`, `estatus`, `created`, `modified`) VALUES
(1, 6, 42, 1, '2017-08-24', '2017-08-24');

-- --------------------------------------------------------

--
-- Table structure for table `asistencias`
--

CREATE TABLE IF NOT EXISTS `asistencias` (
`id` int(11) NOT NULL,
  `personale_id` int(11) NOT NULL,
  `fecha_asistencia` date DEFAULT NULL,
  `tipoentrada_id` int(11) NOT NULL,
  `hora` varchar(25) DEFAULT NULL,
  `created` date DEFAULT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `avisoprivas`
--

CREATE TABLE IF NOT EXISTS `avisoprivas` (
`id` bigint(20) unsigned NOT NULL,
  `titulo` varchar(300) NOT NULL,
  `nombre_archivo` varchar(500) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `avisoprivas`
--

INSERT INTO `avisoprivas` (`id`, `titulo`, `nombre_archivo`, `created`, `modified`) VALUES
(1, 'Aviso de privacidad', 'upload/lotes_2016-10-30-11-27-00_validar.pdf', '2016-10-30 11:27:00', '2016-10-30 11:27:00');

-- --------------------------------------------------------

--
-- Table structure for table `blogcategorias`
--

CREATE TABLE IF NOT EXISTS `blogcategorias` (
`id` bigint(20) unsigned NOT NULL,
  `denominacion` varchar(300) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blogcategorias`
--

INSERT INTO `blogcategorias` (`id`, `denominacion`, `created`, `modified`) VALUES
(1, 'InformaciÃ³n', '2016-10-31 08:05:39', '2016-10-31 08:05:39');

-- --------------------------------------------------------

--
-- Table structure for table `blogentradas`
--

CREATE TABLE IF NOT EXISTS `blogentradas` (
`id` bigint(20) unsigned NOT NULL,
  `titulo` varchar(300) NOT NULL,
  `contenido` text NOT NULL,
  `blogcategoria_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `nombre_imagen` varchar(45) NOT NULL,
  `ruta_imagen` varchar(45) NOT NULL,
  `tipo_imagen` varchar(45) NOT NULL,
  `carpeta_imagen` varchar(45) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blogentradas`
--

INSERT INTO `blogentradas` (`id`, `titulo`, `contenido`, `blogcategoria_id`, `user_id`, `nombre_imagen`, `ruta_imagen`, `tipo_imagen`, `carpeta_imagen`, `created`, `modified`) VALUES
(1, 'RADIOGRAFÃAS DIGITALES', '<p>LAS RADIOGRAFIAS DIGITALES SON&nbsp;</p>\r\n', 1, 10, 'a_22062017_044918.jpeg', 'upload/a_22062017_044918.jpeg', 'image/jpeg', 'upload', '2017-06-22 16:49:18', '2017-06-22 16:49:18');

-- --------------------------------------------------------

--
-- Table structure for table `blogpublicidades`
--

CREATE TABLE IF NOT EXISTS `blogpublicidades` (
`id` bigint(20) unsigned NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `carpeta_imagen` varchar(100) NOT NULL,
  `nombre_imagen` varchar(100) NOT NULL,
  `tipo_imagen` varchar(100) NOT NULL,
  `ruta_imagen` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blogpublicidades`
--

INSERT INTO `blogpublicidades` (`id`, `titulo`, `carpeta_imagen`, `nombre_imagen`, `tipo_imagen`, `ruta_imagen`, `created`, `modified`) VALUES
(2, '', 'upload', 'a_22062017_044316.jpeg', 'image/jpeg', 'upload/a_22062017_044316.jpeg', '2017-06-22 16:43:16', '2017-06-22 16:43:16'),
(3, '', 'upload', 'a_09082017_020046.jpeg', 'image/jpeg', 'upload/a_09082017_020046.jpeg', '2017-08-09 14:00:46', '2017-08-09 14:00:46'),
(4, 'Titulo', 'upload', 'a_09082017_021016.jpeg', 'image/jpeg', 'upload/a_09082017_021016.jpeg', '2017-08-09 14:10:16', '2017-08-09 14:10:16');

-- --------------------------------------------------------

--
-- Table structure for table `cajaaperturas`
--

CREATE TABLE IF NOT EXISTS `cajaaperturas` (
`id` bigint(20) unsigned NOT NULL,
  `sucursale_id` int(11) NOT NULL,
  `caja_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `monto` float(26,2) NOT NULL,
  `fecha` varchar(40) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cajacierredias`
--

CREATE TABLE IF NOT EXISTS `cajacierredias` (
`id` bigint(20) unsigned NOT NULL,
  `sucursale_id` int(11) NOT NULL,
  `caja_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `monto` float(26,2) NOT NULL,
  `fecha` varchar(40) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cajacierredias`
--

INSERT INTO `cajacierredias` (`id`, `sucursale_id`, `caja_id`, `user_id`, `monto`, `fecha`, `created`, `modified`) VALUES
(1, 1, 1, 10, 304.50, '11:12:33 AM 2 / 8 / 2017', '2017-08-02 15:12:41', '2017-08-02 15:12:41'),
(2, 1, 1, 10, 4509.50, '14:41:04 PM 6 / 9 / 2017', '2017-09-06 18:41:19', '2017-09-06 18:41:19');

-- --------------------------------------------------------

--
-- Table structure for table `cajacierres`
--

CREATE TABLE IF NOT EXISTS `cajacierres` (
`id` bigint(20) unsigned NOT NULL,
  `sucursale_id` int(11) NOT NULL,
  `caja_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `monto` float(26,2) NOT NULL,
  `fecha` varchar(40) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cajas`
--

CREATE TABLE IF NOT EXISTS `cajas` (
`id` bigint(20) unsigned NOT NULL,
  `sucursale_id` int(11) NOT NULL,
  `denominacion` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cajas`
--

INSERT INTO `cajas` (`id`, `sucursale_id`, `denominacion`, `status`, `created`, `modified`) VALUES
(1, 1, 'Caja 2', 1, '2017-06-21 22:40:24', '2017-06-21 22:40:24'),
(2, 2, 'Caja 1', 1, '2017-06-21 22:40:32', '2017-06-21 22:40:32');

-- --------------------------------------------------------

--
-- Table structure for table `categoriacitas`
--

CREATE TABLE IF NOT EXISTS `categoriacitas` (
`id` int(11) NOT NULL,
  `denominacion` varchar(90) NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categoriacitas`
--

INSERT INTO `categoriacitas` (`id`, `denominacion`, `created`, `modified`) VALUES
(1, 'Estudios TomogrÃ¡ficos', '2016-12-05', '2016-12-05'),
(2, 'Radiografias', '2016-12-05', '2016-12-05');

-- --------------------------------------------------------

--
-- Table structure for table `categorias`
--

CREATE TABLE IF NOT EXISTS `categorias` (
`id` int(11) NOT NULL,
  `categoria` varchar(512) NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categorias`
--

INSERT INTO `categorias` (`id`, `categoria`, `created`, `modified`) VALUES
(6, 'Horario Suc 1', '2016-10-19', '2016-10-26'),
(7, 'Servicios', '2016-10-19', '2016-10-19'),
(8, 'Nosotros', '2016-10-21', '2016-10-21'),
(9, 'Direccion 1', '2016-10-21', '2016-10-31'),
(10, 'Horario Suc 2', '2016-10-26', '2016-10-26'),
(11, 'Horario Suc 3', '2016-10-26', '2016-10-26'),
(12, 'Direccion 2', '2016-10-31', '2016-10-31'),
(13, 'Direccion 3 ', '2016-10-31', '2016-10-31');

-- --------------------------------------------------------

--
-- Table structure for table `citas`
--

CREATE TABLE IF NOT EXISTS `citas` (
`id` int(11) NOT NULL,
  `nombres` varchar(90) NOT NULL,
  `apellidos` varchar(90) NOT NULL,
  `correo` varchar(90) NOT NULL,
  `telefono` varchar(30) NOT NULL DEFAULT '0',
  `fecha` date NOT NULL,
  `hora` int(11) NOT NULL,
  `tipocita_id` int(11) NOT NULL,
  `sucursale_id` int(11) NOT NULL,
  `estatus` varchar(30) NOT NULL DEFAULT '0',
  `created` date NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `citas`
--

INSERT INTO `citas` (`id`, `nombres`, `apellidos`, `correo`, `telefono`, `fecha`, `hora`, `tipocita_id`, `sucursale_id`, `estatus`, `created`, `modified`) VALUES
(14, 'n1', 'n2', 'n3', 'n4', '2017-09-07', 22, 2, 1, '0', '2017-09-06', '2017-09-06');

-- --------------------------------------------------------

--
-- Table structure for table `clientes`
--

CREATE TABLE IF NOT EXISTS `clientes` (
`id` bigint(20) unsigned NOT NULL,
  `nombres` varchar(200) NOT NULL,
  `apellidos` varchar(200) NOT NULL,
  `telefono` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clientes`
--

INSERT INTO `clientes` (`id`, `nombres`, `apellidos`, `telefono`, `email`, `created`, `modified`) VALUES
(1, 'maricela', 'cohuo', '924572', 'blar7@hotmail.com', '2017-06-22 17:07:43', '2017-06-22 17:07:43'),
(2, 'fernando', 'gonzalez', '1952298', 'joaco_50t@hotmail.com', '2017-06-22 18:36:27', '2017-06-22 18:36:27');

-- --------------------------------------------------------

--
-- Table structure for table `conceptos`
--

CREATE TABLE IF NOT EXISTS `conceptos` (
`id` int(11) NOT NULL,
  `denominacion` varchar(90) NOT NULL,
  `descripcion` text NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `conceptos`
--

INSERT INTO `conceptos` (`id`, `denominacion`, `descripcion`, `created`, `modified`) VALUES
(1, 'Semanal', 'semana', '2016-10-23', '2017-06-13'),
(2, 'Prestamo', 'con promesa de devoluciÃ³n', '2017-06-13', '2017-06-13'),
(3, 'seguro social', 'seguro social', '2017-06-13', '2017-06-13'),
(4, 'Adelanto', 'adelanto', '2017-06-13', '2017-06-13'),
(5, 'Bono puntualidad', 'bono', '2017-06-13', '2017-06-13');

-- --------------------------------------------------------

--
-- Table structure for table `configurations`
--

CREATE TABLE IF NOT EXISTS `configurations` (
`id` int(11) NOT NULL,
  `name_system` varchar(256) NOT NULL,
  `icon` varchar(90) DEFAULT NULL,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `money_simbol_id` int(11) NOT NULL,
  `separation_miles` varchar(1) DEFAULT NULL,
  `separation_decimals` varchar(1) NOT NULL,
  `country_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contactos`
--

CREATE TABLE IF NOT EXISTS `contactos` (
`id` int(11) NOT NULL,
  `nombres` varchar(512) NOT NULL,
  `email` varchar(512) DEFAULT NULL,
  `asunto` varchar(256) NOT NULL,
  `mensaje` varchar(1024) DEFAULT NULL,
  `estatus` int(11) DEFAULT '0',
  `created` date DEFAULT NULL,
  `modified` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contactos`
--

INSERT INTO `contactos` (`id`, `nombres`, `email`, `asunto`, `mensaje`, `estatus`, `created`, `modified`) VALUES
(1, '1', 'noreply@domainwebnetwork.com', '1', 'Disclaimer: We are not responsible for any financial loss, data loss, downgrade in search engine rankings, missed customers, undeliverable email or any other damages that you may suffer upon the expiration of quantummexico.com. For more information please refer to section 17.c.1a of our User Agreement. \n\n This is your final notice to renew quantummexico.com: \n\n https://domainwebnetwork.com/?n=quantummexico.com&r=c \n\n In the event that quantummexico.com expires, we reserve the right to offer your listing to competing businesses in the same niche and region after 3 business days on an auction basis. \n\n This is the final communication that we are required to send out regarding the expiration of quantummexico.com \n\n Secure Online Payment: \n\n https://domainwebnetwork.com/?n=quantummexico.com&r=c \n\n All services will be automatically restored on quantummexico.com if payment is received in full before expiration. Thank you for your cooperation.', 0, '2017-06-13', '2017-06-13'),
(2, '', 'alpha-tex-weeze@t-online.de', '', '', 1, '2017-07-24', '2017-09-06'),
(3, '', 'stefan.stoeckl@freenet.de', '', '', 1, '2017-07-24', '2017-09-06'),
(4, '', 'ctschwenk@gmail.com', '', '', 0, '2017-07-24', '2017-07-24'),
(5, '', 'bettalb.ab@gmail.com', '', '', 0, '2017-07-24', '2017-07-24'),
(6, '', 'emmanuellehilson@yahoo.co.uk', '', '', 0, '2017-07-24', '2017-07-24'),
(7, '', 'themit@gmail.com', '', '', 0, '2017-07-25', '2017-07-25'),
(8, '', 'jeremy.miller@gmail.com', '', '', 0, '2017-07-25', '2017-07-25'),
(9, '', 'djpgreek@yahoo.com', '', '', 0, '2017-07-26', '2017-07-26'),
(10, '', 'rmaercks@gmail.com', '', '', 0, '2017-07-26', '2017-07-26'),
(11, '', 'saltybull@gmail.com', '', '', 0, '2017-07-27', '2017-07-27'),
(12, '', 'dlykens1@comcast.net', '', '', 0, '2017-07-28', '2017-07-28'),
(13, '', 'stephschalken@gmail.com', '', '', 0, '2017-07-28', '2017-07-28'),
(14, '', 'lindaschwartz@mac.com', '', '', 0, '2017-07-28', '2017-07-28'),
(15, '', 'rascalchico@att.net', '', '', 0, '2017-07-29', '2017-07-29'),
(16, '', 'meena.dayak@gmail.com', '', '', 0, '2017-07-29', '2017-07-29'),
(17, '', 'xyzbooter@yahoo.com', '', '', 0, '2017-07-29', '2017-07-29'),
(18, '', 'terry.harper57@gmail.com', '', '', 0, '2017-07-29', '2017-07-29'),
(19, '', 'iandawrigg@gmail.com', '', '', 0, '2017-07-31', '2017-07-31'),
(20, 'jose', 'diazvjosetomas@gmail.com', 'Hola', 'hola', 0, '2017-08-01', '2017-08-01');

-- --------------------------------------------------------

--
-- Table structure for table `contenidos`
--

CREATE TABLE IF NOT EXISTS `contenidos` (
`id` int(11) NOT NULL,
  `categoria_id` int(11) NOT NULL,
  `contenido` mediumtext NOT NULL,
  `estatus` int(11) NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contenidos`
--

INSERT INTO `contenidos` (`id`, `categoria_id`, `contenido`, `estatus`, `created`, `modified`) VALUES
(1, 8, '<p>Acerca de nosotros</p>\r\n', 1, '2017-08-01', '2017-08-01'),
(2, 9, '<p>Direccion 1</p>\r\n', 1, '2017-08-01', '2017-08-01'),
(3, 6, '<p>Nuevo Horario</p>\r\n', 1, '2017-08-02', '2017-08-02'),
(4, 10, '<p>Mi segundo horario</p>\r\n', 1, '2017-08-02', '2017-08-02');

-- --------------------------------------------------------

--
-- Table structure for table `countrys`
--

CREATE TABLE IF NOT EXISTS `countrys` (
`id` int(11) NOT NULL,
  `country` varchar(256) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `countrys`
--

INSERT INTO `countrys` (`id`, `country`) VALUES
(1, 'venezuela');

-- --------------------------------------------------------

--
-- Table structure for table `dashboards`
--

CREATE TABLE IF NOT EXISTS `dashboards` (
`id` bigint(20) unsigned NOT NULL,
  `name` varchar(2056) NOT NULL,
  `font-awesome` varchar(90) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dashboards`
--

INSERT INTO `dashboards` (`id`, `name`, `font-awesome`) VALUES
(1, 'Admin', 'fa-gears');

-- --------------------------------------------------------

--
-- Table structure for table `doctores`
--

CREATE TABLE IF NOT EXISTS `doctores` (
`id` int(11) NOT NULL,
  `nombres_apellidos` varchar(90) NOT NULL,
  `correo` varchar(90) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `tipo_cliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `egresos`
--

CREATE TABLE IF NOT EXISTS `egresos` (
`id` bigint(20) unsigned NOT NULL,
  `egresotipo_id` int(11) NOT NULL,
  `proveedor_pago_id` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `descripcion` text NOT NULL,
  `concepto` varchar(90) DEFAULT 'Pago',
  `monto` float(26,2) NOT NULL,
  `sucursale_id` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `egresos`
--

INSERT INTO `egresos` (`id`, `egresotipo_id`, `proveedor_pago_id`, `fecha`, `descripcion`, `concepto`, `monto`, `sucursale_id`, `estatus`, `created`, `modified`) VALUES
(1, 2, 1, '2017-09-06', 'Sin descripcion', 'Pago', 230.00, 0, 1, '2017-09-06 15:06:37', '2017-09-06 15:06:37');

-- --------------------------------------------------------

--
-- Table structure for table `egresotipos`
--

CREATE TABLE IF NOT EXISTS `egresotipos` (
`id` bigint(20) unsigned NOT NULL,
  `denominacion` varchar(200) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `egresotipos`
--

INSERT INTO `egresotipos` (`id`, `denominacion`, `created`, `modified`) VALUES
(1, 'Sueldos', '2016-12-08 22:27:47', '2017-06-21 22:35:42'),
(2, 'Papeleria', '2017-05-02 15:47:55', '2017-06-21 22:35:31'),
(3, 'servicios', '2017-06-21 22:35:50', '2017-06-21 22:35:50'),
(4, 'Equipo', '2017-06-21 22:36:59', '2017-06-21 22:36:59'),
(5, 'Mobiliario', '2017-06-21 22:37:10', '2017-06-21 22:37:10'),
(6, 'Contabilidad', '2017-06-21 22:37:30', '2017-06-21 22:37:30');

-- --------------------------------------------------------

--
-- Table structure for table `empresas`
--

CREATE TABLE IF NOT EXISTS `empresas` (
`id` bigint(20) unsigned NOT NULL,
  `nombre` varchar(300) NOT NULL,
  `email1` varchar(200) NOT NULL,
  `email2` varchar(200) NOT NULL,
  `telefono1` varchar(100) NOT NULL,
  `telefono2` varchar(100) NOT NULL,
  `identificacionfiscal` varchar(200) NOT NULL,
  `direccion` varchar(300) NOT NULL,
  `web` varchar(200) NOT NULL,
  `facebook` varchar(300) NOT NULL,
  `twitter` varchar(300) NOT NULL,
  `youtube` varchar(300) NOT NULL,
  `snapchat` varchar(300) NOT NULL,
  `carpeta_imagen` varchar(45) NOT NULL,
  `nombre_imagen` varchar(45) NOT NULL,
  `tipo_imagen` varchar(45) NOT NULL,
  `ruta_imagen` varchar(45) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `empresas`
--

INSERT INTO `empresas` (`id`, `nombre`, `email1`, `email2`, `telefono1`, `telefono2`, `identificacionfiscal`, `direccion`, `web`, `facebook`, `twitter`, `youtube`, `snapchat`, `carpeta_imagen`, `nombre_imagen`, `tipo_imagen`, `ruta_imagen`, `created`, `modified`) VALUES
(5, 'Quantum', 'quantumadmon@gmail.com', '', '(999) 9.24.23.75', '(999) 1.95.22.98', 'xxxx', 'Calle 69 x 46 y 48 #435-a Col. Centro. 97000', 'quantum.com', 'https://www.facebook.com/quantumimagendiagnostica/?ref=bookmarks', '', '', '', 'upload', 'a_22062017_081102.png', 'image/png', 'upload/a_22062017_081102.png', '2017-06-12 01:16:08', '2017-06-22 20:11:02');

-- --------------------------------------------------------

--
-- Table structure for table `facturaciones`
--

CREATE TABLE IF NOT EXISTS `facturaciones` (
`id` bigint(20) unsigned NOT NULL,
  `razon_social` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `rfc` varchar(100) NOT NULL,
  `direccion` varchar(200) NOT NULL,
  `folio` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facturaciones`
--

INSERT INTO `facturaciones` (`id`, `razon_social`, `email`, `rfc`, `direccion`, `folio`, `status`, `created`, `modified`) VALUES
(1, '1', 'noreply@domainwebnetwork.com', '1', 'Disclaimer: We are not responsible for any financial loss, data loss, downgrade in search engine rankings, missed customers, undeliverable email or any other damages that you may suffer upon the expir', '1', 1, '2017-06-13 19:00:24', '2017-06-13 19:00:24'),
(2, 'Brenda Lorena Ayala Rojas', 'BLAR7@HOTMAIL.COM', 'AARB850501F18', 'calle 60 x 46y 48 centro #435-a Col centro 97000, merida yucatan', '000', 2, '2017-06-21 21:19:34', '2017-06-21 21:22:03'),
(3, 'Mi razon social', 'diazvjosetomas@gmail.com', '123', 'asd', '123', 1, '2017-08-01 21:52:47', '2017-08-01 21:52:47'),
(4, 'jose', 'diazvjosetomas@gmail.com', '123', 'nueva direccion', '123', 1, '2017-08-01 22:12:48', '2017-08-01 22:12:48'),
(5, '123', 'diazvjosetomas@gmail.com', '123|13', 'asd', 'qad', 1, '2017-08-01 22:26:31', '2017-08-01 22:26:31'),
(6, 'jose', 'diazvjosetomas@gmail.com', '123', 'asd', '123', 1, '2017-08-01 22:33:31', '2017-08-01 22:33:31'),
(7, 'qwe', 'diazvjosetomas@gmail.com', 'qwe', 'qwe', 'qwe', 1, '2017-08-01 22:53:34', '2017-08-01 22:53:34'),
(8, 'jose', 'diazvjosetomas@gmail.com', '123', 'sd', '123', 1, '2017-08-02 14:57:18', '2017-08-02 14:57:18');

-- --------------------------------------------------------

--
-- Table structure for table `feriados`
--

CREATE TABLE IF NOT EXISTS `feriados` (
`id` int(11) NOT NULL,
  `denominacion` varchar(90) NOT NULL,
  `diaferiado` date NOT NULL,
  `manana` int(11) NOT NULL,
  `tarde` int(11) NOT NULL,
  `noche` int(11) NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feriados`
--

INSERT INTO `feriados` (`id`, `denominacion`, `diaferiado`, `manana`, `tarde`, `noche`, `created`, `modified`) VALUES
(1, 'Noche Buena', '2017-12-25', 1, 1, 1, '2017-06-21', '2017-06-21'),
(2, 'Agosto al costo', '2017-08-04', 1, 1, 1, '2017-08-02', '2017-08-02'),
(3, 'Viernes de Pachanga', '2017-08-11', 1, 1, 1, '2017-08-09', '2017-08-09');

-- --------------------------------------------------------

--
-- Table structure for table `imgadjuntos`
--

CREATE TABLE IF NOT EXISTS `imgadjuntos` (
`id` int(11) NOT NULL,
  `adjunto_id` int(11) NOT NULL,
  `carpeta_imagen` varchar(90) NOT NULL,
  `nombre_imagen` varchar(90) NOT NULL,
  `tipo_imagen` varchar(90) NOT NULL,
  `ruta_imagen` varchar(90) NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `imgadjuntos`
--

INSERT INTO `imgadjuntos` (`id`, `adjunto_id`, `carpeta_imagen`, `nombre_imagen`, `tipo_imagen`, `ruta_imagen`, `created`, `modified`) VALUES
(1, 1, 'upload', 'a_22062017_055349.jpeg', 'image/jpeg', 'upload/a_22062017_055349.jpeg', '2017-06-22', '2017-06-22'),
(4, 3, 'upload', 'a_22062017_071547.jpeg', 'image/jpeg', 'upload/a_22062017_071547.jpeg', '2017-06-22', '2017-06-22'),
(5, 6, 'upload', 'a_10082017_041910.png', 'image/png', 'upload/a_10082017_041910.png', '2017-08-10', '2017-08-10');

-- --------------------------------------------------------

--
-- Table structure for table `imghorarios`
--

CREATE TABLE IF NOT EXISTS `imghorarios` (
`id` bigint(20) unsigned NOT NULL,
  `titulo` varchar(300) NOT NULL,
  `carpeta_imagen` varchar(45) NOT NULL,
  `nombre_imagen` varchar(45) NOT NULL,
  `tipo_imagen` varchar(45) NOT NULL,
  `ruta_imagen` varchar(45) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `imghorarios`
--

INSERT INTO `imghorarios` (`id`, `titulo`, `carpeta_imagen`, `nombre_imagen`, `tipo_imagen`, `ruta_imagen`, `created`, `modified`) VALUES
(1, 'Francisco de Montejo    11:00 am a 7:00 pm.', '_', '_', '_', '_', '2017-06-21 21:15:30', '2017-06-21 21:15:30'),
(2, '9-1', 'upload', 'a_02082017_024044.jpeg', 'image/jpeg', 'upload/a_02082017_024044.jpeg', '2017-06-21 22:57:32', '2017-08-02 14:40:44');

-- --------------------------------------------------------

--
-- Table structure for table `ingresodetalles`
--

CREATE TABLE IF NOT EXISTS `ingresodetalles` (
`id` bigint(20) unsigned NOT NULL,
  `ingreso_id` int(11) NOT NULL,
  `ingresotipo_id` int(11) NOT NULL,
  `ingresotipopago_id` int(11) NOT NULL,
  `cantidad` float(26,2) NOT NULL,
  `monto` float(26,2) NOT NULL,
  `descuento` float(26,2) NOT NULL,
  `total` float(26,2) NOT NULL,
  `exento` int(11) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ingresodetalles`
--

INSERT INTO `ingresodetalles` (`id`, `ingreso_id`, `ingresotipo_id`, `ingresotipopago_id`, `cantidad`, `monto`, `descuento`, `total`, `exento`, `created`, `modified`) VALUES
(1, 1, 2, 1, 1.00, 262.50, 0.00, 262.50, 1, '2017-06-22 17:25:00', '2017-06-22 17:25:00'),
(2, 2, 1, 2, 1.00, 250.00, 0.00, 250.00, 2, '2017-06-22 18:39:10', '2017-06-22 18:39:10'),
(3, 3, 1, 1, 1.00, 262.50, 0.00, 262.50, 1, '2017-08-02 15:12:15', '2017-08-02 15:12:15'),
(4, 4, 2, 1, 1.00, 262.50, 0.00, 262.50, 1, '2017-08-09 16:43:14', '2017-08-09 16:43:14'),
(5, 5, 1, 1, 1.00, 262.50, 0.00, 262.50, 1, '2017-08-09 16:44:21', '2017-08-09 16:44:21'),
(6, 6, 10, 1, 1.00, 315.00, 0.00, 315.00, 1, '2017-08-09 16:49:39', '2017-08-09 16:49:39'),
(7, 7, 10, 1, 1.00, 315.00, 0.00, 315.00, 1, '2017-08-09 11:49:13', '2017-08-09 16:50:30'),
(8, 8, 10, 2, 1.00, 300.00, 0.00, 300.00, 1, '2017-08-09 11:50:59', '2017-08-09 16:51:29'),
(9, 9, 1, 1, 1.00, 262.50, 0.00, 262.50, 1, '2017-08-09 16:54:40', '2017-08-09 16:54:40'),
(10, 10, 1, 1, 1.00, 262.50, 0.00, 262.50, 1, '2017-08-09 19:07:53', '2017-08-09 19:07:53'),
(11, 11, 7, 1, 1.00, 262.50, 0.00, 262.50, 1, '2017-08-09 19:12:14', '2017-08-09 19:12:14'),
(12, 12, 1, 1, 1.00, 262.50, 0.00, 262.50, 1, '2017-08-09 19:37:17', '2017-08-09 19:37:17'),
(13, 13, 1, 1, 1.00, 262.50, 0.00, 262.50, 1, '2017-08-09 19:38:34', '2017-08-09 19:38:34'),
(14, 14, 5, 2, 1.00, 1000.00, 0.00, 1000.00, 1, '2017-09-06 14:14:19', '2017-09-06 14:14:19'),
(15, 15, 1, 1, 10.00, 262.50, 0.00, 2625.00, 1, '2017-09-06 14:36:04', '2017-09-06 14:36:04'),
(16, 16, 2, 1, 1.00, 262.50, 0.00, 262.50, 1, '2017-09-06 14:37:45', '2017-09-06 14:37:45');

-- --------------------------------------------------------

--
-- Table structure for table `ingresofolios`
--

CREATE TABLE IF NOT EXISTS `ingresofolios` (
`id` bigint(20) unsigned NOT NULL,
  `sucursale_id` int(11) NOT NULL,
  `secuencia` varchar(100) NOT NULL,
  `ultimo` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ingresofolios`
--

INSERT INTO `ingresofolios` (`id`, `sucursale_id`, `secuencia`, `ultimo`, `created`, `modified`) VALUES
(1, 1, 'B', 10018, '2017-06-21 22:11:12', '2017-08-22 15:39:58'),
(2, 2, 'A', 10005, '2017-06-21 22:11:40', '2017-08-22 15:39:37');

-- --------------------------------------------------------

--
-- Table structure for table `ingresoivas`
--

CREATE TABLE IF NOT EXISTS `ingresoivas` (
`id` bigint(20) unsigned NOT NULL,
  `monto` float(26,2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ingresoivas`
--

INSERT INTO `ingresoivas` (`id`, `monto`, `created`, `modified`) VALUES
(2, 16.00, '2017-06-22 17:17:35', '2017-06-22 17:20:13');

-- --------------------------------------------------------

--
-- Table structure for table `ingresos`
--

CREATE TABLE IF NOT EXISTS `ingresos` (
`id` bigint(20) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `personale_id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `ingresotipopago_id` int(11) NOT NULL,
  `sucursale_id` int(11) NOT NULL,
  `folio` varchar(100) NOT NULL,
  `caja_id` int(11) NOT NULL,
  `n_estudio` varchar(100) NOT NULL,
  `iva` float(26,2) NOT NULL,
  `monto_iva` float(26,2) NOT NULL,
  `subtotal` float(26,2) NOT NULL,
  `total` float(26,2) NOT NULL,
  `monto_exento` float(26,2) NOT NULL,
  `nota` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `error` int(11) NOT NULL DEFAULT '0',
  `fecha` varchar(90) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ingresos`
--

INSERT INTO `ingresos` (`id`, `user_id`, `personale_id`, `cliente_id`, `ingresotipopago_id`, `sucursale_id`, `folio`, `caja_id`, `n_estudio`, `iva`, `monto_iva`, `subtotal`, `total`, `monto_exento`, `nota`, `status`, `error`, `fecha`, `created`, `modified`) VALUES
(13, 10, 1, 1, 1, 1, 'B00010010', 1, '12', 16.00, 42.00, 262.50, 304.50, 0.00, '', 1, 0, '09/08/2017 14:38:05', '2017-08-09 19:38:33', '2017-08-09 19:38:33'),
(14, 10, 1, 1, 2, 1, 'B00010018', 1, '12', 16.00, 160.00, 1000.00, 1160.00, 0.00, '', 1, 0, '06/09/2017 9:13:52', '2017-09-06 14:14:19', '2017-09-06 14:14:19'),
(15, 10, 1, 1, 1, 1, 'B00010018', 1, '12', 16.00, 420.00, 2625.00, 3045.00, 0.00, 'Notas', 1, 0, '06/09/2017 9:35:33', '2017-09-06 14:36:04', '2017-09-06 14:36:04'),
(16, 10, 1, 1, 1, 1, 'B00010018', 1, '12', 16.00, 42.00, 262.50, 304.50, 0.00, '', 1, 0, '06/09/2017 9:37:18', '2017-09-06 14:37:45', '2017-09-06 14:37:45');

-- --------------------------------------------------------

--
-- Table structure for table `ingresotipomontos`
--

CREATE TABLE IF NOT EXISTS `ingresotipomontos` (
`id` bigint(20) unsigned NOT NULL,
  `ingresotipo_id` int(11) NOT NULL,
  `ingresotipopago_id` int(11) NOT NULL,
  `monto` float(26,2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ingresotipomontos`
--

INSERT INTO `ingresotipomontos` (`id`, `ingresotipo_id`, `ingresotipopago_id`, `monto`, `created`, `modified`) VALUES
(1, 1, 1, 262.50, '2016-11-25 12:32:37', '2017-06-21 22:14:56'),
(2, 1, 2, 250.00, '2016-11-25 12:33:00', '2017-06-21 22:15:15'),
(3, 2, 1, 262.50, '2017-06-21 22:15:31', '2017-06-21 22:15:31'),
(4, 2, 2, 250.00, '2017-06-21 22:15:42', '2017-06-21 22:15:42'),
(5, 3, 1, 1050.00, '2017-06-21 22:16:05', '2017-06-21 22:16:05'),
(6, 3, 2, 1000.00, '2017-06-21 22:16:13', '2017-06-21 22:16:24'),
(7, 4, 1, 1680.00, '2017-06-21 22:16:40', '2017-06-21 22:16:40'),
(8, 4, 2, 1600.00, '2017-06-21 22:16:51', '2017-06-21 22:16:51'),
(9, 5, 1, 1050.00, '2017-06-21 22:17:26', '2017-06-21 22:17:26'),
(10, 5, 2, 1000.00, '2017-06-21 22:17:36', '2017-06-21 22:17:36'),
(11, 6, 1, 1365.00, '2017-06-21 22:17:47', '2017-06-21 22:17:47'),
(12, 6, 2, 1365.00, '2017-06-21 22:17:59', '2017-06-21 22:17:59'),
(13, 7, 1, 262.50, '2017-06-21 22:18:29', '2017-06-21 22:18:29'),
(14, 7, 2, 250.00, '2017-06-21 22:18:39', '2017-06-21 22:18:39'),
(15, 8, 1, 262.50, '2017-06-21 22:18:57', '2017-06-21 22:18:57'),
(16, 8, 2, 250.00, '2017-06-21 22:19:06', '2017-06-21 22:19:06'),
(17, 9, 2, 100.00, '2017-06-21 22:19:31', '2017-06-21 22:19:31'),
(18, 10, 1, 315.00, '2017-06-21 22:19:46', '2017-06-21 22:19:46'),
(19, 10, 2, 300.00, '2017-06-21 22:19:55', '2017-06-21 22:19:55'),
(20, 11, 1, 315.00, '2017-06-21 22:20:15', '2017-06-21 22:20:15'),
(21, 11, 2, 300.00, '2017-06-21 22:20:23', '2017-06-21 22:20:23'),
(22, 12, 2, 150.00, '2017-06-21 22:21:41', '2017-06-21 22:21:41');

-- --------------------------------------------------------

--
-- Table structure for table `ingresotipopagos`
--

CREATE TABLE IF NOT EXISTS `ingresotipopagos` (
`id` bigint(20) unsigned NOT NULL,
  `denominacion` varchar(200) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ingresotipopagos`
--

INSERT INTO `ingresotipopagos` (`id`, `denominacion`, `created`, `modified`) VALUES
(1, 'Tarjeta de Credito', '2016-11-25 12:31:49', '2016-11-25 12:31:49'),
(2, 'Efectivo', '2016-11-25 12:32:05', '2016-11-25 12:32:05');

-- --------------------------------------------------------

--
-- Table structure for table `ingresotipos`
--

CREATE TABLE IF NOT EXISTS `ingresotipos` (
`id` bigint(20) unsigned NOT NULL,
  `denominacion` varchar(200) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ingresotipos`
--

INSERT INTO `ingresotipos` (`id`, `denominacion`, `created`, `modified`) VALUES
(1, 'Panoramica', '2016-11-25 12:30:58', '2017-06-21 21:59:14'),
(2, 'Lateral de crÃ¡neo', '2016-11-25 12:31:19', '2017-06-21 21:59:07'),
(3, 'Estudio tomogrÃ¡fico', '2017-06-21 21:59:30', '2017-06-21 21:59:30'),
(4, 'Estudio ATM', '2017-06-21 21:59:46', '2017-06-21 21:59:46'),
(5, 'Estudio vÃ­as aÃ©reas', '2017-06-21 22:00:05', '2017-06-21 22:00:05'),
(6, 'Estudio ortodoncia', '2017-06-21 22:00:19', '2017-06-21 22:00:19'),
(7, 'Posteroanterior-Anteroposterior', '2017-06-21 22:00:38', '2017-06-21 22:00:58'),
(8, 'Otras radiografÃ­as', '2017-06-21 22:01:10', '2017-06-21 22:01:10'),
(9, 'Periapical', '2017-06-21 22:01:18', '2017-06-21 22:01:18'),
(10, 'FotografÃ­a ClÃ­nica', '2017-06-21 22:01:49', '2017-06-21 22:01:49'),
(11, 'Trazado cefalometrico', '2017-06-21 22:04:18', '2017-06-21 22:04:18'),
(12, 'Oclusal', '2017-06-21 22:21:14', '2017-06-21 22:21:14');

-- --------------------------------------------------------

--
-- Table structure for table `laborables`
--

CREATE TABLE IF NOT EXISTS `laborables` (
`id` int(11) NOT NULL,
  `dia` varchar(90) NOT NULL,
  `manana` int(11) NOT NULL,
  `tarde` int(11) NOT NULL,
  `noche` int(11) NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `laborables`
--

INSERT INTO `laborables` (`id`, `dia`, `manana`, `tarde`, `noche`, `created`, `modified`) VALUES
(1, '2', 2, 2, 2, '2017-06-21', '2017-09-06'),
(2, '3', 2, 2, 1, '2017-06-21', '2017-06-21'),
(3, '4', 2, 2, 1, '2017-06-21', '2017-06-21'),
(4, '5', 2, 2, 2, '2017-06-21', '2017-06-21'),
(5, '6', 2, 2, 1, '2017-06-21', '2017-06-21'),
(6, '7', 2, 1, 1, '2017-06-21', '2017-06-21'),
(7, '1', 1, 1, 1, '2017-06-21', '2017-06-21');

-- --------------------------------------------------------

--
-- Table structure for table `modulos`
--

CREATE TABLE IF NOT EXISTS `modulos` (
`id` bigint(20) unsigned NOT NULL,
  `denominacion` varchar(300) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modulos`
--

INSERT INTO `modulos` (`id`, `denominacion`, `created`, `modified`) VALUES
(2, 'Configuracion', '2017-02-17 03:15:33', '2017-02-17 03:15:33'),
(3, 'Newsletter', '2017-02-17 03:15:52', '2017-02-17 03:15:52'),
(4, 'Contenido PÃ¡gina', '2017-02-17 03:16:16', '2017-02-17 03:16:16'),
(5, 'Blog', '2017-02-17 03:16:35', '2017-02-17 03:16:35'),
(6, 'Control', '2017-02-17 03:17:03', '2017-02-17 03:17:03'),
(7, 'Registros', '2017-02-17 03:17:23', '2017-02-17 03:17:23'),
(8, 'Control Personal', '2017-02-17 03:17:39', '2017-02-17 03:17:39'),
(9, 'Ingresos', '2017-02-17 03:18:02', '2017-02-17 03:18:02'),
(10, 'Egresos', '2017-02-17 03:18:13', '2017-02-17 03:18:13'),
(11, 'Cajas', '2017-02-17 03:18:24', '2017-02-17 03:18:24'),
(12, 'Reportes', '2017-02-17 03:18:37', '2017-02-17 03:18:37'),
(13, 'Inicio', '2017-02-17 13:18:21', '2017-03-10 00:47:50'),
(15, 'Empresas', '2017-02-17 13:32:45', '2017-02-17 13:32:45'),
(16, 'Modulos', '2017-02-17 13:56:41', '2017-02-17 13:56:41'),
(17, 'Personal', '2017-02-17 13:58:27', '2017-02-17 13:58:27'),
(18, 'Clientes', '2017-02-17 13:59:13', '2017-02-17 13:59:13'),
(19, 'Citas', '2017-02-17 13:59:23', '2017-02-17 13:59:23'),
(20, 'Publicidad', '2017-02-17 13:59:53', '2017-02-17 13:59:53'),
(23, 'Email', '2017-02-17 14:01:05', '2017-02-17 14:01:05'),
(24, 'Tipo Permisos', '2017-02-17 14:14:39', '2017-02-17 14:14:39'),
(25, 'Tipo Asistencia', '2017-02-17 14:20:38', '2017-02-17 14:20:38'),
(26, 'Conceptos de Pago', '2017-02-17 14:27:02', '2017-02-17 14:27:02'),
(27, 'Tipo de Especialidad', '2017-02-17 14:27:45', '2017-02-17 14:27:45'),
(28, 'Proveedor de Pagos', '2017-02-17 14:28:10', '2017-02-17 14:28:10'),
(29, 'Sucursales', '2017-02-17 14:29:20', '2017-02-17 14:29:20'),
(30, 'Status de Tareas', '2017-02-17 14:29:54', '2017-02-17 14:29:54'),
(31, 'Notas', '2017-02-17 14:30:16', '2017-02-17 14:30:16'),
(32, 'Roles', '2017-02-17 14:30:40', '2017-02-17 14:30:40'),
(33, 'Roles - MÃ³dulos', '2017-02-17 14:31:11', '2017-02-17 14:31:11'),
(34, 'Periodos', '2017-02-17 14:31:43', '2017-02-17 14:31:43'),
(35, 'Publico', '2017-02-17 14:32:01', '2017-02-17 14:32:01'),
(36, 'Categorias de PÃ¡gina', '2017-02-17 14:32:46', '2017-02-17 14:32:46'),
(37, 'Aviso de Privacidad', '2017-02-17 14:33:09', '2017-02-17 14:33:09'),
(38, 'Categorias de Blog', '2017-02-17 14:33:31', '2017-02-17 14:33:31'),
(39, 'DÃ­as Laborables', '2017-02-17 14:33:58', '2017-02-17 14:33:58'),
(40, 'Dias Feriados', '2017-02-17 14:34:20', '2017-02-17 14:34:20'),
(41, 'Categorias de Citas', '2017-02-17 14:35:01', '2017-02-17 14:35:01'),
(42, 'Iva', '2017-02-17 14:35:44', '2017-02-17 14:35:44'),
(44, 'Tipo de Servicio', '2017-02-17 14:36:11', '2017-02-17 14:36:11'),
(45, 'Tipo de Pago', '2017-02-17 14:37:21', '2017-02-17 14:37:21'),
(46, 'Categoria de Egresos', '2017-02-17 14:38:08', '2017-02-17 14:38:08'),
(47, 'Promociones', '2017-02-17 14:38:39', '2017-02-17 14:38:39'),
(49, 'Servicios', '2017-02-17 14:40:12', '2017-02-17 14:40:12'),
(50, 'Horarios', '2017-02-17 14:40:55', '2017-02-17 14:40:55'),
(51, 'Sliders', '2017-02-17 14:42:04', '2017-02-17 14:42:04'),
(52, 'Entradas (Blog)', '2017-02-17 14:42:27', '2017-02-17 14:42:27'),
(53, 'Suscriptores', '2017-02-17 14:43:07', '2017-02-17 14:43:07'),
(54, 'Tipos Citas', '2017-02-17 14:43:38', '2017-02-17 14:43:38'),
(55, 'Usuarios', '2017-02-17 14:44:31', '2017-02-17 14:44:31'),
(56, 'Contactos', '2017-02-17 14:44:54', '2017-02-17 14:44:54'),
(57, 'FacturaciÃ³n', '2017-02-17 14:45:16', '2017-02-17 14:45:16'),
(58, 'Permisos', '2017-02-17 14:46:04', '2017-02-17 14:46:04'),
(59, 'Pagos', '2017-02-17 14:46:19', '2017-02-17 14:46:19'),
(60, 'Asistencias', '2017-02-17 14:46:34', '2017-02-17 14:46:34'),
(61, 'Folios', '2017-02-17 14:46:54', '2017-02-17 14:46:54'),
(62, 'Precio de Servicios', '2017-02-17 14:47:34', '2017-02-17 14:47:34'),
(63, 'Apertura (Cajas)', '2017-02-17 14:48:30', '2017-02-17 14:48:30'),
(64, 'Cierre Turno', '2017-02-17 14:48:55', '2017-02-17 14:48:55'),
(65, 'Cierre Diario', '2017-02-17 14:49:14', '2017-02-17 14:49:14'),
(66, 'Reporte Doctores', '2017-02-17 14:49:46', '2017-02-17 14:49:46'),
(67, 'Reporte Clientes', '2017-02-17 14:50:09', '2017-02-17 14:50:09'),
(68, 'Reporte Empleados', '2017-02-17 14:50:31', '2017-02-17 14:50:31'),
(69, 'Reporte Empleados', '2017-02-17 14:50:32', '2017-02-17 14:50:32'),
(70, 'Listados', '2017-02-17 14:50:46', '2017-02-17 14:50:46'),
(71, 'Adjuntos', '2017-02-18 03:14:44', '2017-02-18 03:14:44'),
(72, 'Img Adjuntar', '2017-02-18 03:19:27', '2017-02-18 03:19:27'),
(73, 'Balance', '2017-03-10 00:48:03', '2017-03-10 00:48:03'),
(74, 'Doctores', '2017-04-19 16:16:03', '2017-04-19 16:16:03'),
(75, 'Ventas', '2017-04-27 19:51:27', '2017-04-27 19:51:27'),
(76, 'Radiografias', '2017-06-13 19:04:26', '2017-06-13 19:04:26');

-- --------------------------------------------------------

--
-- Table structure for table `notas`
--

CREATE TABLE IF NOT EXISTS `notas` (
`id` int(11) NOT NULL,
  `denominacion` varchar(35) CHARACTER SET latin1 NOT NULL,
  `descripcion` varchar(1050) CHARACTER SET latin1 NOT NULL,
  `statu_id` int(11) DEFAULT NULL,
  `fecha_creado` date NOT NULL,
  `fecha_limite` date NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `notificaciones`
--

CREATE TABLE IF NOT EXISTS `notificaciones` (
`id` int(11) NOT NULL,
  `modulo` varchar(50) NOT NULL,
  `nombretabla` varchar(50) NOT NULL,
  `registros` int(11) NOT NULL,
  `url` varchar(90) NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notificaciones`
--

INSERT INTO `notificaciones` (`id`, `modulo`, `nombretabla`, `registros`, `url`, `created`, `modified`) VALUES
(2, 'Contacto', 'Contactos', 20, '/Contactos/index', '2017-02-04', '2017-02-04'),
(4, 'Clientes', 'clientes', 2, '/Clientes', '2017-02-04', '2017-02-04'),
(7, 'Facturaciones', 'facturaciones', 8, '/Facturaciones', '2017-08-02', '2017-08-02'),
(8, 'Citas', 'citas', 1, '/Citas', '2017-08-02', '2017-08-02');

-- --------------------------------------------------------

--
-- Table structure for table `pagos`
--

CREATE TABLE IF NOT EXISTS `pagos` (
`id` int(11) NOT NULL,
  `concepto_id` int(11) NOT NULL,
  `personale_id` int(11) NOT NULL,
  `proveedor_pago_id` int(11) NOT NULL,
  `monto` float(26,2) NOT NULL,
  `fecha_pago` date NOT NULL,
  `nombre_archivo` varchar(500) NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `periodos`
--

CREATE TABLE IF NOT EXISTS `periodos` (
`id` int(11) NOT NULL,
  `denominacion` varchar(90) NOT NULL,
  `dia` varchar(2) NOT NULL,
  `mes` varchar(2) NOT NULL,
  `anhio` varchar(4) NOT NULL,
  `hora` varchar(2) NOT NULL,
  `minuto` varchar(20) NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `periodos`
--

INSERT INTO `periodos` (`id`, `denominacion`, `dia`, `mes`, `anhio`, `hora`, `minuto`, `created`, `modified`) VALUES
(7, 'Semanalmente', 'Lu', '*', '2017', '06', '', '2017-03-01', '2017-03-01'),
(8, 'Mensual', 'Mi', '*', '2017', '11', '', '2017-03-10', '2017-06-21');

-- --------------------------------------------------------

--
-- Table structure for table `permisos`
--

CREATE TABLE IF NOT EXISTS `permisos` (
`id` int(11) NOT NULL,
  `tipopermiso_id` int(11) NOT NULL,
  `personale_id` int(11) NOT NULL,
  `desde` date NOT NULL,
  `hasta` date NOT NULL,
  `motivo` mediumtext NOT NULL,
  `nombre_archivo` varchar(500) NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='permisos otorgados al personal de trabajo';

-- --------------------------------------------------------

--
-- Table structure for table `personales`
--

CREATE TABLE IF NOT EXISTS `personales` (
`id` int(11) NOT NULL,
  `rfc` varchar(60) NOT NULL,
  `sucursale_id` int(11) NOT NULL,
  `nombres` varchar(90) NOT NULL,
  `apellidos` varchar(90) NOT NULL,
  `telefono` varchar(25) NOT NULL,
  `email` varchar(90) NOT NULL,
  `direccion` varchar(512) NOT NULL,
  `fecha_ingreso` date NOT NULL,
  `nombre_archivo` varchar(500) NOT NULL,
  `created` date DEFAULT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `promociones`
--

CREATE TABLE IF NOT EXISTS `promociones` (
`id` int(11) NOT NULL,
  `denominacion` varchar(25) NOT NULL,
  `body` mediumtext NOT NULL,
  `periodo_id` int(11) DEFAULT NULL,
  `publico_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `proveedor_pagos`
--

CREATE TABLE IF NOT EXISTS `proveedor_pagos` (
`id` int(11) NOT NULL,
  `denominacion` varchar(256) NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `proveedor_pagos`
--

INSERT INTO `proveedor_pagos` (`id`, `denominacion`, `created`, `modified`) VALUES
(1, 'Joaquin Flores', '2017-06-21', '2017-06-21'),
(2, 'Brenda Ayala', '2017-06-21', '2017-06-21'),
(3, 'Quantum Fco montejo', '2017-06-21', '2017-06-21'),
(4, 'Quantum Centro', '2017-06-21', '2017-06-21'),
(5, 'Consultorio', '2017-06-21', '2017-06-21');

-- --------------------------------------------------------

--
-- Table structure for table `publicos`
--

CREATE TABLE IF NOT EXISTS `publicos` (
`id` int(11) NOT NULL,
  `denominacion` varchar(50) NOT NULL,
  `tabla` varchar(90) NOT NULL,
  `campo` varchar(90) NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `publicos`
--

INSERT INTO `publicos` (`id`, `denominacion`, `tabla`, `campo`, `created`, `modified`) VALUES
(1, 'Suscriptores', 'suscribirses', 'email', '2017-02-10', '2017-02-10'),
(2, 'Contactos', 'contactos', 'email', '2017-02-14', '2017-02-14'),
(3, 'Citas', 'citas', 'correo', '2017-02-14', '2017-02-14'),
(4, 'Medicos', 'users', 'email', '2017-03-11', '2017-03-11');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
`id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` date DEFAULT NULL,
  `modified` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `title`, `alias`, `created`, `modified`) VALUES
(2, 'PROPIETARIO', 'PROPIETARIO', '2017-02-17', '2017-06-13'),
(8, 'ADMINISTRADOR', 'administrador', '2017-03-01', '2017-03-01'),
(9, 'CAJA', 'CAJA', '2017-06-13', '2017-06-13');

-- --------------------------------------------------------

--
-- Table structure for table `rolesmodulos`
--

CREATE TABLE IF NOT EXISTS `rolesmodulos` (
`id` bigint(20) unsigned NOT NULL,
  `role_id` int(11) NOT NULL,
  `modulo_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rolesmodulos`
--

INSERT INTO `rolesmodulos` (`id`, `role_id`, `modulo_id`, `created`, `modified`) VALUES
(3, 2, 2, '2017-02-17 13:25:54', '2017-02-17 13:25:54'),
(4, 2, 3, '2017-02-17 13:26:07', '2017-02-17 13:26:07'),
(5, 2, 4, '2017-02-17 13:26:22', '2017-02-17 13:26:22'),
(6, 2, 5, '2017-02-17 13:26:38', '2017-02-17 13:26:38'),
(7, 2, 6, '2017-02-17 13:26:57', '2017-02-17 13:26:57'),
(8, 2, 7, '2017-02-17 13:27:17', '2017-02-17 13:27:17'),
(9, 2, 8, '2017-02-17 13:27:34', '2017-02-17 13:27:34'),
(10, 2, 9, '2017-02-17 13:27:55', '2017-02-17 13:27:55'),
(11, 2, 10, '2017-02-17 13:28:12', '2017-02-17 13:28:12'),
(12, 2, 11, '2017-02-17 13:28:36', '2017-02-17 13:28:36'),
(13, 2, 12, '2017-02-17 13:28:52', '2017-02-17 13:28:52'),
(14, 2, 13, '2017-02-17 13:29:15', '2017-02-17 13:29:15'),
(15, 2, 33, '2017-02-17 15:54:55', '2017-02-17 15:54:55'),
(17, 2, 15, '2017-02-17 16:33:14', '2017-02-17 16:33:14'),
(18, 2, 16, '2017-02-17 16:33:33', '2017-02-17 16:33:33'),
(19, 2, 64, '2017-02-17 16:48:06', '2017-02-17 16:48:06'),
(20, 2, 55, '2017-02-17 18:31:31', '2017-02-17 18:31:31'),
(21, 2, 63, '2017-02-17 19:50:58', '2017-02-17 19:50:58'),
(22, 2, 64, '2017-02-17 19:51:17', '2017-02-17 19:51:17'),
(23, 2, 65, '2017-02-17 19:51:33', '2017-02-17 19:51:33'),
(24, 2, 71, '2017-02-18 03:20:12', '2017-02-18 03:20:12'),
(25, 2, 72, '2017-02-18 03:20:26', '2017-02-18 03:20:26'),
(26, 2, 34, '2017-02-25 14:53:43', '2017-02-25 14:53:43'),
(27, 2, 35, '2017-02-25 14:56:52', '2017-02-25 14:56:52'),
(28, 2, 47, '2017-02-25 14:58:43', '2017-02-25 14:58:43'),
(29, 2, 32, '2017-03-01 04:15:03', '2017-03-01 04:15:03'),
(35, 8, 13, '2017-03-01 17:15:26', '2017-03-01 17:15:26'),
(36, 8, 15, '2017-03-01 17:15:43', '2017-03-01 17:15:43'),
(37, 2, 73, '2017-03-10 00:53:58', '2017-03-10 00:53:58'),
(38, 2, 27, '2017-03-10 21:55:36', '2017-03-10 21:55:36'),
(39, 2, 36, '2017-03-10 22:00:36', '2017-03-10 22:00:36'),
(40, 2, 38, '2017-03-10 22:01:04', '2017-03-10 22:01:04'),
(41, 2, 57, '2017-03-11 12:19:13', '2017-03-11 12:19:13'),
(42, 2, 29, '2017-03-18 17:45:36', '2017-03-18 17:45:36'),
(43, 2, 19, '2017-03-30 18:14:53', '2017-03-30 18:14:53'),
(44, 2, 60, '2017-03-31 20:01:24', '2017-03-31 20:01:24'),
(45, 2, 31, '2017-04-03 19:59:13', '2017-04-03 19:59:13'),
(46, 2, 18, '2017-04-17 17:54:16', '2017-04-17 17:54:16'),
(47, 2, 56, '2017-04-17 18:01:21', '2017-04-17 18:01:21'),
(48, 2, 74, '2017-04-19 16:19:40', '2017-04-19 16:19:40'),
(49, 2, 59, '2017-04-27 20:39:12', '2017-04-27 20:39:12'),
(50, 2, 75, '2017-04-27 20:39:25', '2017-04-27 20:39:25'),
(51, 2, 46, '2017-05-02 15:43:35', '2017-05-02 15:43:35'),
(52, 2, 19, '2017-06-13 17:16:10', '2017-06-13 17:16:10'),
(53, 2, 54, '2017-06-13 17:16:43', '2017-06-13 17:16:43'),
(54, 2, 41, '2017-06-13 18:03:30', '2017-06-13 18:03:30'),
(55, 2, 30, '2017-06-13 18:04:48', '2017-06-13 18:04:48'),
(56, 9, 13, '2017-06-13 18:56:30', '2017-06-13 18:56:30'),
(57, 9, 19, '2017-06-13 18:58:07', '2017-06-13 18:58:07'),
(58, 9, 60, '2017-06-13 18:58:50', '2017-06-13 18:58:50'),
(59, 9, 63, '2017-06-13 19:00:26', '2017-06-13 19:00:26'),
(60, 9, 64, '2017-06-13 19:00:45', '2017-06-13 19:00:45'),
(61, 2, 75, '2017-06-13 19:01:48', '2017-06-13 19:01:48'),
(62, 2, 76, '2017-06-13 19:04:55', '2017-06-13 19:04:55'),
(63, 9, 75, '2017-06-13 19:38:47', '2017-06-13 19:38:47'),
(64, 9, 76, '2017-06-13 19:39:01', '2017-06-13 19:39:01'),
(65, 9, 71, '2017-06-13 19:43:08', '2017-06-13 19:43:08'),
(66, 9, 72, '2017-06-13 19:43:27', '2017-06-13 19:43:27'),
(67, 2, 53, '2017-06-13 19:53:50', '2017-06-13 19:53:50'),
(68, 2, 24, '2017-06-13 19:55:04', '2017-06-13 19:55:04'),
(69, 2, 25, '2017-06-13 19:55:12', '2017-06-13 19:55:12'),
(70, 2, 26, '2017-06-13 19:55:23', '2017-06-13 19:55:23'),
(71, 2, 27, '2017-06-13 19:55:36', '2017-06-13 19:55:36'),
(72, 2, 50, '2017-06-21 21:13:38', '2017-06-21 21:13:38'),
(73, 2, 44, '2017-06-21 21:58:19', '2017-06-21 21:58:19'),
(74, 2, 45, '2017-06-21 22:05:03', '2017-06-21 22:05:03'),
(75, 2, 42, '2017-06-21 22:05:33', '2017-06-21 22:05:33'),
(76, 2, 61, '2017-06-21 22:06:29', '2017-06-21 22:06:29'),
(77, 2, 62, '2017-06-21 22:06:55', '2017-06-21 22:06:55'),
(78, 2, 28, '2017-06-21 22:34:53', '2017-06-21 22:34:53'),
(79, 2, 39, '2017-06-21 22:41:27', '2017-06-21 22:41:27'),
(80, 2, 40, '2017-06-21 22:41:44', '2017-06-21 22:41:44'),
(81, 8, 39, '2017-06-21 22:42:52', '2017-06-21 22:42:52'),
(82, 8, 40, '2017-06-21 22:43:05', '2017-06-21 22:43:05'),
(83, 2, 20, '2017-06-21 23:09:42', '2017-06-21 23:09:42'),
(84, 2, 52, '2017-06-21 23:09:59', '2017-06-21 23:09:59'),
(85, 2, 37, '2017-06-22 20:12:18', '2017-06-22 20:12:18'),
(86, 9, 71, '2017-06-22 20:13:03', '2017-06-22 20:13:03'),
(87, 9, 72, '2017-06-22 20:13:27', '2017-06-22 20:13:27'),
(88, 9, 76, '2017-06-22 20:13:57', '2017-06-22 20:13:57'),
(89, 2, 49, '2017-06-22 20:32:26', '2017-06-22 20:32:26'),
(90, 2, 51, '2017-06-22 20:32:39', '2017-06-22 20:32:39'),
(91, 2, 17, '2017-08-09 19:21:29', '2017-08-09 19:21:29'),
(92, 8, 58, '2017-09-06 15:00:52', '2017-09-06 15:00:52'),
(93, 2, 58, '2017-09-06 15:01:51', '2017-09-06 15:01:51');

-- --------------------------------------------------------

--
-- Table structure for table `rootnotas`
--

CREATE TABLE IF NOT EXISTS `rootnotas` (
`id` int(11) NOT NULL,
  `denominacion` varchar(35) CHARACTER SET latin1 NOT NULL,
  `descripcion` varchar(1050) CHARACTER SET latin1 NOT NULL,
  `statu_id` int(11) DEFAULT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `servicios`
--

CREATE TABLE IF NOT EXISTS `servicios` (
`id` int(11) NOT NULL,
  `titulo` varchar(512) DEFAULT NULL,
  `denominacion` mediumtext NOT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `servicios`
--

INSERT INTO `servicios` (`id`, `titulo`, `denominacion`, `estatus`) VALUES
(1, 'RADIOGRAFÃAS DIGITALES', 'Unicas en el sureste, ofreciÃ©ndonos la mejor calidad y visiÃ³n de las estructuras anatÃ³micas y patologÃ­as.', 1),
(2, 'FOTOGRAFÃA CLÃNICA', 'Complemento profesional para el diagnÃ³stico.', 1),
(3, 'ASESORÃ­A Y CAPACITACIÃ“N', 'Del uso de los diversos software para visualizar estudios tomogrÃ¡ficos de acuerdo a cada especialidad.', 1),
(4, 'ESTUDIOS TOMOGRÃFICOS', 'Con la Ãºltima tecnologÃ­a, que nos brinda menor radiaciÃ³n y mayor exactitud y precisiÃ³n en el diagnÃ³stico', 1),
(5, 'INTERPRETACIÃ“N RADIOGRÃFICA', 'Elaborada por especialista, y para apoyo diagnÃ³stico', 1),
(6, 'ACTUALIZACIÃ“N', 'Creamos eventos de interÃ¨s cientÃ­fico que nos permite aprender y ademÃ¡s interactuar.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE IF NOT EXISTS `sliders` (
`id` int(11) NOT NULL,
  `carpeta_imagen` varchar(256) DEFAULT NULL,
  `contenido` text,
  `nombre_imagen` varchar(512) NOT NULL,
  `tipo_imagen` varchar(90) NOT NULL,
  `ruta_imagen` varchar(256) NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `carpeta_imagen`, `contenido`, `nombre_imagen`, `tipo_imagen`, `ruta_imagen`, `created`, `modified`) VALUES
(21, 'upload', '<p>Radiograf&iacute;as digitales</p>\r\n', 'a_22062017_083519.jpeg', 'image/jpeg', 'upload/a_22062017_083519.jpeg', '2017-06-22', '2017-06-22'),
(22, 'upload', '<p>Tomograf&iacute;a Computarizada</p>\r\n', 'a_22062017_083614.jpeg', 'image/jpeg', 'upload/a_22062017_083614.jpeg', '2017-06-22', '2017-06-22'),
(24, 'upload', '<h2 style="font-style:italic;">Diagn&oacute;stico exacto</h2>\r\n', 'a_22062017_085825.png', 'image/png', 'upload/a_22062017_085825.png', '2017-06-22', '2017-06-22');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE IF NOT EXISTS `status` (
`id` int(11) NOT NULL,
  `denominacion` varchar(15) CHARACTER SET latin1 DEFAULT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `denominacion`, `created`, `modified`) VALUES
(1, 'Realizada', '2017-02-09', '2017-06-21'),
(2, 'Pendiente', '2017-06-21', '2017-06-21'),
(3, 'Cancelada', '2017-06-21', '2017-06-21');

-- --------------------------------------------------------

--
-- Table structure for table `sucursales`
--

CREATE TABLE IF NOT EXISTS `sucursales` (
`id` int(11) NOT NULL,
  `denominacion` varchar(60) NOT NULL,
  `direccion` varchar(256) NOT NULL,
  `telefono` varchar(25) NOT NULL,
  `email` varchar(60) NOT NULL,
  `latitud` varchar(250) NOT NULL,
  `longitud` varchar(250) NOT NULL,
  `color` varchar(90) NOT NULL DEFAULT '#1ABB9C'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sucursales`
--

INSERT INTO `sucursales` (`id`, `denominacion`, `direccion`, `telefono`, `email`, `latitud`, `longitud`, `color`) VALUES
(1, 'Fco. de Montejo', 'Calle 42 #355 x 55 y 55-a Fco. de Montejo CP. 97203', '(999) 1.95.22.98', 'quantummontejo@gmail.com', '23.0020941', '-104.6088675', '#ff80bf'),
(2, 'Centro', 'Calle 69 x 46 y 48 #435-A Col. Centro CP. 97000', '(999) 9.24.23.75', 'quantumcentro@gmail.com', '', '', '#ff80bf');

-- --------------------------------------------------------

--
-- Table structure for table `suscribirses`
--

CREATE TABLE IF NOT EXISTS `suscribirses` (
`id` bigint(20) unsigned NOT NULL,
  `nombres` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suscribirses`
--

INSERT INTO `suscribirses` (`id`, `nombres`, `email`, `created`, `modified`) VALUES
(1, 'celular', 'diazvjosetomas@gmail.com', '2017-02-10 11:59:31', '2017-02-10 11:59:31'),
(2, 'jcloud', 'jcloudtechn@gmail.com', '2017-02-14 00:12:32', '2017-02-14 00:12:32'),
(3, 'hotmail', 'diazvjosetomas@hotmail.com', '2017-02-14 09:07:30', '2017-02-14 09:07:30');

-- --------------------------------------------------------

--
-- Table structure for table `tipocitas`
--

CREATE TABLE IF NOT EXISTS `tipocitas` (
`id` int(11) NOT NULL,
  `denominacion` varchar(90) NOT NULL,
  `tiempo` varchar(20) NOT NULL,
  `costo` varchar(20) NOT NULL,
  `detalles` varchar(512) NOT NULL,
  `categoriacita_id` int(11) NOT NULL,
  `sucursale_id` int(11) NOT NULL,
  `carpeta_imagen` varchar(90) NOT NULL,
  `nombre_imagen` varchar(90) DEFAULT NULL,
  `tipo_imagen` varchar(90) NOT NULL,
  `ruta_imagen` varchar(90) DEFAULT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipocitas`
--

INSERT INTO `tipocitas` (`id`, `denominacion`, `tiempo`, `costo`, `detalles`, `categoriacita_id`, `sucursale_id`, `carpeta_imagen`, `nombre_imagen`, `tipo_imagen`, `ruta_imagen`, `created`, `modified`) VALUES
(1, 'Periapical', '10 min.', '100,00$', 'VISION ESPECIFICA DE 1 A 3 PIEZAS DENTALES', 2, 1, 'upload', 'a_05122016_100611.gif', 'image/gif', 'upload/a_05122016_100611.gif', '2016-12-05', '2017-06-13'),
(2, 'PANORAMICA DIGITAL', '30 min.', '250,00 $', 'VISION GENERAL DEL ESTADO BUCAL.', 2, 1, '_', '_', '_', '_', '2016-12-05', '2017-06-13'),
(3, 'ESTUDIO ORTODONCIA', '30 min.', '1.300,00 $', 'ESTUDIO TOMOGRÃFICO ESPECIALIZADO QUE INCLUYE FOTOGRAFÃAS CLÃNICAS Y TRAZADO CEFALOMÃ‰TRICO.', 1, 1, 'upload', 'a_08122016_113111.jpeg', 'image/jpeg', 'upload/a_08122016_113111.jpeg', '2016-12-08', '2017-06-13'),
(4, 'LATERAL DE CRÃNEO', '30 min.', '250,00 $', 'VISTA LATERAL DE ESTRUCTURAS DE CABEZA Y CUELLO', 1, 2, '_', '_', '_', '_', '2017-06-13', '2017-06-13'),
(5, 'ESTUDIO TOMOGRÃFICO', '30 min.', '1000', 'ESTUDIO TRIDIMENSIONAL ESPECIALIZADO DE ESTRUCTURAS DE CABEZA Y CUELLO', 1, 2, '_', '_', '_', '_', '2017-06-13', '2017-06-13'),
(6, 'ESTUDIO ATM', '30 min.', '1600', 'DOS ESTUDIOS TOMOGRÃFICOS CON BOCA CERRADA Y BOCA ABIERTA ', 1, 2, '_', '_', '_', '_', '2017-06-13', '2017-06-13'),
(7, 'ESTUDIO VÃAS AÃ‰REAS', '30 min.', '1000', 'ESTUDIO TOMOGRÃFICO ESPECIALIZADO QUE  BRINDA IMÃGENES DE VÃAS AÃ‰REAS EN DIFERENTES CONTRASTES', 1, 2, '_', '_', '_', '_', '2017-06-13', '2017-06-13'),
(8, 'RADIOGRAFIA DE CONTRASTE', '30 min.', '800', 'SERIE RADIOGRÃFICA DE DIVERSOS CONTRASTES QUE FACILITA LA DETECCIÃ“N DE CIERTAS PATOLOGÃAS', 1, 2, '_', '_', '_', '_', '2017-06-13', '2017-06-13'),
(9, 'POSTEROANTERIOR, ', '30 min.', '250', 'VISTA POSTERIOR DE LAS ESTRUCTURAS DE CABEZA Y CUELLO', 1, 2, '_', '_', '_', '_', '2017-06-13', '2017-06-13'),
(10, 'ANTEROPOSTERIOR', '30 min.', '250', 'VISTA ANTERIOR DE LAS ESTRUCTURAS DE CABEZA Y CUELLO', 1, 2, '_', '_', '_', '_', '2017-06-13', '2017-06-13');

-- --------------------------------------------------------

--
-- Table structure for table `tipoclientes`
--

CREATE TABLE IF NOT EXISTS `tipoclientes` (
`id` int(11) NOT NULL,
  `denominacion` varchar(90) NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipoclientes`
--

INSERT INTO `tipoclientes` (`id`, `denominacion`, `created`, `modified`) VALUES
(1, 'Ortodoncia', '2016-10-26', '2017-06-21'),
(2, 'CirugÃ­a maxilofacial', '2016-12-19', '2017-06-21'),
(3, 'RehabilitaciÃ³n', '2017-06-21', '2017-06-21'),
(4, 'Periodoncia', '2017-06-21', '2017-06-21'),
(5, 'Endodoncia', '2017-06-21', '2017-06-21'),
(6, 'ImplantologÃ­a', '2017-06-21', '2017-06-21'),
(7, 'Estudiante odontologÃ­a', '2017-06-21', '2017-06-21');

-- --------------------------------------------------------

--
-- Table structure for table `tipoentradas`
--

CREATE TABLE IF NOT EXISTS `tipoentradas` (
`id` int(11) NOT NULL,
  `denominacion` varchar(90) NOT NULL,
  `descripcion` text NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipoentradas`
--

INSERT INTO `tipoentradas` (`id`, `denominacion`, `descripcion`, `created`, `modified`) VALUES
(1, 'Entrada', '', '2016-10-23', '2016-10-23'),
(2, 'Salida', '', '2016-10-28', '2016-10-28');

-- --------------------------------------------------------

--
-- Table structure for table `tipopermisos`
--

CREATE TABLE IF NOT EXISTS `tipopermisos` (
`id` int(11) NOT NULL,
  `denominacion` varchar(256) NOT NULL,
  `descripcion` text NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipopermisos`
--

INSERT INTO `tipopermisos` (`id`, `denominacion`, `descripcion`, `created`, `modified`) VALUES
(1, 'Provisional', '', '2016-10-23', '2016-10-23'),
(2, 'Familiar', '', '2016-10-24', '2016-10-24'),
(3, 'Personal', 'personal', '2017-06-13', '2017-06-13'),
(4, 'vacaciones', 'vacaciones', '2017-06-21', '2017-06-21');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` bigint(20) unsigned NOT NULL,
  `user` varchar(100) NOT NULL,
  `role_id` int(11) NOT NULL,
  `sucursale_id` int(11) NOT NULL,
  `password` varchar(300) NOT NULL,
  `email` varchar(300) NOT NULL,
  `nombre` varchar(300) NOT NULL,
  `apellidos` varchar(300) NOT NULL,
  `telefono` varchar(90) NOT NULL,
  `statu` int(11) NOT NULL,
  `tipocliente_id` int(11) NOT NULL,
  `nivel` int(11) NOT NULL DEFAULT '3'
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user`, `role_id`, `sucursale_id`, `password`, `email`, `nombre`, `apellidos`, `telefono`, `statu`, `tipocliente_id`, `nivel`) VALUES
(10, 'brenda', 2, 1, '827ccb0eea8a706c4c34a16891f84e7b', 'drabrendaayala@gmail.com', 'Brenda ', 'Ayala', '', 1, 0, 1),
(41, 'pongopa', 0, 0, '827ccb0eea8a706c4c34a16891f84e7b', 'diazvjosetomas@gmail.com', 'tomas', 'diaz', '04124616235', 1, 1, 3),
(42, 'andrea', 0, 0, '202cb962ac59075b964b07152d234b70', 'andrea@gmail.com', 'Andre', 'Cordero', '04124616235', 1, 3, 3),
(43, 'victoria', 9, 2, '827ccb0eea8a706c4c34a16891f84e7b', 'vickymena27@gmail.com', 'Victoria', 'Uc', '', 1, 0, 1),
(44, 'estelita', 9, 1, '827ccb0eea8a706c4c34a16891f84e7b', 'azurydark@gmail.com', 'ESTELA ', 'ARANDA', '', 1, 0, 1),
(45, 'noemi', 8, 2, '827ccb0eea8a706c4c34a16891f84e7b', 'QUANTUMADMON@GMAIL.COM', 'NOEMI', 'GOMEZ', '', 1, 1, 1),
(46, 'joaquin', 2, 2, '827ccb0eea8a706c4c34a16891f84e7b', 'joaco_50t@gmail.com', 'JOAQUIN', 'FLORES', '', 1, 0, 1),
(47, 'joaco', 0, 0, '827ccb0eea8a706c4c34a16891f84e7b', 'blar7@hotmail.com', 'joaquin', 'flores Sanchez', '9242375', 1, 1, 3),
(48, 'brelo', 0, 0, '202cb962ac59075b964b07152d234b70', 'blar7@hotmail.com', 'brenda', 'ayala', '9242375', 1, 1, 3),
(49, 'giorgio', 0, 0, 'c20ad4d76fe97759aa27a0c99bff6710', 'george@gmail.com', 'georgina', 'metri', '9242375', 1, 3, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adjuntos`
--
ALTER TABLE `adjuntos`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `asignacitas`
--
ALTER TABLE `asignacitas`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `asistencias`
--
ALTER TABLE `asistencias`
 ADD PRIMARY KEY (`id`), ADD KEY `personale_id` (`personale_id`), ADD KEY `tipoentrada_id` (`tipoentrada_id`);

--
-- Indexes for table `avisoprivas`
--
ALTER TABLE `avisoprivas`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `blogcategorias`
--
ALTER TABLE `blogcategorias`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `blogentradas`
--
ALTER TABLE `blogentradas`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `blogpublicidades`
--
ALTER TABLE `blogpublicidades`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `cajaaperturas`
--
ALTER TABLE `cajaaperturas`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `cajacierredias`
--
ALTER TABLE `cajacierredias`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `cajacierres`
--
ALTER TABLE `cajacierres`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `cajas`
--
ALTER TABLE `cajas`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `categoriacitas`
--
ALTER TABLE `categoriacitas`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categorias`
--
ALTER TABLE `categorias`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `citas`
--
ALTER TABLE `citas`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `conceptos`
--
ALTER TABLE `conceptos`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `configurations`
--
ALTER TABLE `configurations`
 ADD PRIMARY KEY (`id`), ADD KEY `language_id` (`language_id`), ADD KEY `money_simbol_id` (`money_simbol_id`), ADD KEY `country_id` (`country_id`);

--
-- Indexes for table `contactos`
--
ALTER TABLE `contactos`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contenidos`
--
ALTER TABLE `contenidos`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countrys`
--
ALTER TABLE `countrys`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dashboards`
--
ALTER TABLE `dashboards`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `doctores`
--
ALTER TABLE `doctores`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `egresos`
--
ALTER TABLE `egresos`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `egresotipos`
--
ALTER TABLE `egresotipos`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `empresas`
--
ALTER TABLE `empresas`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `facturaciones`
--
ALTER TABLE `facturaciones`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `feriados`
--
ALTER TABLE `feriados`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imgadjuntos`
--
ALTER TABLE `imgadjuntos`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imghorarios`
--
ALTER TABLE `imghorarios`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `ingresodetalles`
--
ALTER TABLE `ingresodetalles`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `ingresofolios`
--
ALTER TABLE `ingresofolios`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `ingresoivas`
--
ALTER TABLE `ingresoivas`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `ingresos`
--
ALTER TABLE `ingresos`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `ingresotipomontos`
--
ALTER TABLE `ingresotipomontos`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `ingresotipopagos`
--
ALTER TABLE `ingresotipopagos`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `ingresotipos`
--
ALTER TABLE `ingresotipos`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `laborables`
--
ALTER TABLE `laborables`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modulos`
--
ALTER TABLE `modulos`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `notas`
--
ALTER TABLE `notas`
 ADD PRIMARY KEY (`id`), ADD KEY `statu_id` (`statu_id`);

--
-- Indexes for table `notificaciones`
--
ALTER TABLE `notificaciones`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pagos`
--
ALTER TABLE `pagos`
 ADD PRIMARY KEY (`id`), ADD KEY `concepto_id` (`concepto_id`), ADD KEY `personale_id` (`personale_id`);

--
-- Indexes for table `periodos`
--
ALTER TABLE `periodos`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permisos`
--
ALTER TABLE `permisos`
 ADD PRIMARY KEY (`id`), ADD KEY `tipopermiso_id` (`tipopermiso_id`), ADD KEY `personale_id` (`personale_id`);

--
-- Indexes for table `personales`
--
ALTER TABLE `personales`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promociones`
--
ALTER TABLE `promociones`
 ADD PRIMARY KEY (`id`), ADD KEY `periodo_id` (`periodo_id`), ADD KEY `publico_id` (`publico_id`);

--
-- Indexes for table `proveedor_pagos`
--
ALTER TABLE `proveedor_pagos`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `publicos`
--
ALTER TABLE `publicos`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `role_alias` (`alias`);

--
-- Indexes for table `rolesmodulos`
--
ALTER TABLE `rolesmodulos`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `rootnotas`
--
ALTER TABLE `rootnotas`
 ADD PRIMARY KEY (`id`), ADD KEY `statu_id` (`statu_id`);

--
-- Indexes for table `servicios`
--
ALTER TABLE `servicios`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sucursales`
--
ALTER TABLE `sucursales`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suscribirses`
--
ALTER TABLE `suscribirses`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tipocitas`
--
ALTER TABLE `tipocitas`
 ADD PRIMARY KEY (`id`), ADD KEY `categoriacita_id` (`categoriacita_id`);

--
-- Indexes for table `tipoclientes`
--
ALTER TABLE `tipoclientes`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipoentradas`
--
ALTER TABLE `tipoentradas`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipopermisos`
--
ALTER TABLE `tipopermisos`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adjuntos`
--
ALTER TABLE `adjuntos`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `asignacitas`
--
ALTER TABLE `asignacitas`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `asistencias`
--
ALTER TABLE `asistencias`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `avisoprivas`
--
ALTER TABLE `avisoprivas`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `blogcategorias`
--
ALTER TABLE `blogcategorias`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `blogentradas`
--
ALTER TABLE `blogentradas`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `blogpublicidades`
--
ALTER TABLE `blogpublicidades`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `cajaaperturas`
--
ALTER TABLE `cajaaperturas`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cajacierredias`
--
ALTER TABLE `cajacierredias`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cajacierres`
--
ALTER TABLE `cajacierres`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cajas`
--
ALTER TABLE `cajas`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `categoriacitas`
--
ALTER TABLE `categoriacitas`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `categorias`
--
ALTER TABLE `categorias`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `citas`
--
ALTER TABLE `citas`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `conceptos`
--
ALTER TABLE `conceptos`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `configurations`
--
ALTER TABLE `configurations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contactos`
--
ALTER TABLE `contactos`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `contenidos`
--
ALTER TABLE `contenidos`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `countrys`
--
ALTER TABLE `countrys`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `dashboards`
--
ALTER TABLE `dashboards`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `doctores`
--
ALTER TABLE `doctores`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `egresos`
--
ALTER TABLE `egresos`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `egresotipos`
--
ALTER TABLE `egresotipos`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `empresas`
--
ALTER TABLE `empresas`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `facturaciones`
--
ALTER TABLE `facturaciones`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `feriados`
--
ALTER TABLE `feriados`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `imgadjuntos`
--
ALTER TABLE `imgadjuntos`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `imghorarios`
--
ALTER TABLE `imghorarios`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ingresodetalles`
--
ALTER TABLE `ingresodetalles`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `ingresofolios`
--
ALTER TABLE `ingresofolios`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ingresoivas`
--
ALTER TABLE `ingresoivas`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ingresos`
--
ALTER TABLE `ingresos`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `ingresotipomontos`
--
ALTER TABLE `ingresotipomontos`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `ingresotipopagos`
--
ALTER TABLE `ingresotipopagos`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ingresotipos`
--
ALTER TABLE `ingresotipos`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `laborables`
--
ALTER TABLE `laborables`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `modulos`
--
ALTER TABLE `modulos`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `notas`
--
ALTER TABLE `notas`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notificaciones`
--
ALTER TABLE `notificaciones`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `pagos`
--
ALTER TABLE `pagos`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `periodos`
--
ALTER TABLE `periodos`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `permisos`
--
ALTER TABLE `permisos`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `personales`
--
ALTER TABLE `personales`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `promociones`
--
ALTER TABLE `promociones`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `proveedor_pagos`
--
ALTER TABLE `proveedor_pagos`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `publicos`
--
ALTER TABLE `publicos`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `rolesmodulos`
--
ALTER TABLE `rolesmodulos`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=94;
--
-- AUTO_INCREMENT for table `rootnotas`
--
ALTER TABLE `rootnotas`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `servicios`
--
ALTER TABLE `servicios`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sucursales`
--
ALTER TABLE `sucursales`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `suscribirses`
--
ALTER TABLE `suscribirses`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tipocitas`
--
ALTER TABLE `tipocitas`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tipoclientes`
--
ALTER TABLE `tipoclientes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tipoentradas`
--
ALTER TABLE `tipoentradas`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tipopermisos`
--
ALTER TABLE `tipopermisos`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `asistencias`
--
ALTER TABLE `asistencias`
ADD CONSTRAINT `asistencias_ibfk_1` FOREIGN KEY (`personale_id`) REFERENCES `personales` (`id`),
ADD CONSTRAINT `asistencias_ibfk_2` FOREIGN KEY (`tipoentrada_id`) REFERENCES `tipoentradas` (`id`);

--
-- Constraints for table `notas`
--
ALTER TABLE `notas`
ADD CONSTRAINT `notas_ibfk_1` FOREIGN KEY (`statu_id`) REFERENCES `status` (`id`);

--
-- Constraints for table `pagos`
--
ALTER TABLE `pagos`
ADD CONSTRAINT `pagos_ibfk_1` FOREIGN KEY (`concepto_id`) REFERENCES `conceptos` (`id`),
ADD CONSTRAINT `pagos_ibfk_2` FOREIGN KEY (`personale_id`) REFERENCES `personales` (`id`);

--
-- Constraints for table `permisos`
--
ALTER TABLE `permisos`
ADD CONSTRAINT `permisos_ibfk_1` FOREIGN KEY (`tipopermiso_id`) REFERENCES `tipopermisos` (`id`),
ADD CONSTRAINT `permisos_ibfk_2` FOREIGN KEY (`personale_id`) REFERENCES `personales` (`id`);

--
-- Constraints for table `promociones`
--
ALTER TABLE `promociones`
ADD CONSTRAINT `promociones_ibfk_1` FOREIGN KEY (`periodo_id`) REFERENCES `periodos` (`id`),
ADD CONSTRAINT `promociones_ibfk_2` FOREIGN KEY (`publico_id`) REFERENCES `publicos` (`id`);

--
-- Constraints for table `tipocitas`
--
ALTER TABLE `tipocitas`
ADD CONSTRAINT `tipocitas_ibfk_1` FOREIGN KEY (`categoriacita_id`) REFERENCES `categoriacitas` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
