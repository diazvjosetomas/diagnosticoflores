<?php
App::uses('AppModel', 'Model');
/**
 * DataWebSite Model
 *
 */
class DataWebSite extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name_sites';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name_sites' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
