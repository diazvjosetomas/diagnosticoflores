<?php
App::uses('AppModel', 'Model');
/**
 * Pago Model
 *
 * @property Concepto $Concepto
 * @property Personale $Personale
 */
class Pago extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'concepto_id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'concepto_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'personale_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'monto' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'fecha_pago' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Concepto' => array(
			'className' => 'Concepto',
			'foreignKey' => 'concepto_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Personale' => array(
			'className' => 'Personale',
			'foreignKey' => 'personale_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ProveedorPago' => array(
			'className' => 'ProveedorPago',
			'foreignKey' => 'proveedor_pago_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
