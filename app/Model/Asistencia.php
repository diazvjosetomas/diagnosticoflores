<?php
App::uses('AppModel', 'Model');
/**
 * Asistencia Model
 *
 * @property Personale $Personale
 * @property Tipoentrada $Tipoentrada
 */
class Asistencia extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'personale_id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'personale_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'fecha_asistencia' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'tipoentrada_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'hora' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Personale' => array(
			'className' => 'Personale',
			'foreignKey' => 'personale_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Tipoentrada' => array(
			'className' => 'Tipoentrada',
			'foreignKey' => 'tipoentrada_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
