<?php
App::uses('AppModel', 'Model');
/**
 * Permiso Model
 *
 * @property Tipopermiso $Tipopermiso
 * @property Personale $Personale
 */
class Permiso extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'personale_id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'tipopermiso_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'personale_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'desde' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'hasta' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'motivo' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Tipopermiso' => array(
			'className' => 'Tipopermiso',
			'foreignKey' => 'tipopermiso_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Personale' => array(
			'className' => 'Personale',
			'foreignKey' => 'personale_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
