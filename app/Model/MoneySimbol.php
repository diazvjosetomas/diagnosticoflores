<?php
App::uses('AppModel', 'Model');
/**
 * MoneySimbol Model
 *
 * @property Configuration $Configuration
 */
class MoneySimbol extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'coins';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'coins' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Configuration' => array(
			'className' => 'Configuration',
			'foreignKey' => 'money_simbol_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
