<?php
App::uses('AppController', 'Controller');
/**
 * Publicos Controller
 *
 * @property Publico $Publico
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class PublicosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');
	public $layout = 'gentella';

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(35);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Publico->recursive = 0;
		//$this->set('publicos', $this->Paginator->paginate());
		  $this->set('publicos', $this->Publico->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Publico->exists($id)) {
			throw new NotFoundException(__('Invalid publico'));
		}
		$options = array('conditions' => array('Publico.' . $this->Publico->primaryKey => $id));
		$this->set('publico', $this->Publico->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Publico->create();
			if ($this->Publico->save($this->request->data)) {
				
				$this->Flash->success(__('The publico has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The publico could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Publico->exists($id)) {
			throw new NotFoundException(__('Invalid publico'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Publico->save($this->request->data)) {
				$this->Flash->success(__('The publico has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The publico could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Publico.' . $this->Publico->primaryKey => $id));
			$this->request->data = $this->Publico->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Publico->id = $id;
		if (!$this->Publico->exists()) {
			throw new NotFoundException(__('Invalid publico'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Publico->delete()) {
			$this->Flash->success(__('The publico has been deleted.'));
		} else {
			$this->Flash->error(__('The publico could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
