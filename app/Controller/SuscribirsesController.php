<?php
App::uses('AppController', 'Controller');
/**
 * Suscribirses Controller
 *
 * @property Suscribirse $Suscribirse
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SuscribirsesController extends AppController {

/**
 * Components
 *
 * @var array
 */
		public $components = array('Paginator', 'Session','Flash');

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(53);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = 'gentella';
		//$this->Suscribirse->recursive = 0;
		//$this->set('suscribirses', $this->Paginator->paginate());
		$this->set('suscribirses', $this->Suscribirse->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->layout = 'gentella';
		if (!$this->Suscribirse->exists($id)) {
			throw new NotFoundException(__('Invalid suscribirse'));
		}
		$options = array('conditions' => array('Suscribirse.' . $this->Suscribirse->primaryKey => $id));
		$this->set('suscribirse', $this->Suscribirse->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = 'gentella';
		if ($this->request->is('post')) {
			$this->Suscribirse->create();
			if ($this->Suscribirse->save($this->request->data)) {
				
				$this->Flash->success(__('The suscribirse has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The suscribirse could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout = 'gentella';
		if (!$this->Suscribirse->exists($id)) {
			throw new NotFoundException(__('Invalid suscribirse'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Suscribirse->save($this->request->data)) {
				$this->Flash->success(__('The suscribirse has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The suscribirse could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Suscribirse.' . $this->Suscribirse->primaryKey => $id));
			$this->request->data = $this->Suscribirse->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->layout = 'gentella';
		$this->Suscribirse->id = $id;
		if (!$this->Suscribirse->exists()) {
			throw new NotFoundException(__('Invalid suscribirse'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Suscribirse->delete()) {
			$this->Flash->success(__('The suscribirse has been deleted.'));
		} else {
			$this->Flash->error(__('The suscribirse could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
