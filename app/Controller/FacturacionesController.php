<?php
App::uses('AppController', 'Controller');
/**
 * Facturaciones Controller
 *
 * @property Facturacione $Facturacione
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class FacturacionesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

	var $layout = "gentella";

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(57);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Facturacione->recursive = 0;
		//$this->set('facturaciones', $this->Paginator->paginate());
		$this->set('facturaciones', $this->Facturacione->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Facturacione->exists($id)) {
			throw new NotFoundException(__('Invalid facturacione'));
		}
		$options = array('conditions' => array('Facturacione.' . $this->Facturacione->primaryKey => $id));
		$this->set('facturacione', $this->Facturacione->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Facturacione->create();
			if ($this->Facturacione->save($this->request->data)) {
				
				$this->Flash->success(__('The facturacione has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The facturacione could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Facturacione->exists($id)) {
			throw new NotFoundException(__('Invalid facturacione'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Facturacione->save($this->request->data)) {
				$this->Flash->success(__('The facturacione has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The facturacione could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Facturacione.' . $this->Facturacione->primaryKey => $id));
			$this->request->data = $this->Facturacione->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Facturacione->id = $id;
		if (!$this->Facturacione->exists()) {
			throw new NotFoundException(__('Invalid facturacione'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Facturacione->delete()) {
			$this->Flash->success(__('The facturacione has been deleted.'));
		} else {
			$this->Flash->error(__('The facturacione could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
