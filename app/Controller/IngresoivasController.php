<?php
App::uses('AppController', 'Controller');
/**
 * Ingresoivas Controller
 *
 * @property Ingresoiva $Ingresoiva
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class IngresoivasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


var $layout = "gentella";

 public function beforeFilter() {
	$this->checkSession(42);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Ingresoiva->recursive = 0;
		//$this->set('ingresoivas', $this->Paginator->paginate());
		  $this->set('ingresoivas', $this->Ingresoiva->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Ingresoiva->exists($id)) {
			throw new NotFoundException(__('Invalid ingresoiva'));
		}
		$options = array('conditions' => array('Ingresoiva.' . $this->Ingresoiva->primaryKey => $id));
		$this->set('ingresoiva', $this->Ingresoiva->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Ingresoiva->create();
			if ($this->Ingresoiva->save($this->request->data)) {
				
				$this->Flash->success(__('The ingresoiva has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ingresoiva could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Ingresoiva->exists($id)) {
			throw new NotFoundException(__('Invalid ingresoiva'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Ingresoiva->save($this->request->data)) {
				$this->Flash->success(__('The ingresoiva has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ingresoiva could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Ingresoiva.' . $this->Ingresoiva->primaryKey => $id));
			$this->request->data = $this->Ingresoiva->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Ingresoiva->id = $id;
		if (!$this->Ingresoiva->exists()) {
			throw new NotFoundException(__('Invalid ingresoiva'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Ingresoiva->delete()) {
			$this->Flash->success(__('The ingresoiva has been deleted.'));
		} else {
			$this->Flash->error(__('The ingresoiva could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
