<?php
App::uses('AppController', 'Controller');
/**
 * Tipoentradas Controller
 *
 * @property Tipoentrada $Tipoentrada
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class TipoentradasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(25);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = 'gentella';
		//$this->Tipoentrada->recursive = 0;
		//$this->set('tipoentradas', $this->Paginator->paginate());
		$this->set('tipoentradas', $this->Tipoentrada->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->layout = 'gentella';
		if (!$this->Tipoentrada->exists($id)) {
			throw new NotFoundException(__('Invalid tipoentrada'));
		}
		$options = array('conditions' => array('Tipoentrada.' . $this->Tipoentrada->primaryKey => $id));
		$this->set('tipoentrada', $this->Tipoentrada->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = 'gentella';
		if ($this->request->is('post')) {
			$this->Tipoentrada->create();
			if ($this->Tipoentrada->save($this->request->data)) {
				
				$this->Flash->success(__('The tipoentrada has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The tipoentrada could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout = 'gentella';
		if (!$this->Tipoentrada->exists($id)) {
			throw new NotFoundException(__('Invalid tipoentrada'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Tipoentrada->save($this->request->data)) {
				$this->Flash->success(__('The tipoentrada has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The tipoentrada could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Tipoentrada.' . $this->Tipoentrada->primaryKey => $id));
			$this->request->data = $this->Tipoentrada->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->layout = 'gentella';
		$this->Tipoentrada->id = $id;
		if (!$this->Tipoentrada->exists()) {
			throw new NotFoundException(__('Invalid tipoentrada'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Tipoentrada->delete()) {
			$this->Flash->success(__('The tipoentrada has been deleted.'));
		} else {
			$this->Flash->error(__('The tipoentrada could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
