<?php class BalanceController extends AppController {
    public $helpers = array('Html', 'Form');
    public $name = 'Dashboards';

    public $uses = array('Dashboards','Cita','Nota','Statu','Rootnota');

    public $components = array('Flash','Session');

    public $layout = 'gentella';


     public function beforeFilter() {
        $this->checkSession(13);      
    } 

    public function balance(){
        $this->set('rootnotas', $this->Rootnota->find('all'));
        
    }




    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
        public function rootview($id = null) {
            if (!$this->Rootnota->exists($id)) {
                throw new NotFoundException(__('Invalid rootnota'));
            }
            $options = array('conditions' => array('Rootnota.' . $this->Rootnota->primaryKey => $id));
            $this->set('rootnota', $this->Rootnota->find('first', $options));
        }

        /**
         * add method
         *
         * @return void
         */
            public function rootadd() {
                if ($this->request->is('post')) {
                    $this->Rootnota->create();
                    if ($this->Rootnota->save($this->request->data)) {
                        
                        $this->Flash->success(__('La nota fue agregada con exito.'));
                        return $this->redirect(array('action' => 'balance'));
                    } else {
                        $this->Flash->error(__('La nota no pudo ser agregada, intente de nuevo.'));
                    }
                }
                $status = $this->Rootnota->Statu->find('list');
                $this->set(compact('status'));
            }

        /**
         * edit method
         *
         * @throws NotFoundException
         * @param string $id
         * @return void
         */
            public function rootedit($id = null) {
                if (!$this->Rootnota->exists($id)) {
                    throw new NotFoundException(__('Invalid rootnota'));
                }
                if ($this->request->is(array('post', 'put'))) {
                    if ($this->Rootnota->save($this->request->data)) {
                        $this->Flash->success(__('La nota fue editada con exito.'));
                        return $this->redirect(array('action' => 'balance'));
                    } else {
                        $this->Flash->error(__('La nota no se pudo editar, intente de nuevo.'));
                    }
                } else {
                    $options = array('conditions' => array('Rootnota.' . $this->Rootnota->primaryKey => $id));
                    $this->request->data = $this->Rootnota->find('first', $options);
                }
                $status = $this->Rootnota->Statu->find('list');
                $this->set(compact('status'));
            }

        /**
         * delete method
         *
         * @throws NotFoundException
         * @param string $id
         * @return void
         */
            public function rootdelete($id = null) {
                $this->Rootnota->id = $id;
                if (!$this->Rootnota->exists()) {
                    throw new NotFoundException(__('Invalid rootnota'));
                }
                $this->request->allowMethod('post', 'delete');
                if ($this->Rootnota->delete()) {
                    $this->Flash->success(__('La nota fue eliminada.'));
                } else {
                    $this->Flash->error(__('La nota no pudo ser eliminada, intente de nuevo.'));
                }
                return $this->redirect(array('action' => 'balance'));
            }
    //=============================================[ function index  ] start!
    public function index() {       
         
         $this->layout = 'gentella';
          $this->set('citas', $this->Cita->find('all'));
         $this->Session->write('DASHBOARDS', $this->Dashboards->find('all'));
         $this->set('dashboards', $this->Dashboards->find('all'));
          $this->set('notas', $this->Nota->find('all'));

    }

    public function configuration() {       
         
         $this->layout = 'gentella';
         
         $this->Session->write('DASHBOARDS', $this->Dashboards->find('all'));
         $this->set('dashboards', $this->Dashboards->find('all'));

    }

    public function calendario(){
         $this->layout = 'gentella';
         $this->set('citas', $this->Cita->find('all'));
    }


    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
        public function view($id = null) {
            if (!$this->Nota->exists($id)) {
                throw new NotFoundException(__('Invalid nota'));
            }
            $options = array('conditions' => array('Nota.' . $this->Nota->primaryKey => $id));
            $this->set('nota', $this->Nota->find('first', $options));
        }

    /**
     * add method
     *
     * @return void
     */
        public function add() {
            if ($this->request->is('post')) {
                $this->Nota->create();
                if ($this->Nota->save($this->request->data)) {
                    
                    $this->Flash->success(__('The nota has been saved.'));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Flash->error(__('The nota could not be saved. Please, try again.'));
                }
            }
            $status = $this->Nota->Status->find('list');
            $this->set(compact('status'));
        }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
        public function edit($id = null) {
            if (!$this->Nota->exists($id)) {
                throw new NotFoundException(__('Invalid nota'));
            }
            if ($this->request->is(array('post', 'put'))) {
                if ($this->Nota->save($this->request->data)) {
                    $this->Flash->success(__('The nota has been saved.'));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Flash->error(__('The nota could not be saved. Please, try again.'));
                }
            } else {
                $options = array('conditions' => array('Nota.' . $this->Nota->primaryKey => $id));
                $this->request->data = $this->Nota->find('first', $options);
            }
            $status = $this->Nota->Status->find('list');
            $this->set(compact('status'));
        }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
        public function delete($id = null) {
            $this->Nota->id = $id;
            if (!$this->Nota->exists()) {
                throw new NotFoundException(__('Invalid nota'));
            }
            $this->request->allowMethod('post', 'delete');
            if ($this->Nota->delete()) {
                $this->Flash->success(__('The nota has been deleted.'));
            } else {
                $this->Flash->error(__('The nota could not be deleted. Please, try again.'));
            }
            return $this->redirect(array('action' => 'index'));
        }

        public function npermisos(){
            
        }

}
?>