<?php
App::uses('AppController', 'Controller');
/**
 * Avisoprivas Controller
 *
 * @property Avisopriva $Avisopriva
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class AvisoprivasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

	var $layout = "gentella";

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(37);		
}


/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Avisopriva->recursive = 0;
		//$this->set('avisoprivas', $this->Paginator->paginate());
		$this->set('avisoprivas', $this->Avisopriva->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Avisopriva->exists($id)) {
			throw new NotFoundException(__('Invalid avisopriva'));
		}
		$options = array('conditions' => array('Avisopriva.' . $this->Avisopriva->primaryKey => $id));
		$this->set('avisopriva', $this->Avisopriva->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$name = 'upload/lotes_'.date("Y-m-d-h-i-s").'_'.$this->request->data["Avisopriva"]["contenido"]["name"];
			$this->request->data["Avisopriva"]["nombre_archivo"] = $name;
			$this->Avisopriva->create();
			if ($this->Avisopriva->save($this->request->data)) {
				if (move_uploaded_file($this->request->data["Avisopriva"]["contenido"]["tmp_name"], $name)){
					chmod($name,0777);
			        $this->Flash->success(__('The avisopriva has been saved.'));
					return $this->redirect(array('action' => 'index'));
			    }else{
			        $this->Flash->error(__('The avisopriva could not be saved. Please, try again.'));
			    } 
			} else {
				$this->Flash->error(__('The avisopriva could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Avisopriva->exists($id)) {
			throw new NotFoundException(__('Invalid avisopriva'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$name = 'upload/lotes_'.date("Y-m-d-h-i-s").'_'.$this->request->data["Avisopriva"]["contenido"]["name"];
			$this->request->data["Avisopriva"]["nombre_archivo"] = $name;
			if ($this->Avisopriva->save($this->request->data)) {
                if (move_uploaded_file($this->request->data["Avisopriva"]["contenido"]["tmp_name"], $name)){
					chmod($name,0777);
			        $this->Flash->success(__('The avisopriva has been saved.'));
					return $this->redirect(array('action' => 'index'));
			    }else{
			        $this->Flash->error(__('The avisopriva could not be saved. Please, try again.'));
			    } 
			} else {
				$this->Flash->error(__('The avisopriva could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Avisopriva.' . $this->Avisopriva->primaryKey => $id));
			$this->request->data = $this->Avisopriva->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Avisopriva->id = $id;
		if (!$this->Avisopriva->exists()) {
			throw new NotFoundException(__('Invalid avisopriva'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Avisopriva->delete()) {
			$this->Flash->success(__('The avisopriva has been deleted.'));
		} else {
			$this->Flash->error(__('The avisopriva could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
