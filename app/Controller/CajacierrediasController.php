<?php
App::uses('AppController', 'Controller');
/**
 * Cajacierredias Controller
 *
 * @property Cajacierredia $Cajacierredia
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CajacierrediasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

	public $layout = "gentella";

	var $uses = array('Ingreso', 
					  'Ingresotipomonto', 
					  'Ingresotipo', 
					  'Ingresoiva', 
					  'Ingresodetalle',
					  'Ingresofolio',
					  'Caja',
					  'Cajacierredia',
					  'Sucursale');

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(65);		
} 



public function planilla($id_sucursal, $id_caja, $fecha){
	$this->layout = 'pdf';
	App::import('Vendor', 'Fpdf', array('file' => 'fpdf181/fpdf.php'));

	$this->Ingreso->recursive = 3;
	$fecha1 = $fecha.' 00:00:00';
	$fecha2 = $fecha.' 23:59:59';
	$this->set('data', $this->Ingreso->find('all', array('conditions' => array('Ingreso.sucursale_id ' => $id_sucursal, 'Ingreso.caja_id' => $id_caja, 'Ingreso.created BETWEEN ? AND ? '=>array($fecha1, $fecha2)))));





	$this->set('titulo','Cierre Diario '.$fecha);
	$this->set('sucursal', $this->Sucursale->find('all', array('conditions'=>array('Sucursale.id '=>$id_sucursal))));
	$this->set('info','PDF_'.time().'_'.$fecha.'.pdf');
	
}


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->set('cajacierredias', $this->Cajacierredia->find('all'));
	}

	public function montodia($sucursal, $caja){
		$this->layout = 'ajax';
		$fecha1 = date('Y-m-d').' 00:00:00';
		$fecha2 = date('Y-m-d').' 23:59:59';
		$this->set('monto',$this->Ingreso->find('all',array('conditions'=>array('Ingreso.sucursale_id '=>$sucursal, 'Ingreso.caja_id '=>$caja,'Ingreso.created  BETWEEN ? AND ? '=>array($fecha1, $fecha2)))));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Cajacierredia->exists($id)) {
			throw new NotFoundException(__('Invalid cajacierredia'));
		}
		$options = array('conditions' => array('Cajacierredia.' . $this->Cajacierredia->primaryKey => $id));
		$this->set('cajacierredia', $this->Cajacierredia->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Cajacierredia->create();
			if ($this->Cajacierredia->save($this->request->data)) {
				
				$this->Flash->success(__('The cajacierredia has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The cajacierredia could not be saved. Please, try again.'));
			}
		}
		$cajas      = $this->Cajacierredia->Caja->find('list');
		$users      = $this->Cajacierredia->User->find('list');
		$sucursales = $this->Cajacierredia->Sucursale->find('list');
		$this->set(compact('cajas', 'users', 'sucursales'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Cajacierredia->exists($id)) {
			throw new NotFoundException(__('Invalid cajacierredia'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Cajacierredia->save($this->request->data)) {
				$this->Flash->success(__('The cajacierredia has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The cajacierredia could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Cajacierredia.' . $this->Cajacierredia->primaryKey => $id));
			$this->request->data = $this->Cajacierredia->find('first', $options);
		}
		$cajas      = $this->Cajaapertura->Caja->find('list', array('conditions'=>array('Caja.sucursale_id'=>$this->request->data['Cajacierredia']['sucursale_id'], 'Caja.id'=>$this->request->data['Cajacierredia']['caja_id'])));
		$users      = $this->Cajacierredia->User->find('list', array('conditions'=>array('User.id'=>$this->request->data['Cajacierredia']['user_id'])));
		$sucursales = $this->Cajacierredia->Sucursale->find('list', array('conditions'=>array('Sucursale.id'=>$this->request->data['Cajacierredia']['sucursale_id'])));
		$this->set(compact('cajas', 'users', 'sucursales'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Cajacierredia->id = $id;
		if (!$this->Cajacierredia->exists()) {
			throw new NotFoundException(__('Invalid cajacierredia'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Cajacierredia->delete()) {
			$this->Flash->success(__('The cajacierredia has been deleted.'));
		} else {
			$this->Flash->error(__('The cajacierredia could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}


/**
 * cajas method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function cajas($var1=null){
		$this->layout = "ajax";
		$cajas = $this->Cajacierredia->Caja->find('list', array('conditions'=>array('Caja.sucursale_id'=>$var1)));
        $this->set('cajas', $cajas);
	}
}
