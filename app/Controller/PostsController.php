<?php class PostsController extends AppController {
    public $helpers = array('Html', 'Form');
    public $name = 'Posts';

    public $uses = array('Post','DataWebSite');

    public $components = array('Flash','Session');


    //=============================================[ function index  ] start!
    public function index() {
         $this->set('posts', $this->Post->find('all'));
         $this->Session->write('DATA_WEB_SITE', $this->DataWebSite->find('all'));
         $this->set('data_web_site', $this->DataWebSite->find('all'));

    }


    //=============================================[ function view ] start!
    public function view($id = null) {
        $this->set('post', $this->Post->findById($id));
    }

    //=============================================[ function add ] start!
    public function add() {
        if ($this->request->is('post')) {
            if ($this->Post->save($this->request->data)) {
                $this->Flash->success(__('Your post has been saved.'));
                $this->redirect(array('action' => 'index'));
            }
        }
    }
 

    //=============================================[ edit ] start!
    public function edit($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }

        $post = $this->Post->findById($id);
        if (!$post) {
            throw new NotFoundException(__('Invalid post'));
        }

        if ($this->request->is(array('post', 'put'))) {
            $this->Post->id = $id;
            if ($this->Post->save($this->request->data)) {
                $this->Flash->success(__('Your post has been updated.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Unable to update your post.'));
        }

        if (!$this->request->data) {
            $this->request->data = $post;
        }
    }

    //=============================================[ delete ] start!
    function delete($id) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        if ($this->Post->delete($id)) {
            $this->Flash->success('The post with id: ' . $id . ' has been deleted.');
            $this->redirect(array('action' => 'index'));
        }
    }

}
?>