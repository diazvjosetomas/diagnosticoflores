<?php
App::uses('AppController', 'Controller');
/**
 * Laborables Controller
 *
 * @property Laborable $Laborable
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class LaborablesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(39);		
}


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = 'gentella';
		$this->Laborable->recursive = 0;
		$this->set('laborables', $this->Paginator->paginate());
		$this->set('laborables', $this->Laborable->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->layout = 'gentella';
		if (!$this->Laborable->exists($id)) {
			throw new NotFoundException(__('Invalid laborable'));
		}
		$options = array('conditions' => array('Laborable.' . $this->Laborable->primaryKey => $id));
		$this->set('laborable', $this->Laborable->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = 'gentella';
		if ($this->request->is('post')) {
			$this->Laborable->create();
			if ($this->Laborable->save($this->request->data)) {
				
				$this->Flash->success(__('The laborable has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The laborable could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout = 'gentella';
		if (!$this->Laborable->exists($id)) {
			throw new NotFoundException(__('Invalid laborable'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Laborable->save($this->request->data)) {
				$this->Flash->success(__('The laborable has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The laborable could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Laborable.' . $this->Laborable->primaryKey => $id));
			$this->request->data = $this->Laborable->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->layout = 'gentella';
		$this->Laborable->id = $id;
		if (!$this->Laborable->exists()) {
			throw new NotFoundException(__('Invalid laborable'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Laborable->delete()) {
			$this->Flash->success(__('The laborable has been deleted.'));
		} else {
			$this->Flash->error(__('The laborable could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
