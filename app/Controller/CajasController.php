<?php
App::uses('AppController', 'Controller');
/**
 * Cajas Controller
 *
 * @property Caja $Caja
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CajasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

	public $layout = "gentella";

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(11);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Caja->recursive = 0;
		//$this->set('cajas', $this->Paginator->paginate());
		  $this->set('cajas', $this->Caja->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Caja->exists($id)) {
			throw new NotFoundException(__('Invalid caja'));
		}
		$options = array('conditions' => array('Caja.' . $this->Caja->primaryKey => $id));
		$this->set('caja', $this->Caja->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Caja->create();
			if ($this->Caja->save($this->request->data)) {
				
				$this->Flash->success(__('The caja has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The caja could not be saved. Please, try again.'));
			}
		}
		$sucursales = $this->Caja->Sucursale->find('list');
		$this->set(compact('sucursales'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Caja->exists($id)) {
			throw new NotFoundException(__('Invalid caja'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Caja->save($this->request->data)) {
				$this->Flash->success(__('The caja has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The caja could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Caja.' . $this->Caja->primaryKey => $id));
			$this->request->data = $this->Caja->find('first', $options);
		}
		$sucursales = $this->Caja->Sucursale->find('list');
		$this->set(compact('sucursales'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Caja->id = $id;
		if (!$this->Caja->exists()) {
			throw new NotFoundException(__('Invalid caja'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Caja->delete()) {
			$this->Flash->success(__('The caja has been deleted.'));
		} else {
			$this->Flash->error(__('The caja could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
