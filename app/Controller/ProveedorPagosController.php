<?php
App::uses('AppController', 'Controller');
/**
 * ProveedorPagos Controller
 *
 * @property ProveedorPago $ProveedorPago
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ProveedorPagosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(28);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = 'gentella';
		//$this->ProveedorPago->recursive = 0;
		//$this->set('proveedorPagos', $this->Paginator->paginate());
		$this->set('proveedorPagos', $this->ProveedorPago->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->layout = 'gentella';
		if (!$this->ProveedorPago->exists($id)) {
			throw new NotFoundException(__('Invalid proveedor pago'));
		}
		$options = array('conditions' => array('ProveedorPago.' . $this->ProveedorPago->primaryKey => $id));
		$this->set('proveedorPago', $this->ProveedorPago->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = 'gentella';
		if ($this->request->is('post')) {
			$this->ProveedorPago->create();
			if ($this->ProveedorPago->save($this->request->data)) {
				
				$this->Flash->success(__('The proveedor pago has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The proveedor pago could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout = 'gentella';
		if (!$this->ProveedorPago->exists($id)) {
			throw new NotFoundException(__('Invalid proveedor pago'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ProveedorPago->save($this->request->data)) {
				$this->Flash->success(__('The proveedor pago has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The proveedor pago could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ProveedorPago.' . $this->ProveedorPago->primaryKey => $id));
			$this->request->data = $this->ProveedorPago->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->layout = 'gentella';
		$this->ProveedorPago->id = $id;
		if (!$this->ProveedorPago->exists()) {
			throw new NotFoundException(__('Invalid proveedor pago'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ProveedorPago->delete()) {
			$this->Flash->success(__('The proveedor pago has been deleted.'));
		} else {
			$this->Flash->error(__('The proveedor pago could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
