<?php
App::uses('AppController', 'Controller');
/**
 * Rootnotas Controller
 *
 * @property Rootnota $Rootnota
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class RootnotasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


/* public function beforeFilter() {
	$this->checkSession();		
} */


/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Rootnota->recursive = 0;
		//$this->set('rootnotas', $this->Paginator->paginate());
		  $this->set('rootnotas', $this->Rootnota->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Rootnota->exists($id)) {
			throw new NotFoundException(__('Invalid rootnota'));
		}
		$options = array('conditions' => array('Rootnota.' . $this->Rootnota->primaryKey => $id));
		$this->set('rootnota', $this->Rootnota->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Rootnota->create();
			if ($this->Rootnota->save($this->request->data)) {
				
				$this->Flash->success(__('The rootnota has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The rootnota could not be saved. Please, try again.'));
			}
		}
		$status = $this->Rootnota->Statu->find('list');
		$this->set(compact('status'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Rootnota->exists($id)) {
			throw new NotFoundException(__('Invalid rootnota'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Rootnota->save($this->request->data)) {
				$this->Flash->success(__('The rootnota has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The rootnota could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Rootnota.' . $this->Rootnota->primaryKey => $id));
			$this->request->data = $this->Rootnota->find('first', $options);
		}
		$status = $this->Rootnota->Statu->find('list');
		$this->set(compact('status'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Rootnota->id = $id;
		if (!$this->Rootnota->exists()) {
			throw new NotFoundException(__('Invalid rootnota'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Rootnota->delete()) {
			$this->Flash->success(__('The rootnota has been deleted.'));
		} else {
			$this->Flash->error(__('The rootnota could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
