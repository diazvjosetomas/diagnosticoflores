<?php
App::uses('AppController', 'Controller');
/**
 * Tipopermisos Controller
 *
 * @property Tipopermiso $Tipopermiso
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class TipopermisosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(24);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = 'gentella';
		//$this->Tipopermiso->recursive = 0;
		//$this->set('tipopermisos', $this->Paginator->paginate());
		$this->set('tipopermisos', $this->Tipopermiso->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->layout = 'gentella';
		if (!$this->Tipopermiso->exists($id)) {
			throw new NotFoundException(__('Invalid tipopermiso'));
		}
		$options = array('conditions' => array('Tipopermiso.' . $this->Tipopermiso->primaryKey => $id));
		$this->set('tipopermiso', $this->Tipopermiso->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = 'gentella';
		if ($this->request->is('post')) {
			$this->Tipopermiso->create();
			if ($this->Tipopermiso->save($this->request->data)) {
				
				$this->Flash->success(__('The tipopermiso has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The tipopermiso could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout = 'gentella';
		if (!$this->Tipopermiso->exists($id)) {
			throw new NotFoundException(__('Invalid tipopermiso'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Tipopermiso->save($this->request->data)) {
				$this->Flash->success(__('The tipopermiso has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The tipopermiso could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Tipopermiso.' . $this->Tipopermiso->primaryKey => $id));
			$this->request->data = $this->Tipopermiso->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->layout = 'gentella';
		$this->Tipopermiso->id = $id;
		if (!$this->Tipopermiso->exists()) {
			throw new NotFoundException(__('Invalid tipopermiso'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Tipopermiso->delete()) {
			$this->Flash->success(__('The tipopermiso has been deleted.'));
		} else {
			$this->Flash->error(__('The tipopermiso could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
