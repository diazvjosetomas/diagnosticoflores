<?php
App::uses('AppController', 'Controller');
/**
 * Contenidos Controller
 *
 * @property Contenido $Contenido
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ContenidosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(4);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = 'gentella';
		//$this->Contenido->recursive = 0;
		//$this->set('contenidos', $this->Paginator->paginate());
		$this->set('contenidos', $this->Contenido->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->layout = 'gentella';
		if (!$this->Contenido->exists($id)) {
			throw new NotFoundException(__('Invalid contenido'));
		}
		$options = array('conditions' => array('Contenido.' . $this->Contenido->primaryKey => $id));
		$this->set('contenido', $this->Contenido->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = 'gentella';
		if ($this->request->is('post')) {
			$this->Contenido->create();
			if ($this->Contenido->save($this->request->data)) {
				
				$this->Flash->success(__('The contenido has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The contenido could not be saved. Please, try again.'));
			}
		}
		$categorias = $this->Contenido->Categoria->find('list');
		$this->set(compact('categorias'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout = 'gentella';
		if (!$this->Contenido->exists($id)) {
			throw new NotFoundException(__('Invalid contenido'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Contenido->save($this->request->data)) {
				$this->Flash->success(__('The contenido has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The contenido could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Contenido.' . $this->Contenido->primaryKey => $id));
			$this->request->data = $this->Contenido->find('first', $options);
		}
		$categorias = $this->Contenido->Categoria->find('list');
		$this->set(compact('categorias'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->layout = 'gentella';
		$this->Contenido->id = $id;
		if (!$this->Contenido->exists()) {
			throw new NotFoundException(__('Invalid contenido'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Contenido->delete()) {
			$this->Flash->success(__('The contenido has been deleted.'));
		} else {
			$this->Flash->error(__('The contenido could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}


	
}
