<?php
App::uses('AppController', 'Controller');
/**
 * Imgadjuntos Controller
 *
 * @property Imgadjunto $Imgadjunto
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ImgadjuntosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');
	public $layout = 'gentella';
	public $uses   = array('Adjunto', 'Imgadjunto');
/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(72);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Imgadjunto->recursive = 0;
		//$this->set('imgadjuntos', $this->Paginator->paginate());
		  $this->set('imgadjuntos', $this->Imgadjunto->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Imgadjunto->exists($id)) {
			throw new NotFoundException(__('Invalid imgadjunto'));
		}
		$options = array('conditions' => array('Imgadjunto.' . $this->Imgadjunto->primaryKey => $id));
		$this->set('imgadjunto', $this->Imgadjunto->find('first', $options));
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
		public function view2($id = null) {
			$this->set('datadjuntos', $this->Imgadjunto->find('all', array('conditions'=>array('adjunto_id'=>$id))));
		}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Imgadjunto->create();

			if (!empty($this->request->data['Imgadjunto']['imagen']) && imagenType($this->request->data['Imgadjunto']['imagen']['type']) != false) {
			               $ds = '/';
			               $storeFolder = 'upload';
			               $tempFile    = $this->request->data['Imgadjunto']['imagen']['tmp_name'];
			               $targetPath  = ROOT . $ds .'app'. $ds .'webroot'. $ds .'img'. $ds . $storeFolder . $ds;
			               $extencion   = imagenType($this->request->data['Imgadjunto']['imagen']['type']);
			               $renameFile  = 'a_'.date('dmY_his').$extencion;
			               $targetFile  = $targetPath.$renameFile;
			               if(move_uploaded_file($tempFile,$targetFile)){
			                   $tipo_imagen = $this->request->data['Imgadjunto']['imagen']['type'];
			                   $nombre_imagen = $renameFile;;
			                   $this->request->data['Imgadjunto']['carpeta_imagen'] = $storeFolder;
			                   $this->request->data['Imgadjunto']['nombre_imagen']  = $nombre_imagen;
			                   $this->request->data['Imgadjunto']['tipo_imagen']    = $tipo_imagen;
			                   $this->request->data['Imgadjunto']['ruta_imagen']    = $storeFolder.'/'.$nombre_imagen;
			               }else{
			                   $this->request->data['Imgadjunto']['carpeta_imagen']  = "_";
			                   $this->request->data['Imgadjunto']['nombre_imagen']   = "_";
			                   $this->request->data['Imgadjunto']['tipo_imagen']     = "_";
			                   $this->request->data['Imgadjunto']['ruta_imagen'] = "_";
			               }

			           }else{
			                   $this->request->data['Imgadjunto']['carpeta_imagen']  = "_";
			                   $this->request->data['Imgadjunto']['nombre_imagen']   = "_";
			                   $this->request->data['Imgadjunto']['tipo_imagen']     = "_";
			                   $this->request->data['Imgadjunto']['ruta_imagen'] = "_";
			           }









			if ($this->Imgadjunto->save($this->request->data)) {
				
				$this->Flash->success(__('The imgadjunto has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The imgadjunto could not be saved. Please, try again.'));
			}
		}
		$adjuntos = $this->Imgadjunto->Adjunto->find('list');
		$this->set(compact('adjuntos'));
	}



	/**
	 * add method
	 *
	 * @return void
	 */
		public function add2($adjunto_id = null) {
			if ($this->request->is('post')) {
				$this->Imgadjunto->create();

				if (!empty($this->request->data['Imgadjunto']['imagen']) && imagenType($this->request->data['Imgadjunto']['imagen']['type']) != false) {
				               $ds = '/';
				               $storeFolder = 'upload';
				               $tempFile    = $this->request->data['Imgadjunto']['imagen']['tmp_name'];
				               $targetPath  = ROOT . $ds .'app'. $ds .'webroot'. $ds .'img'. $ds . $storeFolder . $ds;
				               $extencion   = imagenType($this->request->data['Imgadjunto']['imagen']['type']);
				               $renameFile  = 'a_'.date('dmY_his').$extencion;
				               $targetFile  = $targetPath.$renameFile;
				               if(move_uploaded_file($tempFile,$targetFile)){
				                   $tipo_imagen = $this->request->data['Imgadjunto']['imagen']['type'];
				                   $nombre_imagen = $renameFile;;
				                   $this->request->data['Imgadjunto']['carpeta_imagen'] = $storeFolder;
				                   $this->request->data['Imgadjunto']['nombre_imagen']  = $nombre_imagen;
				                   $this->request->data['Imgadjunto']['tipo_imagen']    = $tipo_imagen;
				                   $this->request->data['Imgadjunto']['ruta_imagen']    = $storeFolder.'/'.$nombre_imagen;
				               }else{
				                   $this->request->data['Imgadjunto']['carpeta_imagen']  = "_";
				                   $this->request->data['Imgadjunto']['nombre_imagen']   = "_";
				                   $this->request->data['Imgadjunto']['tipo_imagen']     = "_";
				                   $this->request->data['Imgadjunto']['ruta_imagen'] 	 = "_";
				               }

				           }else{
				                   $this->request->data['Imgadjunto']['carpeta_imagen']  = "_";
				                   $this->request->data['Imgadjunto']['nombre_imagen']   = "_";
				                   $this->request->data['Imgadjunto']['tipo_imagen']     = "_";
				                   $this->request->data['Imgadjunto']['ruta_imagen']     = "_";
				           }









				if ($this->Imgadjunto->save($this->request->data)) {
					
					$this->Flash->success(__('La imagen ha sido adjuntada con éxito.'));
					return $this->redirect(array('action' => 'index','controller'=>'Adjuntos'));
				} else {
					$this->Flash->error(__('La imagen no pudo adjuntarse, intenta de nuevo.'));
				}
			}
			$adjuntos = $this->Adjunto->find('list',array('conditions'=>array('id'=>$adjunto_id)));
			$this->set(compact('adjuntos'));
		}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Imgadjunto->exists($id)) {
			throw new NotFoundException(__('Invalid imgadjunto'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Imgadjunto->save($this->request->data)) {
				$this->Flash->success(__('The imgadjunto has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The imgadjunto could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Imgadjunto.' . $this->Imgadjunto->primaryKey => $id));
			$this->request->data = $this->Imgadjunto->find('first', $options);
		}
		$adjuntos = $this->Imgadjunto->Adjunto->find('list');
		$this->set(compact('adjuntos'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Imgadjunto->id = $id;
		if (!$this->Imgadjunto->exists()) {
			throw new NotFoundException(__('Invalid imgadjunto'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Imgadjunto->delete()) {
			$this->Flash->success(__('La imagen se ha eliminado.'));
		} else {
			$this->Flash->error(__('La imagen no se ha podido eliminar, intenta de nuevo.'));
		}
		return $this->redirect(array('action' => 'index', 'controller'=>'Adjuntos'));
	}
}
