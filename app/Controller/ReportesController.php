<?php
App::uses('AppController', 'Controller');
/**
 * Cajas Controller
 *
 * @property Caja $Caja
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ReportesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $uses = array('User','Cliente','Personale','Ingreso','Ingresodetalle','Ingresotipos','Egreso','Sucursale');

	public $components = array('Paginator', 'Session', 'Flash');

	public $layout = "gentella";

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(12);		
} 


public function ingre_egre_mensuales_pdf($anhio, $id_sucursale){
    $this->layout = 'pdf';
    

    App::import('Vendor', 'Fpdf', array('file' => 'fpdf181/fpdf.php'));

    if ($anhio == '') {
        $anhio = date('Y');
    }

    $this->set('sucursal', $this->Sucursale-> find('all', array('conditions'=>array('Sucursale.id'=>$id_sucursale))));
    $this->set('anhio', $anhio);
    
    $desde = $anhio.'-01-01';
    $hasta = $anhio.'-12-31';
    $this->set('ingresos', $this->Ingreso->find('all',array('conditions'=>array("Ingreso.sucursale_id"=>$id_sucursale, "Ingreso.created BETWEEN '".$desde."' AND '".$hasta."' "))));
    $this->set('egresos', $this->Egreso->find('all',array('conditions'=>array("Egreso.sucursale_id"=>$id_sucursale,"Egreso.fecha BETWEEN '".$desde."' AND '".$hasta."' "))));
}


 public function doctores(){
 
 	$this->set('users', $this->User->find('all' , array('conditions'=>array('User.tipocliente_id !='=>0))));
 }


 public function reportedoctor($id = null){
    $this->Ingreso->recursive = 2;
 	$this->set('ingresos', $this->Ingreso->find('all', array('conditions'=>array('Ingreso.user_id '=>$id))));

 	$detalleingreso = $this->Ingreso->find('all', array('conditions'=>array('Ingreso.user_id '=>$id ) )  );

 	$id = $detalleingreso[0]['Ingreso']['id'];
 	foreach ($detalleingreso as $key) {
 		$sql[] = $key['Ingreso']['id'];
 	}
   
 	$this->set('detalles', $this->Ingresodetalle->find('all', array('conditions'=>array('Ingresodetalle.ingreso_id '=>$sql))));
 	


 }


 public function clientes(){
 	$this->set('clientes', $this->Cliente->find('all'));
 }


 public function reporteclientes($id = null){
 	   $this->Ingreso->recursive = 2;
 		$this->set('ingresos', $this->Ingreso->find('all', array('conditions'=>array('Ingreso.cliente_id '=>$id))));

 		$detalleingreso = $this->Ingreso->find('all', array('conditions'=>array('Ingreso.cliente_id '=>$id ) )  );

 		$id = $detalleingreso[0]['Ingreso']['id'];
 		foreach ($detalleingreso as $key) {
 			$sql[] = $key['Ingreso']['id'];
 		}
 	  
 		$this->set('detalles', $this->Ingresodetalle->find('all', array('conditions'=>array('Ingresodetalle.ingreso_id '=>$sql))));
 }


 public function empleados(){
 	$this->set('personales', $this->Personale->find('all'));
 }

 public function reporteempleados($id = null){
 	   $this->Ingreso->recursive = 2;
 		$this->set('ingresos', $this->Ingreso->find('all', array('conditions'=>array('Ingreso.personale_id '=>$id))));

 		$detalleingreso = $this->Ingreso->find('all', array('conditions'=>array('Ingreso.personale_id '=>$id ) )  );

 		$id = $detalleingreso[0]['Ingreso']['id'];
 		foreach ($detalleingreso as $key) {
 			$sql[] = $key['Ingreso']['id'];
 		}
 	  
 		$this->set('detalles', $this->Ingresodetalle->find('all', array('conditions'=>array('Ingresodetalle.ingreso_id '=>$sql))));

 }

 public function listados(){
 	$this->Ingreso->recursive = 2;
 	$this->set('user',  $this->Ingreso->query('SELECT DISTINCT(user_id) FROM ingresos'));

 

 ///	$this->set('detalles', $this->Ingresos->find('all', array('conditions'=> array('Ingreso.user_id' => $sql))));

 	
 }

  public function listadosajax(){
  	$this->layout = 'ajax';
 	$this->Ingreso->recursive = 2;
 	$this->set('user',  $this->Ingreso->query('SELECT DISTINCT(user_id) FROM ingresos'));

 

 ///	$this->set('detalles', $this->Ingresos->find('all', array('conditions'=> array('Ingreso.user_id' => $sql))));

 	
 }

 public function cantidadremitidos($id = null){
 	$this->layout = 'ajax';
 	$this->set('cantidad', $this->Ingreso->query('SELECT COUNT(*) AS cantidad FROM ingresos WHERE  user_id = '.$id));


 	$this->set('doctor', $this->User->find('all', array('conditions'=>array('User.id '=> $id))));
 }

 public function rangocantidadremitidos($id = null, $desde = null, $hasta = null){
 	$this->layout = 'ajax';

 	$desde = $desde.' 00:00:00';
 	$hasta = $hasta.' 00:00:00';

 	$this->set('cantidad', $this->Ingreso->query('SELECT COUNT(*) AS cantidad FROM ingresos WHERE user_id = "'.$id.'" AND created BETWEEN "'.$desde.'" and "'.$hasta.'"'));


 	$this->set('doctor', $this->User->find('all', array('conditions'=>array('User.id '=> $id))));

 }


 public function rangoservicios($desde = null, $hasta = null){
 	$this->layout = 'ajax';
 	$this->Ingreso->recursive = 2;
    //$this->Ingresotipo->recursive = 3;
 	
 	$this->set('servicios', $this->Ingresotipos->find('all'));
    $this->set('sucursales', $this->Sucursale->find('all'));

 	if ( ($desde == 'n') || ($hasta == 'n')) {
 		$this->set('ingresos', $this->Ingreso->find('all'));
 		
 	}else{
 		$desde = '2015-01-01 00:00:00';
 		$hasta = $hasta.' 00:00:00';
 		$this->set('ingresos', $this->Ingreso->find('all',array('conditions'=>array("Ingreso.created BETWEEN '".$desde."' AND '".$hasta."' "))));
 		
 	}
 

 }

  public function rangoempleados($desde = null, $hasta = null){
 	$this->layout = 'ajax';
 	$this->Ingreso->recursive = 2;
    
 	$desde = $desde.' 00:00:00';
 	$hasta = $hasta.' 00:00:00';
 	$this->set('personales', $this->Personale->find('all'));
    $this->set('sucursales', $this->Sucursale->find('all'));

 	if ( ($desde == '') || ($hasta == '')) {
 		$this->set('ingresos', $this->Ingreso->find('all'));
 		
 	}else{
 		$desde = '2015-01-01 00:00:00';
 		$hasta = $hasta.' 00:00:00';
 		$this->set('ingresos', $this->Ingreso->find('all',array('conditions'=>array("Ingreso.created BETWEEN '".$desde."' AND '".$hasta."' "))));
 		
 	}

 }

  public function rangoingresos($desde = null, $hasta = null){
    $this->layout = 'ajax';
    $this->Ingreso->recursive = 2;
    //$this->Ingresotipo->recursive = 3;
    
    $this->set('servicios', $this->Ingresotipos->find('all'));
    $this->set('sucursales', $this->Sucursale->find('all'));

    if ( ($desde == '') || ($hasta == '')) {
        $this->set('ingresos', $this->Ingreso->find('all'));
        
    }else{
        $desde = '2015-01-01 00:00:00';
        $hasta = $hasta.' 00:00:00';
        $this->set('ingresos', $this->Ingreso->find('all',array('conditions'=>array("Ingreso.created BETWEEN '".$desde."' AND '".$hasta."' "))));
        
    }

 }




 function ingre_egre_mensuales($anhio, $id_sucursale){
    $this->layout = 'ajax';

    if ($anhio == '') {
        $anhio = date('Y');
    }
    
    $desde = $anhio.'-01-01';
    $hasta = $anhio.'-12-31';
    $this->set('ingresos', $this->Ingreso->find('all',array('conditions'=>array("Ingreso.sucursale_id"=>$id_sucursale, "Ingreso.created BETWEEN '".$desde."' AND '".$hasta."' "))));
    $this->set('egresos', $this->Egreso->find('all',array('conditions'=>array("Egreso.sucursale_id"=>$id_sucursale,"Egreso.fecha BETWEEN '".$desde."' AND '".$hasta."' "))));
 }




 function ingre_egre_anuales($anhio){
    $this->layout = 'ajax';

    if ($anhio == '') {
        $anhio = date('Y');
    }
    $inicio_anhio = $anhio;
    $desde = $anhio.'-01-01';
    $anhio = $anhio + 5;
    $hasta = $anhio.'-12-31';
    $this->set('inicio_anhio', $inicio_anhio);
    $this->set('ingresos', $this->Ingreso->find('all',array('conditions'=>array( "Ingreso.created BETWEEN '".$desde."' AND '".$hasta."' "))));
    $this->set('egresos', $this->Egreso->find('all',array('conditions'=>array( "Egreso.fecha BETWEEN '".$desde."' AND '".$hasta."' "))));
 }

}
