<?php
App::uses('AppController', 'Controller');
/**
 * Permisos Controller
 *
 * @property Permiso $Permiso
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class PermisosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(58);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = 'gentella';
		//$this->Permiso->recursive = 0;
		//$this->set('permisos', $this->Paginator->paginate());
		$this->set('permisos', $this->Permiso->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->layout = 'gentella';
		if (!$this->Permiso->exists($id)) {
			throw new NotFoundException(__('Invalid permiso'));
		}
		$options = array('conditions' => array('Permiso.' . $this->Permiso->primaryKey => $id));
		$this->set('permiso', $this->Permiso->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = 'gentella';
		if ($this->request->is('post')) {
			$name = 'upload/lotes_'.date("Y-m-d-h-i-s").'_'.$this->request->data["Permiso"]["contenido"]["name"];
			$this->request->data["Permiso"]["nombre_archivo"] = $name;
			$this->Permiso->create();
			if ($this->Permiso->save($this->request->data)) {
				if (move_uploaded_file($this->request->data["Permiso"]["contenido"]["tmp_name"], $name)){
					chmod($name,0777);
			        $this->Flash->success(__('El permiso fue guardado con éxito.'));
					return $this->redirect(array('action' => 'index'));
			    }else{
			        $this->Flash->success(__('El permiso fue agregado con éxito, el archivo no fue cargado.'));
			        return $this->redirect(array('action' => 'index'));
			    } 
			} else {
				$this->Flash->error(__('El permiso no fue agregado con éxito, intente de nuevo.'));
			}
		}
		$tipopermisos = $this->Permiso->Tipopermiso->find('list');
		$personales = $this->Permiso->Personale->find('list');
		$this->set(compact('tipopermisos', 'personales'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout = 'gentella';
		if (!$this->Permiso->exists($id)) {
			throw new NotFoundException(__('Invalid permiso'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$name = 'upload/lotes_'.date("Y-m-d-h-i-s").'_'.$this->request->data["Permiso"]["contenido"]["name"];
			$this->request->data["Permiso"]["nombre_archivo"] = $name;
			if ($this->Permiso->save($this->request->data)) {
				if (move_uploaded_file($this->request->data["Permiso"]["contenido"]["tmp_name"], $name)){
					chmod($name,0777);
			        $this->Flash->success(__('The permiso has been saved.'));
					return $this->redirect(array('action' => 'index'));
			    }else{
			        $this->Flash->error(__('The permiso could not be saved. Please, try again.'));
			    } 
			} else {
				$this->Flash->error(__('The permiso could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Permiso.' . $this->Permiso->primaryKey => $id));
			$this->request->data = $this->Permiso->find('first', $options);
		}
		$tipopermisos = $this->Permiso->Tipopermiso->find('list');
		$personales = $this->Permiso->Personale->find('list');
		$this->set(compact('tipopermisos', 'personales'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->layout = 'gentella';
		$this->Permiso->id = $id;
		if (!$this->Permiso->exists()) {
			throw new NotFoundException(__('Invalid permiso'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Permiso->delete()) {
			$this->Flash->success(__('The permiso has been deleted.'));
		} else {
			$this->Flash->error(__('The permiso could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
