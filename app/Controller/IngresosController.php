<?php
App::uses('AppController', 'Controller');
/**
 * Ingresos Controller
 *
 * @property Ingreso $Ingreso
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class IngresosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

	var $uses = array('Ingreso','Empresa', 'Ingresotipomonto', 'Ingresotipo', 'Ingresoiva', 'Ingresodetalle','Ingresofolio','Caja');

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


var $layout = "gentella";

 public function beforeFilter() {
	$this->checkSession(9);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Ingreso->recursive = 0;
		//$this->set('ingresos', $this->Paginator->paginate());
		  $this->set('ingresos', $this->Ingreso->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Ingreso->recursive = 3;
		if (!$this->Ingreso->exists($id)) {
			throw new NotFoundException(__('Invalid ingreso'));
		}
		$options = array('conditions' => array('Ingreso.' . $this->Ingreso->primaryKey => $id));
		$this->set('id', $id);
		$this->set('ingreso', $this->Ingreso->find('first', $options));
	}


/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function imprimir($id = null) {
		$this->layout= 'pdf';
		App::import('Vendor', 'Fpdf', array('file' => 'fpdf181/fpdf.php'));
		$this->Ingreso->recursive = 4;
		if (!$this->Ingreso->exists($id)) {
			throw new NotFoundException(__('Invalid ingreso'));
		}


		$this->set('datoempresas', $this->Empresa->find('all'));


		$this->Ingresodetalle->recursive = 2;
		$this->set('ingresodetalles', $this->Ingresodetalle->find('all', array('conditions'=>array('Ingresodetalle.ingreso_id'=>$id))));
		



		$options = array('conditions' => array('Ingreso.' . $this->Ingreso->primaryKey => $id));
		$this->set('ingreso', $this->Ingreso->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Ingreso->create();
			if ($this->Ingreso->save($this->request->data)) {
				$id   =  $this->Ingreso->id;
				$stop = 0;
				if(!empty($this->request->data["Ingreso"]["Productos"])){
					foreach($this->request->data["Ingreso"]["Productos"] as $producto){
						$this->request->data["Ingresodetalle"]["ingreso_id"]         = $id;
						$this->request->data["Ingresodetalle"]["ingresotipo_id"]     = $producto['pro'];
						$this->request->data["Ingresodetalle"]["ingresotipopago_id"] = $this->request->data["Ingreso"]["ingresotipopago_id"];
						$this->request->data["Ingresodetalle"]["cantidad"]           = $producto['cant'];
						$this->request->data["Ingresodetalle"]["monto"]              = $producto['pre'];
						$this->request->data["Ingresodetalle"]["descuento"]          = $producto['des'];
						$this->request->data["Ingresodetalle"]["total"]              = $producto['total'];
                        if(!empty($producto['exento'])){
                        	$this->request->data["Ingresodetalle"]["exento"]              = 2;
                        }
						$this->Ingreso->Ingresodetalle->create();
						if ($this->Ingreso->Ingresodetalle->save($this->request->data)){

						}else{
							$stop = 1;
						}
					}
				}
				if($stop==0){
                    $this->Ingreso->commit();
                	$this->Flash->success(__('Registro Guardado.'));
					return $this->redirect(array('action' => 'index'));

                }else{
                	$this->Ingreso->rollback();
                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
                }
			} else {
				$this->Flash->error(__('The ingreso could not be saved. Please, try again.'));
			}
		}
		$users            = $this->Ingreso->User->find('list', array('conditions'=>array('User.tipocliente_id !='=>0)));
		$personales       = $this->Ingreso->Personale->find('list');
		$clientes         = $this->Ingreso->Cliente->find('list');
		$sucursales       = $this->Ingreso->Sucursale->find('list');
		$ingresotipopagos = $this->Ingreso->Ingresotipopago->find('list');
		$ingresotipos     = $this->Ingresotipo->find('all');
		$ingresoivas      = $this->Ingresoiva->find('all');
		$this->set('iva', $ingresoivas[0]['Ingresoiva']['monto']);
		$this->set(compact('users', 'personales', 'clientes', 'ingresotipopagos', 'ingresotipos', 'sucursales'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Ingreso->exists($id)) {
			throw new NotFoundException(__('Invalid ingreso'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Ingreso->save($this->request->data)) {
				$this->Ingresodetalle->deleteAll(array('Ingresodetalle.ingreso_id'=>$id));
				$stop = 0;
				if(!empty($this->request->data["Ingreso"]["Productos"])){
					foreach($this->request->data["Ingreso"]["Productos"] as $producto){
						$this->request->data["Ingresodetalle"]["ingreso_id"]         = $id;
						$this->request->data["Ingresodetalle"]["ingresotipo_id"]     = $producto['pro'];
						$this->request->data["Ingresodetalle"]["ingresotipopago_id"] = $this->request->data["Ingreso"]["ingresotipopago_id"];
						$this->request->data["Ingresodetalle"]["cantidad"]           = $producto['cant'];
						$this->request->data["Ingresodetalle"]["monto"]              = $producto['pre'];
						$this->request->data["Ingresodetalle"]["descuento"]          = $producto['des'];
						$this->request->data["Ingresodetalle"]["total"]              = $producto['total'];
						if(!empty($producto['exento'])){
                        	$this->request->data["Ingresodetalle"]["exento"]              = 2;
                        }
						$this->Ingreso->Ingresodetalle->create();
						if ($this->Ingreso->Ingresodetalle->save($this->request->data)){

						}else{
							$stop = 1;
						}
					}
				}
				if($stop==0){
                    $this->Ingreso->commit();
                	$this->Flash->success(__('Registro Guardado.'));
					return $this->redirect(array('action' => 'index'));

                }else{
                	$this->Ingreso->rollback();
                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
                }
			} else {
				$this->Flash->error(__('The ingreso could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Ingreso.' . $this->Ingreso->primaryKey => $id));
			$this->request->data = $this->Ingreso->find('first', $options);
		}
		$users            = $this->Ingreso->User->find('list', array('conditions'=>array('User.tipocliente_id !='=>0)));
		$personales       = $this->Ingreso->Personale->find('list');
		$clientes         = $this->Ingreso->Cliente->find('list');
		$ingresotipopagos = $this->Ingreso->Ingresotipopago->find('list');
		$ingresotipos     = $this->Ingresotipo->find('all');
		$sucursales       = $this->Ingreso->Sucursale->find('list');
		$ingresodetalles  = $this->Ingreso->Ingresodetalle->find('all', array('conditions'=>array('Ingresodetalle.ingreso_id'=>$id)));
		$this->set(compact('sucursales', 'users', 'personales', 'clientes', 'ingresotipopagos', 'ingresotipos', 'ingresodetalles'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Ingreso->id = $id;
		if (!$this->Ingreso->exists()) {
			throw new NotFoundException(__('Invalid ingreso'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Ingreso->delete()) {
			$this->Flash->success(__('The ingreso has been deleted.'));
		} else {
			$this->Flash->error(__('The ingreso could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}



/**
 * precio_p method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function precio($var1=null, $var2=null){
		$this->layout = "ajax";
		$a = $this->request->data['var1'];
		$b = $this->request->data['var2'];
		$servicio = $this->Ingresotipomonto->find('all', array('conditions'=>array('Ingresotipomonto.ingresotipo_id'=>$a, 'Ingresotipomonto.ingresotipopago_id'=>$b)));
		$this->set('precio', isset($servicio[0]['Ingresotipomonto']['monto'])?$servicio[0]['Ingresotipomonto']['monto']:0);
	}



/**
 * folios method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function folios($var1=null){
		$this->layout = "ajax";
		$ingresofolios = $this->Ingresofolio->find('all', array('conditions'=>array('Ingresofolio.sucursale_id'=>$var1)));
		if(isset($ingresofolios[0]['Ingresofolio']['ultimo'])){
	        $value         = $ingresofolios[0]['Ingresofolio']['ultimo']+1;
	        
	        $value = $ingresofolios[0]['Ingresofolio']['secuencia'].mascara($value,8);
    	}else{
    		$value = 0;
    	}

    	$cajas = $this->Caja->find('list', array('conditions'=>array('sucursale_id ' => $var1)));
		$this->set(compact('cajas'));
		$this->set('value', $value);
	}



/**
 * pagos method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function pagos($var1=null){
		$this->layout = "ajax";
		$ingresotipos     = $this->Ingresotipo->find('all');
		$this->set('ingresotipos', $ingresotipos);
		
	}


	public function sender($email, $asunto, $id){
		$this->layout = "ajax";
		$this->set('email', $email);
		$this->set('asunto', $asunto);
	

		$this->Ingreso->recursive = 2;
		if (!$this->Ingreso->exists($id)) {
			throw new NotFoundException(__('Invalid ingreso'));
		}
		$options = array('conditions' => array('Ingreso.' . $this->Ingreso->primaryKey => $id));
		$this->set('id', $id);
		$this->set('ingreso', $this->Ingreso->find('first', $options));
	}





}
