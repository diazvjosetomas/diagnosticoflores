<?php
App::uses('AppController', 'Controller');
/**
 * Pagos Controller
 *
 * @property Pago $Pago
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class PagosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');
	public $uses = array('Egreso','Pago');

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(59);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = 'gentella';
		//$this->Pago->recursive = 0;
		//$this->set('pagos', $this->Paginator->paginate());
		$this->set('pagos', $this->Pago->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->layout = 'gentella';
		if (!$this->Pago->exists($id)) {
			throw new NotFoundException(__('Invalid pago'));
		}
		$options = array('conditions' => array('Pago.' . $this->Pago->primaryKey => $id));
		$this->set('pago', $this->Pago->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = 'gentella';
		if ($this->request->is('post')) {
			$name = 'upload/lotes_'.date("Y-m-d-h-i-s").'_'.$this->request->data["Pago"]["contenido"]["name"];
			$this->request->data["Pago"]["nombre_archivo"] = $name;
			$this->Pago->create();
			if ($this->Pago->save($this->request->data)) {
				if (move_uploaded_file($this->request->data["Pago"]["contenido"]["tmp_name"], $name)){
					chmod($name,0777);


					//Si el pago se realizo lo guardo en egresos
					$this->Egreso->create();
					$this->request->data['Egreso']['egresotipo_id'] = 2;
					$this->request->data['Egreso']['proveedor_pago_id'] = $this->request->data['Pago']['proveedor_pago_id'];
					$this->request->data['Egreso']['fecha'] = $this->request->data['Pago']['fecha_pago'];
					$this->request->data['Egreso']['descripcion'] = 'Sin descripcion';
					$this->request->data['Egreso']['monto'] = $this->request->data['Pago']['monto'];
					$this->request->data['Egreso']['estatus'] = 1;
					$this->Egreso->save($this->request->data);


			        $this->Flash->success(__('El pago fue agregado con éxito.'));
					return $this->redirect(array('action' => 'index'));
			    }else{
			        $this->Flash->success(__('El pago fue salvado, no fue adjuntado ningun archivo.'));
			        return $this->redirect(array('action' => 'index'));
			    } 
			} else {
				$this->Flash->error(__('The pago could not be saved. Please, try again.'));
			}
		}
		$conceptos = $this->Pago->Concepto->find('list');
		$personales = $this->Pago->Personale->find('list');
		$proveedor_pagos = $this->Pago->ProveedorPago->find('list');
		$this->set(compact('conceptos', 'personales','proveedor_pagos'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout = 'gentella';
		if (!$this->Pago->exists($id)) {
			throw new NotFoundException(__('Invalid pago'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$name = 'upload/lotes_'.date("Y-m-d-h-i-s").'_'.$this->request->data["Pago"]["contenido"]["name"];
			$this->request->data["Pago"]["nombre_archivo"] = $name;
			if ($this->Pago->save($this->request->data)) {
				if (move_uploaded_file($this->request->data["Pago"]["contenido"]["tmp_name"], $name)){
					chmod($name,0777);
			        $this->Flash->success(__('The pago has been saved.'));
					return $this->redirect(array('action' => 'index'));
			    }else{
			        $this->Flash->error(__('The pago could not be saved. Please, try again.'));
			    } 
			} else {
				$this->Flash->error(__('The pago could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Pago.' . $this->Pago->primaryKey => $id));
			$this->request->data = $this->Pago->find('first', $options);
		}
		$conceptos = $this->Pago->Concepto->find('list');
		$personales = $this->Pago->Personale->find('list');
		$this->set(compact('conceptos', 'personales'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->layout = 'gentella';
		$this->Pago->id = $id;
		if (!$this->Pago->exists()) {
			throw new NotFoundException(__('Invalid pago'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Pago->delete()) {
			$this->Flash->success(__('The pago has been deleted.'));
		} else {
			$this->Flash->error(__('The pago could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
