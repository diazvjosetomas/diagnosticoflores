<?php
App::uses('AppController', 'Controller');
/**
 * Blogcategorias Controller
 *
 * @property Blogcategoria $Blogcategoria
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class BlogcategoriasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

	var $layout = 'gentella';

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(38);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Blogcategoria->recursive = 0;
		//$this->set('blogcategorias', $this->Paginator->paginate());
		$this->set('blogcategorias', $this->Blogcategoria->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Blogcategoria->exists($id)) {
			throw new NotFoundException(__('Invalid blogcategoria'));
		}
		$options = array('conditions' => array('Blogcategoria.' . $this->Blogcategoria->primaryKey => $id));
		$this->set('blogcategoria', $this->Blogcategoria->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Blogcategoria->create();
			if ($this->Blogcategoria->save($this->request->data)) {
				
				$this->Flash->success(__('The blogcategoria has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The blogcategoria could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Blogcategoria->exists($id)) {
			throw new NotFoundException(__('Invalid blogcategoria'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Blogcategoria->save($this->request->data)) {
				$this->Flash->success(__('The blogcategoria has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The blogcategoria could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Blogcategoria.' . $this->Blogcategoria->primaryKey => $id));
			$this->request->data = $this->Blogcategoria->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Blogcategoria->id = $id;
		if (!$this->Blogcategoria->exists()) {
			throw new NotFoundException(__('Invalid blogcategoria'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Blogcategoria->delete()) {
			$this->Flash->success(__('The blogcategoria has been deleted.'));
		} else {
			$this->Flash->error(__('The blogcategoria could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
