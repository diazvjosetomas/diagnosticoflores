<?php
App::uses('AppController', 'Controller');
/**
 * Periodos Controller
 *
 * @property Periodo $Periodo
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class PeriodosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');
	public $layout = 'gentella';

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(34);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Periodo->recursive = 0;
		//$this->set('periodos', $this->Paginator->paginate());
		  $this->set('periodos', $this->Periodo->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Periodo->exists($id)) {
			throw new NotFoundException(__('Invalid periodo'));
		}
		$options = array('conditions' => array('Periodo.' . $this->Periodo->primaryKey => $id));
		$this->set('periodo', $this->Periodo->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Periodo->create();
			if ($this->Periodo->save($this->request->data)) {
				
				$this->Flash->success(__('El periodo fue guardado con exito.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('El perio no pudo ser guardado, por favor, intenta de nuevo.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Periodo->exists($id)) {
			throw new NotFoundException(__('Invalid periodo'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Periodo->save($this->request->data)) {
				$this->Flash->success(__('El periodo fue editado con éxito.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('El periodo no se pudo editar, por favor, intenta de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Periodo.' . $this->Periodo->primaryKey => $id));
			$this->request->data = $this->Periodo->find('first', $options);
			$this->set('dia', $this->Periodo->find('all', array('conditions'=>array('Periodo.id'=>$id))));
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Periodo->id = $id;
		if (!$this->Periodo->exists()) {
			throw new NotFoundException(__('Invalid periodo'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Periodo->delete()) {
			$this->Flash->success(__('El periodo fue eliminado.'));
		} else {
			$this->Flash->error(__('El periodo no pudo ser eliminado, por favor, intenta de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
