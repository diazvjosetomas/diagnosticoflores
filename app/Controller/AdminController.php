<?php
App::uses('AppController', 'Controller');

class AdminController extends AppController {

  public $uses = array('User');

  /**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/**
 * index method
 *
 * @return void
 */
  public function index() {
    $this->layout = 'login_admin';
  }



/**
 * login method
 *
 * @return void
 */
  public function login() {
    $this->layout = 'login_admin';
    $user     = trim(strtoupper($this->request->data['Admin']['user']));
    $password = md5(trim(strtoupper($this->request->data['Admin']['password'])));
    $c = $this->User->find('count',array('conditions'=>array('user'=>$user,'password'=>$password, 'statu'=>1)));
    if($c!=0){
       $this->User->recursive = 3;
      $d = $this->User->find('all',  array('conditions'=>array('user'=>$user,'password'=>$password, 'statu'=>1)));
	    extract($d[0]["User"]);
      if ($nivel == 2 || $nivel == 3) {
        return $this->redirect(array('controller' => 'Sites', 'action' => 'index'));
      }

      $this->Session->write('USUARIO_NIVEL', $nivel);
	    $this->Session->write('USUARIO_ID',    $id);
	    $this->Session->write('USUARIO_EMAIL', $email);
      $this->Session->write('USUARIO_USER', $user);
      $this->Session->write('USUARIO_NAME', $nombre." ".$apellidos);
      $this->Session->write('usuario_valido', true);

      $this->Session->write('MODULOS', $d[0]["Role"]["Rolesmodulo"]);
      $this->Session->write('ROL', $role_id);
      $this->Flash->success(__('Bienvenido al panel de administrador, ha iniciado sesion correctamente.'));

      //pr($d);
     return $this->redirect(array('controller' => 'Dashboards', 'action' => 'index'));
    }else{
      $this->Flash->success(__('Lo siento, su usuario o password son incorrectos.'));
      return $this->redirect(array('controller' => 'admin', 'action' => 'index'));
    }
  }
/**
 * panel method
 *
 * @return void
 */
  public function panel() {
    $this->layout = 'gentella';
    if($this->checkSession()){
    	$this->redirect("/Dashboards/index/");
    }
  }
/**
 * logout method
 *
 * @return void
 */
  public function logout($close = null) {
    $this->layout = 'login_admin';
    $this->Session->delete('usuario_valido');
    if($close == 'close'){
      $this->Flash->success(__('Debe iniciar sesion.'));

    }else{
      $this->Flash->success(__('Su sesion ha sido cerrada correctamente.'));
      $this->Session->delete('USUARIO_ID');
	  $this->Session->delete('USUARIO_EMAIL');
	  $this->Session->delete('USUARIO_NIVEL');
    }
    return $this->redirect(array('controller' => 'admin', 'action' => 'index'));
  }



}
