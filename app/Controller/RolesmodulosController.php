<?php
App::uses('AppController', 'Controller');
/**
 * Rolesmodulos Controller
 *
 * @property Rolesmodulo $Rolesmodulo
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class RolesmodulosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');
	public $layout = 'gentella';

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(33);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Rolesmodulo->recursive = 0;
		//$this->set('rolesmodulos', $this->Paginator->paginate());
		  $this->set('rolesmodulos', $this->Rolesmodulo->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Rolesmodulo->exists($id)) {
			throw new NotFoundException(__('Invalid rolesmodulo'));
		}
		$options = array('conditions' => array('Rolesmodulo.' . $this->Rolesmodulo->primaryKey => $id));
		$this->set('rolesmodulo', $this->Rolesmodulo->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Rolesmodulo->create();
			if ($this->Rolesmodulo->save($this->request->data)) {
				
				$this->Flash->success(__('The rolesmodulo has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The rolesmodulo could not be saved. Please, try again.'));
			}
		}
		$roles = $this->Rolesmodulo->Role->find('list');
		$modulos = $this->Rolesmodulo->Modulo->find('list');
		$this->set(compact('roles', 'modulos'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Rolesmodulo->exists($id)) {
			throw new NotFoundException(__('Invalid rolesmodulo'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Rolesmodulo->save($this->request->data)) {
				$this->Flash->success(__('The rolesmodulo has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The rolesmodulo could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Rolesmodulo.' . $this->Rolesmodulo->primaryKey => $id));
			$this->request->data = $this->Rolesmodulo->find('first', $options);
		}
		$roles = $this->Rolesmodulo->Role->find('list');
		$modulos = $this->Rolesmodulo->Modulo->find('list');
		$this->set(compact('roles', 'modulos'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Rolesmodulo->id = $id;
		if (!$this->Rolesmodulo->exists()) {
			throw new NotFoundException(__('Invalid rolesmodulo'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Rolesmodulo->delete()) {
			$this->Flash->success(__('The rolesmodulo has been deleted.'));
		} else {
			$this->Flash->error(__('The rolesmodulo could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
