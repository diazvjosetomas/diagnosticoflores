<?php
App::uses('AppController', 'Controller');
/**
 * Contactos Controller
 *
 * @property Contacto $Contacto
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ContactosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(56);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = 'gentella';
		//$this->Contacto->recursive = 0;
		//$this->set('contactos', $this->Paginator->paginate());
		$this->set('contactos', $this->Contacto->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->layout = 'gentella';
		if (!$this->Contacto->exists($id)) {
			throw new NotFoundException(__('Invalid contacto'));
		}
		$options = array('conditions' => array('Contacto.' . $this->Contacto->primaryKey => $id));
		$this->set('contacto', $this->Contacto->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = 'gentella';
		if ($this->request->is('post')) {
			$this->Contacto->create();
			if ($this->Contacto->save($this->request->data)) {
				
				$this->Flash->success(__('The contacto has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The contacto could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout = 'gentella';
		if (!$this->Contacto->exists($id)) {
			throw new NotFoundException(__('Invalid contacto'));
		}
		if ($this->request->is(array('post', 'put'))) {

			if ($this->Contacto->save($this->request->data)) {
				$this->Flash->success(__('The contacto has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The contacto could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Contacto.' . $this->Contacto->primaryKey => $id));
			$this->request->data = $this->Contacto->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->layout = 'gentella';
		$this->Contacto->id = $id;
		if (!$this->Contacto->exists()) {
			throw new NotFoundException(__('Invalid contacto'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Contacto->delete()) {
			$this->Flash->success(__('The contacto has been deleted.'));
		} else {
			$this->Flash->error(__('The contacto could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
