<?php
App::uses('AppController', 'Controller');
/**
 * Sucursales Controller
 *
 * @property Sucursale $Sucursale
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SucursalesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


public function beforeFilter() {
	$this->checkSession(29);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = 'gentella';
		//$this->Sucursale->recursive = 0;
		//$this->set('sucursales', $this->Paginator->paginate());
		$this->set('sucursales', $this->Sucursale->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->layout = 'gentella';
		if (!$this->Sucursale->exists($id)) {
			throw new NotFoundException(__('Invalid sucursale'));
		}
		$options = array('conditions' => array('Sucursale.' . $this->Sucursale->primaryKey => $id));
		$this->set('sucursale', $this->Sucursale->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = 'gentella';
		if ($this->request->is('post')) {
			$this->Sucursale->create();
			if ($this->Sucursale->save($this->request->data)) {
				
				$this->Flash->success(__('The sucursale has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The sucursale could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout = 'gentella';
		if (!$this->Sucursale->exists($id)) {
			throw new NotFoundException(__('Invalid sucursale'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Sucursale->save($this->request->data)) {
				$this->Flash->success(__('The sucursale has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The sucursale could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Sucursale.' . $this->Sucursale->primaryKey => $id));
			$this->request->data = $this->Sucursale->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->layout = 'gentella';
		$this->Sucursale->id = $id;
		if (!$this->Sucursale->exists()) {
			throw new NotFoundException(__('Invalid sucursale'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Sucursale->delete()) {
			$this->Flash->success(__('The sucursale has been deleted.'));
		} else {
			$this->Flash->error(__('The sucursale could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
