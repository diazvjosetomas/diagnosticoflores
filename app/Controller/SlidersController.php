<?php
App::uses('AppController', 'Controller');
/**
 * Sliders Controller
 *
 * @property Slider $Slider
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SlidersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(51);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = 'gentella';
		//$this->Slider->recursive = 0;
		//$this->set('sliders', $this->Paginator->paginate());
		$this->set('sliders', $this->Slider->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->layout = 'gentella';
		if (!$this->Slider->exists($id)) {
			throw new NotFoundException(__('Invalid slider'));
		}
		$options = array('conditions' => array('Slider.' . $this->Slider->primaryKey => $id));
		$this->set('slider', $this->Slider->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = 'gentella';
		if ($this->request->is('post')) {
			$this->Slider->create();
			if (!empty($this->request->data['Slider']['imagen']) && imagenType($this->request->data['Slider']['imagen']['type']) != false) {
			               $ds = '/';
			               $storeFolder = 'upload';
			               $tempFile    = $this->request->data['Slider']['imagen']['tmp_name'];
			               $targetPath  = ROOT . $ds .'app'. $ds .'webroot'. $ds .'img'. $ds . $storeFolder . $ds;
			               $extencion   = imagenType($this->request->data['Slider']['imagen']['type']);
			               $renameFile  = 'a_'.date('dmY_his').$extencion;
			               $targetFile  = $targetPath.$renameFile;
			               if(move_uploaded_file($tempFile,$targetFile)){
			                   $tipo_imagen = $this->request->data['Slider']['imagen']['type'];
			                   $nombre_imagen = $renameFile;;
			                   $this->request->data['Slider']['carpeta_imagen'] = $storeFolder;	
			                   $this->request->data['Slider']['nombre_imagen']  = $nombre_imagen;
			                   $this->request->data['Slider']['tipo_imagen']    = $tipo_imagen;
			                   $this->request->data['Slider']['ruta_imagen']    = $storeFolder.'/'.$nombre_imagen;
			               }else{
			                   $this->request->data['Slider']['carpeta_imagen']  = "_";
			                   $this->request->data['Slider']['nombre_imagen']   = "_";
			                   $this->request->data['Slider']['tipo_imagen']     = "_";
			                   $this->request->data['Slider']['ruta_imagen'] = "_";
			               }

			           }else{
			                   $this->request->data['Slider']['carpeta_imagen']  = "_";
			                   $this->request->data['Slider']['nombre_imagen']   = "_";
			                   $this->request->data['Slider']['tipo_imagen']     = "_";
			                   $this->request->data['Slider']['ruta_imagen'] = "_";
			           }
			if ($this->Slider->save($this->request->data)) {
				
				$this->Flash->success(__('The slider has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The slider could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout = 'gentella';
		if (!$this->Slider->exists($id)) {
			throw new NotFoundException(__('Invalid slider'));
		}
		if ($this->request->is(array('post', 'put'))) {




			if (!empty($this->request->data['Slider']['imagen']) && imagenType($this->request->data['Slider']['imagen']['type']) != false) {
			               $ds = '/';
			               $storeFolder = 'upload';
			               $tempFile    = $this->request->data['Slider']['imagen']['tmp_name'];
			               $targetPath  = ROOT . $ds .'app'. $ds .'webroot'. $ds .'img'. $ds . $storeFolder . $ds;
			               $extencion   = imagenType($this->request->data['Slider']['imagen']['type']);
			               $renameFile  = 'a_'.date('dmY_his').$extencion;
			               $targetFile  = $targetPath.$renameFile;
			               if(move_uploaded_file($tempFile,$targetFile)){
			                   $tipo_imagen = $this->request->data['Slider']['imagen']['type'];
			                   $nombre_imagen = $renameFile;;
			                   $this->request->data['Slider']['carpeta_imagen'] = $storeFolder;
			                   $this->request->data['Slider']['nombre_imagen']  = $nombre_imagen;
			                   $this->request->data['Slider']['tipo_imagen']    = $tipo_imagen;
			                   $this->request->data['Slider']['ruta_imagen']    = $storeFolder.'/'.$nombre_imagen;
			               }

			           }
			if ($this->Slider->save($this->request->data)) {
				$this->Flash->success(__('The slider has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The slider could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Slider.' . $this->Slider->primaryKey => $id));
			$this->request->data = $this->Slider->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->layout = 'gentella';
		$this->Slider->id = $id;
		if (!$this->Slider->exists()) {
			throw new NotFoundException(__('Invalid slider'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Slider->delete()) {
			$this->Flash->success(__('The slider has been deleted.'));
		} else {
			$this->Flash->error(__('The slider could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
