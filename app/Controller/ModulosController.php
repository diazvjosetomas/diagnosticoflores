<?php
App::uses('AppController', 'Controller');
/**
 * Modulos Controller
 *
 * @property Modulo $Modulo
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ModulosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');
	public $layout = 'gentella';

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(16);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Modulo->recursive = 0;
		//$this->set('modulos', $this->Paginator->paginate());
		  $this->set('modulos', $this->Modulo->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Modulo->exists($id)) {
			throw new NotFoundException(__('Invalid modulo'));
		}
		$options = array('conditions' => array('Modulo.' . $this->Modulo->primaryKey => $id));
		$this->set('modulo', $this->Modulo->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Modulo->create();
			if ($this->Modulo->save($this->request->data)) {
				
				$this->Flash->success(__('The modulo has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The modulo could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Modulo->exists($id)) {
			throw new NotFoundException(__('Invalid modulo'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Modulo->save($this->request->data)) {
				$this->Flash->success(__('The modulo has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The modulo could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Modulo.' . $this->Modulo->primaryKey => $id));
			$this->request->data = $this->Modulo->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Modulo->id = $id;
		if (!$this->Modulo->exists()) {
			throw new NotFoundException(__('Invalid modulo'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Modulo->delete()) {
			$this->Flash->success(__('The modulo has been deleted.'));
		} else {
			$this->Flash->error(__('The modulo could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
