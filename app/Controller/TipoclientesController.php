<?php
App::uses('AppController', 'Controller');
/**
 * Tipoclientes Controller
 *
 * @property Tipocliente $Tipocliente
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class TipoclientesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(27);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = 'gentella';
		//$this->Tipocliente->recursive = 0;
		//$this->set('tipoclientes', $this->Paginator->paginate());
		$this->set('tipoclientes', $this->Tipocliente->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->layout = 'gentella';
		if (!$this->Tipocliente->exists($id)) {
			throw new NotFoundException(__('Invalid tipocliente'));
		}
		$options = array('conditions' => array('Tipocliente.' . $this->Tipocliente->primaryKey => $id));
		$this->set('tipocliente', $this->Tipocliente->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = 'gentella';
		if ($this->request->is('post')) {
			$this->Tipocliente->create();
			if ($this->Tipocliente->save($this->request->data)) {
				
				$this->Flash->success(__('The tipocliente has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The tipocliente could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout = 'gentella';
		if (!$this->Tipocliente->exists($id)) {
			throw new NotFoundException(__('Invalid tipocliente'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Tipocliente->save($this->request->data)) {
				$this->Flash->success(__('The tipocliente has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The tipocliente could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Tipocliente.' . $this->Tipocliente->primaryKey => $id));
			$this->request->data = $this->Tipocliente->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->layout = 'gentella';
		$this->Tipocliente->id = $id;
		if (!$this->Tipocliente->exists()) {
			throw new NotFoundException(__('Invalid tipocliente'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Tipocliente->delete()) {
			$this->Flash->success(__('The tipocliente has been deleted.'));
		} else {
			$this->Flash->error(__('The tipocliente could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
