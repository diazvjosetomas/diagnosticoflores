<?php
App::uses('AppController', 'Controller');
/**
 * Cajaaperturas Controller
 *
 * @property Cajaapertura $Cajaapertura
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CajaaperturasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');
	public $uses = array('Caja','Cajaapertura');

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */

public $layout = "gentella";

public function beforeFilter() {
	$this->checkSession(63);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Cajaapertura->recursive = 0;
		//$this->set('cajaaperturas', $this->Paginator->paginate());
		  $this->set('cajaaperturas', $this->Cajaapertura->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Cajaapertura->exists($id)) {
			throw new NotFoundException(__('Invalid cajaapertura'));
		}
		$options = array('conditions' => array('Cajaapertura.' . $this->Cajaapertura->primaryKey => $id));
		$this->set('cajaapertura', $this->Cajaapertura->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Cajaapertura->create();
			$idCaja = $this->request->data['Cajaapertura']['caja_id'];
			$this->Caja->query("UPDATE cajas SET status = 1 WHERE cajas.id = '".$idCaja."' ");
			if ($this->Cajaapertura->save($this->request->data)) {
				
				$this->Flash->success(__('The cajaapertura has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The cajaapertura could not be saved. Please, try again.'));
			}
		}
		$cajas      = $this->Cajaapertura->Caja->find('list');
		$users      = $this->Cajaapertura->User->find('list');
		$sucursales = $this->Cajaapertura->Sucursale->find('list');
		$this->set(compact('cajas', 'users', 'sucursales'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Cajaapertura->exists($id)) {
			throw new NotFoundException(__('Invalid cajaapertura'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Cajaapertura->save($this->request->data)) {
				$this->Flash->success(__('The cajaapertura has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The cajaapertura could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Cajaapertura.' . $this->Cajaapertura->primaryKey => $id));
			$this->request->data = $this->Cajaapertura->find('first', $options);
		}
		$cajas      = $this->Cajaapertura->Caja->find('list', array('conditions'=>array('Caja.sucursale_id'=>$this->request->data['Cajaapertura']['sucursale_id'], 'Caja.id'=>$this->request->data['Cajaapertura']['caja_id'])));
		$users      = $this->Cajaapertura->User->find('list', array('conditions'=>array('User.id'=>$this->request->data['Cajaapertura']['user_id'])));
		$sucursales = $this->Cajaapertura->Sucursale->find('list', array('conditions'=>array('Sucursale.id'=>$this->request->data['Cajaapertura']['sucursale_id'])));
		$this->set(compact('cajas', 'users', 'sucursales'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Cajaapertura->id = $id;
		if (!$this->Cajaapertura->exists()) {
			throw new NotFoundException(__('Invalid cajaapertura'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Cajaapertura->delete()) {
			$this->Flash->success(__('The cajaapertura has been deleted.'));
		} else {
			$this->Flash->error(__('The cajaapertura could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}


/**
 * cajas method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function cajas($var1=null){
		$this->layout = "ajax";
		$cajas = $this->Cajaapertura->Caja->find('list', array('conditions'=>array('Caja.sucursale_id'=>$var1)));
        $this->set('cajas', $cajas);
	}


}
