<?php
App::uses('AppController', 'Controller');
/**
 * Egresotipos Controller
 *
 * @property Egresotipo $Egresotipo
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class EgresotiposController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

var $layout = "gentella";

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(46);		
}



/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Egresotipo->recursive = 0;
		//$this->set('egresotipos', $this->Paginator->paginate());
		$this->set('egresotipos', $this->Egresotipo->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Egresotipo->exists($id)) {
			throw new NotFoundException(__('Invalid egresotipo'));
		}
		$options = array('conditions' => array('Egresotipo.' . $this->Egresotipo->primaryKey => $id));
		$this->set('egresotipo', $this->Egresotipo->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Egresotipo->create();
			if ($this->Egresotipo->save($this->request->data)) {
				
				$this->Flash->success(__('The egresotipo has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The egresotipo could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Egresotipo->exists($id)) {
			throw new NotFoundException(__('Invalid egresotipo'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Egresotipo->save($this->request->data)) {
				$this->Flash->success(__('The egresotipo has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The egresotipo could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Egresotipo.' . $this->Egresotipo->primaryKey => $id));
			$this->request->data = $this->Egresotipo->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Egresotipo->id = $id;
		if (!$this->Egresotipo->exists()) {
			throw new NotFoundException(__('Invalid egresotipo'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Egresotipo->delete()) {
			$this->Flash->success(__('The egresotipo has been deleted.'));
		} else {
			$this->Flash->error(__('The egresotipo could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
