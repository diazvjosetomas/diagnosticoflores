<?php
App::uses('AppController', 'Controller');
/**
 * Asignacitas Controller
 *
 * @property Asignacita $Asignacita
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class AsignacitasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


/* public function beforeFilter() {
	$this->checkSession();		
} */


/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Asignacita->recursive = 0;
		//$this->set('asignacitas', $this->Paginator->paginate());
		$this->layout='gentella';
		  $this->set('asignacitas', $this->Asignacita->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->layout='gentella';
		if (!$this->Asignacita->exists($id)) {
			throw new NotFoundException(__('Invalid asignacita'));
		}
		$options = array('conditions' => array('Asignacita.' . $this->Asignacita->primaryKey => $id));
		$this->set('asignacita', $this->Asignacita->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout='gentella';
		if ($this->request->is('post')) {
			$this->Asignacita->create();
			if ($this->Asignacita->save($this->request->data)) {
				
				$this->Flash->success(__('The asignacita has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The asignacita could not be saved. Please, try again.'));
			}
		}
		$citas = $this->Asignacita->Cita->find('list');
		//$users = $this->Asignacita->User->find('list', 'conditions'->User.id != 0);
		$users = $this->Asignacita->User->find('list', array('conditions'=>array('User.tipocliente_id != '=> 0 )));
		$this->set(compact('citas', 'users'));
	}


/**
 * add method
 *
 * @return void
 */
	public function add2($idCita) {
		$this->layout='gentella';
		if ($this->request->is('post')) {
			$this->Asignacita->create();
			if ($this->Asignacita->save($this->request->data)) {
				
				$this->Flash->success(__('La cita fue salvada.'));
				return $this->redirect('/Citasadmin/');
			} else {
				$this->Flash->error(__('La cita no se pudo asignar intenta de nuevo.'));
			}
		}
		$citas = $this->Asignacita->Cita->find('list', array('conditions'=>array('Cita.id = ' => $idCita )));
		//$users = $this->Asignacita->User->find('list', 'conditions'->User.id != 0);
		$users = $this->Asignacita->User->find('list', array('conditions'=>array('User.tipocliente_id != ' => 0 )));
		$this->set(compact('citas', 'users'));
	}
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout='gentella';
		if (!$this->Asignacita->exists($id)) {
			throw new NotFoundException(__('Invalid asignacita'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Asignacita->save($this->request->data)) {
				$this->Flash->success(__('The asignacita has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The asignacita could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Asignacita.' . $this->Asignacita->primaryKey => $id));
			$this->request->data = $this->Asignacita->find('first', $options);
		}
		$citas = $this->Asignacita->Cita->find('list');
		$users = $this->Asignacita->User->find('list');
		$this->set(compact('citas', 'users'));
	}



	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
		public function edit2($id = null) {
			$this->layout='gentella';
			if (!$this->Asignacita->exists($id)) {
				throw new NotFoundException(__('Invalid asignacita'));
			}
			if ($this->request->is(array('post', 'put'))) {
				if ($this->Asignacita->save($this->request->data)) {
					$this->Flash->success(__('The asignacita has been saved.'));
					return $this->redirect(array('action' => 'index','controller'=>'citas'));
				} else {
					$this->Flash->error(__('The asignacita could not be saved. Please, try again.'));
				}
			} else {
				$options = array('conditions' => array('Asignacita.' . $this->Asignacita->primaryKey => $id));
				$this->request->data = $this->Asignacita->find('first', $options);
			}
			$citas = $this->Asignacita->Cita->find('list');
			$users = $this->Asignacita->User->find('list');
			$this->set(compact('citas', 'users'));
		}




/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->layout='gentella';
		$this->Asignacita->id = $id;
		if (!$this->Asignacita->exists()) {
			throw new NotFoundException(__('Invalid asignacita'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Asignacita->delete()) {
			$this->Flash->success(__('The asignacita has been deleted.'));
		} else {
			$this->Flash->error(__('The asignacita could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
