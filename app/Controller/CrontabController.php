<?php
App::uses('AppController', 'Controller');
/**
 * Citas Controller
 *
 * @property Cita $Cita
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CrontabController extends AppController {


	//var $uses = array('Laborable','Cita');

	public $uses = array('Feriado','Blogentrada','Configurations','Contenido','Servicio','Contacto','Slider','Doctor', 'Avisopriva','Imghorario','Facturacione','Empresa','User', 'Blogpublicidade','Sucursales','Suscribirse','Laborable','Cita','Tipocita','Categoriacita','Asignacita','Nota','Statu','Promocione','Publico');

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');




	/**
	 * recordatorio de cita
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
		public function recordatoriocita($id = null) {
			$this->layout = 'gentella';

			$hoy = date('Y-m-j');
			$fecha = date('Y-m-j');
			$nuevafecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;
			$nuevafecha = date ( 'Y-m-j' , $nuevafecha );

			$this->set('citas', $this->Cita->find('all',array('conditions'=>array("Cita.fecha BETWEEN '".$hoy."' AND '".$nuevafecha."'"))));

		}


	/**
	 * 	Promociones	
	 */	

		public function promociones(){
			App::import('Vendor', 'PHPMailer', array('file' => 'phpmailer/class.phpmailer.php'));
	
			$this->layout = 'gentella';
			$this->set('promociones',$this->Promocione->find('all', array('conditions'=>array('Promocione.status '=>'1'))));

			$publicos = $this->Publico->find('all');

			foreach ($publicos as $key) {
				$Modelo = $key['Publico']['tabla'];
				$Modelo = ucwords($Modelo);
				$Modelo = substr($Modelo, 0, -1);
				$array[] = $this->$Modelo->find('all');
			}

			$this->set('array', $array);



		}
}
