<?php
App::uses('AppController', 'Controller');
/**
 * Tipocitas Controller
 *
 * @property Tipocita $Tipocita
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class TipocitasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(54);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		  $this->layout = 'gentella';
		//$this->Tipocita->recursive = 0;
		//$this->set('tipocitas', $this->Paginator->paginate());
		  $this->set('tipocitas', $this->Tipocita->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->layout = 'gentella';
		if (!$this->Tipocita->exists($id)) {
			throw new NotFoundException(__('Invalid tipocita'));
		}
		$options = array('conditions' => array('Tipocita.' . $this->Tipocita->primaryKey => $id));
		$this->set('tipocita', $this->Tipocita->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = 'gentella';
		if ($this->request->is('post')) {
			$this->Tipocita->create();
						if (!empty($this->request->data['Tipocita']['imagen']) && imagenType($this->request->data['Tipocita']['imagen']['type']) != false) {
			               $ds = '/';
			               $storeFolder = 'upload';
			               $tempFile    = $this->request->data['Tipocita']['imagen']['tmp_name'];
			               $targetPath  = ROOT . $ds .'app'. $ds .'webroot'. $ds .'img'. $ds . $storeFolder . $ds;
			               $extencion   = imagenType($this->request->data['Tipocita']['imagen']['type']);
			               $renameFile  = 'a_'.date('dmY_his').$extencion;
			               $targetFile  = $targetPath.$renameFile;
			               if(move_uploaded_file($tempFile,$targetFile)){
			                   $tipo_imagen = $this->request->data['Tipocita']['imagen']['type'];
			                   $nombre_imagen = $renameFile;;
			                   $this->request->data['Tipocita']['carpeta_imagen'] = $storeFolder;
			                   $this->request->data['Tipocita']['nombre_imagen']  = $nombre_imagen;
			                   $this->request->data['Tipocita']['tipo_imagen']    = $tipo_imagen;
			                   $this->request->data['Tipocita']['ruta_imagen']    = $storeFolder.'/'.$nombre_imagen;
			               }else{
			                   $this->request->data['Tipocita']['carpeta_imagen']  = "_";
			                   $this->request->data['Tipocita']['nombre_imagen']   = "_";
			                   $this->request->data['Tipocita']['tipo_imagen']     = "_";
			                   $this->request->data['Tipocita']['ruta_imagen'] = "_";
			               }

			           }else{
			                   $this->request->data['Tipocita']['carpeta_imagen']  = "_";
			                   $this->request->data['Tipocita']['nombre_imagen']   = "_";
			                   $this->request->data['Tipocita']['tipo_imagen']     = "_";
			                   $this->request->data['Tipocita']['ruta_imagen'] = "_";
			           }
					
				if ($this->Tipocita->save($this->request->data)) {
				
					$this->Flash->success(__('The tipocita has been saved.'));
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Flash->error(__('The tipocita could not be saved. Please, try again.'));
			}
		}
		$sucursales = $this->Tipocita->Sucursale->find('list');
		$this->set(compact('sucursales'));
		$categoriacitas = $this->Tipocita->Categoriacita->find('list');
		$this->set(compact('categoriacitas'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout = 'gentella';
		if (!$this->Tipocita->exists($id)) {
			throw new NotFoundException(__('Invalid tipocita'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Tipocita->save($this->request->data)) {
				$this->Flash->success(__('The tipocita has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The tipocita could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Tipocita.' . $this->Tipocita->primaryKey => $id));
			$this->request->data = $this->Tipocita->find('first', $options);
		}
		$categoriacitas = $this->Tipocita->Categoriacita->find('list');
		$this->set(compact('categoriacitas'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->layout = 'gentella';
		$this->Tipocita->id = $id;
		if (!$this->Tipocita->exists()) {
			throw new NotFoundException(__('Invalid tipocita'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Tipocita->delete()) {
			$this->Flash->success(__('The tipocita has been deleted.'));
		} else {
			$this->Flash->error(__('The tipocita could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
