<?php
App::uses('AppController', 'Controller');
/**
 * Configurations Controller
 *
 * @property Configuration $Configuration
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ConfigurationsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');
	public $layout = 'gentella';

	public function beforeFilter() {
		$this->checkSession(2);
		
	}
	
/**
 * index method
 *
 * @return void
 */
	public function index() {   
		//$this->Configuration->recursive = 0;
		//$this->set('configurations', $this->Paginator->paginate());
		$this->set('configurations', $this->Configuration->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Configuration->exists($id)) {
			throw new NotFoundException(__('Invalid configuration'));
		}
		$options = array('conditions' => array('Configuration.' . $this->Configuration->primaryKey => $id));
		$this->set('configuration', $this->Configuration->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Configuration->create();
			if ($this->Configuration->save($this->request->data)) {
				$this->Session->setFlash(__('The configuration has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The configuration could not be saved. Please, try again.'));
			}
		}
		$languages = $this->Configuration->Language->find('list');
		$moneySimbols = $this->Configuration->MoneySimbol->find('list');
		$countries = $this->Configuration->Country->find('list');
		$this->set(compact('languages', 'moneySimbols', 'countries'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Configuration->exists($id)) {
			throw new NotFoundException(__('Invalid configuration'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Configuration->save($this->request->data)) {
				$this->Session->setFlash(__('The configuration has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The configuration could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Configuration.' . $this->Configuration->primaryKey => $id));
			$this->request->data = $this->Configuration->find('first', $options);
		}
		$languages = $this->Configuration->Language->find('list');
		$moneySimbols = $this->Configuration->MoneySimbol->find('list');
		$countries = $this->Configuration->Country->find('list');
		$this->set(compact('languages', 'moneySimbols', 'countries'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Configuration->id = $id;
		if (!$this->Configuration->exists()) {
			throw new NotFoundException(__('Invalid configuration'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Configuration->delete()) {
			$this->Session->setFlash(__('The configuration has been deleted.'));
		} else {
			$this->Session->setFlash(__('The configuration could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
