<?php
App::uses('AppController', 'Controller');
/**
 * Categoriacitas Controller
 *
 * @property Categoriacita $Categoriacita
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CategoriacitasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(41);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = 'gentella';
		$this->Categoriacita->recursive = 0;
		$this->set('categoriacitas', $this->Paginator->paginate());
		$this->set('categoriacitas', $this->Categoriacita->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Categoriacita->exists($id)) {
			throw new NotFoundException(__('Invalid categoriacita'));
		}
		$options = array('conditions' => array('Categoriacita.' . $this->Categoriacita->primaryKey => $id));
		$this->set('categoriacita', $this->Categoriacita->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = 'gentella';
		if ($this->request->is('post')) {
			$this->Categoriacita->create();
			if ($this->Categoriacita->save($this->request->data)) {
				
				$this->Flash->success(__('The categoriacita has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The categoriacita could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout = 'gentella';
		if (!$this->Categoriacita->exists($id)) {
			throw new NotFoundException(__('Invalid categoriacita'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Categoriacita->save($this->request->data)) {
				$this->Flash->success(__('The categoriacita has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The categoriacita could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Categoriacita.' . $this->Categoriacita->primaryKey => $id));
			$this->request->data = $this->Categoriacita->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->layout = 'gentella';
		$this->Categoriacita->id = $id;
		if (!$this->Categoriacita->exists()) {
			throw new NotFoundException(__('Invalid categoriacita'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Categoriacita->delete()) {
			$this->Flash->success(__('The categoriacita has been deleted.'));
		} else {
			$this->Flash->error(__('The categoriacita could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
