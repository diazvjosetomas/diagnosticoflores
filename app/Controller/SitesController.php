<?php class SitesController extends AppController {
    public $helpers = array('Html', 'Form','GoogleMap');

    public $uses = array('Feriado','Blogentrada','Configurations',
    'Contenido','Servicio','Contacto','Slider','Doctor', 
    'Avisopriva','Imghorario','Facturacione','Empresa','User',
     'Blogpublicidade','Sucursales','Suscribirse','Laborable','Cita',
     'Tipocita','Categoriacita','Asignacita','Nota','Statu','Adjunto','Imgadjunto','Sucursale');

    public $components = array('Flash','Session','Paginator');


    //=============================================[ function index  ] start!
    public function index($dir=null) {         
        $this->layout = 'sites';         
        $this->set('configurations', $this->Configurations->find('all'));
        $this->set('horarios',  $this->Contenido->find('all',array('conditions'=>array('categoria_id'=>array('6','10','11')))));
        $this->set('servicio',  $this->Servicio->find('all',array('conditions'=>array('estatus'=>'1'))) );
        $this->set('nosotros',  $this->Contenido->find('all',array('conditions'=>array('categoria_id'=>'8') )) );
        $this->set('direccion', $this->Sucursale->find('all' ));

        $this->set('tipoclientes', $this->User->Tipocliente->find('list'));
        $this->set('avisoprivas', $this->Avisopriva->find('all'));        
        $this->set('sucursal', $this->Sucursales->find('all'));        
        $this->set('blog', $this->Blogentrada->find('all'));
        $this->set('blog', $this->Paginator->paginate());
        $this->set('imghorarios', $this->Imghorario->find('all'));
        $this->set('slider', $this->Slider->find('all'));
        $this->set('empresa', $this->Empresa->find('all'));
        $this->set('dir', 'inicio');

        
    }

    public function verificausuario($usuario = null, $correo = null){
      $this->layout ='ajax';
      //$usuario =  $this->User->query("SELECT count(*) FROM users WHERE user = '".$usuario."'");
      //$correo = $this->User->query("SELECT count(*) FROM users WHERE email = '".$correo."'");

      $user = $this->User->find('count', array('conditions'=>array('User.user ' => $usuario)));
      $email = $this->User->find('count', array('conditions'=>array('User.email ' => $correo)));


      

      if ($user > 0) {
        $this->set('usuario', '1');
      }else{
        $this->set('usuario', '0');
      }

      if ($email > 0) {
          $this->set('correo', '1');
      }else{
          $this->set('correo', '0');        
      }
     
    }
    public function remitidos($dir=null) {         
        $this->layout = 'sites';         
        $this->set('configurations', $this->Configurations->find('all'));
        $this->set('horarios',  $this->Contenido->find('all',array('conditions'=>array('categoria_id'=>array('6','10','11')))));
        $this->set('servicio',  $this->Servicio->find('all',array('conditions'=>array('estatus'=>'1'))) );
        $this->set('nosotros',  $this->Contenido->find('all',array('conditions'=>array('categoria_id'=>'8') )) );
        $this->set('direccion', $this->Contenido->find('all',array('conditions'=>array('categoria_id'=>array('9','12','13') )) ));

        $this->set('tipoclientes', $this->User->Tipocliente->find('list'));
        $this->set('avisoprivas', $this->Avisopriva->find('all'));        
        $this->set('sucursal', $this->Sucursales->find('all'));        
        $this->set('blog', $this->Blogentrada->find('all'));
        $this->set('blog', $this->Paginator->paginate());
        $this->set('imghorarios', $this->Imghorario->find('all'));
        $this->set('slider', $this->Slider->find('all'));
        $this->set('empresa', $this->Empresa->find('all'));
        $this->set('dir', 'remitidos');
        $usuario_id = $this->Session->read('USUARIO_ID');
        $this->set('adjuntos', $this->Adjunto->find('all', array('conditions'=>array('user_id'=>$usuario_id))));

        
    }



    public function detallesremitidos($id=null) {         
        $this->layout = 'sites';         
        $this->set('configurations', $this->Configurations->find('all'));
        $this->set('horarios',  $this->Contenido->find('all',array('conditions'=>array('categoria_id'=>array('6','10','11')))));
        $this->set('servicio',  $this->Servicio->find('all',array('conditions'=>array('estatus'=>'1'))) );
        $this->set('nosotros',  $this->Contenido->find('all',array('conditions'=>array('categoria_id'=>'8') )) );
        $this->set('direccion', $this->Contenido->find('all',array('conditions'=>array('categoria_id'=>array('9','12','13') )) ));

        $this->set('tipoclientes', $this->User->Tipocliente->find('list'));
        $this->set('avisoprivas', $this->Avisopriva->find('all'));        
        $this->set('sucursal', $this->Sucursales->find('all'));        
        $this->set('blog', $this->Blogentrada->find('all'));
        $this->set('blog', $this->Paginator->paginate());
        $this->set('imghorarios', $this->Imghorario->find('all'));
        $this->set('slider', $this->Slider->find('all'));
        $this->set('empresa', $this->Empresa->find('all'));
        $this->set('dir', 'remitidos');
        $this->Adjunto->recursive = 2;
        if (!$this->Adjunto->exists($id)) {
          throw new NotFoundException(__('Invalid adjunto'));
        }
        $options = array('conditions' => array('Adjunto.' . $this->Adjunto->primaryKey => $id));
        $this->set('adjunto', $this->Adjunto->find('first', $options));
        $this->set('imgadjunto', $this->Imgadjunto->find('all', $options));

        
    }

        public function citas() {
              $this->layout = 'citas';
              $this->set('sucursal', $this->Sucursales->find('all'));  
              $this->set('feriados', $this->Feriado->find('all'));
              $this->set('configurations', $this->Configurations->find('all'));
              $this->set('horarios',  $this->Contenido->find('all',array('conditions'=>array('categoria_id'=>array('6','10','11')))));
              $this->set('servicio',  $this->Servicio->find('all',array('conditions'=>array('estatus'=>'1'))) );
              $this->set('nosotros',  $this->Contenido->find('all',array('conditions'=>array('categoria_id'=>'8') )) );
              $this->set('direccion', $this->Contenido->find('all',array('conditions'=>array('categoria_id'=>array('9','12','13') )) ));

              $this->set('tipoclientes', $this->User->Tipocliente->find('list'));
              $this->set('avisoprivas', $this->Avisopriva->find('all'));        
              $this->set('sucursal', $this->Sucursales->find('all'));        
              $this->set('blog', $this->Blogentrada->find('all'));
              $this->set('blog', $this->Paginator->paginate());
              $this->set('imghorarios', $this->Imghorario->find('all'));
              $this->set('slider', $this->Slider->find('all'));
              $this->set('empresa', $this->Empresa->find('all'));
              $this->set('dir', 'citas');
              //$this->Cita->recursive = 0;
              //$this->set('citas', $this->Paginator->paginate());
              //$this->set('citas', $this->Cita->find('all'));
        }


    public function servicio(){
        $this->layout = 'sites';
        $this->set('configurations', $this->Configurations->find('all'));
        $this->set('horarios',  $this->Contenido->find('all',array('conditions'=>array('categoria_id'=>array('6','10','11')))));
        $this->set('servicio',  $this->Servicio->find('all',array('conditions'=>array('estatus'=>'1'))) );
        $this->set('nosotros',  $this->Contenido->find('all',array('conditions'=>array('categoria_id'=>'8') )) );
        $this->set('direccion', $this->Contenido->find('all',array('conditions'=>array('categoria_id'=>array('9','12','13') )) ));
        $this->set('tipoclientes', $this->User->Tipocliente->find('list'));
        $this->set('avisoprivas', $this->Avisopriva->find('all'));
        $this->set('blog', $this->Blogentrada->find('all'));
        $this->set('blog', $this->Paginator->paginate());
        $this->set('imghorarios', $this->Imghorario->find('all'));
        $this->set('slider', $this->Slider->find('all'));
        $this->set('empresa', $this->Empresa->find('all'));
        $this->set('dir', 'servicio');
        $this->render('index');
    }

    public function contacto(){
        $this->layout = 'sites';
        $this->set('configurations', $this->Configurations->find('all'));
        $this->set('horarios',  $this->Contenido->find('all',array('conditions'=>array('categoria_id'=>array('6','10','11')))));
        $this->set('servicio',  $this->Servicio->find('all',array('conditions'=>array('estatus'=>'1'))) );
        $this->set('nosotros',  $this->Contenido->find('all',array('conditions'=>array('categoria_id'=>'8') )) );
        $this->set('direccion', $this->Contenido->find('all',array('conditions'=>array('categoria_id'=>array('9','12','13') )) ));
        $this->set('tipoclientes', $this->User->Tipocliente->find('list'));
        $this->set('avisoprivas', $this->Avisopriva->find('all'));
        $this->set('blog', $this->Blogentrada->find('all'));
        $this->set('blog', $this->Paginator->paginate());
        $this->set('imghorarios', $this->Imghorario->find('all'));
        $this->set('slider', $this->Slider->find('all'));
        $this->set('empresa', $this->Empresa->find('all'));
        $this->set('dir', 'contacto');
        $this->render('index');
    }

    public function nosotros(){
        $this->layout = 'sites';
        $this->set('configurations', $this->Configurations->find('all'));
        $this->set('horarios',  $this->Contenido->find('all',array('conditions'=>array('categoria_id'=>array('6','10','11')))));
        $this->set('servicio',  $this->Servicio->find('all',array('conditions'=>array('estatus'=>'1'))) );
        $this->set('nosotros',  $this->Contenido->find('all',array('conditions'=>array('categoria_id'=>'8') )) );
        $this->set('direccion', $this->Contenido->find('all',array('conditions'=>array('categoria_id'=>array('9','12','13') )) ));
        $this->set('tipoclientes', $this->User->Tipocliente->find('list'));
        $this->set('avisoprivas', $this->Avisopriva->find('all'));
        $this->set('blog', $this->Blogentrada->find('all'));
        $this->set('blog', $this->Paginator->paginate());
        $this->set('imghorarios', $this->Imghorario->find('all'));
        $this->set('slider', $this->Slider->find('all'));
        $this->set('empresa', $this->Empresa->find('all'));
        $this->set('dir', 'nosotros');
        $this->render('index');
    }

    public function blog(){
        $this->checkSession_site();
        $this->layout = 'sites';
        $this->set('horarios',  $this->Contenido->find('all',array('conditions'=>array('categoria_id'=>array('6','10','11')))));
        $this->set('blogentrada', $this->Blogentrada->find('all'));
        $this->set('empresa', $this->Empresa->find('all'));
        $this->set('blogpublicidades', $this->Blogpublicidade->find('all'));
        $this->set('blog', $this->Paginator->paginate());   
        $this->set('servicio',  $this->Servicio->find('all',array('conditions'=>array('estatus'=>'1'))) );
        $this->set('dir', 'blog');
    }


    /**
 * add method
 *
 * @return void
 */
    public function add() {
        $this->layout = 'gentella';
        if ($this->request->is('post')) {
            $this->Contacto->create();
            if ($this->Contacto->save($this->request->data)) {
                
                $this->Flash->success(__('The contenido has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The contenido could not be saved. Please, try again.'));
            }
        }
        /*$categorias = $this->Contenido->Categoria->find('list');
        $this->set(compact('categorias'));*/
    }


       public function addOdontologo() {
        
        if ($this->request->is('post')) {
            $this->User->create();

            $this->request->data["User"]["statu"] = 1;
            $this->request->data["User"]["nivel"] = 3;
            $this->request->data["User"]["password"] = md5($this->request->data["User"]["password"]);


            if ($this->User->save($this->request->data)) {
                
                $this->Flash->success(__('The contenido has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The contenido could not be saved. Please, try again.'));
                return $this->redirect(array('action' => 'index'));
            }
        }
        /*$categorias = $this->Contenido->Categoria->find('list');
        $this->set(compact('categorias'));*/
    }



    /**
     * add method
     *
     * @return void
     */
        public function addSuscriptor() {
            $this->layout = 'gentella';
            if ($this->request->is('post')) {
                $this->Suscribirse->create();
                if ($this->Suscribirse->save($this->request->data)) {
                    
                    $this->Flash->success(__('The suscribirse has been saved.'));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Flash->error(__('The suscribirse could not be saved. Please, try again.'));
                }
            }
        }

/**
 * add method
 *
 * @return void
 */
    public function addFactura() {
        if ($this->request->is('post')) {
            $this->Facturacione->create();
            if ($this->Facturacione->save($this->request->data)) {
                $this->Session->write('SOLICITAFACTURA', '1');
                
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The facturacione could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * login method
     *
     * @return void
     */
      public function login() {
        $this->layout = 'login_admin';
        $user     = trim(strtoupper($this->request->data['Admin']['user']));
        $password = md5(trim(strtoupper($this->request->data['Admin']['password'])));
        $c = $this->User->find('count',array('conditions'=>array('user'=>$user,'password'=>$password, 'statu'=>1)));
        if($c!=0){
          $d = $this->User->find('all',  array('conditions'=>array('user'=>$user,'password'=>$password, 'statu'=>1)));
            extract($d[0]["User"]);
          $this->Session->write('USUARIO_NIVEL', $nivel);
          $this->Session->write('USUARIO_ID',    $id);
          $this->Session->write('USUARIO_EMAIL', $email);
          $this->Session->write('USUARIO_USER', $user);
          $this->Session->write('USUARIO_NAME', $nombre." ".$apellidos);
          $this->Session->write('USUARIO_TIPO', $tipocliente_id);
          $this->Session->write('usuario_valido', true);
          $this->Flash->success(__('Bienvenido al panel de administrador, ha iniciado sesion correctamente.'));
          return $this->redirect(array('controller' => 'sites', 'action' => 'blog'));
        }else{
          $this->Session->delete('USUARIO_ID');
          $this->Session->delete('USUARIO_EMAIL');
          $this->Session->delete('USUARIO_NIVEL');
          $this->Session->delete('USUARIO_USER');
          $this->Session->delete('USUARIO_NAME');
          $this->Session->delete('USUARIO_TIPO');
          $this->Flash->success(__('Lo siento, su usuario o password son incorrectos.'));
          return $this->redirect(array('controller' => 'sites', 'action' => 'index'));
        }
      }

    public function logout($close = null) {
    $this->layout = 'login_admin';
    $this->Session->delete('usuario_valido');
    if($close == 'close'){
      $this->Flash->success(__('Debe iniciar sesion.'));

    }else{
      $this->Flash->success(__('Su sesion ha sido cerrada correctamente.'));
      $this->Session->delete('USUARIO_ID');
      $this->Session->delete('USUARIO_EMAIL');
      $this->Session->delete('USUARIO_NIVEL');
      $this->Session->delete('USUARIO_USER');
      $this->Session->delete('USUARIO_NAME');
      $this->Session->delete('USUARIO_TIPO');
    }
    return $this->redirect(array('controller' => 'sites', 'action' => 'index'));
  }




}
?>