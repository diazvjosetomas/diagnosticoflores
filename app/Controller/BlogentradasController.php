<?php
App::uses('AppController', 'Controller');
/**
 * Blogentradas Controller
 *
 * @property Blogentrada $Blogentrada
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class BlogentradasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

	var $layout = 'gentella';

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(52);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Blogentrada->recursive = 0;
		//$this->set('blogentradas', $this->Paginator->paginate());
		$this->set('blogentradas', $this->Blogentrada->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Blogentrada->exists($id)) {
			throw new NotFoundException(__('Invalid blogentrada'));
		}
		$options = array('conditions' => array('Blogentrada.' . $this->Blogentrada->primaryKey => $id));
		$this->set('blogentrada', $this->Blogentrada->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			if (!empty($this->request->data['Blogentrada']['imagen']) && imagenType($this->request->data['Blogentrada']['imagen']['type']) != false) {
               $ds = '/';
               $storeFolder = 'upload';
               $tempFile    = $this->request->data['Blogentrada']['imagen']['tmp_name'];
               $targetPath  = ROOT . $ds .'app'. $ds .'webroot'. $ds .'img'. $ds . $storeFolder . $ds;
               $extencion   = imagenType($this->request->data['Blogentrada']['imagen']['type']);
               $renameFile  = 'a_'.date('dmY_his').$extencion;
               $targetFile  = $targetPath.$renameFile;
               if(move_uploaded_file($tempFile,$targetFile)){
                   $tipo_imagen = $this->request->data['Blogentrada']['imagen']['type'];
                   $nombre_imagen = $renameFile;;
                   $this->request->data['Blogentrada']['carpeta_imagen'] = $storeFolder;
                   $this->request->data['Blogentrada']['nombre_imagen']  = $nombre_imagen;
                   $this->request->data['Blogentrada']['tipo_imagen']    = $tipo_imagen;
                   $this->request->data['Blogentrada']['ruta_imagen']    = $storeFolder.'/'.$nombre_imagen;
               }else{
                   $this->request->data['Blogentrada']['carpeta_imagen']  = "_";
                   $this->request->data['Blogentrada']['nombre_imagen']   = "_";
                   $this->request->data['Blogentrada']['tipo_imagen']     = "_";
                   $this->request->data['Blogentrada']['ruta_imagen'] = "_";
               }

           }else{
                   $this->request->data['Blogentrada']['carpeta_imagen']  = "_";
                   $this->request->data['Blogentrada']['nombre_imagen']   = "_";
                   $this->request->data['Blogentrada']['tipo_imagen']     = "_";
                   $this->request->data['Blogentrada']['ruta_imagen'] = "_";
           }
			$this->Blogentrada->create();
			$user_id = $this->Session->read('USUARIO_ID');
			$this->request->data['Blogentrada']['user_id'] = $user_id;
			if ($this->Blogentrada->save($this->request->data)) {
				$this->Flash->success(__('The blogentrada has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The blogentrada could not be saved. Please, try again.'));
			}
		}
		$blogcategorias = $this->Blogentrada->Blogcategoria->find('list');
		$users = $this->Blogentrada->User->find('list');
		$this->set(compact('blogcategorias', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Blogentrada->exists($id)) {
			throw new NotFoundException(__('Invalid blogentrada'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if (!empty($this->request->data['Blogentrada']['imagen']) && imagenType($this->request->data['Blogentrada']['imagen']['type']) != false) {
	               $ds = '/';
	               $storeFolder = 'upload';
	               $tempFile    = $this->request->data['Blogentrada']['imagen']['tmp_name'];
	               $targetPath  = ROOT . $ds .'app'. $ds .'webroot'. $ds .'img'. $ds . $storeFolder . $ds;
	               $extencion   = imagenType($this->request->data['Blogentrada']['imagen']['type']);
	               $renameFile  = 'a_'.date('dmY_his').$extencion;
	               $targetFile  = $targetPath.$renameFile;
	               if(move_uploaded_file($tempFile,$targetFile)){
	                   $tipo_imagen = $this->request->data['Blogentrada']['imagen']['type'];
	                   $nombre_imagen = $renameFile;;
	                   $this->request->data['Blogentrada']['carpeta_imagen'] = $storeFolder;
	                   $this->request->data['Blogentrada']['nombre_imagen']  = $nombre_imagen;
	                   $this->request->data['Blogentrada']['tipo_imagen']    = $tipo_imagen;
	                   $this->request->data['Blogentrada']['ruta_imagen']    = $storeFolder.'/'.$nombre_imagen;
	               }else{
	                   $this->request->data['Blogentrada']['carpeta_imagen']  = "_";
	                   $this->request->data['Blogentrada']['nombre_imagen']   = "_";
	                   $this->request->data['Blogentrada']['tipo_imagen']     = "_";
	                   $this->request->data['Blogentrada']['ruta_imagen'] = "_";
	               }

	           }else{
	                   $this->request->data['Blogentrada']['carpeta_imagen']  = "_";
	                   $this->request->data['Blogentrada']['nombre_imagen']   = "_";
	                   $this->request->data['Blogentrada']['tipo_imagen']     = "_";
	                   $this->request->data['Blogentrada']['ruta_imagen'] = "_";
	           }
			if ($this->Blogentrada->save($this->request->data)) {
				$this->Flash->success(__('The blogentrada has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The blogentrada could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Blogentrada.' . $this->Blogentrada->primaryKey => $id));
			$this->request->data = $this->Blogentrada->find('first', $options);
		}
		$blogcategorias = $this->Blogentrada->Blogcategoria->find('list');
		$users = $this->Blogentrada->User->find('list');
		$this->set(compact('blogcategorias', 'users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Blogentrada->id = $id;
		if (!$this->Blogentrada->exists()) {
			throw new NotFoundException(__('Invalid blogentrada'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Blogentrada->delete()) {
			$this->Flash->success(__('The blogentrada has been deleted.'));
		} else {
			$this->Flash->error(__('The blogentrada could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
