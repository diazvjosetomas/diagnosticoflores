<?php
App::uses('AppController', 'Controller');
/**
 * Promociones Controller
 *
 * @property Promocione $Promocione
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class PromocionesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');
	public $layout = 'gentella';

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(47);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Promocione->recursive = 0;
		//$this->set('promociones', $this->Paginator->paginate());
		  $this->set('promociones', $this->Promocione->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Promocione->exists($id)) {
			throw new NotFoundException(__('Invalid promocione'));
		}
		$options = array('conditions' => array('Promocione.' . $this->Promocione->primaryKey => $id));
		$this->set('promocione', $this->Promocione->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Promocione->create();
			if ($this->Promocione->save($this->request->data)) {
				
				$this->Flash->success(__('The promocione has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The promocione could not be saved. Please, try again.'));
			}
		}
		$periodos = $this->Promocione->Periodo->find('list');
		$publicos = $this->Promocione->Publico->find('list');
		$this->set(compact('periodos', 'publicos'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Promocione->exists($id)) {
			throw new NotFoundException(__('Invalid promocione'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Promocione->save($this->request->data)) {
				$this->Flash->success(__('The promocione has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The promocione could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Promocione.' . $this->Promocione->primaryKey => $id));
			$this->request->data = $this->Promocione->find('first', $options);
		}
		$periodos = $this->Promocione->Periodo->find('list');
		$publicos = $this->Promocione->Publico->find('list');
		$this->set(compact('periodos', 'publicos'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Promocione->id = $id;
		if (!$this->Promocione->exists()) {
			throw new NotFoundException(__('Invalid promocione'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Promocione->delete()) {
			$this->Flash->success(__('The promocione has been deleted.'));
		} else {
			$this->Flash->error(__('The promocione could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
