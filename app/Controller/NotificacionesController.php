<?php
App::uses('AppController', 'Controller');
/**
 * Notificaciones Controller
 *
 * @property Notificacione $Notificacione
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class NotificacionesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $uses = array('Contacto','Notificacione','Tipocliente','Suscribirse','Cliente','Facturacione','Cita');
	public $components = array('Paginator', 'Session','Flash');
	public $layout = 'gentella';
/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


/* public function beforeFilter() {
	$this->checkSession();		
} */


/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Notificacione->recursive = 0;
		//$this->set('notificaciones', $this->Paginator->paginate());
		  $this->set('notificaciones', $this->Notificacione->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Notificacione->exists($id)) {
			throw new NotFoundException(__('Invalid notificacione'));
		}
		$options = array('conditions' => array('Notificacione.' . $this->Notificacione->primaryKey => $id));
		$this->set('notificacione', $this->Notificacione->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Notificacione->create();
			if ($this->Notificacione->save($this->request->data)) {
				
				$this->Flash->success(__('The notificacione has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The notificacione could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Notificacione->exists($id)) {
			throw new NotFoundException(__('Invalid notificacione'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Notificacione->save($this->request->data)) {
				$this->Flash->success(__('The notificacione has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The notificacione could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Notificacione.' . $this->Notificacione->primaryKey => $id));
			$this->request->data = $this->Notificacione->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Notificacione->id = $id;
		if (!$this->Notificacione->exists()) {
			throw new NotFoundException(__('Invalid notificacione'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Notificacione->delete()) {
			$this->Flash->success(__('The notificacione has been deleted.'));
		} else {
			$this->Flash->error(__('The notificacione could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}


	public function notificaciones(){
		$this->layout = 'ajax';
		$this->set('notificaciones', $this->Notificacione->find('all'));
		

		$not = $this->Notificacione->find('all');

		foreach ($not as $key) {
			$modelo = ucwords(substr($key['Notificacione']['nombretabla'],0,-1));
			$sql[$key['Notificacione']['nombretabla']] = $this->$modelo->find('count');
		}

		$this->set('sql',$sql);

		$notificaciones2 = $this->Notificacione->find('all');
		foreach ($notificaciones2 as $key) {
			if ($key['Notificacione']['registros'] != $sql[$key['Notificacione']['nombretabla']]) {
				$this->Notificacione->query('UPDATE notificaciones SET registros = '.$sql[$key['Notificacione']['nombretabla']].' WHERE notificaciones.id = '.$key['Notificacione']['id']);
			}
		}

		
	}
}
