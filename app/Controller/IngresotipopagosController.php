<?php
App::uses('AppController', 'Controller');
/**
 * Ingresotipopagos Controller
 *
 * @property Ingresotipopago $Ingresotipopago
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class IngresotipopagosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */

var $layout = "gentella";

 public function beforeFilter() {
	$this->checkSession(45);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Ingresotipopago->recursive = 0;
		//$this->set('ingresotipopagos', $this->Paginator->paginate());
		  $this->set('ingresotipopagos', $this->Ingresotipopago->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Ingresotipopago->exists($id)) {
			throw new NotFoundException(__('Invalid ingresotipopago'));
		}
		$options = array('conditions' => array('Ingresotipopago.' . $this->Ingresotipopago->primaryKey => $id));
		$this->set('ingresotipopago', $this->Ingresotipopago->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Ingresotipopago->create();
			if ($this->Ingresotipopago->save($this->request->data)) {
				
				$this->Flash->success(__('The ingresotipopago has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ingresotipopago could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Ingresotipopago->exists($id)) {
			throw new NotFoundException(__('Invalid ingresotipopago'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Ingresotipopago->save($this->request->data)) {
				$this->Flash->success(__('The ingresotipopago has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ingresotipopago could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Ingresotipopago.' . $this->Ingresotipopago->primaryKey => $id));
			$this->request->data = $this->Ingresotipopago->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Ingresotipopago->id = $id;
		if (!$this->Ingresotipopago->exists()) {
			throw new NotFoundException(__('Invalid ingresotipopago'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Ingresotipopago->delete()) {
			$this->Flash->success(__('The ingresotipopago has been deleted.'));
		} else {
			$this->Flash->error(__('The ingresotipopago could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
