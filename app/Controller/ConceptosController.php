<?php
App::uses('AppController', 'Controller');
/**
 * Conceptos Controller
 *
 * @property Concepto $Concepto
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ConceptosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(26);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = 'gentella';
		//$this->Concepto->recursive = 0;
		//$this->set('conceptos', $this->Paginator->paginate());
		$this->set('conceptos', $this->Concepto->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->layout = 'gentella';
		if (!$this->Concepto->exists($id)) {
			throw new NotFoundException(__('Invalid concepto'));
		}
		$options = array('conditions' => array('Concepto.' . $this->Concepto->primaryKey => $id));
		$this->set('concepto', $this->Concepto->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = 'gentella';
		if ($this->request->is('post')) {
			$this->Concepto->create();
			if ($this->Concepto->save($this->request->data)) {
				
				$this->Flash->success(__('The concepto has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The concepto could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout = 'gentella';
		if (!$this->Concepto->exists($id)) {
			throw new NotFoundException(__('Invalid concepto'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Concepto->save($this->request->data)) {
				$this->Flash->success(__('The concepto has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The concepto could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Concepto.' . $this->Concepto->primaryKey => $id));
			$this->request->data = $this->Concepto->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->layout = 'gentella';
		$this->Concepto->id = $id;
		if (!$this->Concepto->exists()) {
			throw new NotFoundException(__('Invalid concepto'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Concepto->delete()) {
			$this->Flash->success(__('The concepto has been deleted.'));
		} else {
			$this->Flash->error(__('The concepto could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
