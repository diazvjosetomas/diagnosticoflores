<?php
App::uses('AppController', 'Controller');
/**
 * Citas Controller
 *
 * @property Cita $Cita
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CitasadminController extends AppController {


	//var $uses = array('Laborable','Cita');

	public $uses = array('Feriado','Blogentrada','Configurations','Contenido','Servicio','Contacto','Slider','Doctor', 'Avisopriva','Imghorario','Facturacione','Empresa','User', 'Blogpublicidade','Sucursales','Suscribirse','Laborable','Cita','Tipocita','Categoriacita','Asignacita','Nota','Statu');

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(19);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
 		  $this->layout = 'gentella';
		  $this->Cita->recursive = 2;
		  
		  $this->set('citas', $this->Cita->find('all'));
		  $this->set('asignacitas', $this->Asignacita->find('all'));
		  $this->set('notas', $this->Nota->find('all'));
	}

/**
 * citas method
 *
 * @return void
 */
	public function citas() {
 		  $this->layout = 'citas';
 		  $this->set('sucursal', $this->Sucursales->find('all'));  
 		  $this->set('feriados', $this->Feriado->find('all'));
 		  $this->set('configurations', $this->Configurations->find('all'));
 		  $this->set('horarios',  $this->Contenido->find('all',array('conditions'=>array('categoria_id'=>array('6','10','11')))));
 		  $this->set('servicio',  $this->Servicio->find('all',array('conditions'=>array('estatus'=>'1'))) );
 		  $this->set('nosotros',  $this->Contenido->find('all',array('conditions'=>array('categoria_id'=>'8') )) );
 		  $this->set('direccion', $this->Contenido->find('all',array('conditions'=>array('categoria_id'=>array('9','12','13') )) ));

 		  $this->set('tipoclientes', $this->User->Tipocliente->find('list'));
 		  $this->set('avisoprivas', $this->Avisopriva->find('all'));        
 		  $this->set('sucursal', $this->Sucursales->find('all'));        
 		  $this->set('blog', $this->Blogentrada->find('all'));
 		  $this->set('blog', $this->Paginator->paginate());
 		  $this->set('imghorarios', $this->Imghorario->find('all'));
 		  $this->set('slider', $this->Slider->find('all'));
 		  $this->set('empresa', $this->Empresa->find('all'));
 		  $this->set('dir', 'citas');
		  //$this->Cita->recursive = 0;
		  //$this->set('citas', $this->Paginator->paginate());
		  //$this->set('citas', $this->Cita->find('all'));
	}


/**
 * date method
 *
 * @return void
 */
	public function laborables($date) {
 		  $this->layout = 'ajax';
 		  
 		  $fechats = strtotime($date); 
 		  $dateLaborable = date('w', $fechats);
 		  $dateLaborable++;

 		  $this->set('feriados', $this->Feriado->find('all',array('conditions'=>array('Feriado.diaferiado' => $date))));
 		  $this->set('citas', $this->Cita->find('all',array('conditions'=>array('Cita.fecha' => $date))));
 		  $this->set('laborables', $this->Laborable->find('all',array('conditions'=>array('Laborable.dia' =>$dateLaborable))));
 		  

 		 // echo json_encode(array('data'=>$this->Laborable->find('all')));



		  //$this->Cita->recursive = 0;
		  //$this->set('citas', $this->Paginator->paginate());
		  //$this->set('citas', $this->Cita->find('all'));
	}



/**
 * date method
 *
 * @return void
 */
	public function feriados() {
 		  $this->layout = 'ajax';
 		  $this->set('feriados', $this->Feriado->find('all'));
 		  

 		 // echo json_encode(array('data'=>$this->Laborable->find('all')));



		  //$this->Cita->recursive = 0;
		  //$this->set('citas', $this->Paginator->paginate());
		  //$this->set('citas', $this->Cita->find('all'));
	}	


/**
 * date method
 *
 * @return void
 */
	public function tipocitas($id = null) {
 		  $this->layout = 'ajax';

 		  
 		  $this->set('tipocitas', $this->Tipocita->find('all', array('conditions'=>array('sucursale_id'=>$id)))); 		  
 		  $this->set('categoriacitas', $this->Categoriacita->find('all'));
 		  
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->layout = 'gentella';
		if (!$this->Cita->exists($id)) {
			throw new NotFoundException(__('Invalid cita'));
		}
		$options = array('conditions' => array('Cita.' . $this->Cita->primaryKey => $id));
		$this->set('cita', $this->Cita->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = 'gentella';
		if ($this->request->is('post')) {
			$this->Cita->create();
			if ($this->Cita->save($this->request->data)) {
				

				$correo = $this->request->data['Cita']['correo'];


				// $email = $this->Cita->find('all',array('conditions'=>array('id'=>$id)));

				// $email_send = $email[0]['Cita']['correo'];

				$nombre   = $this->request->data['Cita']['nombres'];
				$nombres = strtolower(ucwords($nombre));
				//$correo   = 'diazvjosetomas@gmail.com';
				
				// Cabecera
				$cabeceras  = "MIME-Version: 1.0\r\n";
				$cabeceras .= "Content-Transfer-Encoding: 8Bit\r\n";
				$cabeceras .= "Content-Type: text/html; charset=\"utf-8\"\r\n";
				$cabeceras .= "Reply-to: \r\n";
				$cabeceras .= "From: DE \r\n";
				$cabeceras .= "Errors-To: \r\n";

				// ----------------------------------------------------------------------
				// Cuerpo
				$cuerpo = "Su cita fue agendada correctamente. <br><br>\r\n";

				// ----------------------------------------------------------------------
				// Envio
				// Correo, Titulo, Cuerpo, Cabecera
				mail( "$nombre <$correo>", "Cita agendada. ", $cuerpo, $cabeceras );








				
				$this->Flash->success(__('La cita fue agendada'.$correo));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('La cita no se pudo agendar.'));
			}
		}
		$tipocitas = $this->Cita->Tipocita->find('list');
		$this->set(compact('tipocitas'));

		$sucursales = $this->Cita->Sucursale->find('list');
		$this->set(compact('sucursales'));
	}


/**
 * add method
 *
 * @return void
 */
	public function add_2() {
		$this->layout = 'gentella';
		if ($this->request->is('post')) {
			$this->Cita->create();
			if ($this->Cita->save($this->request->data)) {


				$correo = $this->request->data['Cita']['correo'];


				// $email = $this->Cita->find('all',array('conditions'=>array('id'=>$id)));

				// $email_send = $email[0]['Cita']['correo'];

				$nombre   = $this->request->data['Cita']['nombres'];
				$nombres = strtolower(ucwords($nombre));
				//$correo   = 'diazvjosetomas@gmail.com';
				
				// Cabecera
				$cabeceras  = "MIME-Version: 1.0\r\n";
				$cabeceras .= "Content-Transfer-Encoding: 8Bit\r\n";
				$cabeceras .= "Content-Type: text/html; charset=\"utf-8\"\r\n";
				$cabeceras .= "Reply-to: \r\n";
				$cabeceras .= "From: soporte@diagnosticoflores.com \r\n";
				$cabeceras .= "Errors-To: \r\n";

				// ----------------------------------------------------------------------
				// Cuerpo				
				$cuerpo .= "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>\r\n";
				$cuerpo .= "<html xmlns='http://www.w3.org/1999/xhtml'>\r\n";
				$cuerpo .= "<head>\r\n";
				$cuerpo .= "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />\r\n";
				$cuerpo .= "<title>Notificacion de Cita</title>\r\n";
				$cuerpo .= "<style type='text/css'> \r\n";
				$cuerpo .= "body {margin: 0; padding: 0; min-width: 100%!important;}\r\n";
				$cuerpo .= ".content {width: 100%; max-width: 600px;}</style> </head>  \r\n";
				$cuerpo .= "<style type='text/css'>";
				$cuerpo .= "			    
						    @media only screen and (min-device-width: 601px) {
						    .content {width: 800px !important;}
						    }

						    .header {padding: 40px 10px 20px 10px;}


						    .footer {padding: 20px 30px 15px 30px;}
						    .footercopy {font-family: sans-serif; font-size: 14px; color: #ffffff;}
						    .footercopy a {color: #ffffff; text-decoration: underline;}

						    @media only screen and (max-width: 550px), screen and (max-device-width: 550px) {
						    body[yahoo] .buttonwrapper {background-color: transparent!important;}
						    body[yahoo] .button a {background-color: #e05443; padding: 15px 15px 13px!important; display: block!important;}
						    }
						    </style>\r\n";
				$cuerpo .= "    <body yahoo bgcolor='#f6f8f1'>
				  
				                    <table class='content' align='center' cellpadding='0' cellspacing='0' border='0'>
				                        <tr>
				                            <td class='header' bgcolor='#01B9AF'>
				                                
				                               <table width='70' align='left' border='0' cellpadding='0' cellspacing='0'>
				                                   <tr>
				                                       <td height='70' style='padding: 0 10px 10px 0;'>
				                                           <h1 style='color:white;'>Quamtun.</h1>
				                                           <h5 style='color:white;'>Cita Agendada</h5>
				                                       </td>
				                                   </tr>
				                               </table>     


				                            </td>
				                        </tr>

				                        <tr>
				                            <td class='innerpadding borderbottom'>
				                            	<br>
				                            	<br>
				                            	<br>
				                                <table width='100%'' border='0' cellspacing='0' cellpadding='0'>
				                                    <tr>
				                                        <td class='h2'>
				                                           Estimado(a), $nombre
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class='bodycopy'>
				                                            Su cita fue agendada correctamente.
				                                        </td>
				                                    </tr>
				                                </table>
				                            </td>
				                        </tr>
				                    </table>

				    </body>
				</html>\r\n";

				// ----------------------------------------------------------------------
				// Envio
				// Correo, Titulo, Cuerpo, Cabecera
				mail( "$nombre <$correo>", "Cita agendada. ", $cuerpo, $cabeceras );
				
				
				$this->Session->write('CITAENAGENDA', '1');
				return $this->redirect('/Sites/index');
			} else {
				$this->Flash->error(__('The cita could not be saved. Please, try again.'));
			}
		}


		$tipocitas = $this->Cita->Tipocita->find('list');
		$this->set(compact('tipocitas'));

		$sucursales = $this->Cita->Sucursale->find('list');
		$this->set(compact('sucursales'));

	}





/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout = 'gentella';
		if (!$this->Cita->exists($id)) {
			throw new NotFoundException(__('Invalid cita'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Cita->save($this->request->data)) {
				$this->Flash->success(__('The cita has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The cita could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Cita.' . $this->Cita->primaryKey => $id));
			$this->request->data = $this->Cita->find('first', $options);
		}
		$tipocitas = $this->Cita->Tipocita->find('list');
		$this->set(compact('tipocitas'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->layout = 'gentella';
		$this->Cita->id = $id;
		if (!$this->Cita->exists()) {
			throw new NotFoundException(__('Invalid cita'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Cita->delete()) {
			$this->Flash->success(__('The cita has been deleted.'));
		} else {
			$this->Flash->error(__('The cita could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}



	/**
	 * recordatorio de cita
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
		public function recordatoriocita($id = null) {

			$hoy = date('Y-m-j');
			$fecha = date('Y-m-j');
			$nuevafecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;
			$nuevafecha = date ( 'Y-m-j' , $nuevafecha );

			$this->layout = 'gentella';
			$this->set('citas', $this->Cita->find('all',array('conditions'=>array("Cita.fecha BETWEEN '".$hoy."' AND '".$nuevafecha."'"))));

		}
}
