<?php
App::uses('AppController', 'Controller');
/**
 * Ingresofolios Controller
 *
 * @property Ingresofolio $Ingresofolio
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class IngresofoliosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');


var $layout = "gentella";

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(61);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Ingresofolio->recursive = 0;
		//$this->set('ingresofolios', $this->Paginator->paginate());
		  $this->set('ingresofolios', $this->Ingresofolio->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Ingresofolio->exists($id)) {
			throw new NotFoundException(__('Invalid ingresofolio'));
		}
		$options = array('conditions' => array('Ingresofolio.' . $this->Ingresofolio->primaryKey => $id));
		$this->set('ingresofolio', $this->Ingresofolio->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Ingresofolio->create();
			if ($this->Ingresofolio->save($this->request->data)) {
				
				$this->Flash->success(__('The ingresofolio has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ingresofolio could not be saved. Please, try again.'));
			}
		}
		$sucursales = $this->Ingresofolio->Sucursale->find('list');
		$this->set(compact('sucursales'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Ingresofolio->exists($id)) {
			throw new NotFoundException(__('Invalid ingresofolio'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Ingresofolio->save($this->request->data)) {
				$this->Flash->success(__('The ingresofolio has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ingresofolio could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Ingresofolio.' . $this->Ingresofolio->primaryKey => $id));
			$this->request->data = $this->Ingresofolio->find('first', $options);
		}
		$sucursales = $this->Ingresofolio->Sucursale->find('list');
		$this->set(compact('sucursales'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Ingresofolio->id = $id;
		if (!$this->Ingresofolio->exists()) {
			throw new NotFoundException(__('Invalid ingresofolio'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Ingresofolio->delete()) {
			$this->Flash->success(__('The ingresofolio has been deleted.'));
		} else {
			$this->Flash->error(__('The ingresofolio could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
