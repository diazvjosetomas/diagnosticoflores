<?php
App::uses('AppController', 'Controller');
/**
 * Adjuntos Controller
 *
 * @property Adjunto $Adjunto
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class AdjuntosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');
	public $layout = 'gentella';
	public $uses = array('Adjunto','Imgadjunto','Ingresotipo');
/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(71);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Adjunto->recursive = 0;
		//$this->set('adjuntos', $this->Paginator->paginate());
		  $this->layout = 'gentella';
		  $this->set('adjuntos', $this->Adjunto->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Adjunto->recursive = 2;
		if (!$this->Adjunto->exists($id)) {
			throw new NotFoundException(__('Invalid adjunto'));
		}
		$options = array('conditions' => array('Adjunto.' . $this->Adjunto->primaryKey => $id));
		$this->set('adjunto', $this->Adjunto->find('first', $options));
		$this->set('imgadjunto', $this->Imgadjunto->find('all', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Adjunto->create();
			if ($this->Adjunto->save($this->request->data)) {
				
				$this->Flash->success(__('The adjunto has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The adjunto could not be saved. Please, try again.'));
			}
		}
		$users = $this->Adjunto->User->find('list');
		$clientes = $this->Adjunto->Cliente->find('list');
		$ingresotipos = $this->Adjunto->Ingresotipo->find('list');
		$this->set(compact('users', 'clientes', 'ingresotipos'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Adjunto->exists($id)) {
			throw new NotFoundException(__('Invalid adjunto'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Adjunto->save($this->request->data)) {
				$this->Flash->success(__('The adjunto has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The adjunto could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Adjunto.' . $this->Adjunto->primaryKey => $id));
			$this->request->data = $this->Adjunto->find('first', $options);
		}
		$users = $this->Adjunto->User->find('list');
		$clientes = $this->Adjunto->Cliente->find('list');
		$ingresotipos = $this->Adjunto->Ingresotipootipo->find('list');
		$this->set(compact('users', 'clientes', 'ingresotipos'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Adjunto->id = $id;
		if (!$this->Adjunto->exists()) {
			throw new NotFoundException(__('Invalid adjunto'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Adjunto->delete()) {
			$this->Flash->success(__('The adjunto has been deleted.'));
		} else {
			$this->Flash->error(__('The adjunto could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
