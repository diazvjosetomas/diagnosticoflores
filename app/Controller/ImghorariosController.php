<?php
App::uses('AppController', 'Controller');
/**
 * Imghorarios Controller
 *
 * @property Imghorario $Imghorario
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ImghorariosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

	var $layout = "gentella";

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(50);		
}


/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Imghorario->recursive = 0;
		//$this->set('imghorarios', $this->Paginator->paginate());
		$this->set('imghorarios', $this->Imghorario->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Imghorario->exists($id)) {
			throw new NotFoundException(__('Invalid imghorario'));
		}
		$options = array('conditions' => array('Imghorario.' . $this->Imghorario->primaryKey => $id));
		$this->set('imghorario', $this->Imghorario->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Imghorario->create();
				if (!empty($this->request->data['Imghorario']['imagen']) && imagenType($this->request->data['Imghorario']['imagen']['type']) != false) {
	               $ds = '/';
	               $storeFolder = 'upload';
	               $tempFile    = $this->request->data['Imghorario']['imagen']['tmp_name'];
	               $targetPath  = ROOT . $ds .'app'. $ds .'webroot'. $ds .'img'. $ds . $storeFolder . $ds;
	               $extencion   = imagenType($this->request->data['Imghorario']['imagen']['type']);
	               $renameFile  = 'a_'.date('dmY_his').$extencion;
	               $targetFile  = $targetPath.$renameFile;
	               if(move_uploaded_file($tempFile,$targetFile)){
	                   $tipo_imagen = $this->request->data['Imghorario']['imagen']['type'];
	                   $nombre_imagen = $renameFile;;
	                   $this->request->data['Imghorario']['carpeta_imagen'] = $storeFolder;
	                   $this->request->data['Imghorario']['nombre_imagen']  = $nombre_imagen;
	                   $this->request->data['Imghorario']['tipo_imagen']    = $tipo_imagen;
	                   $this->request->data['Imghorario']['ruta_imagen']    = $storeFolder.'/'.$nombre_imagen;
	               }else{
	                   $this->request->data['Imghorario']['carpeta_imagen']  = "_";
	                   $this->request->data['Imghorario']['nombre_imagen']   = "_";
	                   $this->request->data['Imghorario']['tipo_imagen']     = "_";
	                   $this->request->data['Imghorario']['ruta_imagen'] = "_";
	               }

	           }else{
	                   $this->request->data['Imghorario']['carpeta_imagen']  = "_";
	                   $this->request->data['Imghorario']['nombre_imagen']   = "_";
	                   $this->request->data['Imghorario']['tipo_imagen']     = "_";
	                   $this->request->data['Imghorario']['ruta_imagen'] = "_";
	           }
			if ($this->Imghorario->save($this->request->data)) {
				
				$this->Flash->success(__('The imghorario has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The imghorario could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Imghorario->exists($id)) {
			throw new NotFoundException(__('Invalid imghorario'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if (!empty($this->request->data['Imghorario']['imagen']) && imagenType($this->request->data['Imghorario']['imagen']['type']) != false) {
               $ds = '/';
               $storeFolder = 'upload';
               $tempFile    = $this->request->data['Imghorario']['imagen']['tmp_name'];
               $targetPath  = ROOT . $ds .'app'. $ds .'webroot'. $ds .'img'. $ds . $storeFolder . $ds;
               $extencion   = imagenType($this->request->data['Imghorario']['imagen']['type']);
               $renameFile  = 'a_'.date('dmY_his').$extencion;
               $targetFile  = $targetPath.$renameFile;
               if(move_uploaded_file($tempFile,$targetFile)){
                   $tipo_imagen = $this->request->data['Imghorario']['imagen']['type'];
                   $nombre_imagen = $renameFile;;
                   $this->request->data['Imghorario']['carpeta_imagen'] = $storeFolder;
                   $this->request->data['Imghorario']['nombre_imagen']  = $nombre_imagen;
                   $this->request->data['Imghorario']['tipo_imagen']    = $tipo_imagen;
                   $this->request->data['Imghorario']['ruta_imagen']    = $storeFolder.'/'.$nombre_imagen;
               }else{
                   $this->request->data['Imghorario']['carpeta_imagen']  = "_";
                   $this->request->data['Imghorario']['nombre_imagen']   = "_";
                   $this->request->data['Imghorario']['tipo_imagen']     = "_";
                   $this->request->data['Imghorario']['ruta_imagen'] = "_";
               }

            }else{
                   $this->request->data['Imghorario']['carpeta_imagen']  = "_";
                   $this->request->data['Imghorario']['nombre_imagen']   = "_";
                   $this->request->data['Imghorario']['tipo_imagen']     = "_";
                   $this->request->data['Imghorario']['ruta_imagen'] = "_";
            }
			if ($this->Imghorario->save($this->request->data)) {
				$this->Flash->success(__('The imghorario has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The imghorario could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Imghorario.' . $this->Imghorario->primaryKey => $id));
			$this->request->data = $this->Imghorario->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Imghorario->id = $id;
		if (!$this->Imghorario->exists()) {
			throw new NotFoundException(__('Invalid imghorario'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Imghorario->delete()) {
			$this->Flash->success(__('The imghorario has been deleted.'));
		} else {
			$this->Flash->error(__('The imghorario could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
