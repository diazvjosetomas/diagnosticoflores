<?php
App::uses('AppController', 'Controller');
/**
 * Ingresotipos Controller
 *
 * @property Ingresotipo $Ingresotipo
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class IngresotiposController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


var $layout = "gentella";

 public function beforeFilter() {
	$this->checkSession(44);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Ingresotipo->recursive = 0;
		//$this->set('ingresotipos', $this->Paginator->paginate());
		  $this->set('ingresotipos', $this->Ingresotipo->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Ingresotipo->exists($id)) {
			throw new NotFoundException(__('Invalid ingresotipo'));
		}
		$options = array('conditions' => array('Ingresotipo.' . $this->Ingresotipo->primaryKey => $id));
		$this->set('ingresotipo', $this->Ingresotipo->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Ingresotipo->create();
			if ($this->Ingresotipo->save($this->request->data)) {
				
				$this->Flash->success(__('The ingresotipo has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ingresotipo could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Ingresotipo->exists($id)) {
			throw new NotFoundException(__('Invalid ingresotipo'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Ingresotipo->save($this->request->data)) {
				$this->Flash->success(__('The ingresotipo has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ingresotipo could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Ingresotipo.' . $this->Ingresotipo->primaryKey => $id));
			$this->request->data = $this->Ingresotipo->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Ingresotipo->id = $id;
		if (!$this->Ingresotipo->exists()) {
			throw new NotFoundException(__('Invalid ingresotipo'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Ingresotipo->delete()) {
			$this->Flash->success(__('The ingresotipo has been deleted.'));
		} else {
			$this->Flash->error(__('The ingresotipo could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
