<?php
App::uses('AppController', 'Controller');
/**
 * Ingresotipomontos Controller
 *
 * @property Ingresotipomonto $Ingresotipomonto
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class IngresotipomontosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


var $layout = "gentella";

 public function beforeFilter() {
	$this->checkSession(62);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Ingresotipomonto->recursive = 0;
		//$this->set('ingresotipomontos', $this->Paginator->paginate());
		  $this->set('ingresotipomontos', $this->Ingresotipomonto->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Ingresotipomonto->exists($id)) {
			throw new NotFoundException(__('Invalid ingresotipomonto'));
		}
		$options = array('conditions' => array('Ingresotipomonto.' . $this->Ingresotipomonto->primaryKey => $id));
		$this->set('ingresotipomonto', $this->Ingresotipomonto->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Ingresotipomonto->create();
			if ($this->Ingresotipomonto->save($this->request->data)) {
				
				$this->Flash->success(__('The ingresotipomonto has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ingresotipomonto could not be saved. Please, try again.'));
			}
		}
		$ingresotipos = $this->Ingresotipomonto->Ingresotipo->find('list');
		$ingresotipopagos = $this->Ingresotipomonto->Ingresotipopago->find('list');
		$this->set(compact('ingresotipos', 'ingresotipopagos'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Ingresotipomonto->exists($id)) {
			throw new NotFoundException(__('Invalid ingresotipomonto'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Ingresotipomonto->save($this->request->data)) {
				$this->Flash->success(__('The ingresotipomonto has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ingresotipomonto could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Ingresotipomonto.' . $this->Ingresotipomonto->primaryKey => $id));
			$this->request->data = $this->Ingresotipomonto->find('first', $options);
		}
		$ingresotipos = $this->Ingresotipomonto->Ingresotipo->find('list');
		$ingresotipopagos = $this->Ingresotipomonto->Ingresotipopago->find('list');
		$this->set(compact('ingresotipos', 'ingresotipopagos'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Ingresotipomonto->id = $id;
		if (!$this->Ingresotipomonto->exists()) {
			throw new NotFoundException(__('Invalid ingresotipomonto'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Ingresotipomonto->delete()) {
			$this->Flash->success(__('The ingresotipomonto has been deleted.'));
		} else {
			$this->Flash->error(__('The ingresotipomonto could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
