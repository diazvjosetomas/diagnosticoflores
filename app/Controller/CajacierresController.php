<?php
App::uses('AppController', 'Controller');
/**
 * Cajacierres Controller
 *
 * @property Cajacierre $Cajacierre
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CajacierresController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

	public $layout = "gentella";

	public $uses = array('Caja','Cajacierre');

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(64);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Cajacierre->recursive = 0;
		//$this->set('cajacierres', $this->Paginator->paginate());
		  $this->set('cajacierres', $this->Cajacierre->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Cajacierre->exists($id)) {
			throw new NotFoundException(__('Invalid cajacierre'));
		}
		$options = array('conditions' => array('Cajacierre.' . $this->Cajacierre->primaryKey => $id));
		$this->set('cajacierre', $this->Cajacierre->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Cajacierre->create();
			$idCaja = $this->request->data['Cajacierre']['caja_id'];
			$this->Caja->query("UPDATE cajas SET status = 0 WHERE cajas.id = '".$idCaja."' ");
			if ($this->Cajacierre->save($this->request->data)) {
				
				$this->Flash->success(__('The cajacierre has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The cajacierre could not be saved. Please, try again.'));
			}
		}
		$cajas      = $this->Cajacierre->Caja->find('list');
		$users      = $this->Cajacierre->User->find('list');
		$sucursales = $this->Cajacierre->Sucursale->find('list');
		$this->set(compact('cajas', 'users', 'sucursales'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Cajacierre->exists($id)) {
			throw new NotFoundException(__('Invalid cajacierre'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Cajacierre->save($this->request->data)) {
				$this->Flash->success(__('The cajacierre has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The cajacierre could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Cajacierre.' . $this->Cajacierre->primaryKey => $id));
			$this->request->data = $this->Cajacierre->find('first', $options);
		}
		$cajas      = $this->Cajacierre->Caja->find('list', array('conditions'=>array('Caja.sucursale_id'=>$this->request->data['Cajacierre']['sucursale_id'], 'Caja.id'=>$this->request->data['Cajacierre']['caja_id'])));
		$users      = $this->Cajacierre->User->find('list', array('conditions'=>array('User.id'=>$this->request->data['Cajacierre']['user_id'])));
		$sucursales = $this->Cajacierre->Sucursale->find('list', array('conditions'=>array('Sucursale.id'=>$this->request->data['Cajacierre']['sucursale_id'])));
		$this->set(compact('cajas', 'users', 'sucursales'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Cajacierre->id = $id;
		if (!$this->Cajacierre->exists()) {
			throw new NotFoundException(__('Invalid cajacierre'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Cajacierre->delete()) {
			$this->Flash->success(__('The cajacierre has been deleted.'));
		} else {
			$this->Flash->error(__('The cajacierre could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}


/**
 * cajas method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function cajas($var1=null){
		$this->layout = "ajax";
		$cajas = $this->Cajacierre->Caja->find('list', array('conditions'=>array('Caja.sucursale_id'=>$var1,'Caja.status'=>1)));
        $this->set('cajas', $cajas);
	}
}
