<?php
App::uses('AppController', 'Controller');
/**
 * Personales Controller
 *
 * @property Personale $Personale
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class PersonalesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');
	public $uses = array('Personale', 'Pago', 'Asistencia','Permiso');

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(17);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = 'gentella';
		$this->set('personales', $this->Personale->find('all'));
		
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->layout = 'gentella';
		if (!$this->Personale->exists($id)) {
			throw new NotFoundException(__('Invalid personale'));
		}
		$options = array('conditions' => array('Personale.' . $this->Personale->primaryKey => $id));
		$this->set('personale', $this->Personale->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = 'gentella';
		if ($this->request->is('post')) {
			$name = 'upload/lotes_'.date("Y-m-d-h-i-s").'_'.$this->request->data["Personale"]["contenido"]["name"];
			$this->request->data["Personale"]["nombre_archivo"] = $name;
			$this->Personale->create();

			if ($this->request->data['Personale']['fecha_ingreso'] == '') {
			 	$this->request->data['Personale']['fecha_ingreso'] = date('Y-m-d');
			 }  




			if ($this->Personale->save($this->request->data)) {
					move_uploaded_file($this->request->data["Personale"]["contenido"]["tmp_name"], $name);
					chmod($name,0777);
			        $this->Flash->success(__('Registro guardado.'));
					return $this->redirect(array('action' => 'index'));

			    }else{
			        $this->Flash->error(__('El personale no ha sido guardado, intenta de nuevo.'));
			    }
			} else {
				$this->Flash->error(__('El personal no ha sido guardado, por favor intenta de nuevo.'));
			}
		
		$sucursales = $this->Personale->Sucursale->find('list');
		$this->set(compact('sucursales'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout = 'gentella';
		if (!$this->Personale->exists($id)) {
			throw new NotFoundException(__('Invalid personale'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$name = 'upload/lotes_'.date("Y-m-d-h-i-s").'_'.$this->request->data["Personale"]["contenido"]["name"];
			$this->request->data["Personale"]["nombre_archivo"] = $name;
			if ($this->Personale->save($this->request->data)) {
				$this->Flash->success(__('The personale has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The personale could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Personale.' . $this->Personale->primaryKey => $id));
			$this->request->data = $this->Personale->find('first', $options);
		}
		$sucursales = $this->Personale->Sucursale->find('list');
		$this->set(compact('sucursales'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->layout = 'gentella';
		$this->Personale->id = $id;
		if (!$this->Personale->exists()) {
			throw new NotFoundException(__('Invalid personale'));
		}
		$this->request->allowMethod('post', 'delete');
		$this->Pago->query('DELETE FROM pagos WHERE personale_id = "'.$id.'"');
		$this->Asistencia->query('DELETE FROM asistencias WHERE personale_id = "'.$id.'"');
		$this->Permiso->query('DELETE FROM permisos WHERE personale_id = "'.$id.'"');

		if ($this->Personale->delete()) {
			$this->Flash->success(__('The personale has been deleted.'));
		} else {
			$this->Flash->error(__('The personale could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
