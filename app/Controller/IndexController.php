<?php class PostsController extends AppController {
    public $helpers = array('Html', 'Form');
    public $name = 'Posts';

    public $uses = array('Post','DataWebSite');

    public $components = array('Flash','Session');


    //=============================================[ function index  ] start!
    public function index() {
         $this->set('posts', $this->Post->find('all'));
         $this->Session->write('DATA_WEB_SITE', $this->DataWebSite->find('all'));
         $this->set('data_web_site', $this->DataWebSite->find('all'));

    }



}
?>