<?php
App::uses('AppController', 'Controller');
/**
 * Asistencias Controller
 *
 * @property Asistencia $Asistencia
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class AsistenciasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(60);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = 'gentella';
		//$this->Asistencia->recursive = 0;
		//$this->set('asistencias', $this->Paginator->paginate());
		$this->set('asistencias', $this->Asistencia->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->layout = 'gentella';
		if (!$this->Asistencia->exists($id)) {
			throw new NotFoundException(__('Invalid asistencia'));
		}
		$options = array('conditions' => array('Asistencia.' . $this->Asistencia->primaryKey => $id));
		$this->set('asistencia', $this->Asistencia->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = 'gentella';
		if ($this->request->is('post')) {
			$this->Asistencia->create();
			if ($this->Asistencia->save($this->request->data)) {
				
				$this->Flash->success(__('The asistencia has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The asistencia could not be saved. Please, try again.'));
			}
		}
		$personales = $this->Asistencia->Personale->find('list');
		$tipoentradas = $this->Asistencia->Tipoentrada->find('list');
		$this->set(compact('personales', 'tipoentradas'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout = 'gentella';
		if (!$this->Asistencia->exists($id)) {
			throw new NotFoundException(__('Invalid asistencia'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Asistencia->save($this->request->data)) {
				$this->Flash->success(__('The asistencia has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The asistencia could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Asistencia.' . $this->Asistencia->primaryKey => $id));
			$this->request->data = $this->Asistencia->find('first', $options);
		}
		$personales = $this->Asistencia->Personale->find('list');
		$tipoentradas = $this->Asistencia->Tipoentrada->find('list');
		$this->set(compact('personales', 'tipoentradas'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->layout = 'gentella';
		$this->Asistencia->id = $id;
		if (!$this->Asistencia->exists()) {
			throw new NotFoundException(__('Invalid asistencia'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Asistencia->delete()) {
			$this->Flash->success(__('The asistencia has been deleted.'));
		} else {
			$this->Flash->error(__('The asistencia could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
