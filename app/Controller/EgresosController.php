<?php
App::uses('AppController', 'Controller');
/**
 * Egresos Controller
 *
 * @property Egreso $Egreso
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class EgresosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

var $layout = "gentella";

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(10);		
}



/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Egreso->recursive = 0;
		//$this->set('egresos', $this->Paginator->paginate());
		$this->set('egresos', $this->Egreso->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Egreso->exists($id)) {
			throw new NotFoundException(__('Invalid egreso'));
		}
		$options = array('conditions' => array('Egreso.' . $this->Egreso->primaryKey => $id));
		$this->set('egreso', $this->Egreso->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Egreso->create();
			if ($this->Egreso->save($this->request->data)) {
				
				$this->Flash->success(__('The egreso has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The egreso could not be saved. Please, try again.'));
			}
		}
		$egresotipos = $this->Egreso->Egresotipo->find('list');
		$proveedor_pagos = $this->Egreso->ProveedorPago->find('list');
		$sucursales = $this->Egreso->Sucursale->find('list');
		$this->set(compact('egresotipos', 'proveedor_pagos','sucursales'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Egreso->exists($id)) {
			throw new NotFoundException(__('Invalid egreso'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Egreso->save($this->request->data)) {
				$this->Flash->success(__('The egreso has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The egreso could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Egreso.' . $this->Egreso->primaryKey => $id));
			$this->request->data = $this->Egreso->find('first', $options);
		}
		$egresotipos = $this->Egreso->Egresotipo->find('list');
		$proveedor_pagos = $this->Egreso->ProveedorPago->find('list');
		$sucursales = $this->Egreso->Sucursale->find('list');
		$this->set(compact('egresotipos', 'proveedor_pagos','sucursales'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Egreso->id = $id;
		if (!$this->Egreso->exists()) {
			throw new NotFoundException(__('Invalid egreso'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Egreso->delete()) {
			$this->Flash->success(__('The egreso has been deleted.'));
		} else {
			$this->Flash->error(__('The egreso could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
