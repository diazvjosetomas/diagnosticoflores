<?php
App::uses('AppController', 'Controller');
/**
 * Blogpublicidades Controller
 *
 * @property Blogpublicidade $Blogpublicidade
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class BlogpublicidadesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

	var $layout = "gentella";

/**
 * function beforeFilter(){
 *	funcion para chequear la sesion de los usuarios
 *  @return void
 * }
 *
 */


 public function beforeFilter() {
	$this->checkSession(20);		
} 


/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Blogpublicidade->recursive = 0;
		//$this->set('blogpublicidades', $this->Paginator->paginate());
		$this->set('blogpublicidades', $this->Blogpublicidade->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Blogpublicidade->exists($id)) {
			throw new NotFoundException(__('Invalid blogpublicidade'));
		}
		$options = array('conditions' => array('Blogpublicidade.' . $this->Blogpublicidade->primaryKey => $id));
		$this->set('blogpublicidade', $this->Blogpublicidade->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Blogpublicidade->create();
			if (!empty($this->request->data['Blogpublicidade']['imagen']) && imagenType($this->request->data['Blogpublicidade']['imagen']['type']) != false) {
	               $ds = '/';
	               $storeFolder = 'upload';
	               $tempFile    = $this->request->data['Blogpublicidade']['imagen']['tmp_name'];
	               $targetPath  = ROOT . $ds .'app'. $ds .'webroot'. $ds .'img'. $ds . $storeFolder . $ds;
	               $extencion   = imagenType($this->request->data['Blogpublicidade']['imagen']['type']);
	               $renameFile  = 'a_'.date('dmY_his').$extencion;
	               $targetFile  = $targetPath.$renameFile;
	               if(move_uploaded_file($tempFile,$targetFile)){
	                   $tipo_imagen = $this->request->data['Blogpublicidade']['imagen']['type'];
	                   $nombre_imagen = $renameFile;;
	                   $this->request->data['Blogpublicidade']['carpeta_imagen'] = $storeFolder;
	                   $this->request->data['Blogpublicidade']['nombre_imagen']  = $nombre_imagen;
	                   $this->request->data['Blogpublicidade']['tipo_imagen']    = $tipo_imagen;
	                   $this->request->data['Blogpublicidade']['ruta_imagen']    = $storeFolder.'/'.$nombre_imagen;
	               }else{
	                   $this->request->data['Blogpublicidade']['carpeta_imagen']  = "_";
	                   $this->request->data['Blogpublicidade']['nombre_imagen']   = "_";
	                   $this->request->data['Blogpublicidade']['tipo_imagen']     = "_";
	                   $this->request->data['Blogpublicidade']['ruta_imagen'] = "_";
	               }

	           }else{
	                   $this->request->data['Blogpublicidade']['carpeta_imagen']  = "_";
	                   $this->request->data['Blogpublicidade']['nombre_imagen']   = "_";
	                   $this->request->data['Blogpublicidade']['tipo_imagen']     = "_";
	                   $this->request->data['Blogpublicidade']['ruta_imagen'] = "_";
	           }
			if ($this->Blogpublicidade->save($this->request->data)) {
				
				$this->Flash->success(__('La publicidad ha sido añadido correctamente.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('La publicidad no pudo ser añadida.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Blogpublicidade->exists($id)) {
			throw new NotFoundException(__('Invalid blogpublicidade'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if (!empty($this->request->data['Blogpublicidade']['imagen']) && imagenType($this->request->data['Blogpublicidade']['imagen']['type']) != false) {
               $ds = '/';
               $storeFolder = 'upload';
               $tempFile    = $this->request->data['Blogpublicidade']['imagen']['tmp_name'];
               $targetPath  = ROOT . $ds .'app'. $ds .'webroot'. $ds .'img'. $ds . $storeFolder . $ds;
               $extencion   = imagenType($this->request->data['Blogpublicidade']['imagen']['type']);
               $renameFile  = 'a_'.date('dmY_his').$extencion;
               $targetFile  = $targetPath.$renameFile;
               if(move_uploaded_file($tempFile,$targetFile)){
                   $tipo_imagen = $this->request->data['Blogpublicidade']['imagen']['type'];
                   $nombre_imagen = $renameFile;;
                   $this->request->data['Blogpublicidade']['carpeta_imagen'] = $storeFolder;
                   $this->request->data['Blogpublicidade']['nombre_imagen']  = $nombre_imagen;
                   $this->request->data['Blogpublicidade']['tipo_imagen']    = $tipo_imagen;
                   $this->request->data['Blogpublicidade']['ruta_imagen']    = $storeFolder.'/'.$nombre_imagen;
               }else{
                   $this->request->data['Blogpublicidade']['carpeta_imagen']  = "_";
                   $this->request->data['Blogpublicidade']['nombre_imagen']   = "_";
                   $this->request->data['Blogpublicidade']['tipo_imagen']     = "_";
                   $this->request->data['Blogpublicidade']['ruta_imagen'] = "_";
               }

            }else{
                   $this->request->data['Blogpublicidade']['carpeta_imagen']  = "_";
                   $this->request->data['Blogpublicidade']['nombre_imagen']   = "_";
                   $this->request->data['Blogpublicidade']['tipo_imagen']     = "_";
                   $this->request->data['Blogpublicidade']['ruta_imagen'] = "_";
            }
			if ($this->Blogpublicidade->save($this->request->data)) {
				$this->Flash->success(__('The blogpublicidade has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The blogpublicidade could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Blogpublicidade.' . $this->Blogpublicidade->primaryKey => $id));
			$this->request->data = $this->Blogpublicidade->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Blogpublicidade->id = $id;
		if (!$this->Blogpublicidade->exists()) {
			throw new NotFoundException(__('Invalid blogpublicidade'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Blogpublicidade->delete()) {
			$this->Flash->success(__('The blogpublicidade has been deleted.'));
		} else {
			$this->Flash->error(__('The blogpublicidade could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
