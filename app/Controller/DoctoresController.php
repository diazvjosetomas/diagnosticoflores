<?php
App::uses('AppController', 'Controller');
/**
 * Doctores Controller
 *
 * @property Doctore $Doctore
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class DoctoresController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');
	public $uses = array('sucursale','User','Role');

/**
 * index method
 *
 * @return void
 */

	public function beforeFilter() {
		$this->checkSession(74);
		
	}


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = "gentella";

		$this->set('users', $this->User->find('all',array('conditions'=>array('or'=>array('nivel'=>array('2','3'))))));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->layout = "gentella";
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = "gentella";
		if ($this->request->is('post')) {
			$this->User->create();
			$this->request->data["User"]["password"] = md5($this->request->data["User"]["password"]);
			$this->request->data["User"]["statu"] = 1;
			if ($this->User->save($this->request->data)) {
				$this->Flash->success(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->success(__('The user could not be saved. Please, try again.'));
			}
		}

		$roles = $this->User->Role->find('list');
		$sucursales = $this->User->Sucursale->find('list');
		
		$this->set(compact('roles','sucursales'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout = "gentella";
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
            if($this->request->data["User"]["password2"]!=$this->request->data["User"]["password"]){
				$this->request->data['User']['password'] = md5(trim(strtoupper($this->request->data['User']['password'])));
			}
			if ($this->User->save($this->request->data)) {
				$this->Flash->success(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}

		$roles = $this->User->Role->find('list');
		
		$this->set(compact('roles'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->layout = "gentella";
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Flash->success(__('The user has been deleted.'));
		} else {
			$this->Flash->error(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}






	public function permisos(){
		$this->User->recursive = 3;
		$this->set('permisos', $this->User->find('all'));
	}
}
