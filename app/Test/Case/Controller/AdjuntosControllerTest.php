<?php
App::uses('AdjuntosController', 'Controller');

/**
 * AdjuntosController Test Case
 *
 */
class AdjuntosControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.adjunto',
		'app.user',
		'app.tipocliente',
		'app.role',
		'app.rolesmodulo',
		'app.modulo',
		'app.sucursale',
		'app.cliente',
		'app.ingreso',
		'app.personale',
		'app.asistencia',
		'app.tipoentrada',
		'app.pago',
		'app.concepto',
		'app.proveedor_pago',
		'app.permiso',
		'app.tipopermiso',
		'app.ingresotipopago',
		'app.ingresotipomonto',
		'app.ingresotipo',
		'app.ingresodetalle',
		'app.servicio',
		'app.imgadjunto'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

}
