<?php
App::uses('Blogcategoria', 'Model');

/**
 * Blogcategoria Test Case
 *
 */
class BlogcategoriaTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.blogcategoria',
		'app.blogentrada'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Blogcategoria = ClassRegistry::init('Blogcategoria');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Blogcategoria);

		parent::tearDown();
	}

}
