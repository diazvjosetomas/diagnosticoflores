<?php
App::uses('Ingresofolio', 'Model');

/**
 * Ingresofolio Test Case
 *
 */
class IngresofolioTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.ingresofolio',
		'app.surcusale'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Ingresofolio = ClassRegistry::init('Ingresofolio');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Ingresofolio);

		parent::tearDown();
	}

}
