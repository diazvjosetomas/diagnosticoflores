<?php
App::uses('Rootnota', 'Model');

/**
 * Rootnota Test Case
 *
 */
class RootnotaTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.rootnota',
		'app.statu'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Rootnota = ClassRegistry::init('Rootnota');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Rootnota);

		parent::tearDown();
	}

}
