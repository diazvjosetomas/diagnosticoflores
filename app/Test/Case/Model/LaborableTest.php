<?php
App::uses('Laborable', 'Model');

/**
 * Laborable Test Case
 *
 */
class LaborableTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.laborable'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Laborable = ClassRegistry::init('Laborable');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Laborable);

		parent::tearDown();
	}

}
