<?php
App::uses('Ingresodetalle', 'Model');

/**
 * Ingresodetalle Test Case
 *
 */
class IngresodetalleTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.ingresodetalle',
		'app.ingreso',
		'app.user',
		'app.tipocliente',
		'app.personale',
		'app.sucursale',
		'app.asistencia',
		'app.tipoentrada',
		'app.pago',
		'app.concepto',
		'app.proveedor_pago',
		'app.permiso',
		'app.tipopermiso',
		'app.cliente',
		'app.ingresotipo',
		'app.ingresotipomonto',
		'app.ingresotipopago'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Ingresodetalle = ClassRegistry::init('Ingresodetalle');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Ingresodetalle);

		parent::tearDown();
	}

}
