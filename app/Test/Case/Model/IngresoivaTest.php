<?php
App::uses('Ingresoiva', 'Model');

/**
 * Ingresoiva Test Case
 *
 */
class IngresoivaTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.ingresoiva'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Ingresoiva = ClassRegistry::init('Ingresoiva');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Ingresoiva);

		parent::tearDown();
	}

}
