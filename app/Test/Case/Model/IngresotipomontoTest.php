<?php
App::uses('Ingresotipomonto', 'Model');

/**
 * Ingresotipomonto Test Case
 *
 */
class IngresotipomontoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.ingresotipomonto',
		'app.ingresotipo',
		'app.ingresotipopago'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Ingresotipomonto = ClassRegistry::init('Ingresotipomonto');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Ingresotipomonto);

		parent::tearDown();
	}

}
