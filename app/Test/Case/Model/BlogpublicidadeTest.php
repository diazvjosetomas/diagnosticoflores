<?php
App::uses('Blogpublicidade', 'Model');

/**
 * Blogpublicidade Test Case
 *
 */
class BlogpublicidadeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.blogpublicidade'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Blogpublicidade = ClassRegistry::init('Blogpublicidade');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Blogpublicidade);

		parent::tearDown();
	}

}
