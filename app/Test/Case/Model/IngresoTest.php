<?php
App::uses('Ingreso', 'Model');

/**
 * Ingreso Test Case
 *
 */
class IngresoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.ingreso',
		'app.user',
		'app.tipocliente',
		'app.personale',
		'app.sucursale',
		'app.asistencia',
		'app.tipoentrada',
		'app.pago',
		'app.concepto',
		'app.proveedor_pago',
		'app.permiso',
		'app.tipopermiso',
		'app.cliente'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Ingreso = ClassRegistry::init('Ingreso');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Ingreso);

		parent::tearDown();
	}

}
