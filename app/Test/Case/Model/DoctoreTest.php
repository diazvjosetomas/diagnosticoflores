<?php
App::uses('Doctore', 'Model');

/**
 * Doctore Test Case
 *
 */
class DoctoreTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.doctore'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Doctore = ClassRegistry::init('Doctore');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Doctore);

		parent::tearDown();
	}

}
