<?php
App::uses('Egresotipo', 'Model');

/**
 * Egresotipo Test Case
 *
 */
class EgresotipoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.egresotipo',
		'app.egreso'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Egresotipo = ClassRegistry::init('Egresotipo');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Egresotipo);

		parent::tearDown();
	}

}
