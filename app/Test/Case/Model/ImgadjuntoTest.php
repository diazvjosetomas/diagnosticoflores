<?php
App::uses('Imgadjunto', 'Model');

/**
 * Imgadjunto Test Case
 *
 */
class ImgadjuntoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.imgadjunto',
		'app.adjunto',
		'app.user',
		'app.tipocliente',
		'app.role',
		'app.rolesmodulo',
		'app.modulo',
		'app.sucursale',
		'app.cliente',
		'app.ingreso',
		'app.personale',
		'app.asistencia',
		'app.tipoentrada',
		'app.pago',
		'app.concepto',
		'app.proveedor_pago',
		'app.permiso',
		'app.tipopermiso',
		'app.ingresotipopago',
		'app.ingresotipomonto',
		'app.ingresotipo',
		'app.ingresodetalle',
		'app.servicio'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Imgadjunto = ClassRegistry::init('Imgadjunto');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Imgadjunto);

		parent::tearDown();
	}

}
