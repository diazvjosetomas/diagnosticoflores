<?php
App::uses('Ingresotipopago', 'Model');

/**
 * Ingresotipopago Test Case
 *
 */
class IngresotipopagoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.ingresotipopago',
		'app.ingresotipomonto'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Ingresotipopago = ClassRegistry::init('Ingresotipopago');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Ingresotipopago);

		parent::tearDown();
	}

}
