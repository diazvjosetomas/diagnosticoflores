<?php
App::uses('Configuration', 'Model');

/**
 * Configuration Test Case
 *
 */
class ConfigurationTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.configuration',
		'app.language',
		'app.money_simbol',
		'app.country'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Configuration = ClassRegistry::init('Configuration');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Configuration);

		parent::tearDown();
	}

}
