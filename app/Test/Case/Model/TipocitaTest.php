<?php
App::uses('Tipocita', 'Model');

/**
 * Tipocita Test Case
 *
 */
class TipocitaTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.tipocita',
		'app.categoriacita',
		'app.cita'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Tipocita = ClassRegistry::init('Tipocita');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Tipocita);

		parent::tearDown();
	}

}
