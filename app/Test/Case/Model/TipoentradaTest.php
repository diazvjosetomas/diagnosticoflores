<?php
App::uses('Tipoentrada', 'Model');

/**
 * Tipoentrada Test Case
 *
 */
class TipoentradaTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.tipoentrada',
		'app.asistencia'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Tipoentrada = ClassRegistry::init('Tipoentrada');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Tipoentrada);

		parent::tearDown();
	}

}
