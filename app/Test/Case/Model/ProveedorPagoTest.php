<?php
App::uses('ProveedorPago', 'Model');

/**
 * ProveedorPago Test Case
 *
 */
class ProveedorPagoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.proveedor_pago'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ProveedorPago = ClassRegistry::init('ProveedorPago');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ProveedorPago);

		parent::tearDown();
	}

}
