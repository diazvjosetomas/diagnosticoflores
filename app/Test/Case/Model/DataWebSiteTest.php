<?php
App::uses('DataWebSite', 'Model');

/**
 * DataWebSite Test Case
 *
 */
class DataWebSiteTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.data_web_site'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->DataWebSite = ClassRegistry::init('DataWebSite');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->DataWebSite);

		parent::tearDown();
	}

}
