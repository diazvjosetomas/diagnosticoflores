<?php
App::uses('Avisopriva', 'Model');

/**
 * Avisopriva Test Case
 *
 */
class AvisoprivaTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.avisopriva'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Avisopriva = ClassRegistry::init('Avisopriva');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Avisopriva);

		parent::tearDown();
	}

}
