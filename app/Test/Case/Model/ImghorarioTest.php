<?php
App::uses('Imghorario', 'Model');

/**
 * Imghorario Test Case
 *
 */
class ImghorarioTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.imghorario'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Imghorario = ClassRegistry::init('Imghorario');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Imghorario);

		parent::tearDown();
	}

}
