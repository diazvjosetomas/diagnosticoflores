<?php
App::uses('Ingresotipo', 'Model');

/**
 * Ingresotipo Test Case
 *
 */
class IngresotipoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.ingresotipo',
		'app.ingresotipomonto'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Ingresotipo = ClassRegistry::init('Ingresotipo');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Ingresotipo);

		parent::tearDown();
	}

}
