<?php
App::uses('Suscribirse', 'Model');

/**
 * Suscribirse Test Case
 *
 */
class SuscribirseTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.suscribirse'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Suscribirse = ClassRegistry::init('Suscribirse');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Suscribirse);

		parent::tearDown();
	}

}
