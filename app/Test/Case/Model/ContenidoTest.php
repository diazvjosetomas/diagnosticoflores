<?php
App::uses('Contenido', 'Model');

/**
 * Contenido Test Case
 *
 */
class ContenidoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.contenido',
		'app.categoria'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Contenido = ClassRegistry::init('Contenido');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Contenido);

		parent::tearDown();
	}

}
