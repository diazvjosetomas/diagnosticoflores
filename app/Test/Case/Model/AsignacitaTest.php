<?php
App::uses('Asignacita', 'Model');

/**
 * Asignacita Test Case
 *
 */
class AsignacitaTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.asignacita',
		'app.cita',
		'app.tipocita',
		'app.categoriacita',
		'app.sucursale',
		'app.user',
		'app.tipocliente'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Asignacita = ClassRegistry::init('Asignacita');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Asignacita);

		parent::tearDown();
	}

}
