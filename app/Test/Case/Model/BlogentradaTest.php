<?php
App::uses('Blogentrada', 'Model');

/**
 * Blogentrada Test Case
 *
 */
class BlogentradaTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.blogentrada',
		'app.blogcategoria',
		'app.user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Blogentrada = ClassRegistry::init('Blogentrada');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Blogentrada);

		parent::tearDown();
	}

}
