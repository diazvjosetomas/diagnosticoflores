<?php
App::uses('Categoriacita', 'Model');

/**
 * Categoriacita Test Case
 *
 */
class CategoriacitaTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.categoriacita',
		'app.tipocita'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Categoriacita = ClassRegistry::init('Categoriacita');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Categoriacita);

		parent::tearDown();
	}

}
