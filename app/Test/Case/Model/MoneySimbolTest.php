<?php
App::uses('MoneySimbol', 'Model');

/**
 * MoneySimbol Test Case
 *
 */
class MoneySimbolTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.money_simbol',
		'app.configuration',
		'app.language',
		'app.country'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->MoneySimbol = ClassRegistry::init('MoneySimbol');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->MoneySimbol);

		parent::tearDown();
	}

}
