<?php
App::uses('Cajacierre', 'Model');

/**
 * Cajacierre Test Case
 *
 */
class CajacierreTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.cajacierre',
		'app.caja',
		'app.user',
		'app.tipocliente'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Cajacierre = ClassRegistry::init('Cajacierre');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Cajacierre);

		parent::tearDown();
	}

}
