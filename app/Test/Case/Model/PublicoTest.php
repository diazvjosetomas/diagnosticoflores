<?php
App::uses('Publico', 'Model');

/**
 * Publico Test Case
 *
 */
class PublicoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.publico',
		'app.promocione'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Publico = ClassRegistry::init('Publico');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Publico);

		parent::tearDown();
	}

}
