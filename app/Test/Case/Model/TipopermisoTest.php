<?php
App::uses('Tipopermiso', 'Model');

/**
 * Tipopermiso Test Case
 *
 */
class TipopermisoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.tipopermiso',
		'app.permiso'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Tipopermiso = ClassRegistry::init('Tipopermiso');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Tipopermiso);

		parent::tearDown();
	}

}
