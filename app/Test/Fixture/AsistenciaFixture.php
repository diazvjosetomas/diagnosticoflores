<?php
/**
 * AsistenciaFixture
 *
 */
class AsistenciaFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'personale_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'fecha_asistencia' => array('type' => 'date', 'null' => true, 'default' => null),
		'tipoentrada_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'hora' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 25, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'date', 'null' => true, 'default' => null),
		'modified' => array('type' => 'date', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'personale_id' => array('column' => 'personale_id', 'unique' => 0),
			'tipoentrada_id' => array('column' => 'tipoentrada_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'personale_id' => 1,
			'fecha_asistencia' => '2016-10-23',
			'tipoentrada_id' => 1,
			'hora' => 'Lorem ipsum dolor sit a',
			'created' => '2016-10-23',
			'modified' => '2016-10-23'
		),
	);

}
