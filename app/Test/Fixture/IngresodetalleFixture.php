<?php
/**
 * IngresodetalleFixture
 *
 */
class IngresodetalleFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary'),
		'ingreso_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'ingresotipo_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'ingresotipopago_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'ingresotipomonto_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'cantidad' => array('type' => 'float', 'null' => false, 'default' => null, 'length' => '26,2', 'unsigned' => false),
		'monto' => array('type' => 'float', 'null' => false, 'default' => null, 'length' => '26,2', 'unsigned' => false),
		'descuento' => array('type' => 'float', 'null' => false, 'default' => null, 'length' => '26,2', 'unsigned' => false),
		'total' => array('type' => 'float', 'null' => false, 'default' => null, 'length' => '26,2', 'unsigned' => false),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'id' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '',
			'ingreso_id' => 1,
			'ingresotipo_id' => 1,
			'ingresotipopago_id' => 1,
			'ingresotipomonto_id' => 1,
			'cantidad' => 1,
			'monto' => 1,
			'descuento' => 1,
			'total' => 1,
			'created' => '2016-11-24 23:33:58',
			'modified' => '2016-11-24 23:33:58'
		),
	);

}
