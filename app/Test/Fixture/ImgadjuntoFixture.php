<?php
/**
 * ImgadjuntoFixture
 *
 */
class ImgadjuntoFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'adjunto_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'carpeta_imagen' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 90, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'nombre_imagen' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 90, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'tipo_imagen' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 90, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'ruta_imagen' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 90, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'date', 'null' => false, 'default' => null),
		'modified' => array('type' => 'date', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'adjunto_id' => 1,
			'carpeta_imagen' => 'Lorem ipsum dolor sit amet',
			'nombre_imagen' => 'Lorem ipsum dolor sit amet',
			'tipo_imagen' => 'Lorem ipsum dolor sit amet',
			'ruta_imagen' => 'Lorem ipsum dolor sit amet',
			'created' => '2017-02-18',
			'modified' => '2017-02-18'
		),
	);

}
