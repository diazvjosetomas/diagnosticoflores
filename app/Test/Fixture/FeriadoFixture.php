<?php
/**
 * FeriadoFixture
 *
 */
class FeriadoFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'denominacion' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 90, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'diaferiado' => array('type' => 'date', 'null' => false, 'default' => null),
		'manana' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'tarde' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'noche' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'created' => array('type' => 'date', 'null' => false, 'default' => null),
		'modified' => array('type' => 'date', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'denominacion' => 'Lorem ipsum dolor sit amet',
			'diaferiado' => '2016-11-30',
			'manana' => 1,
			'tarde' => 1,
			'noche' => 1,
			'created' => '2016-11-30',
			'modified' => '2016-11-30'
		),
	);

}
