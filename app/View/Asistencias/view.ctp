<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Asistencias </h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href=""> Home</a></li>	
					<li><i class="fa fa-arrow-left"></i>
					<a href="<?=$this->Html->url('/asistencias/')?>"> Volver a Asistencias </a> </li>              
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Asistencia'); ?></h2>
	<table class="table table-striped">
	<tbody>
		<tr><td><?php echo __('Personal'); ?></td>
		<td>
			<?php echo h($asistencia['Personale']['rfc']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Fecha Asistencia'); ?></td>
		<td>
			<?php echo h($asistencia['Asistencia']['fecha_asistencia']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Tipo Asistencia'); ?></td>
		<td>
			<?php echo h($asistencia['Tipoentrada']['denominacion']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Hora'); ?></td>
		<td>
			<?php echo h($asistencia['Asistencia']['hora']); ?>
			&nbsp;
		</td></tr>
	</tbody>
	</table>
</div>

</div>