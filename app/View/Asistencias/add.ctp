<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Asistencias </h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href=""> Home</a></li>	
					<li><i class="fa fa-arrow-left"></i>
					<a href="<?=$this->Html->url('/asistencias/')?>"> Volver a Asistencias </a> </li>              
		</ol>		
	</div>	    
</div>

<div class="row">
		     <div class="col-sm-12">
	  		   <section class="panel" style="padding: 10px;">

<div class="asistencias form">
<?php echo $this->Form->create('Asistencia'); ?>
	<fieldset>
		<legend><?php echo __('Add Asistencia'); ?></legend>
<div class='row'><div class='col-sm-12'>		<?php echo $this->Form->input('personale_id', array('label'=>'Personal','class'=>'form-control')); ?> 
 </div></div><div class='row'><div class='col-sm-12'>		<?php echo $this->Form->input('fecha_asistencia', array('type'=>'text', 'value'=>date("Y-m-d"), 'readonly'=>true,  'label'=>'Fecha Asistencia','class'=>'form-control','id'=>'AsistenciaFechaAsistencia')); ?> 
 </div></div><div class='row'><div class='col-sm-12'>		<?php echo $this->Form->input('tipoentrada_id', array('label'=>'Tipo Asistencia','class'=>'form-control')); ?> 
 </div></div><div class='row'><div class='col-sm-12'>		<?php echo $this->Form->input('hora', array('type'=>'text','readonly'=>true,  'label'=>'Hora','class'=>'form-control')); ?> 
 </div></div>	</fieldset>
<br>
	<div class="pull-right">		
		<?php echo $this->Form->submit(__('Guardar', true), array('label'=>false, 'class'=>'btn btn-primary'));?>
		<?php echo $this->Form->end(__('')); ?>
	</div>

</div>

</section>
</div>
</div>
<script type="text/javascript">
	function hora(){
	var fecha = new Date()
	var hora = fecha.getHours()
	var minuto = fecha.getMinutes()
	var segundo = fecha.getSeconds()

	if (hora >= 12) {
		var tipohora = 'PM';
	}else{
		var tipohora = 'AM';
	}

	if (hora < 10) {hora = "0" + hora}
	if (minuto < 10) {minuto = "0" + minuto}
	if (segundo < 10) {segundo = "0" + segundo}
	var horita = hora + ":" + minuto + ":" + segundo;
	$("#AsistenciaHora").val(horita+' '+tipohora);
	//document.getElementById('AsistenciaHora').firstChild.nodeValue = horita
	
	}

	hora();
</script>