

<div class="container">




<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Doctores</h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
                    <li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Doctores'), array('action' => 'index')); ?></li>
		</ol>		
	</div>	    
</div>


<div class="panel">
	<div class="panel-heading panel-success">
		<h4>&nbsp;
			<span class="pull-right">
				<div class="btn-group">
				  <button type="button" class="btn btn-primary">Actions</button>
				  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
				    <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu" role="menu">
				    <li><?php echo $this->Html->link(__('Nuevo'), array('action' => 'add')); ?></li>
				    
				  </ul>
				</div>
			</span>
		</h4>
	</div>
	<div class="panel-body">
			<table id="datos" cellpadding="0" cellspacing="0" class="table table-striped table-rounded">
			<thead>
			<tr>
							<th><?php echo h('Usuario'); ?></th>
							<th><?php echo h('Doctores'); ?></th>
							
							<th><?php echo h('Email'); ?></th>
							
							<th><?php echo h('Teléfono'); ?></th>
							<th class="actions"><?php echo __('Actions'); ?></th>
			</tr>
			</thead>
			<tbody>
			<?php foreach ($users as $user): ?>
	<tr>
		<td><?php echo h($user['User']['user']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['nombre'].' '.$user['User']['apellidos']); ?>&nbsp;</td>
		
		<td><?php echo h($user['User']['email']); ?>&nbsp;</td>
		
		
		<td><?php echo $user['User']['telefono']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link('Ver',      array('action' => 'view', $user['User']['id']), array('class'=>'btn btn-primary btn-sm', 'escapeTitle'=>false)); ?>
			<?php echo $this->Html->link('Editar',     array('action' => 'edit', $user['User']['id']), array('class'=>'btn btn-success btn-sm', 'escapeTitle'=>false)); ?>
			<?php echo $this->Form->postLink('Borrar', array('action' => 'delete', $user['User']['id']), array('class'=>'btn btn-danger btn-sm', 'confirm' => __('Esta seguro que desea eliminar el registro # %s?', $user['User']['id']), 'escapeTitle'=>false)); ?>
		</td>
	</tr>
<?php endforeach; ?>
			</tbody>
			</table>
		</section>
	</div>

</div>
<script type="text/javascript">
    //$(document).ready(function() {
        $('#datos').DataTable( {
            dom: 'Bfrtlip',          
            responsive: true,
	        buttons: [
	            {
	                extend: 'excel',
	                exportOptions: {
	                    columns: [0,1,2,3,4,5]
	                }
	            },
	            {
	                extend: 'pdf',
	                exportOptions: {
	                    columns: [0,1,2,3,4,5]
	                }
	            },
	            {
	                extend: 'copy',
	                exportOptions: {
	                    columns: [0,1,2,3,4,5]
	                }
	            },
	            {
	                extend: 'csv',
	                exportOptions: {
	                    columns: [0,1,2,3,4,5]
	                }
	            },
	            {
	                extend: 'print',
	                exportOptions: {
	                    columns: [0,1,2,3,4,5]
	                }
	            }



	        ],
            "language": 
            {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ".",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        } );
    //} );
</script>
