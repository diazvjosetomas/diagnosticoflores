<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header"><i class="fa fa-th"></i> Doctores</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="<?=$this->Html->url('/dashboards/index/')?>"> Home</a></li>
			<li><i class="fa fa-th"></i> Doctores</li>                
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
	  <section class="panel">
	  
    	<div class="panel-body">
				<?php echo $this->Form->create('User'); ?>
					<div class='row'>
	<div class='col-sm-12'>
			<?php echo $this->Form->input('user', array('label'=>'Usuario','class'=>'form-control')); ?>
	</div>
</div>
<div class='row'>
	<div class='col-sm-12'>
			<?php echo $this->Form->input('password', array('label'=>'Clave','class'=>'form-control')); ?>
	</div>
</div>
<div class='row'>
	<div class='col-sm-12'>
			<?php echo $this->Form->input('nombre', array('label'=>'Nombre','class'=>'form-control')); ?>
	</div>
</div>
<div class='row'>
	<div class='col-sm-12'>
			<?php echo $this->Form->input('apellidos', array('label'=>'Apellidos','class'=>'form-control')); ?>
	</div>
</div>
<div class='row'>
	<div class='col-sm-12'>
			<?php echo $this->Form->input('email', array('class'=>'form-control')); ?>
	</div>
</div>
<div class='row'>
	<div class='col-sm-12'>
			<?php echo $this->Form->input('nivel', array('class'=>'form-control', 'options'=>array('1'=>'Root', '2'=>'Consultor'), 'empty'=>'--Seleccione--')); ?>
	</div>
</div>

<div class='row'>
	<div class='col-sm-12'>
			<?php echo $this->Form->input('role_id', array('class'=>'form-control' )); ?>
	</div>
</div>

<div class='row'>
	<div class='col-sm-12'>
			<?php echo $this->Form->input('sucursale_id', array('class'=>'form-control' )); ?>
	</div>
</div>
					<br />
					
	<div class="pull-right">		
		<?php echo $this->Form->submit(__('Submit', true), array('label'=>false, 'class'=>'btn btn-primary'));?>
		<?php echo $this->Form->end(__('')); ?>
	</div>
			</div>
		</section>
	</div>

	
</div>
