<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Suscribirses</h3>
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
				<li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Suscribirses'), array('action' => 'index')); ?></li>             
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Suscribirse'); ?></h2>
	<table class="table table-striped">
	<tbody>
		<tr><td><?php echo __('Id'); ?></td>
		<td>
			<?php echo h($suscribirse['Suscribirse']['id']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Nombres'); ?></td>
		<td>
			<?php echo h($suscribirse['Suscribirse']['nombres']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Email'); ?></td>
		<td>
			<?php echo h($suscribirse['Suscribirse']['email']); ?>
			&nbsp;
		</td></tr>
	
	</tbody>
	</table>
</div>
</div>