<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Pagos </h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href=""> Home</a></li>	
					<li><i class="fa fa-arrow-left"></i>
					<a href="<?=$this->Html->url('/pagos/')?>"> Volver a Pagos </a> </li>              
		</ol>		
	</div>	    
</div>

<div class="row">
		     <div class="col-sm-12">
	  		   <section class="panel" style="padding: 10px;">

<div class="pagos form">
<?php echo $this->Form->create('Pago', array('enctype'=>'multipart/form-data')); ?>
	<fieldset>
		<legend><?php echo __('Edit Pago'); ?></legend>
<div class='row'><div class='col-sm-12'>		<?php echo $this->Form->input('id', array('label'=>'id','class'=>'form-control')); ?> 
 </div></div><div class='row'><div class='col-sm-12'>		<?php echo $this->Form->input('concepto_id', array('label'=>'Concepto','class'=>'form-control')); ?> 
 </div></div><div class='row'><div class='col-sm-12'>		<?php echo $this->Form->input('personale_id', array('label'=>'Personal','class'=>'form-control')); ?> 
 </div></div><div class='row'><div class='col-sm-12'>		<?php echo $this->Form->input('monto', array('label'=>'Monto','class'=>'form-control')); ?> 
 </div></div><div class='row'><div class='col-sm-12'>		<?php echo $this->Form->input('fecha_pago', array('label'=>'Fecha de Pago','class'=>'form-control','id'=>'PagoFechaPago')); ?> 
 </div></div>
<div class='row'>
					 <div class='col-sm-12'>		
					 	<?php echo $this->Form->input('contenido', array('label'=>'Archivo', 'class'=>'form-control', 'type'=>"file"));?>	
					 	<?php echo'<p for="" class="help-block text-red">* El archivo debe estar en formato .pdf</p>';?>	
					 </div>
				 </div>

 </fieldset>
<br>
	<div class="pull-right">		
		<?php echo $this->Form->submit(__('Submit', true), array('label'=>false, 'class'=>'btn btn-primary'));?>
		<?php echo $this->Form->end(__('')); ?>
	</div>

</div>

</section>
</div>
</div>
