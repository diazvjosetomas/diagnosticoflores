<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Pagos </h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
					<li><i class="fa fa-arrow-left"></i>
					<a href="<?=$this->Html->url('/pagos/')?>"> Volver a Pagos </a> </li>              
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Pago'); ?></h2>
	<table class="table table-striped">
	<tbody>
		<tr><td><?php echo __('Concepto'); ?></td>
		<td>
			<?php echo h($pago['Concepto']['denominacion']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Personale'); ?></td>
		<td>
			<?php echo h($pago['Personale']['nombres'].' '.$pago['Personale']['apellidos']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Monto'); ?></td>
		<td>
			<?php echo h($pago['Pago']['monto']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Fecha Pago'); ?></td>
		<td>
			<?php echo h($pago['Pago']['fecha_pago']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Descarcar'); ?></td>
		<td>
			<a href="<?= $this->webroot.''.$pago['Pago']['nombre_archivo']?>">Descarga de archivo</a>&nbsp;
		</td></tr>
	</tbody>
	</table>
</div>
</div>