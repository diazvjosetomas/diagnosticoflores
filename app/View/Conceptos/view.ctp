<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Conceptos de pago </h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href=""> Home</a></li>	
					<li><i class="fa fa-arrow-left"></i>
					<a href="<?=$this->Html->url('/conceptos/')?>"> Volver a Conceptos de pago </a> </li>              
		</ol>		
	</div>	    
</div>
<div class="panel">
	
	



<div class="panel panel-body">
<h2><?php echo __('Conceptos de pago'); ?></h2>
	<table class="table table-striped">

	<tbody>
		
		
		<tr><td><?php echo __('Id'); ?></td>
		<td>
			<?php echo h($concepto['Concepto']['id']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Denominacion'); ?></td>
		<td>
			<?php echo h($concepto['Concepto']['denominacion']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Descripción'); ?></td>
		<td>
			<?php echo h($concepto['Concepto']['descripcion']); ?>
			&nbsp;
		</td></tr>
	
	</tbody>
	</table>
</div>

</div>