
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php 
            //echo $cakeDescription 
    ?>
		<?php 
          //echo $this->fetch('title'); 
            echo "Quantum";
    ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		//echo $this->Html->css('cake.generic');
		echo $this->Html->css('gentella/bootstrap.min');
		echo $this->Html->css('gentella/fonts/css/font-awesome.min');
		echo $this->Html->css('gentella/animate.min');
		echo $this->Html->css('gentella/custom');
		echo $this->Html->css('gentella/icheck/flat/green');
		echo $this->Html->css('gentella/floatexamples');
		echo $this->Html->css('gentella/maps/jquery-jvectormap-2.0.3');
    echo $this->Html->css('gentella/alertify');
    echo $this->Html->css('gentella/build/css/custom.min');
    echo $this->Html->css('gentella/vendors/colorpicker/dist/css/bootstrap-colorpicker.min.css');
    
    // script    
    echo $this->Html->script('gentella/alertify.min');
		echo $this->Html->script('gentella/jquery.min');
    echo $this->Html->script('gentella/nprogress');
    echo $this->Html->script('function');
		
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
      <!-- DataTable CSS -->
      <?php 
      // echo $this->Html->css('datatable/jquery.dataTables.css'); 
      ?>
      <?php echo $this->Html->css('datatable/buttons.dataTables.min.css'); ?>
      <?php echo $this->Html->css('datatable/dataTables.bootstrap.min.css'); ?>
      <!-- DataTable JS -->
      <?php echo $this->Html->script('datatable/jquery.dataTables.js'); ?>
      <?php echo $this->Html->script('datatable/dataTables.bootstrap.min.js'); ?>
      <?php echo $this->Html->script('datatable/dataTables.buttons.min.js'); ?>
      <?php echo $this->Html->script('datatable/dataTables.buttons.flash.min.js'); ?>
      <?php echo $this->Html->script('datatable/jszip.min.js'); ?>
      <?php echo $this->Html->script('datatable/pdfmake.min.js'); ?>
      <?php echo $this->Html->script('datatable/vfs_fonts.js'); ?>
      <?php echo $this->Html->script('datatable/buttons.html5.min.js'); ?>
      <?php echo $this->Html->script('datatable/buttons.print.min.js'); ?>
      <!-- highcharts JS -->
      <?php echo $this->Html->script('highcharts/highcharts.js'); ?>
      <?php echo $this->Html->script('highcharts/exporting.js'); ?>
      <?php echo $this->Html->script('highcharts/drilldown.js'); ?>
      <?php echo $this->Html->script('highcharts/data.js'); ?>
      <?php echo $this->Html->script('gentella/select/bootstrap-select.js'); ?>
    

      <!-- Calendar JS -->
      <?php echo $this->Html->css('jquery-ui.css'); ?>
      <?php echo $this->Html->script('jquery-ui.min.js'); ?>


          <?php 

          echo $this->Html->css('gentella/vendors/bootstrap/dist/css/bootstrap.min');
          echo $this->Html->css('gentella/vendors/bootstrap-daterangepicker/daterangepicker');
          echo $this->Html->css('gentella/vendors/nprogress/nprogress');
          echo $this->Html->css('gentella/vendors/fullcalendar/dist/fullcalendar.min');
        
          echo $this->Html->css('gentella/build/css/custom.min');


     ?>

      <script type="text/javascript">
        jQuery(function($){
            $.datepicker.regional['es'] = {
                closeText: 'Cerrar',
                prevText: '&#x3c;Ant',
                nextText: 'Sig&#x3e;',
                currentText: 'Hoy',
                monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
                'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
                monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
                'Jul','Ago','Sep','Oct','Nov','Dic'],
                dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
                dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
                dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
                weekHeader: 'Sm',
                dateFormat: 'yy-mm-dd',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''};
            $.datepicker.setDefaults($.datepicker.regional['es']);
        });
    </script>
</head>

<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <div class="navbar nav_title" style="border: 0;">
            <? 
                 $user = $this->Session->read('USUARIO_USER');
            ?>
            <a href="<?=$this->Html->url('/dashboards/index/')?>" class="site_title"><i class="fa fa-gears"></i> 
            <span>  
              <?
                  echo "Panel de Control";
              ?>
            </span>
            </a>
          </div>
          <!-- menu prile quick info -->
          <div class="profile">
            <div class="profile_info">
            </div>
          </div>
          <!-- sidebar menu -->

          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
               <br><br><br><br>

            <div class="menu_section">
            
              <ul class="nav side-menu">
               
                  <li>

                    <a href="<?php echo $this->Html->url('/Dashboards/'); ?>"><i class="fa fa-home"></i> Inicio </a>

                  </li>

                  <li>

                    <a href="<?=$this->Html->url('/asistencias/')?>"><i class="fa fa-users"></i>Asistencias</a>

                  </li>
                
                  <li>

                    <a href="<?=$this->Html->url('/Citas/')?>"><i class="fa fa-file"></i>Citas</a>

                  </li>
                
                  <li><a><i class="fa fa-credit-card" aria-hidden="true"></i>Caja <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu" style="display: none">
                        <li><a href="<?=$this->Html->url('/Cajas/')?>"><i class="fa fa-angle-double-right"></i>Cajas</a></li>
                        <li><a href="<?=$this->Html->url('/Cajaaperturas/')?>"><i class="fa fa-angle-double-right"></i>Apertura</a></li>
                        <li><a href="<?=$this->Html->url('/Cajacierres/')?>"><i class="fa fa-angle-double-right"></i>Cierre turno</a></li>
                        <li><a href="<?=$this->Html->url('/Cajacierredias/')?>"><i class="fa fa-angle-double-right"></i>Cierre diario</a></li>
                      </ul>
                  </li> 

                  <li>
                        
                      <a href="<?=$this->Html->url('/promociones/')?>"><i class="fa fa-envelope-o"></i> Email</a>
                          
                  </li>

                  <li><a><i class="fa fa-users"></i> Clientes <span class="fa fa-chevron-down"></span></a>
                     <ul class="nav child_menu" style="display: none">
                      <li><a href="<?=$this->Html->url('/users/')?>"><i class="fa fa-angle-double-right"></i>Doctores</a></li>
                      <li><a href="<?=$this->Html->url('/Clientes/')?>"><i class="fa fa-angle-double-right"></i>Clientes</a></li>
                      <li><a href="<?=$this->Html->url('/Facturaciones/')?>"><i class="fa fa-angle-double-right"></i>Facturación</a></li>
                      <li><a href="<?=$this->Html->url('/contactos/')?>"><i class="fa fa-angle-double-right"></i>Contactos</a></li>
                     </ul>
                  </li>

                <li>
                  <a>
                    <i class="fa  fa-file-image-o"></i> Radiografias  
                      <span class="fa fa-chevron-down"></span>
                  </a>
                    <ul class="nav child_menu" style="display: none">
                      

                      <li>
                        <a href="<?=$this->Html->url('/Adjuntos/')?>"><i class="fa fa-angle-double-right"></i>Adjuntar Info</a>
                      </li>
                      <li>
                        <a href="<?=$this->Html->url('/Imgadjuntos/')?>"><i class="fa fa-angle-double-right"></i>Adjuntar Imagenes</a>
                      </li>
                    </ul>

                  

                </li>

                <li><a><i class="fa fa-codepen"></i> Personal <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu" style="display: none">
                      <li><a href="<?=$this->Html->url('/permisos/')?>"><i class="fa fa-angle-double-right"></i>Permisos</a></li>
                      
                      <li><a href="<?=$this->Html->url('/pagos/')?>"><i class="fa fa-angle-double-right"></i>Pagos</a></li>
                      <li><a href="<?=$this->Html->url('/asistencias/')?>"><i class="fa fa-angle-double-right"></i>Asistencias</a></li>
                     
                    </ul>
                </li>

                <li><a><i class="fa fa-line-chart"></i> Contabilidad<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu" style="display: none">

                      <li>
                      
                        <a href="<?=$this->Html->url('/Ingresos/')?>"><i class="fa fa-angle-double-right"></i>Ingresos</a>
                    
                      </li>

                      <li>
                        
                        <a href="<?=$this->Html->url('/Egresos/')?>"><i class="fa fa-angle-double-right"></i>Egresos</a>
                      </li>


                      <li>
                        <a href="<?php echo $this->Html->url('/Dashboards/balance'); ?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i>
                        Balances </a>
                      </li>

                      


                      <ul class="nav child_menu">
                        
                          <li class="sub-menu">
                            <a>
                              <i class="fa fa-file-pdf-o" aria-hidden="true"></i>Reportes 
                                <span class="fa fa-chevron-down"></span>
                            </a>
                               <ul class="nav child_menu" style="display: none">
                                 <li class="sub_menu"><a href="<?=$this->Html->url('/Reportes/doctores')?>"><i class="fa fa-angle-double-right"></i>Doctores</a></li>
                                 <li class="sub_menu"><a href="<?=$this->Html->url('/Reportes/clientes')?>"><i class="fa fa-angle-double-right"></i>Clientes</a></li>
                                 <li class="sub_menu"><a href="<?=$this->Html->url('/Reportes/empleados')?>"><i class="fa fa-angle-double-right"></i>Empleados</a></li>
                                 <li class="sub_menu"><a href="<?=$this->Html->url('/Reportes/listados')?>"><i class="fa fa-angle-double-right"></i>Listados</a></li>
                               </ul>                                
                          </li> 
                      </ul>                      
                    </ul>
                </li> 

                <li><a><i class="fa fa-gears"></i> Configuracion<span class="fa fa-chevron-down"></span></a>           
                     <ul class="nav child_menu">                                          
                          <li><a>Empresas <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu" style="display: none">
                              <li class="sub_menu"><a href="<?=$this->Html->url('/Empresas/')?>"><i class="fa fa-angle-double-right "></i>Datos Generales</a></li> 

                              <li class="sub_menu"><a href="<?=$this->Html->url('/sucursales/')?>"><i class="fa fa-angle-double-right"></i>Sucursales</a></li>

                            </ul>
                          </li>  


                          <li><a>Página<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu" style="display: none">
                              <li class="sub_menu"><a href="<?=$this->Html->url('/Avisoprivas/')?>"><i class="fa fa-angle-double-right"></i>Aviso de privacidad</a></li>   
                              <li class="sub_menu"><a href="<?=$this->Html->url('/contenidos/')?>"><i class="fa fa-angle-double-right"></i>Contenido</a></li>   
                              <li class="sub_menu"><a href="<?=$this->Html->url('/servicios/')?>"><i class="fa fa-angle-double-right"></i>Servicios</a></li>
                              <li class="sub_menu"><a href="<?=$this->Html->url('/Imghorarios/')?>"><i class="fa fa-angle-double-right"></i>Horarios</a></li>
                              
                              <li class="sub_menu"><a href="<?=$this->Html->url('/sliders/')?>"><i class="fa fa-angle-double-right"></i>Sliders</a></li>

                              <li class="sub_menu"><a href="<?=$this->Html->url('/categorias/')?>"><i class="fa fa-file"></i>Categorias</a></li>                            
                            </ul>
                          </li>  


                          <li><a>Blog<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu" style="display: none">
                              <li class="sub_menu"><a href="<?=$this->Html->url('/Blogentradas/')?>"><i class="fa fa-angle-double-right"></i>Entradas</a></li>
                              <li class="sub_menu"><a href="<?=$this->Html->url('/Suscribirses/')?>"><i class="fa fa-angle-double-right"></i>Suscriptores</a></li>                                   
                              <li class="sub_menu"><a href="<?=$this->Html->url('/Blogpublicidades/')?>"><i class="fa fa-angle-double-right"></i>Publicidad</a></li>
                              <li class="sub_menu"><a href="<?=$this->Html->url('/Blogcategorias/')?>"><i class="fa fa-book"></i>Categorias</a></li>
                            </ul>
                          </li>  


                          <li><a>Citas<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu" style="display: none;">
                              <li class="sub_menu"><a href="<?=$this->Html->url('/Tipocitas/')?>"><i class="fa fa-angle-double-right"></i>Tipo Citas
                              </a></li> 
                               <li class="sub_menu"><a href="<?=$this->Html->url('/Categoriacitas/')?>"><i class="fa fa-calendar"></i>Categoría</a></li>                                  
                            </ul>
                          </li> 


                          <li><a>Personal<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu" style="display: none">
                              <li class="sub_menu"><a href="<?=$this->Html->url('/tipopermisos/')?>"><i class="fa fa-angle-double-right"></i> Tipo Permisos</a></li>
                              <li class="sub_menu"><a href="<?=$this->Html->url('/tipoentradas/')?>"><i class="fa fa-angle-double-right"></i>Tipo Asistencia</a>
                              </li>
                              <li class="sub_menu"><a href="<?=$this->Html->url('/conceptos/')?>"><i class="fa fa-angle-double-right"></i>Conceptos de Pago</a>
                              </li>                                  
                            </ul>
                          </li>  

                          <li><a>Usuarios<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu" style="display: none">
                              <li class="sub_menu"><a href="<?=$this->Html->url('/Roles/')?>"><i class="fa fa-angle-double-right"></i>Roles</a></li>
                              <li class="sub_menu"><a href="<?=$this->Html->url('/Rolesmodulos/')?>"><i class="fa fa-angle-double-right"></i>Rolesmodulos</a></li>
                              <li class="sub_menu"><a href="<?=$this->Html->url('/Modulos/')?>"><i class="fa fa-angle-double-right"></i>Módulos</a></li>                                   
                            </ul>
                          </li> 

                          <li><a>Publicidad<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu" style="display: none">
                              <li class="sub_menu"> <a href="<?=$this->Html->url('/periodos/')?>"><i class="fa fa-angle-double-right"></i> Periodos</a></li>
                              <li class="sub_menu"> <a href="<?=$this->Html->url('/publicos/')?>"><i class="fa fa-angle-double-right"></i> Público</a></li>                                  
                            </ul>
                          </li>   

                          <li><a>Tareas<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu" style="display: none">
                              <li class="sub_menu"><a href="<?=$this->Html->url('/Notas/')?>"><i class="fa fa-angle-double-right"></i>Notas</a>
                              </li>                                  
                              <li class="sub_menu"><a href="<?=$this->Html->url('/Statuses/')?>"><i class="fa fa-angle-double-right"></i>Estatus de Tarea</a></li>
                            </ul>
                          </li> 

                          <li><a>Ingresos<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu" style="display: none">
                              <li class="sub_menu"><a href="<?=$this->Html->url('/Ingresoivas/')?>"><i class="fa fa-line-chart"></i>Iva</a></li>
                              <li class="sub_menu"><a href="<?=$this->Html->url('/Ingresotipopagos/')?>"><i class="fa fa-line-chart"></i>Tipo de pagos</a></li>
                              <li class="sub_menu"><a href="<?=$this->Html->url('/Ingresotipos/')?>"><i class="fa fa-line-chart"></i>Tipo de servicio</a></li>
                              <li class="sub_menu"><a href="<?=$this->Html->url('/Ingresofolios/')?>"><i class="fa fa-angle-double-right"></i>Folios</a></li>  
                              <li class="sub_menu"><a href="<?=$this->Html->url('/Ingresotipomontos/')?>"><i class="fa fa-angle-double-right"></i>Precio de servicio</a></li>                                   
                            </ul>
                          </li> 

                          <li><a>Egresos<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu" style="display: none">
                              <li class="sub_menu"><a href="<?=$this->Html->url('/ProveedorPagos/')?>"><i class="fa fa-angle-double-right"></i>Proveedor Pago</a></li>       
                              <li class="sub_menu"><a href="<?=$this->Html->url('/Egresotipos/')?>"><i class="fa fa-calculator"></i>Categoria</a></li>                           
                            </ul>
                          </li> 

                          <li><a>Cajas<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu" style="display: none">
                              <li class="sub_menu"><a href="level2.html">Level Two</a></li>                                   
                            </ul>
                          </li> 


                          <li><a>Calendario<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu" style="display: none">
                              <li class="sub_menu"><a href="<?=$this->Html->url('/Laborables/')?>"><i class="fa fa-calendar"></i>Dias Laborables</a></li>
                              <li class="sub_menu"><a href="<?=$this->Html->url('/Feriados/')?>"><i class="fa fa-calendar"></i>Dias Feriados</a></li>                                   
                            </ul>
                          </li> 

                          <li><a>Cientes<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu" style="display: none">
                               <li class="sub_menu"><a href="<?=$this->Html->url('/Tipoclientes/')?>"><i class="fa fa-angle-double-right"></i>Tipo especialidad</a></li>
                                                      
                            </ul>
                          </li>   
                    </ul>
                </li>

                      
                
                
              <li><a href="<?php echo $this->Html->url('/Tutoriales/tutoriales'); ?>">
                <i class="fa fa-video"></i> <i class="fa fa-video-camera" aria-hidden="true"></i>
                Tutoriales </a>
              </li>
              </ul>


            </div>
            
          </div>
          <!-- /sidebar menu -->

          <!-- /menu footer buttons -->
         <!--  <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
              <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
              <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
              <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Cerrar Session" href="<?=$this->Html->url('/admin/logout')?>">
              <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
          </div> -->
          <!-- /menu footer buttons -->
        </div>
      </div>

      <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav class="" role="navigation">
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="images/img.jpg" alt=""><?= $user ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Perfil</a></li>
                    <li><a href="<?=$this->Html->url('/admin/logout')?>"><i class="fa fa-sign-out pull-right"></i>Cerrar Session</a></li>
                  </ul>
                </li>

                  



                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green" id="number_notifications"></span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu"></ul>
                </li>















              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->


      <!-- page content -->
      <div class="right_col" role="main">
               <?php echo $this->Session->flash(); ?>
               <?php echo $this->fetch('content'); ?>
      <!-- footer content -->
        <footer>
          <div class="copyright-info">
            <p class="pull-right">Quantum</p>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
      <!-- /page content -->

    </div>

  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>



       <?php 
     
     




    echo $this->Html->script('gentella/custom'); 


    echo $this->Html->script('gentella/vendors/jquery/dist/jquery.min.js'); 
      /// echo $this->Html->script('gentella/vendors/bootstrap/dist/js/bootstrap.min.js'); 
    echo $this->Html->script('gentella/vendors/moment/min/moment.min.js'); 
    echo $this->Html->script('gentella/vendors/fullcalendar/dist/fullcalendar.js'); 


  	//echo $this->Html->script('gentella/bootstrap.min'); 
  	echo $this->Html->script('gentella/gauge/gauge.min'); 
  	echo $this->Html->script('gentella/gauge/gauge_demo'); 
  	echo $this->Html->script('gentella/progressbar/bootstrap-progressbar.min'); 
  	echo $this->Html->script('gentella/nicescroll/jquery.nicescroll.min'); 
  	echo $this->Html->script('gentella/icheck/icheck.min'); 
  	
  	echo $this->Html->script('gentella/datepicker/daterangepicker'); 
  	echo $this->Html->script('gentella/chartjs/chart.min'); 



    echo $this->Html->script('gentella/vendors/raphael/raphael.min.js'); 
    echo $this->Html->script('gentella/vendors/morris.js/morris.min.js'); 

    echo $this->Html->script('gentella/flot/jquery.flot'); 
    echo $this->Html->script('gentella/flot/jquery.flot.pie'); 
    echo $this->Html->script('gentella/flot/jquery.flot.orderBars'); 
    echo $this->Html->script('gentella/flot/jquery.flot.time.min'); 
    echo $this->Html->script('gentella/flot/jquery.flot.spline'); 
    echo $this->Html->script('gentella/flot/jquery.flot.stack'); 
    echo $this->Html->script('gentella/flot/jquery.flot.resize'); 
    
    echo $this->Html->script('gentella/flot/curvedLines'); 
    echo $this->Html->script('gentella/flot/date'); 

  	echo $this->Html->script('gentella/maps/jquery-jvectormap-2.0.3.min'); 
  	echo $this->Html->script('gentella/maps/gdp-data'); 
  	echo $this->Html->script('gentella/maps/jquery-jvectormap-world-mill-en'); 
  	echo $this->Html->script('gentella/maps/jquery-jvectormap-us-aea-en'); 
  	echo $this->Html->script('gentella/pace/pace.min'); 
  	echo $this->Html->script('gentella/skycons/skycons.min');
    echo $this->Html->script('gentella/sparkline/jquery.sparkline.min');  
    echo $this->Html->script('gentella/vendors/colorpicker/dist/js/bootstrap-colorpicker.min');



  ?>





  <script>
    NProgress.done();
  </script>

  <script type="text/javascript">
  function notificaciones(){    
    $.ajax({
      url: '/Notificaciones/notificaciones',
      type: 'post',
      data: {},

      success:function(response){
        var data = JSON.parse(response);
        console.log(data);
        if (data != ']') {
            var longitud = data.length;
            

            $("#number_notifications").html(longitud);

            for (var i = 0; i <= longitud - 1; i++) {


              $("#menu1").append("<li><a href='"+data[i].link+"'><span><span>"+data[i].modulo+"</span></span><span class='message'>"+data[i].info+"</span></a></li>");

               alertify.success(data[i].info);




               if (Notification) {
               if (Notification.permission !== "granted") {
               Notification.requestPermission()
               }
               var title = "Quantum"
               var extra = {
               icon: "",
               body: data[i].info
               }
               var noti = new Notification( title, extra)
               noti.onclick = {
               // Al hacer click
               }
               noti.onclose = {
               // Al cerrar
               }
               setTimeout( function() { noti.close() }, 10000)
               }
                
            }
        }
      }
    });
  }

  setInterval(function(){ notificaciones() }, 10000);
  </script>
  <!-- /datepicker -->
  <!-- /footer content -->
</body>

</html>

