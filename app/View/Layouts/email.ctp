<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts.Email.html
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
	<title><?php echo $this->fetch('title'); ?></title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>

<style type='text/css'>			    
	@media only screen and (min-device-width: 601px) {
	.content {width: 800px !important;}
	}

	.header {padding: 40px 10px 20px 10px;}

	.footer {padding: 20px 30px 15px 30px;}
	.footercopy {font-family: sans-serif; font-size: 14px; color: #ffffff;}
	.footercopy a {color: #ffffff; text-decoration: underline;}

	@media only screen and (max-width: 550px), screen and (max-device-width: 550px) {
	body[yahoo] .buttonwrapper {background-color: transparent!important;}
	body[yahoo] .button a {background-color: #e05443; padding: 15px 15px 13px!important; display: block!important;}
						    }
</style>
	<table class='content' align='center' cellpadding='0' cellspacing='0' border='0'>
	    <tr>
	        <td class='header' bgcolor='#01B9AF'>
	            
	           <table width='70' align='left' border='0' cellpadding='0' cellspacing='0'>
	               <tr>
	                   <td height='70' style='padding: 0 10px 10px 0;'>
	                       <h1 style='color:white;'>Quamtun.</h1>
	                       <h5 style='color:white;'>Detalles de Ingreso</h5>
	                   </td>
	               </tr>
	           </table>     


	        </td>
	    </tr>

	    <tr>
	        <td class='innerpadding borderbottom'>
	        	<br>
	        	<br>
	        	<br>
	            <table width='100%'' border='0' cellspacing='0' cellpadding='0'>
	                <tr>
	                    <td class='h2'>	                       
							<?php echo $this->fetch('content'); ?>
	                    </td>
	                </tr>
	              
	            </table>
	        </td>
	    </tr>
	</table>


	
</body>
</html>