<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" href="http://livedemo00.template-help.com/wt_55547/images/favicon.ico" type="image/x-icon">
    <title>Home</title>
  

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="<?= $this->Html->templateinclude('bootstrap.css', 'diagnosticoflores');?>">    

    <link rel="stylesheet" type="text/css" href="<?= $this->Html->templateinclude('search.css', 'diagnosticoflores');?>">    
       
    <link rel="stylesheet" type="text/css" href="<?= $this->Html->templateinclude('jquery.css', 'diagnosticoflores');?>">   
    <link rel="stylesheet" type="text/css" href="<?= $this->Html->templateinclude('camera.css', 'diagnosticoflores');?>">  
    <link rel="stylesheet" type="text/css" href="<?= $this->Html->templateinclude('mailform.css', 'diagnosticoflores');?>">  

    <script type="text/javascript"></script>   
    

    <!--[if lt IE 9]>
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/..">
            <img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820"
                 alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."/>
        </a>
    </div>
    <script src="js/html5shiv.js"></script>
    <![endif]-->

    <script type="text/javascript" async="" src="<?= $this->Html->templateinclude('device.js', 'diagnosticoflores');?>"></script> 


    


<style type="text/css">.fancybox-margin{margin-right:13px;}</style>


<body>

<div class="page">
<!--========================================================
                          HEADER
=========================================================-->
    <header>
        <div class="container">
            <div class="header-cnt">
                <div class="navbar-header">
                    <div class="">
                       <!-- <img width="145px" height="145px" src="<?= $this->Html->templateinclude('logo.png', 'diagnosticoflores');?>"> -->
                    </div>

                    <div class="navbar-cnt">
                        <h1 class="navbar-brand">
                            <a data-type="rd-navbar-brand" href="http://livedemo00.template-help.com/wt_55547/">Quantum</a>
                        </h1>
                        <p class="navbar-slogan">
                            Imágen Diagnóstica Especializada
                        </p>
                    </div>


                </div>

                <ul class="list-header">
                    <li><a href="#"></a></li>
                </ul>
                
            </div>
        </div>
        <div id="stuck_container" class="stuck_container">
            <nav class="navbar navbar-default navbar-static-top">
                <div class="container">
                    <ul class="navbar-nav sf-menu navbar-left sf-js-enabled sf-arrows" data-type="navbar">
                        <li class="active">
                            <a href="">INICIO</a>
                        </li>
                        <li>
                            <a href="#nosotros">NOSOTROS</a>
                        </li>

                        <li>
                            <a href="#servicio">SERVICIO AL CLIENTE</a>
                        </li>
                        <!-- <li>
                            <a onclick="muestraModalOdonto()" href="#">REGISTRATE</a>
                        </li> -->

                        <li >
                            <a href="#contacto" class="sf-with-ul">CONTACTO</a>
                        </li>
                        <?php 
                            $nivel = $this->Session->read('USUARIO_NIVEL');
                         ?>


                        <?php if ( $nivel == 2 || $nivel == 1 ): ?>
                            <li>                            
                                <a href="#blog">Blog </a>                             
                            </li>
                        <?php endif ?>


                        <li>
                            <a href="">CITAS</a>
                        </li>
                    </ul>


                    <div class="search-form">
                        

                        <?php 
                            $user = $this->Session->read('USUARIO_USER');
                         ?>


                        <?php if ( $user != '' ): ?>
                        <a href="<?=$this->Html->url('/Sites/logout')?>">CERRAR SESSION </a>                             
                        <?php endif ?>

                        <?php if ( $user == '' ): ?>
                         <a onclick="muestraModalOdonto()" href="#">REGISTRATE</a> |
                         <a onclick="iniciaSesion()" href="#">INICIAR SESION </a>                             
                        <?php endif ?>

                    </div>

                    
                </div>
            </nav>
        </div>
        </header>
        <?php echo $this->Session->flash(); ?>
        <?php echo $this->fetch('content'); ?>
    <!--========================================================
                              FOOTER
    =========================================================-->
    <footer>
        <div class="bg-secondary2">
            <div class="footer-well">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="copyright">
                                <a class="footer-brand" href="http://livedemo00.template-help.com/wt_55547/">Quantum</a>
                                <p>©
                                    <span id="copyright-year">2016</span>
                                    <a href="http://livedemo00.template-help.com/wt_55547/index-6.html">Privacy policy</a></p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <h5>Contactanos</h5>
                                CALLE 69 X 46 Y 48 #435-A COL. CENTRO MERIDA YUCATÁN. CP. 97000 Tel:  <a href="callto">(999) 9.24.23.75</a><a href="mailto:#">Email: quantumcentro@gmail.com</a>    
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <h5>Siguenos</h5>
                                <ul class="list-footer">


                                    <?php if (($empresa[0]['Empresa']['facebook']) != '' ): ?>
                                        <li><a target="_blank" href="<?=$empresa[0]['Empresa']['facebook']?>"><img width="25px" height="25px" src="<?=$this->webroot.'img/facebook.png'?>"></a></li>
                                    <?php endif ?>

                                    <?php if (($empresa[0]['Empresa']['twitter']) != '' ): ?>
                                    <li><a target="_blank" href="<?=$empresa[0]['Empresa']['twitter']?>"><img width="25px" height="25px" src="<?=$this->webroot.'img/twitter.png'?>">  </a></li>
                                    <?php endif ?>    
                                            
                                    <?php if (($empresa[0]['Empresa']['youtube']) != '' ): ?>
                                    <li><a target="_blank" href="<?=$empresa[0]['Empresa']['youtube']?>"><img width="25px" height="25px" src="<?=$this->webroot.'img/youtube.png'?>">
                                    </a></li>                                        
                                    <?php endif ?>
                                </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script type="text/javascript" src="<?= $this->Html->templateinclude('ga.js', 'diagnosticoflores');?>"></script>
<script type="text/javascript" src="<?= $this->Html->templateinclude('jquery_003.js', 'diagnosticoflores');?>"></script>
<script type="text/javascript" src="<?= $this->Html->templateinclude('jquery-migrate-1.js', 'diagnosticoflores');?>"></script>    

<script src="<?= $this->Html->templateinclude('bootstrap.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('tm-scripts.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('jquery_010.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('jquery_012.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('tmstickup.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('jquery_004.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('superfish.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('jquery_008.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('wow.js', 'diagnosticoflores');?>"></script>

<script src="<?= $this->Html->templateinclude('jquery_005.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('camera.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('TMSearch.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('jquery_011.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('jquery_002.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('jquery_006.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('jquery_007.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('jquery_009.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('jquery_013.js', 'diagnosticoflores');?>"></script>

<script src="<?= $this->Html->templateinclude('jquery.js', 'diagnosticoflores');?>"></script>
<!-- </script> -->

</body><!-- Google Tag Manager -->
</html>