<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" href="http://livedemo00.template-help.com/wt_55547/images/favicon.ico" type="image/x-icon">
    <title>Home</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="<?= $this->Html->templateinclude('bootstrap.css', 'diagnosticoflores');?>">    
    <link rel="stylesheet" type="text/css" href="<?= $this->Html->templateinclude('search.css', 'diagnosticoflores');?>">    
    <link rel="stylesheet" type="text/css" href="<?= $this->Html->templateinclude('jquery.css', 'diagnosticoflores');?>">   
    <link rel="stylesheet" type="text/css" href="<?= $this->Html->templateinclude('camera.css', 'diagnosticoflores');?>">  
    <link rel="stylesheet" type="text/css" href="<?= $this->Html->templateinclude('mailform.css', 'diagnosticoflores');?>">  


    <?php  echo $this->Html->script('gentella/alertify.min');
           echo $this->Html->script('function');

     ?>

    <script type="text/javascript" async="" src="<?= $this->Html->templateinclude('device.js', 'diagnosticoflores');?>"></script> 

    <script type="text/javascript" async="" src="http://maps.google.com/maps/api/js?key=AIzaSyABXxkIu1wUbJTQ3Se-9nif6qhgPpdyuUw&sensor=false"></script>     

    <style type="text/css">.fancybox-margin{margin-right:13px;}</style>

    
<body onload="inicializar_mapa()">
<div class="page">
<!--========================================================
                          HEADER
=========================================================-->
    <header>
        <div class="container" style="height: 70px;">
            <div class="header-cnt">
                <div class="navbar-header">
                    <div class="" style="margin-left: -55px">

                    <?php if (($empresa[0]['Empresa']['facebook']) != '' ): ?>
                    <?php
                        if(isset($empresa[0]["Empresa"]["carpeta_imagen"])){
                           $img = $empresa[0]["Empresa"]["ruta_imagen"];
                        }else{

                        }
                        echo $this->Html->image($img, array('alt' => 'Imagen', 'class'=>'img-thumbnail', 'style'=>'','width'=>'35px','height'=>'35px')); 
                    ?>
                    <?php endif ?>
                    </div>
                </div>
                <ul class="list-header" style="margin-top: 50px">
                    <li><a href="<?=$this->Html->url('/Admin')?>">Administar</a></li>
                </ul>
            </div>
        </div>
        <div id="stuck_container" class="stuck_container">
            <nav class="navbar navbar-default navbar-static-top">
                <div class="container">
                    <ul class="navbar-nav sf-menu navbar-left sf-js-enabled sf-arrows" data-type="navbar">
                        <li id="inicio_" <?php if($dir=="inicio"){ echo 'class="active"'; } ?> >
                            <a  id="inicio_a" href="<?=$this->Html->url('/Sites/index#inicio')?>">INICIO</a>
                        </li>
                        <li id="nosotros_" <?php if($dir=="nosotros"){ echo 'class="active"';} ?> >
                            <a  id="nosotros_a" href="<?=$this->Html->url('/Sites/nosotros#nosotros')?>">NOSOTROS</a>
                        </li>
                            <li id="servicio_" <?php if($dir=="servicio"){ echo 'class="active"';} ?> >
                            <a  id="servicio_a" href="<?=$this->Html->url('/Sites/servicio#servicio')?>">SERVICIO AL CLIENTE</a>
                        </li>
                        <li id="contacto_" <?php if($dir=="contacto"){ echo 'class="active"';} ?> >
                            <a  id="contacto_a" href="<?=$this->Html->url('/Sites/contacto#contacto')?>" class="sf-with-ul">CONTACTO</a>
                        </li>
                        <?php 
                            $nivel = $this->Session->read('USUARIO_NIVEL');
                            $tipo  = $this->Session->read('USUARIO_TIPO');
                         ?>
                        <?php if ( $nivel == 2 || $nivel == 1 || $tipo==1 || $tipo==2 ||$tipo==3 ): ?>
                            <li id="blog_" <?php if($dir=="blog"){ echo 'class="active"';} ?>>                            
                                <a id="blog_a" href="<?=$this->Html->url('/Sites/blog')?>">MI SITIO </a>                             
                            </li>
                            <li id="blog_"  <?php if($dir=="remitidos"){ echo 'class="active"';} ?>>                            
                                <a id="blog_a" href="<?=$this->Html->url('/Sites/remitidos')?>">MIS RADIOGRAFÍAS </a>                             
                            </li>
                        <?php endif ?>


                        </li>
                            <li id="citas_" <?php if($dir=="citas"){ echo 'class="active"';} ?> >
                            <a  id="citas_a" href="<?=$this->Html->url('/Citas/citas#citas')?>">Citas</a>
                        </li>                        
                    </ul>

                   
                    <div class="search-form" style="width: 250px;">
                        <?php 
                            $user = $this->Session->read('USUARIO_USER');
                         ?>
                        <?php if ( $user != '' ): ?>
                        BIENVENIDO <?= strtoupper($user) ?> - <a href="<?=$this->Html->url('/Sites/logout')?>">CERRAR SESSION </a> 
                        <?php endif ?>

                        <?php if ( $user == '' ): ?>
                         <a onclick="muestraModalOdonto()" href="#">REGISTRATE</a> |
                         <a onclick="iniciaSesion()" href="#">INICIAR SESION </a>                             
                        <?php endif ?>
                    </div>
                </div>
            </nav>
        </div>
        </header>
        <?php echo $this->Session->flash(); ?>
        <?php echo $this->fetch('content'); ?>
    <!--========================================================
                              FOOTER
    =========================================================-->
    <footer>
        <div class="bg-secondary2">
            <div class="footer-well">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="copyright">
                                <?php
                                    if(isset($empresa[0]["Empresa"]["carpeta_imagen"])){
                                       $img = $empresa[0]["Empresa"]["ruta_imagen"];
                                    }else{

                                    }
                                    echo $this->Html->image($img, array('alt' => 'Imagen', 'class'=>'img-thumbnail', 'width'=>'35px', 'height'=>'35px')); 
                                ?>
                                <p>©
                                    <span id="copyright-year"><?=date('Y');?></span>
                                    <a target="_blank" href="https://w3pds.com"> Programadores de Software</a>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-12 col-xs-12">
                         <?php /*
                            <h5>Contactanos</h5>
                                Tel:  <a href="callto"><?php if(isset($empresa[0]["Empresa"]["carpeta_imagen"])){ echo $empresa[0]["Empresa"]["telefono1"]; }?></a>
                                      <a href="mailto:#">Email: <?php if(isset($empresa[0]["Empresa"]["email1"])){ echo $empresa[0]["Empresa"]["email1"]; }?></a>  
                                       */
                        ?>  
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <h5>Siguenos</h5>
                                <ul class="list-footer">
                                    <?php if (($empresa[0]['Empresa']['facebook']) != '' ): ?>
                                        <li><a target="_blank" href="<?=$empresa[0]['Empresa']['facebook']?>"><img width="25px" height="25px" src="<?=$this->webroot.'img/facebook.png'?>"></a></li>
                                    <?php endif ?>

                                    <?php if (($empresa[0]['Empresa']['twitter']) != '' ): ?>
                                    <li><a target="_blank" href="<?=$empresa[0]['Empresa']['twitter']?>"><img width="25px" height="25px" src="<?=$this->webroot.'img/twitter.png'?>">  </a></li>
                                    <?php endif ?>    
                                            
                                    <?php if (($empresa[0]['Empresa']['youtube']) != '' ): ?>
                                    <li><a target="_blank" href="<?=$empresa[0]['Empresa']['youtube']?>"><img width="25px" height="25px" src="<?=$this->webroot.'img/youtube.png'?>">
                                    </a></li>                                        
                                    <?php endif ?>
                                </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script type="text/javascript" src="<?= $this->Html->templateinclude('ga.js', 'diagnosticoflores');?>"></script>
<script type="text/javascript" src="<?= $this->Html->templateinclude('jquery_003.js', 'diagnosticoflores');?>"></script>
<script type="text/javascript" src="<?= $this->Html->templateinclude('jquery-migrate-1.js', 'diagnosticoflores');?>"></script>    

<script src="<?= $this->Html->templateinclude('bootstrap.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('tm-scripts.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('jquery_010.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('jquery_012.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('tmstickup.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('jquery_004.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('superfish.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('jquery_008.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('wow.js', 'diagnosticoflores');?>"></script>

<script src="<?= $this->Html->templateinclude('jquery_005.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('camera.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('TMSearch.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('jquery_011.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('jquery_002.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('jquery_006.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('jquery_007.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('jquery_009.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('jquery_013.js', 'diagnosticoflores');?>"></script>
<script src="<?= $this->Html->templateinclude('jquery.js', 'diagnosticoflores');?>"></script>
<?php
     //echo $this->Html->script('gentella/jquery.min');
     //echo $this->Html->script('function');
?> 
<!-- </script> -->

</body><!-- Google Tag Manager -->
</html>

<!--  <script type="text/javascript">

      function inicializar_mapa() {
        var myLatLng = {lat: 9.912018, lng: -67.3593077};
        var map = new google.maps.Map(document.getElementById('mapa_div'), {
          zoom: 17,
          center: myLatLng
        });  

        

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'Hello World!'
        });
      }
    </script> -->



     <?php if (isset($_SESSION['CITAENAGENDA'])): ?>
         <?php if ($_SESSION['CITAENAGENDA'] == '1'): ?>
            <?php $_SESSION['CITAENAGENDA'] = '0'; ?>
            <div id="modalFacturaAgendada" class="modal fade in" role="dialog" style="display: block;">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class='close' onclick="modalFacturaAgendada()">&times;</button>
                    <h4 class="modal-title">Información</h4>
                  </div>
                  <div class="modal-body">
                    <p>Su cita fue agendada correctamente.</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="modalFacturaAgendada()">Cerrar</button>
                  </div>
                </div>

              </div>
            </div>
                
         <?php endif ?>
     <?php endif ?>
     


          <?php if (isset($_SESSION['SOLICITAFACTURA'])): ?>
         <?php if ($_SESSION['SOLICITAFACTURA'] == '1'): ?>
            <?php $_SESSION['SOLICITAFACTURA'] = '0'; ?>

            <div id="modalCitaAgendada" class="modal fade in" role="dialog" style="display: block;">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class='close' onclick="modalCitaAgendada()">&times;</button>
                    <h4 class="modal-title">Información</h4>
                  </div>
                  <div class="modal-body">
                    <p>Solicitud de factura realizada satisfactoriamente.</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="modalCitaAgendada()">Cerrar</button>
                  </div>
                </div>

              </div>
            </div>
                
         <?php endif ?>
     <?php endif ?>

    

    <script type="text/javascript">
    function modalCitaAgendada(){
        $("#modalCitaAgendada").hide();
    }
    function modalFacturaAgendada(){
        $("#modalFacturaAgendada").hide();
    }
    function inicializar_mapa(){

      if (document.getElementById( "mapa_div" )) {

        var myLatLng = {lat: 23.0020941, lng: -104.6088300};  
        var marker;
    <?php 
        $cont = 0;
        echo "var map = new google.maps.Map(document.getElementById('mapa_div'), {
            zoom: 7,
            center: myLatLng
            });";

        foreach ($sucursal as $sucursales) {


            

        if ($sucursales['Sucursales']['latitud']!='') {


            

        echo "marker = new google.maps.Marker(
                    {
                      position: {lat: ".$sucursales['Sucursales']['latitud'].", lng: ".$sucursales['Sucursales']['longitud']."},
                      map: map,
                      title: '".$sucursales['Sucursales']['denominacion']."'
                    }
                );";
    
        $cont++;
                
        }
                  

            

        }
        
     ?>
     }
      }
    </script>



