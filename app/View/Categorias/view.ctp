<div class="panel">
	<div class="panel-heading">
	<h2><?php echo __('Categoria'); ?></h2>		
	</div>
	



<div class="panel panel-body">
<h2><?php echo __('Categoria'); ?></h2>
	<table class="table table-striped">

	<tbody>
		
		
		<tr><td><?php echo __('Id'); ?></td>
		<td>
			<?php echo h($categoria['Categoria']['id']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Categoria'); ?></td>
		<td>
			<?php echo h($categoria['Categoria']['categoria']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Created'); ?></td>
		<td>
			<?php echo dateformat($categoria['Categoria']['created']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Modified'); ?></td>
		<td>
			<?php echo dateformat($categoria['Categoria']['modified']); ?>
			&nbsp;
		</td></tr>
	
	</tbody>
	</table>
</div>

</div>