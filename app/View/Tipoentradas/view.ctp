<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Tipos de Asistencias </h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
					<li><i class="fa fa-arrow-left"></i>
					<a href="<?=$this->Html->url('/tipoentradas/')?>"> Volver a Tipo de Asistencias </a> </li>              
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Tipo de Asistencias'); ?></h2>
	<table class="table table-striped">
	<tbody>
		<tr><td><?php echo __('Denominación'); ?></td>
		<td>
			<?php echo h($tipoentrada['Tipoentrada']['denominacion']); ?>
			&nbsp;
		</td></tr>
        
        <tr><td><?php echo __('Descripción'); ?></td>
		<td>
			<?php echo h($concepto['Concepto']['descripcion']); ?>
			&nbsp;
		</td></tr>

	</tbody>
	</table>
</div>

</div>