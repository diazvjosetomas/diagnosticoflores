<div class="panel">
	<div class="panel-heading">
	<h2><?php echo __('Contacto'); ?></h2>		
	</div>
<div class="panel panel-body">

	<table class="table table-striped">
	<tbody>
		<tr><td><?php echo __('Nombres'); ?></td>
		<td>
			<?php echo h($contacto['Contacto']['nombres']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Email'); ?></td>
		<td>
			<?php echo h($contacto['Contacto']['email']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Asunto'); ?></td>
		<td>
			<?php echo h($contacto['Contacto']['asunto']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Mensaje'); ?></td>
		<td>
			<?php echo h($contacto['Contacto']['mensaje']); ?>
			&nbsp;
		</td></tr>
	</tbody>
	</table>
</div>

</div>