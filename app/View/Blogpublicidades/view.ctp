<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Blog publicidad</h3>
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
				<li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Blog publicidad'), array('action' => 'index')); ?></li>             
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Blog publicidad'); ?></h2>
	<table class="table table-striped">
	<tbody>
		
		<tr><td><?php echo __('Titulo'); ?></td>
		<td>
			<?php echo h($blogpublicidade['Blogpublicidade']['titulo']); ?>
			&nbsp;
		</td></tr>
	</tbody>
	</table>
	<table class="table table-striped">
	<tbody>
		<tr><td><?php echo __('Imagen'); ?></td></tr>
            <tr><td>
                <?php
                        if(isset($blogpublicidade["Blogpublicidade"]["carpeta_imagen"])){
                           $img = $blogpublicidade["Blogpublicidade"]["ruta_imagen"];
                        }else{

                        }
                        echo $this->Html->image($img, array('alt' => 'Imagen', 'class'=>'img-thumbnail', 'style'=>'border: 0;'));
                    ?>
                &nbsp;
            </td><tr>
	</tbody>
	</table>
</div>
</div>