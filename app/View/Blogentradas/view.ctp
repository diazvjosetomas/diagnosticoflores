<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Blog entradas</h3>
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
				<li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Blog entradas'), array('action' => 'index')); ?></li>             
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Blog entradas'); ?></h2>
	<table class="table table-striped">
	<tbody>
		<tr><td><?php echo __('Titulo'); ?></td>
		<td>
			<?php echo h($blogentrada['Blogentrada']['titulo']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Contenido'); ?></td>
		<td>
			<?php echo h($blogentrada['Blogentrada']['contenido']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Blog categoria'); ?></td>
		<td>
			<?php echo h($blogentrada['Blogcategoria']['denominacion']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('User'); ?></td>
		<td>
			<?php echo h($blogentrada['User']['user']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Created'); ?></td>
		<td>
			<?php echo h($blogentrada['Blogentrada']['created']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Modified'); ?></td>
		<td>
			<?php echo h($blogentrada['Blogentrada']['modified']); ?>
			&nbsp;
		</td></tr>
		
	</tbody>
	</table>
	<table class="table table-striped">
	<tbody>
		<tr><td><?php echo __('Imagen'); ?></td></tr>
            <tr><td>
                <?php
                        if(isset($blogentrada["Blogentrada"]["carpeta_imagen"])){
                           $img = $blogentrada["Blogentrada"]["ruta_imagen"];
                        }else{

                        }
                        echo $this->Html->image($img, array('alt' => 'Imagen', 'class'=>'img-thumbnail', 'style'=>'border: 0;'));
                    ?>
                &nbsp;
            </td><tr>
	</tbody>
	</table>
</div>
</div>