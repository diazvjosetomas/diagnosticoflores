<?php echo $this->Form->create('Admin', array('url'=>'/admin/login', 'class'=>'login-form')); ?>
<div class="login-wrap">
    <p class="login-img"><i class="icon_lock_alt"></i></p>
    <div class="input-group">
      <span class="input-group-addon"><i class="icon_profile"></i></span>
      <input type="text" class="form-control" placeholder="Username" autofocus name="data[Admin][user]">
    </div>
    <div class="input-group">
        <span class="input-group-addon"><i class="icon_key_alt"></i></span>
        <input type="password" class="form-control" placeholder="Password" name="data[Admin][password]" >
    </div>
    <label class="checkbox">
        <span class="pull-right">
        <?php echo $this->Html->link('Olvidaste la contraseña?',  array('controller'=>'admin', 'action'=>'recuperar'), array()); ?>
        </span>
    </label>
    <button class="btn btn-primary btn-lg btn-block" type="submit">Entrar</button>
</div>
</form>