<?php echo $this->Form->create('Admin', array('url'=>'/admin/recuperare', 'class'=>'login-form')); ?>
<div class="login-wrap">
    <p class="login-img"><i class="icon_lock_alt"></i></p>
    <div class="input-group">
      <span class="input-group-addon"><i class="icon_profile"></i></span>
      <input type="text" class="form-control" placeholder="Username" autofocus name="data[Admin][user]">
    </div>
    <button class="btn btn-primary btn-lg btn-block" type="submit">Enviar</button>
</div>
</form>