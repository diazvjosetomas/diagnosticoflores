<?php echo $this->Form->create('Admin', array('url'=>'/admin/cambiar_clave', 'class'=>'login-form')); ?>
<div class="login-wrap">
    <p class="login-img"><i class="icon_lock_alt"></i></p>
    <div class="input-group">
        <span class="input-group-addon"><i class="icon_key_alt"></i></span>
        <input type="hidden"  name="data[UserWebmaster][id]"  value="<?= $id ?>" >
        <input type="hidden"  name="data[UserWebmaster][key]" value="<?= $key ?>" >
        <input type="password" class="form-control" placeholder="Password" name="data[UserWebmaster][password1]" >
    </div>
    <div class="input-group">
        <span class="input-group-addon"><i class="icon_key_alt"></i></span>
        <input type="password" class="form-control" placeholder="confirme Password" name="data[UserWebmaster][password2]" >
    </div>
    <button class="btn btn-primary btn-lg btn-block" type="submit">Enviar</button>
</div>
</form>