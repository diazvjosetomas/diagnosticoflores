<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Sucursales </h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href=""> Home</a></li>	
					<li><i class="fa fa-arrow-left"></i>
					<a href="<?=$this->Html->url('/sucursales/')?>"> Volver a Sucursales </a> </li>              
		</ol>		
	</div>	    
</div>

<div class="row">
		     <div class="col-sm-12">
	  		   <section class="panel" style="padding: 10px;">

<div class="sucursales form">
<?php echo $this->Form->create('Sucursale'); ?>
	<fieldset>
		<legend><?php echo __('Add Sucursal'); ?></legend>
<div class='row'><div class='col-sm-12'>		<?php echo $this->Form->input('denominacion', array('label'=>'Denominacion','class'=>'form-control')); ?> 
 </div></div>

 <div class='row'><div class='col-sm-12'>		<?php echo $this->Form->input('direccion', array('label'=>'Direccion','class'=>'form-control')); ?> 
 </div></div>

 <div class='row'><div class='col-sm-12'>		<?php echo $this->Form->input('telefono', array('label'=>'Teléfono','class'=>'form-control')); ?> 
 </div></div>
 
 <div class='row'><div class='col-sm-12'>		<?php echo $this->Form->input('email', array('label'=>'Email','class'=>'form-control')); ?> 
 </div></div>	
 
 <div class='row'><div class='col-sm-12'>		<?php echo $this->Form->input('latitud', array('label'=>'Latitud','class'=>'form-control')); ?> 
 </div></div>

  <div class='row'><div class='col-sm-12'>		<?php echo $this->Form->input('longitud', array('label'=>'Longitud','class'=>'form-control')); ?> 
 </div></div>
 <div class="row">
 	
 <div class="col-md-12 col-sm-12 col-xs-12">
      <?php 
      //echo $this->Form->input('color', array('label'=>'Color','class'=>'form-control','type'=>'color')); ?> 
      <label>
      	Color
      </label>

      <select name="data[Sucursale][color]" class="form-control">
      	<option value="#ff80bf" style="background-color: #ff80bf;">Color 1</option>
      	<option value="#ff5500" style="background-color: #ff5500;">Color 2</option>
      	<option value="#80ff00" style="background-color: #80ff00;">Color 3</option>
      	<option value="#1a1aff" style="background-color: #1a1aff;">Color 4</option>
      	<option value="#bf4040" style="background-color: #bf4040;">Color 5</option>
      	<option value="#ffff1a" style="background-color: #ffff1a;">Color 6</option>
      	<option value="#800040" style="background-color: #800040;">Color 7</option>
      	<option value="#ff471a" style="background-color: #ff471a;">Color 8</option>
      	<option value="#ff3385" style="background-color: #ff3385;">Color 9</option>
      	<option value="#b3b3ff" style="background-color: #b3b3ff;">Color 10</option>
      </select>
                
        
 </div>
 </div>


 </fieldset>
 	<br>
	<div class="pull-right">		
		<?php echo $this->Form->submit(__('Submit', true), array('label'=>false, 'class'=>'btn btn-primary'));?>
		<?php echo $this->Form->end(__('')); ?>
	</div>

</div>

</section>
</div>
</div>
