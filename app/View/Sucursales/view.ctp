
<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Sucursales </h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
					<li><i class="fa fa-arrow-left"></i>
					<a href="<?=$this->Html->url('/sucursales/')?>"> Volver a Sucursales </a> </li>              
		</ol>		
	</div>	    
</div>



<div class="panel">



<div class="panel panel-body">
<h2><?php echo __('Sucursale'); ?></h2>
	<table class="table table-striped">

	<tbody>
		<tr><td><?php echo __('Denominación'); ?></td>
		<td>
			<?php echo h($sucursale['Sucursale']['denominacion']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Direccion'); ?></td>
		<td>
			<?php echo h($sucursale['Sucursale']['direccion']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Telefono'); ?></td>
		<td>
			<?php echo h($sucursale['Sucursale']['telefono']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Email'); ?></td>
		<td>
			<?php echo h($sucursale['Sucursale']['email']); ?>
			&nbsp;
		</td></tr>
	
	</tbody>
	</table>
</div>

</div>