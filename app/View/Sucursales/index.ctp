<div class="container">


<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Sucursales</h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
                    <li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Sucursales'), array('action' => 'index')); ?></li>
		</ol>		
	</div>	    
</div>







<div class="panel">
	<div class="panel-heading panel-success">
		<h4>&nbsp;
			<span class="pull-right">
				<div class="btn-group">
				  <button type="button" class="btn btn-primary">Actions</button>
				  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
				    <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu" role="menu">
				    <li><?php echo $this->Html->link(__('Nuevo Sucursal'), array('action' => 'add')); ?></li>
				    
				  </ul>
				</div>


			
			</span>
		</h4>
		
	</div>

	<div class="panel-body">








<div class="sucursales index">
	
	<table id="datos" cellpadding="0" cellspacing="0" class="table table-striped table-rounded">
	<thead>
	<tr>
			<th><?php echo h('Denominación'); ?></th>
			<th><?php echo h('Dirección'); ?></th>
			<th><?php echo h('Teléfono'); ?></th>
			<th><?php echo h('Email'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($sucursales as $sucursale): ?>
	<tr>
		<td><?php echo h($sucursale['Sucursale']['denominacion']); ?>&nbsp;</td>
		<td><?php echo h($sucursale['Sucursale']['direccion']); ?>&nbsp;</td>
		<td><?php echo h($sucursale['Sucursale']['telefono']); ?>&nbsp;</td>
		<td><?php echo h($sucursale['Sucursale']['email']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Detalles'), array('action' => 'view', $sucursale['Sucursale']['id']), array('class'=>'btn btn-sm btn-default')); ?>
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $sucursale['Sucursale']['id']), array('class'=>'btn btn-sm btn-success')); ?>
			<?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $sucursale['Sucursale']['id']), array('class'=>'btn btn-sm btn-danger'), __('¿Esta seguro que desea eliminar esta sucursal <b>%s</b>?', $sucursale['Sucursale']['denominacion'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	

	</div>
</div>
</div>
</div>

<script type="text/javascript">
    //$(document).ready(function() {
        $('#datos').DataTable( {
            dom: 'Bfrtlip',          
            responsive: true,
	        buttons: [
	            {
	                extend: 'excel',
	                exportOptions: {
	                    columns: [0,1,2,3]
	                }
	            },
	            {
	                extend: 'pdf',
	                exportOptions: {
	                    columns: [0,1,2,3]
	                }
	            },
	            {
	                extend: 'copy',
	                exportOptions: {
	                    columns: [0,1,2,3]
	                }
	            },
	            {
	                extend: 'csv',
	                exportOptions: {
	                    columns: [0,1,2,3]
	                }
	            },
	            {
	                extend: 'print',
	                exportOptions: {
	                    columns: [0,1,2,3]
	                }
	            }



	        ],
            "language": 
            {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ".",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        } );
    //} );
</script>