
<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Empresas</h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
                    <li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Empresas'), array('action' => 'index')); ?></li>
		</ol>		
	</div>	    
</div>


<div class="row">
		     <div class="col-sm-12">
	  		   <section class="panel" style="padding: 10px;">

<div class="empresas form">
<?php echo $this->Form->create('Empresa', array('enctype'=>'multipart/form-data')); ?>
	<fieldset>
		<legend><?php echo __('Edit Empresa'); ?></legend>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('id', array('label'=>'id','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('nombre', array('label'=>'Nombre','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('imagen', array('label'=>'Logo','class'=>'form-control', 'type'=>'file')); ?> 
		<?php echo'<p for="" class="help-block text-red">* El logo debe estar en formato .png</p>';?>
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('email1', array('label'=>'Email1','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('email2', array('label'=>'Email2','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('telefono1', array('label'=>'Teléfono1','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('telefono2', array('label'=>'Teléfono2','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('identificacionfiscal', array('label'=>'Identificación fiscal','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('direccion', array('label'=>'Dirección','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('web', array('label'=>'Dirección Web','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('facebook', array('label'=>'Facebook','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('twitter', array('label'=>'Twitter','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('youtube', array('label'=>'Youtube','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('snapchat', array('label'=>'Snapchat','class'=>'form-control')); ?> 
 </div>
</div>
	</fieldset>
	<br />
	<div class="pull-right">		
		<?php echo $this->Form->submit(__('Guardar', true), array('label'=>false, 'class'=>'btn btn-primary'));?>
		<?php echo $this->Form->end(__('')); ?>
	</div>

</div>

</section>
</div>
</div>
