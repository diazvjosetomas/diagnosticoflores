<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Empresas</h3>
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
				<li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Empresas'), array('action' => 'index')); ?></li>             
		</ol>		
	</div>	    
</div>
<div class="panel">
	<div class="panel-heading">
	<h2><?php echo __('Empresa'); ?></h2>		
	</div>
<div class="panel panel-body">
<h2><?php echo __('Empresa'); ?></h2>
	<table class="table table-striped">
	<tbody>
		<tr><td><?php echo __('Nombre'); ?></td>
		<td>
			<?php echo h($empresa['Empresa']['nombre']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Email1'); ?></td>
		<td>
			<?php echo h($empresa['Empresa']['email1']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Email2'); ?></td>
		<td>
			<?php echo h($empresa['Empresa']['email2']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Telefono1'); ?></td>
		<td>
			<?php echo h($empresa['Empresa']['telefono1']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Telefono2'); ?></td>
		<td>
			<?php echo h($empresa['Empresa']['telefono2']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Identificacionfiscal'); ?></td>
		<td>
			<?php echo h($empresa['Empresa']['identificacionfiscal']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Direccion'); ?></td>
		<td>
			<?php echo h($empresa['Empresa']['direccion']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Facebook'); ?></td>
		<td>
			<?php echo h($empresa['Empresa']['facebook']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Twitter'); ?></td>
		<td>
			<?php echo h($empresa['Empresa']['twitter']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Youtube'); ?></td>
		<td>
			<?php echo h($empresa['Empresa']['youtube']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Snapchat'); ?></td>
		<td>
			<?php echo h($empresa['Empresa']['snapchat']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Logo'); ?></td>
		<td>
			<?php
                    if(isset($empresa["Empresa"]["carpeta_imagen"])){
                       $img = $empresa["Empresa"]["ruta_imagen"];
                    }else{

                    }
	                echo $this->Html->image($img, array('alt' => 'Imagen', 'class'=>'img-thumbnail', 'style'=>'border: 0;'));
	        	?>
			&nbsp;
		</td>
		</tr>
	</tbody>
	</table>
</div>
</div>