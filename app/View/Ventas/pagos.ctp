<div class="row">
	        <div class='col-sm-12'>
	            <div class="panel panel-solid panel-default">
	                <div class="panel-header with-border">
	                    <div class="panel-tools pull-right">
	                        <button id="newProductoBtn" class="btn btn-panel-tool" data-toggle="tooltip" title="Agregar otro producto" data-widget="chat-pane-toggle"><i class="glyphicon glyphicon-plus"></i></button>
	                    </div><!-- /.box-tools -->
	                </div><!-- /.box-header -->
	                <div class="panel-body">
	                    <div id="productos-container">
	                        <div id="productoscount">
	                        <label for="producto_0"></label>
	                            <div class="form-group pro-con" id="producto_0">
	                                <div class="col-md-2">
	                                    <select class="form-control tel-0" id="Productos_0__Pro" name="data[Ingreso][Productos][0][pro]" required="required" onchange="Javascript:precio_p(0);">
										<option value="">--Servicio--</option>
										<?php
	                                        
	        	                             foreach($ingresotipos as $ingresotipo){
	        	                             	echo "  '<option value=\"".$ingresotipo['Ingresotipo']['id']."\">".$ingresotipo['Ingresotipo']['denominacion']."</option>'+   ";
	        	                             }
	    	                             ?>
										</select>
	                                </div>
	                                <label class="control-label col-md-1">Cantidad</label>
	                                <div class="col-md-1">
										<input class="form-control pro-0-ext text-box single-line"  id="Productos_0__Cantidad" name="data[Ingreso][Productos][0][cant]" required="required" type="text" value="0"  onchange="Javascript:total_precio(0);"/>
	                                </div>
	                                <label class="control-label col-md-1">Pre. U.</label>
	                                <div class="col-md-1">
	                                    <input class="form-control pro-0-ext text-box single-line"  id="Productos_0__Precio"   name="data[Ingreso][Productos][0][pre]" required="required" type="text" value="0" readonly=true />
	                                </div>
	                                <label class="control-label col-md-1">Descuento</label>
	                                <div class="col-md-1">
	                                    <input class="form-control pro-0-ext text-box single-line"  id="Productos_0__Descuento"   name="data[Ingreso][Productos][0][des]" required="required" type="text" value="0" onchange="Javascript:total_precio(0);"/>
	                                </div>
	                                <label class="control-label col-md-1">Sub Total</label>
	                                <div class="col-md-1">
	                                    <input class="form-control pro-0-ext text-box single-line" id="Productos_0__Total" name="data[Ingreso][Productos][0][total]"  required="required" type="text" value="0" readonly=true />
	                                </div>
	                                <div class="col-md-1">
	                                     <input type="checkbox" name="data[Ingreso][Productos][0][exento]" value="1"  onchange="Javascript:total_precio(0);" id="Productos_0__Exento"> Exento
	                                </div>
	                                <div class="col-md-1">
	                                    <button class="btn btn-danger btn-sm" onclick="return false;"><span class="glyphicon glyphicon-trash"></span></button>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	   		</div>
<script type="text/javascript">
	total_precio(0);
</script>	
<script type="text/javascript">
$(document).ready(function() {
    $("#newProductoBtn").click(function (e) {
        var MaxInputs       = 100;
        var x = $("#productos-container #productoscount").length + 1;
        var FieldCount = x-1;
        if(x <= MaxInputs) //max input box allowed
        {
            FieldCount++;'+'
            $("#productos-container").append('<br><div id="productoscount"><label for="producto_'+FieldCount+'"></label>'+
            	                                '<div class="form-group pro-con" id="producto_'+FieldCount+'">'+
			                                        '<div class="col-md-2">'+
			                                            '<select class="form-control tel-'+FieldCount+'" id="Productos_'+FieldCount+'__Pro" name="data[Ingreso][Productos]['+FieldCount+'][pro]" onchange="Javascript:precio_p('+FieldCount+');">'+
														'<option value="">--Servicio--</option>'+
														<?php
				            	                             foreach($ingresotipos as $ingresotipo){
					        	                             	echo "  '<option value=\"".$ingresotipo['Ingresotipo']['id']."\">".$ingresotipo['Ingresotipo']['denominacion']."</option>'+   ";
					        	                             }
			            	                             ?>
														'</select>'+
			                                        '</div>'+
			                                        '<label class="control-label col-md-1">Cantidad</label>'+
			                                        '<div class="col-md-1">'+
														'<input class="form-control pro-'+FieldCount+'-ext text-box single-line"  id="Productos_'+FieldCount+'__Cantidad" name="data[Ingreso][Productos]['+FieldCount+'][cant]" type="text" value="0" onchange="Javascript:total_precio('+FieldCount+');" />'+
			                                        '</div>'+
			                                        '<label class="control-label col-md-1">Pre. U.</label>'+
			                                        '<div class="col-md-1">'+
			                                            '<input class="form-control producto-mask text-box single-line" id="Productos_'+FieldCount+'__Precio" name="data[Ingreso][Productos]['+FieldCount+'][pre]"  type="text" value="0" readonly=true />'+
			                                        '</div>'+
			                                        '<label class="control-label col-md-1">Descuento</label>'+
			                                        '<div class="col-md-1">'+
			                                            '<input class="form-control producto-mask text-box single-line" id="Productos_'+FieldCount+'__Descuento" name="data[Ingreso][Productos]['+FieldCount+'][des]"  type="text" value="0" onchange="Javascript:total_precio('+FieldCount+');"/>'+
			                                        '</div>'+
			                                        '<label class="control-label col-md-1">Sub Total</label>'+
			                                        '<div class="col-md-1">'+
			                                            '<input class="form-control producto-mask text-box single-line" id="Productos_'+FieldCount+'__Total" name="data[Ingreso][Productos]['+FieldCount+'][total]"  type="text" value="0" readonly=true />'+
			                                        '</div>'+
			                                        '<div class="col-md-1">'+
					                                     '<input type="checkbox" name="data[Ingreso][Productos]['+FieldCount+'][exento]" id="Productos_'+FieldCount+'__Exento" value="1"> Exento'+
					                                '</div>'+
			                                        '<div class="col-md-1">'+
			                                            '<button class="btn btn-danger btn-sm" onclick="$(\'#producto_'+FieldCount+'\').remove();  return false;"><span class="glyphicon glyphicon-trash"></span></button>'+
			                                        '</div>'+
			                                    '</div></div>');
            x++; //text box increment
        }
        return false;
    });
});
</script>   	