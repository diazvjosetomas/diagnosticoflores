<?php


class fpdfview extends FPDF{
	function Footer(){}
	function Header(){

		
	}
}
$fpdf = new fpdfview('P','mm','A4');
$fpdf->AddPage();

$fpdf->Image('img/upload/logo.png',10,10,75,20);



$fpdf->Ln(25);
$fpdf->SetFont('Arial','B',18);
$fpdf->Cell(0,5,"REPORTE INGRESOS",'',1,'C');



$fpdf->SetFont('Arial','',12);
$fpdf->Ln(3);
$fpdf->Cell(45,5, "Doctor: ".ucwords(strtolower($ingreso['User']['user'])),'',0,'L');
$fpdf->Ln(5);
$fpdf->Cell(45,5, "Personal que atendió: ".ucwords(strtolower($ingreso['Personale']['nombres'])),'',0,'L');
$fpdf->Ln(5);
$fpdf->Cell(45,5, "Cliente: ".ucwords(strtolower($ingreso['Cliente']['nombres'])),'',0,'L');
$fpdf->Ln(5);
$fpdf->Cell(45,5, "Folio: ".ucwords(strtolower($ingreso['Ingreso']['folio'])),'',0,'L');
$fpdf->Ln(5);
$fpdf->Cell(45,5, "N de Estudio: ".ucwords(strtolower($ingreso['Ingreso']['n_estudio'])),'',0,'L');
$fpdf->Ln(5);
$fpdf->Cell(45,5, "Nota: ".ucwords(strtolower($ingreso['Ingreso']['nota'])),'',0,'L');
$fpdf->Ln(5);
$fpdf->Cell(45,5, "Monto I.V.A.: ".ucwords(strtolower($ingreso['Ingreso']['monto_iva'])),'',0,'L');
$fpdf->Ln(5);
$fpdf->Cell(45,5, "Subtotal: ".ucwords(strtolower($ingreso['Ingreso']['subtotal'])),'',0,'L');
$fpdf->Ln(5);
$fpdf->Cell(45,5, "Total: ".ucwords(strtolower($ingreso['Ingreso']['total'])),'',0,'L');
$fpdf->Ln(5);
$fpdf->Cell(45,5, "Tipo de Pago: ".ucwords(strtolower($ingreso['Ingresotipopago']['denominacion'])),'',0,'L');
$fpdf->Ln(5);
$fpdf->Cell(45,5, "Sucursal: ".ucwords(strtolower($ingreso['Sucursale']['denominacion'])),'',0,'L');

$fpdf->Output('F','DATA.pdf');

?>
