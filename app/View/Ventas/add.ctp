
<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Nota de Ventas</h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
                    <li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Ventas'), array('action' => 'index')); ?></li>
		</ol>		
	</div>	    
</div>
 

<div class="row">
		     <div class="col-sm-12">
	  		   <section class="panel" style="padding: 10px;">

<div class="ingresos form">
<?php echo $this->Form->create('Ingreso'); ?>
	<fieldset>
		<legend><?php echo __('Add Ingreso'); ?></legend>
		<div class='row'>
		<div class='col-sm-12'>
				<?php echo $this->Form->input('user_id', array('label'=>'Remitido','class'=>'form-control', 'empty'=>'--Seleccione--')); ?> 
		 </div>
		</div>
		<div class='row'>
		<div class='col-sm-12'>
				<?php echo $this->Form->input('personale_id', array('label'=>'Atendido','class'=>'form-control', 'empty'=>'--Seleccione--')); ?> 
		</div>
		</div>
		<div class='row'>
		<div class='col-sm-12'>
				<?php echo $this->Form->input('cliente_id', array('label'=>'Cliente','class'=>'form-control', 'empty'=>'--Seleccione--')); ?> 
		</div>
		</div>
		<div class='row'>
		<div class='col-sm-12'>
				<?php echo $this->Form->input('sucursale_id', array('label'=>'Sucursales','class'=>'form-control', 'empty'=>'--Seleccione--', "onChange"=>"selectTagRemote('".$this->Html->url('/Ingresos/folios')."', 'div-folio', this.value);")); ?>
		 </div>
		</div>
		<div class='row'>
		<div class='col-sm-12' id="div-folio">
				<?php echo $this->Form->input('folio', array('readonly'=>true, 'label'=>'Folio','class'=>'form-control')); ?> 
		 </div>
		</div>
		<div class='row'>
		<div class='col-sm-12'>
				<?php echo $this->Form->input('n_estudio', array('label'=>'N° estudio','class'=>'form-control')); ?> 
		 </div>
		</div>
		<div class='row'>
		<div class='col-sm-12'>


					<?php 

					$hora = date('d/m/Y ').(date('H') - 5) .date(':i:s');
					

				 ?>
				<?php echo $this->Form->input('fecha', array('value'=>$hora,'readonly'=>true, 'label'=>'Fecha y Hora','class'=>'form-control')); ?> 
		 </div>
		</div>
		<div class='row'>
		<div class='col-sm-12'>
				<?php echo $this->Form->input('nota', array('label'=>'Nota','class'=>'form-control')); ?> 
		 </div>
		</div>
		<div class='row'>
		<div class='col-sm-12'>
				<?php echo $this->Form->input('ingresotipopago_id', array('label'=>'Tipo de pago','class'=>'form-control', 'empty'=>'--Seleccione--', "onChange"=>"selectTagRemote('".$this->Html->url('/Ingresos/pagos')."', 'div-pagos', this.value);")); ?>
		 </div>
		</div>
        <br>
        <?php
        //pr($ingresotipos);
        ?>
        <div id="div-pagos">
	        <div class="row">
	        <div class='col-sm-12'>
	            <div class="panel panel-solid panel-default">
	                <div class="panel-header with-border">
	                    <div class="panel-tools pull-right">
	                        <button id="newProductoBtn" class="btn btn-panel-tool" data-toggle="tooltip" title="Agregar otro producto" data-widget="chat-pane-toggle"><i class="glyphicon glyphicon-plus"></i></button>
	                    </div><!-- /.box-tools -->
	                </div><!-- /.box-header -->
	                <div class="panel-body">
	                    <div id="productos-container">
	                        <div id="productoscount">
	                        <label for="producto_0"></label>
	                            <div class="form-group pro-con" id="producto_0">
	                                <div class="col-md-2">
	                                    <select class="form-control tel-0" id="Productos_0__Pro" name="data[Ingreso][Productos][0][pro]" required="required" onchange="Javascript:precio_p(0);">
										<option value="">--Servicio--</option>
										<?php
	                                        
	        	                             foreach($ingresotipos as $ingresotipo){
	        	                             	echo "  '<option value=\"".$ingresotipo['Ingresotipo']['id']."\">".$ingresotipo['Ingresotipo']['denominacion']."</option>'+   ";
	        	                             }
	    	                             ?>
										</select>
	                                </div>
	                                <label class="control-label col-md-1">Cantidad</label>
	                                <div class="col-md-1">
										<input class="form-control pro-0-ext text-box single-line"  id="Productos_0__Cantidad" name="data[Ingreso][Productos][0][cant]" required="required" type="text" value="0"  onchange="Javascript:total_precio(0);"/>
	                                </div>
	                                <label class="control-label col-md-1">Pre. U.</label>
	                                <div class="col-md-1">
	                                    <input style="width:80px;" class="form-control pro-0-ext text-box single-line"  id="Productos_0__Precio"   name="data[Ingreso][Productos][0][pre]" required="required" type="text" value="0" readonly=true />
	                                </div>
	                                <label class="control-label col-md-1">Descuento</label>
	                                <div class="col-md-1">
	                                    <input class="form-control pro-0-ext text-box single-line"  id="Productos_0__Descuento"   name="data[Ingreso][Productos][0][des]" required="required" type="text" value="0" onchange="Javascript:total_precio(0);"/>
	                                </div>
	                                <label class="control-label col-md-1">Sub Total</label>
	                                <div class="col-md-1">
	                                    <input style="width:80px;" class="form-control pro-0-ext text-box single-line" id="Productos_0__Total" name="data[Ingreso][Productos][0][total]"  required="required" type="text" value="0" readonly=true />
	                                </div>
	                                <div class="col-md-1">
	                                     <input type="checkbox" name="data[Ingreso][Productos][0][exento]" value="1"  onchange="Javascript:total_precio(0);" id="Productos_0__Exento"> Exento
	                                </div>
	                                <div class="col-md-1">
	                                    <button class="btn btn-danger btn-sm" onclick="return false;"><span class="glyphicon glyphicon-trash"></span></button>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	   		</div>
	   		<script type="text/javascript">
			$(document).ready(function() {
			    $("#newProductoBtn").click(function (e) {
			        var MaxInputs       = 100;
			        var x = $("#productos-container #productoscount").length + 1;
			        var FieldCount = x-1;
			        if(x <= MaxInputs) //max input box allowed
			        {
			            FieldCount++;'+'
			            $("#productos-container").append('<br><div id="productoscount"><label for="producto_'+FieldCount+'"></label>'+
			            	                                '<div class="form-group pro-con" id="producto_'+FieldCount+'">'+
						                                        '<div class="col-md-2">'+
						                                            '<select class="form-control tel-'+FieldCount+'" id="Productos_'+FieldCount+'__Pro" name="data[Ingreso][Productos]['+FieldCount+'][pro]" onchange="Javascript:precio_p('+FieldCount+');">'+
																	'<option value="">--Servicio--</option>'+
																	<?php
							            	                             foreach($ingresotipos as $ingresotipo){
								        	                             	echo "  '<option value=\"".$ingresotipo['Ingresotipo']['id']."\">".$ingresotipo['Ingresotipo']['denominacion']."</option>'+   ";
								        	                             }
						            	                             ?>
																	'</select>'+
						                                        '</div>'+
						                                        '<label class="control-label col-md-1">Cantidad</label>'+
						                                        '<div class="col-md-1">'+
																	'<input class="form-control pro-'+FieldCount+'-ext text-box single-line"  id="Productos_'+FieldCount+'__Cantidad" name="data[Ingreso][Productos]['+FieldCount+'][cant]" type="text" value="0" onchange="Javascript:total_precio('+FieldCount+');" />'+
						                                        '</div>'+
						                                        '<label class="control-label col-md-1">Pre. U.</label>'+
						                                        '<div class="col-md-1">'+
						                                            '<input class="form-control producto-mask text-box single-line" id="Productos_'+FieldCount+'__Precio" name="data[Ingreso][Productos]['+FieldCount+'][pre]"  type="text" value="0" readonly=true />'+
						                                        '</div>'+
						                                        '<label class="control-label col-md-1">Descuento</label>'+
						                                        '<div class="col-md-1">'+
						                                            '<input class="form-control producto-mask text-box single-line" id="Productos_'+FieldCount+'__Descuento" name="data[Ingreso][Productos]['+FieldCount+'][des]"  type="text" value="0" onchange="Javascript:total_precio('+FieldCount+');"/>'+
						                                        '</div>'+
						                                        '<label class="control-label col-md-1">Sub Total</label>'+
						                                        '<div class="col-md-1">'+
						                                            '<input class="form-control producto-mask text-box single-line" id="Productos_'+FieldCount+'__Total" name="data[Ingreso][Productos]['+FieldCount+'][total]"  type="text" value="0" readonly=true />'+
						                                        '</div>'+
						                                        '<div class="col-md-1">'+
								                                     '<input type="checkbox" name="data[Ingreso][Productos]['+FieldCount+'][exento]" id="Productos_'+FieldCount+'__Exento" value="1"> Exento'+
								                                '</div>'+
						                                        '<div class="col-md-1">'+
						                                            '<button class="btn btn-danger btn-sm" onclick="$(\'#producto_'+FieldCount+'\').remove();  return false;"><span class="glyphicon glyphicon-trash"></span></button>'+
						                                        '</div>'+
						                                    '</div></div>');
			            x++; //text box increment
			        }
			        return false;
			    });
			});
			</script>
   		</div>
        <br>
        
        <div class='row'>
		<div class='col-sm-4 pull-right'>
				<?php echo $this->Form->input('subtotal', array('label'=>'Sub total','class'=>'form-control', 'type'=>'text', 'readonly'=>true, 'value'=>'0')); ?> 
		 </div>
		</div>
		<div class='row'>
		<div class='col-sm-4 pull-right'>
				<?php echo $this->Form->input('iva', array('value'=>$iva, 'label'=>'Iva %','class'=>'form-control', 'type'=>'text', 'readonly'=>true)); ?> 
		 </div>
		</div>
		<div class='row'>
		<div class='col-sm-4 pull-right'>
				<?php echo $this->Form->input('monto_iva', array('value'=>'0', 'label'=>'Monto iva','class'=>'form-control', 'type'=>'text', 'readonly'=>true)); ?> 
		 </div>
		</div>
		<div class='row'>
		<div class='col-sm-4 pull-right'>
				<?php echo $this->Form->input('monto_exento', array('value'=>'0', 'label'=>'Monto exento','class'=>'form-control', 'type'=>'text', 'readonly'=>true)); ?> 
		 </div>
		</div>
        <div class='row'>
		<div class='col-sm-4 pull-right'>
				<?php echo $this->Form->input('total', array('label'=>'Total','class'=>'form-control', 'type'=>'text', 'readonly'=>true, 'value'=>'0')); ?> 
		 </div>
		</div>


	</fieldset>
	<br />
	<div class="pull-right">		
		<?php echo $this->Form->submit(__('Guardar', true), array('label'=>false, 'class'=>'btn btn-primary'));?>
		<?php echo $this->Form->end(__('')); ?>
	</div>

</div>

</section>
</div>
</div>
