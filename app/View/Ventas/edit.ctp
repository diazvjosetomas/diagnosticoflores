
<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Ventas</h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
                    <li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Ventas'), array('action' => 'index')); ?></li>
		</ol>		
	</div>	    
</div>


<div class="row">
		     <div class="col-sm-12">
	  		   <section class="panel" style="padding: 10px;">
					<div class="ingresos form">
					<?php echo $this->Form->create('Ingreso'); ?>
						<fieldset>
							<legend><?php echo __('Editar Nota de Venta'); ?></legend>
					<div class='row'>
					<div class='col-sm-12'>
							<?php echo $this->Form->input('id', array('label'=>'id','class'=>'form-control')); ?> 
					 </div>
					</div>
					<div class='row'>
					<div class='col-sm-12'>
							<?php echo $this->Form->input('user_id', array('label'=>'Remitido','class'=>'form-control')); ?> 
					 </div>
					</div>
					<div class='row'>
					<div class='col-sm-12'>
							<?php echo $this->Form->input('personale_id', array('label'=>'Atendido','class'=>'form-control')); ?> 
					 </div>
					</div>
					<div class='row'>
					<div class='col-sm-12'>
							<?php echo $this->Form->input('cliente_id', array('label'=>'Cliente','class'=>'form-control')); ?> 
					 </div>
					</div>
					<div class='row'>
					<div class='col-sm-12'>
							<?php echo $this->Form->input('sucursale_id', array('label'=>'Sucursales','class'=>'form-control', 'empty'=>'--Seleccione--')); ?> 
					 </div>
					</div>
					<div class='row'>
					<div class='col-sm-12'>
							<?php echo $this->Form->input('folio', array('readonly'=>true, 'label'=>'Folio','class'=>'form-control')); ?> 
					 </div>
					</div>
					<div class='row'>
					<div class='col-sm-12'>
							<?php echo $this->Form->input('n_estudio', array('label'=>'N° estudio','class'=>'form-control')); ?> 
					 </div>
					</div>

					<div class='row'>
					<div class='col-sm-12'>


							<?php echo $this->Form->input('fecha', array('type'=>'text', 'readonly'=>true, 'label'=>'Fecha y Hora','class'=>'form-control')); ?> 
					 </div>
					</div>

					<div class='row'>
					<div class='col-sm-12'>
							<?php echo $this->Form->input('nota', array('label'=>'Nota','class'=>'form-control')); ?> 
					 </div>
					</div>

                    <div class='row'>
					<div class='col-sm-12'>
							<?php echo $this->Form->input('status', array('label'=>'Nota','class'=>'form-control', 'options'=>array('1'=>'pendiente', '2'=>'enviada'))); ?> 
					 </div>
					</div>


					<div class='row'>
					<div class='col-sm-12'>
							<?php echo $this->Form->input('error', array('label'=>'Nota','class'=>'form-control', 'options'=>array('1'=>'si', '2'=>'no'))); ?> 
					 </div>
					</div>


					<div class='row'>
					<div class='col-sm-12'>
							<?php echo $this->Form->input('ingresotipopago_id', array('label'=>'Tipo de pago','class'=>'form-control')); ?> 
					 </div>
					</div>
					<br>
			        <?php
			        //pr($ingresotipos);
			        ?>
			        <div class="row">
			        <div class='col-sm-12'>
			            <div class="panel panel-solid panel-default">
			                <div class="panel-header with-border">
			                    <div class="panel-tools pull-right">
			                        <button id="newProductoBtn" class="btn btn-panel-tool" data-toggle="tooltip" title="Agregar otro producto" data-widget="chat-pane-toggle"><i class="glyphicon glyphicon-plus"></i></button>
			                    </div><!-- /.box-tools -->
			                </div><!-- /.box-header -->
			                <div class="panel-body">
			                    <div id="productos-container">
			                    <?php $c = 0;foreach($ingresodetalles as $ingresodetalle){ ?>
			                        <div id="productoscount">
			                        <label for="producto_0"></label>
			                            <div class="form-group pro-con" id="producto_<?= $c ?>">
			                                <div class="col-md-2">
			                                    <select class="form-control tel-<?= $c ?>" id="Productos_<?= $c ?>__Pro" name="data[Ingreso][Productos][<?= $c ?>][pro]" required="required" onchange="Javascript:precio_p(<?= $c ?>);">
												<option value="">--Servicio--</option>
												<?php
			                                        
			        	                             foreach($ingresotipos as $ingresotipo){
			        	                             	if($ingresodetalle['Ingresodetalle']['ingresotipo_id']==$ingresotipo['Ingresotipo']['id']){
				        	                             	echo "  '<option value=\"".$ingresotipo['Ingresotipo']['id']."\" selected>".$ingresotipo['Ingresotipo']['denominacion']."</option>'+   ";
				        	                             }else{
				        	                             	echo "  '<option value=\"".$ingresotipo['Ingresotipo']['id']."\">".$ingresotipo['Ingresotipo']['denominacion']."</option>'+   ";
				        	                             }
			        	                             }
			    	                             ?>
												</select>
			                                </div>
			                                <label class="control-label col-md-1">Cantidad</label>
			                                <div class="col-md-1">
												<input class="form-control pro-<?= $c ?>-ext text-box single-line"  id="Productos_<?= $c ?>__Cantidad" name="data[Ingreso][Productos][<?= $c ?>][cant]" required="required" type="text" value="<?= $ingresodetalle['Ingresodetalle']['cantidad'] ?>"  onchange="Javascript:total_precio(<?= $c ?>);"/>
			                                </div>
			                                <label class="control-label col-md-1">Pre. U.</label>
			                                <div class="col-md-1">
			                                    <input style="width:80px;" class="form-control pro-<?= $c ?>-ext text-box single-line"  id="Productos_<?= $c ?>__Precio"   name="data[Ingreso][Productos][<?= $c ?>][pre]" required="required" type="text" value="<?= $ingresodetalle['Ingresodetalle']['monto'] ?>" readonly=true />
			                                </div>
			                                <label class="control-label col-md-1">Descuento</label>
			                                <div class="col-md-1">
			                                    <input style="width:70px;" class="form-control pro-<?= $c ?>-ext text-box single-line"  id="Productos_<?= $c ?>__Descuento"   name="data[Ingreso][Productos][<?= $c ?>][des]" required="required" type="text" value="<?= $ingresodetalle['Ingresodetalle']['descuento'] ?>" onchange="Javascript:total_precio(<?= $c ?>);"/>
			                                </div>
			                                <label class="control-label col-md-1">Sub Total</label>
			                                <div class="col-md-1">
			                                    <input style="width:80px;" class="form-control pro-<?= $c ?>-ext text-box single-line" id="Productos_<?= $c ?>__Total" name="data[Ingreso][Productos][<?= $c ?>][total]"  required="required" type="text" value="<?= $ingresodetalle['Ingresodetalle']['total'] ?>" readonly=true />
			                                </div>
			                                <div class="col-md-1">
			                                     <input type="checkbox" <?php echo $ingresodetalle['Ingresodetalle']['exento']==1?'':'checked' ?> id="Productos_<?= $c ?>__Exento" name="data[Ingreso][Productos][<?= $c ?>][exento]" value="1"  onchange="Javascript:total_precio(<?= $c ?>);"/> Exento
			                                </div>
			                                <div class="col-md-1">
			                                    <button class="btn btn-danger btn-sm" onclick="return false;"><span class="glyphicon glyphicon-trash"></span></button>
			                                </div>
			                            </div>
			                        </div>
			                    <?php $c++;} ?>
			                    </div>
			                </div>
			            </div>
			        </div>
			   		</div>
			        <br>
			        <div class='row'>
					<div class='col-sm-4 pull-right'>
							<?php echo $this->Form->input('subtotal', array('label'=>'Sub total','class'=>'form-control', 'type'=>'text', 'readonly'=>true)); ?> 
					 </div>
					</div>
					<div class='row'>
					<div class='col-sm-4 pull-right'>
							<?php echo $this->Form->input('iva', array('label'=>'Iva %','class'=>'form-control', 'type'=>'text', 'readonly'=>true)); ?> 
					</div>
					</div>
					<div class='row'>
					<div class='col-sm-4 pull-right'>
							<?php echo $this->Form->input('monto_iva', array('label'=>'Monto iva','class'=>'form-control', 'type'=>'text', 'readonly'=>true)); ?> 
					 </div>
					</div>
					<div class='row'>
					<div class='col-sm-4 pull-right'>
							<?php echo $this->Form->input('monto_exento', array('label'=>'Monto exento','class'=>'form-control', 'type'=>'text', 'readonly'=>true)); ?> 
					 </div>
					</div>
			        <div class='row'>
					<div class='col-sm-4 pull-right'>
							<?php echo $this->Form->input('total', array('label'=>'Total','class'=>'form-control', 'type'=>'text', 'readonly'=>true)); ?> 
					 </div>
					</div>
				</fieldset>
				<br />
				<div class="pull-right">		
					<?php echo $this->Form->submit(__('Guardar', true), array('label'=>false, 'class'=>'btn btn-primary'));?>
					<?php echo $this->Form->end(__('')); ?>
				</div>

				</div>

				</section>
</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $("#newProductoBtn").click(function (e) {
        var MaxInputs       = 100;
        var x = $("#productos-container #productoscount").length + 1;
        var FieldCount = x-1;
        if(x <= MaxInputs) //max input box allowed
        {
            FieldCount++;'+'
            $("#productos-container").append('<br><div id="productoscount"><label for="producto_'+FieldCount+'"></label>'+
            	                                '<div class="form-group pro-con" id="producto_'+FieldCount+'">'+
			                                        '<div class="col-md-2">'+
			                                            '<select class="form-control tel-'+FieldCount+'" id="Productos_'+FieldCount+'__Pro" name="data[Ingreso][Productos]['+FieldCount+'][pro]" onchange="Javascript:precio_p('+FieldCount+');">'+
														'<option value="">--Servicio--</option>'+
														<?php
				            	                             foreach($ingresotipos as $ingresotipo){
					        	                             	echo "  '<option value=\"".$ingresotipo['Ingresotipo']['id']."\">".$ingresotipo['Ingresotipo']['denominacion']."</option>'+   ";
					        	                             }
			            	                             ?>
														'</select>'+
			                                        '</div>'+
			                                        '<label class="control-label col-md-1">Cantidad</label>'+
			                                        '<div class="col-md-1">'+
														'<input class="form-control pro-'+FieldCount+'-ext text-box single-line"  id="Productos_'+FieldCount+'__Cantidad" name="data[Ingreso][Productos]['+FieldCount+'][cant]" type="text" value="0" onchange="Javascript:total_precio('+FieldCount+');" />'+
			                                        '</div>'+
			                                        '<label class="control-label col-md-1">Pre. U.</label>'+
			                                        '<div class="col-md-1">'+
			                                            '<input class="form-control producto-mask text-box single-line" id="Productos_'+FieldCount+'__Precio" name="data[Ingreso][Productos]['+FieldCount+'][pre]"  type="text" value="0" readonly=true />'+
			                                        '</div>'+
			                                        '<label class="control-label col-md-1">Descuento</label>'+
			                                        '<div class="col-md-1">'+
			                                            '<input class="form-control producto-mask text-box single-line" id="Productos_'+FieldCount+'__Descuento" name="data[Ingreso][Productos]['+FieldCount+'][des]"  type="text" value="0" onchange="Javascript:total_precio('+FieldCount+');"/>'+
			                                        '</div>'+
			                                        '<label class="control-label col-md-1">Sub Total</label>'+
			                                        '<div class="col-md-1">'+
			                                            '<input class="form-control producto-mask text-box single-line" id="Productos_'+FieldCount+'__Total" name="data[Ingreso][Productos]['+FieldCount+'][total]"  type="text" value="0" readonly=true />'+
			                                        '</div>'+
			                                        '<div class="col-md-1">'+
					                                     '<input type="checkbox" name="data[Ingreso][Productos]['+FieldCount+'][exento]" id="Productos_'+FieldCount+'__Exento" value="1" /> Exento'+
					                                '</div>'+
			                                        '<div class="col-md-1">'+
			                                            '<button class="btn btn-danger btn-sm" onclick="$(\'#producto_'+FieldCount+'\').remove();  return false;"><span class="glyphicon glyphicon-trash"></span></button>'+
			                                        '</div>'+
			                                    '</div></div>');
            x++; //text box increment
        }
        return false;
    });
});
</script>
