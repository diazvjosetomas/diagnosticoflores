<?php 


		$cabeceras  = "MIME-Version: 1.0\r\n";
		$cabeceras .= "Content-Transfer-Encoding: 8Bit\r\n";
		$cabeceras .= "Content-Type: text/html; charset=\"utf-8\"\r\n";
		$cabeceras .= "Reply-to: \r\n";
		$cabeceras .= "From: quantumadmon@gmail.com \r\n";
		$cabeceras .= "Errors-To: \r\n";


		// ----------------------------------------------------------------------
		// Cuerpo
		$cuerpo = '';				
		$cuerpo .= "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>\r\n";
		$cuerpo .= "<html xmlns='http://www.w3.org/1999/xhtml'>\r\n";
		$cuerpo .= "<head>\r\n";
		$cuerpo .= "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />\r\n";
		$cuerpo .= "<title>Notificacion de Cita</title>\r\n";
		$cuerpo .= "<style type='text/css'> \r\n";
		$cuerpo .= "body {margin: 0; padding: 0; min-width: 100%!important;}\r\n";
		$cuerpo .= ".content {width: 100%; max-width: 600px;}</style> </head>  \r\n";
		$cuerpo .= "<style type='text/css'>";
		$cuerpo .= "			    
				    @media only screen and (min-device-width: 601px) {
				    .content {width: 800px !important;}
				    }

				    .header {padding: 40px 10px 20px 10px;}


				    .footer {padding: 20px 30px 15px 30px;}
				    .footercopy {font-family: sans-serif; font-size: 14px; color: #ffffff;}
				    .footercopy a {color: #ffffff; text-decoration: underline;}

				    @media only screen and (max-width: 550px), screen and (max-device-width: 550px) {
				    body[yahoo] .buttonwrapper {background-color: transparent!important;}
				    body[yahoo] .button a {background-color: #e05443; padding: 15px 15px 13px!important; display: block!important;}
				    }

				    table {
				       width: 100%;
				       border: 1px solid #000;
				    }
				    th, td {
				       width: 25%;
				       text-align: left;
				       vertical-align: top;
				       border: 1px solid #000;
				       border-collapse: collapse;
				       padding: 0.3em;
				       caption-side: bottom;
				    }

				    .td1{
				    	background-color:#cce6ff;
				    }

				    .td2{
				    	background-color:;
				    }

				    caption {
				       padding: 0.3em;
				       color: #fff;
				        background: #000;
				    }
				    th {
				       background: #eee;
				    }
				    </style>\r\n";
		$cuerpo .= "    <body yahoo bgcolor='#f6f8f1'>
		  
		                    <table class='content' align='center' cellpadding='0' cellspacing='0' border='0'>
		                        <tr>
		                            <td class='header' bgcolor='#01B9AF'>
		                                
		                               <table width='70' align='left' border='0' cellpadding='0' cellspacing='0'>
		                                   <tr>
		                                       <td height='70' style='padding: 0 10px 10px 0;'>
		                                           <h1 style='color:white;'>Quantum.</h1>
		                                           
		                                       </td>
		                                   </tr>
		                               </table>     


		                            </td>
		                        </tr>

		                        <tr>
		                            <td class='innerpadding borderbottom'>
		                            	<br>
		                            	<br>
		                            	<br>
		                                <table width='100%'' border='0' cellspacing='0' cellpadding='0'>
		                                    <tr>
		                                        <td class='h2'>
		                                        
		                                           Estimado(o), Sr(a). ".ucwords(strtolower($ingreso['Cliente']['nombres']))." ".ucwords(strtolower($ingreso['Cliente']['apellidos']))."<br>

		                                           <table  width='100%'' border='0' cellspacing='0' cellpadding='0'>
		                                           <tbody>
		                                           	
		                                           	<tr class='td1'><td> Doctor(a)</td>
		                                           	<td>
		                                           		 ".ucwords(strtolower($ingreso['User']['user']))."
		                                           		&nbsp;
		                                           	</td></tr>
		                                           	<tr><td>Personal que atendió</td>
		                                           	<td>
		                                           		".ucwords(strtolower($ingreso['Personale']['nombres']))."
		                                           		&nbsp;
		                                           	</td></tr>
		                                           	<tr class='td1'><td>Cliente</td>
		                                           	<td>
		                                           		".ucwords(strtolower($ingreso['Cliente']['nombres']))."
		                                           		&nbsp;
		                                           	</td></tr>
		                                           	<tr><td>Folio</td>
		                                           	<td>
		                                           		".$ingreso['Ingreso']['folio']."
		                                           		&nbsp;
		                                           	</td></tr>
		                                           	<tr class='td1'><td>N Estudio</td>
		                                           	<td>
		                                           		".$ingreso['Ingreso']['n_estudio']."
		                                           		&nbsp;
		                                           	</td></tr>
		                                           	<tr><td>Nota</td>
		                                           	<td>
		                                           		".$ingreso['Ingreso']['nota']."
		                                           		&nbsp;
		                                           	</td></tr>

		                                           	<tr class='td1'>
		                                           		<td>Monto I.V.A.</td>
		                                           		<td>".$ingreso['Ingreso']['monto_iva']."</td>
		                                           	</tr>

		                                           	<tr>
		                                           		<td>Subtotal</td>
		                                           		<td> ".$ingreso['Ingreso']['subtotal']."</td>
		                                           	</tr>

		                                           	<tr class='td1'>
		                                           		<td>Monto Total</td>
		                                           		<td>".$ingreso['Ingreso']['total']."</td>
		                                           	</tr>

		                                           	<tr>
		                                           		<td>Tipo de Pago</td>
		                                           		<td>".$ingreso['Ingresotipopago']['denominacion']."</td>
		                                           	</tr>

		                                           	<tr>
		                                           		<td class='td1'>Sucursal</td>
		                                           		<td>
		                                           			".$ingreso['Sucursale']['denominacion']."
		                                           		</td>
		                                           	</tr>

		                                           </tbody>
		                                           </table>

		                                       
		                                        </td>
		                                    </tr>
		                                   
		                                </table>
		                            </td>
		                        </tr>
		                    </table>

		    </body>
		</html>\r\n";
	mail( $email, $asunto, $cuerpo, $cabeceras );









 ?>