<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Asignacitas</h3>
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
				<li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Asignacitas'), array('action' => 'index')); ?></li>             
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Asignacita'); ?></h2>
	<table class="table table-striped">
	<tbody>
		<tr><td><?php echo __('Id'); ?></td>
		<td>
			<?php echo h($asignacita['Asignacita']['id']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Cita'); ?></td>
		<td>
			<?php echo $this->Html->link($asignacita['Cita']['nombres'], array('controller' => 'citas', 'action' => 'view', $asignacita['Cita']['id'])); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('User'); ?></td>
		<td>
			<?php echo $this->Html->link($asignacita['User']['user'], array('controller' => 'users', 'action' => 'view', $asignacita['User']['id'])); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Estatus'); ?></td>
		<td>


			<?php
			if ($asignacita['Asignacita']['estatus'] == 0) {
				$asigna = 'No asignado';
			}else{
				$asigna = 'Asignado';
			}
			echo h($asigna); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Created'); ?></td>
		<td>
			<?php echo h($asignacita['Asignacita']['created']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Modified'); ?></td>
		<td>
			<?php echo h($asignacita['Asignacita']['modified']); ?>
			&nbsp;
		</td></tr>
	
	</tbody>
	</table>
</div>
</div>