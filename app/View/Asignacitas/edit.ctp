
<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Asignacitas</h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
                    <li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a asignación de cita'), array('action' => 'index')); ?></li>
		</ol>		
	</div>	    
</div>


<div class="row">
		     <div class="col-sm-12">
	  		   <section class="panel" style="padding: 10px;">

<div class="asignacitas form">
<?php echo $this->Form->create('Asignacita'); ?>
	<fieldset>
		<legend><?php echo __('Editar asignación de cita'); ?></legend>
<div class='row'>

<div class='col-sm-12'>
		<?php echo $this->Form->input('id', array('label'=>'id','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('cita_id', array('label'=>'Cita','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('user_id', array('label'=>'Médico','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('estatus', array('label'=>'Estatus','class'=>'form-control','options'=>array('0'=>'Sin asignar','1'=>'Asignar'))); ?> 
 </div>
</div>
	</fieldset>
	<br />
	<div class="pull-right">		
		<?php echo $this->Form->submit(__('Guardar', true), array('label'=>false, 'class'=>'btn btn-primary'));?>
		<?php echo $this->Form->end(__('')); ?>
	</div>

</div>

</section>
</div>
</div>
