
<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Laborables</h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
                    <li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Laborables'), array('action' => 'index')); ?></li>
		</ol>		
	</div>	    
</div>


<div class="row">
		     <div class="col-sm-12">
	  		   <section class="panel" style="padding: 10px;">

<div class="laborables form">
<?php echo $this->Form->create('Laborable'); ?>
	<fieldset>
		<legend><?php echo __('Añadir Día Laborable'); ?></legend>
<div class='row'>
<div class='col-sm-12'>
		<?php 

			$dias =  array('1' => 'Domingo','2' => 'Lunes','3'=>'Martes','4'=>'Miércoles','5'=>'Jueves','6'=>'Viernes','7'=>'Sábado' );
			$booleam = array('1' => 'No', '2' => 'Si');

		 ?>
		<?php echo $this->Form->input('dia', array('label'=>'Día','class'=>'form-control','options' => $dias)); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('manana', array('label'=>'Mañana','class'=>'form-control','options'=>$booleam)); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('tarde', array('label'=>'Tarde','class'=>'form-control','options'=>$booleam)); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('noche', array('label'=>'Noche','class'=>'form-control','options'=>$booleam)); ?> 
 </div>
</div>
	</fieldset>
	<br />
	<div class="pull-right">		
		<?php echo $this->Form->submit(__('Guardar', true), array('label'=>false, 'class'=>'btn btn-primary'));?>
		<?php echo $this->Form->end(__('')); ?>
	</div>

</div>

</section>
</div>
</div>
