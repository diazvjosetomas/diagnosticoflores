<div class="container">
<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Laborables</h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
					              
		</ol>		
	</div>	    
</div>

<div class="panel">
	<div class="panel-heading panel-success">
		<h4>&nbsp;
			<span class="pull-right">
				<div class="btn-group">
				  <button type="button" class="btn btn-primary">Acción</button>
				  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
				    <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu" role="menu">
				    <li><?php echo $this->Html->link(__('Nuevo Laborable'), array('action' => 'add')); ?></li>
				  </ul>
				</div>
			</span>
		</h4>
	</div>
	<div class="panel-body">
<div class="laborables index">
	<table id="Laborable" cellpadding="0" cellspacing="0" class="table table-striped table-rounded">
	<thead>
	<tr>
		
			<th><?php echo h('Dia'); ?></th>
			<th><?php echo h('Mañana'); ?></th>
			<th><?php echo h('Tarde'); ?></th>
			<th><?php echo h('Noche'); ?></th>			
			<th class="actions"><?php echo __('Acción'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($laborables as $laborable): ?>
	<tr>
		
		<?php

			$dias =  array('1' => 'Domingo','2' => 'Lunes','3'=>'Martes','4'=>'Miércoles','5'=>'Jueves','6'=>'Viernes','7'=>'Sábado' );
			$booleam = array('1' => 'No', '2' => 'Si');

		?>





		<td><?php echo h($dias[$laborable['Laborable']['dia']]); ?>&nbsp;</td>
		<td><?php echo h($booleam[$laborable['Laborable']['manana']]); ?>&nbsp;</td>
		<td><?php echo h($booleam[$laborable['Laborable']['tarde']]); ?>&nbsp;</td>
		<td><?php echo h($booleam[$laborable['Laborable']['noche']]); ?>&nbsp;</td>
		
		<td class="actions">
			<?php echo $this->Html->link(__('Detalles'), array('action' => 'view', $laborable['Laborable']['id']), array('class'=>'btn btn-sm btn-default')); ?>
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $laborable['Laborable']['id']), array('class'=>'btn btn-sm btn-success')); ?>
			<?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $laborable['Laborable']['id']), array('class'=>'btn btn-sm btn-danger'), __('¿Desea eliminar el día <b>%s</b>?', $laborable['Laborable']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
</div>
</div>
</div>
<script type="text/javascript">
    //$(document).ready(function() {
        $('#Laborable').DataTable( {
            dom: 'Bfrtlip',          
            responsive: true,
	        buttons: [
	            {
	                extend: 'excel',
	                exportOptions: {
	                    columns: [0,1,2,3]
	                }
	            },
	            {
	                extend: 'pdf',
	                exportOptions: {
	                    columns: [0,1,2,3]
	                }
	            },
	            {
	                extend: 'copy',
	                exportOptions: {
	                    columns: [0,1,2,3]
	                }
	            },
	            {
	                extend: 'csv',
	                exportOptions: {
	                    columns: [0,1,2,3]
	                }
	            },
	            {
	                extend: 'print',
	                exportOptions: {
	                    columns: [0,1,2,3]
	                }
	            }



	        ],
            "language": 
            {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ".",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        } );
    //} );
</script>