<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Laborables</h3>
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
				<li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Laborables'), array('action' => 'index')); ?></li>             
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Laborable'); ?></h2>
	<table class="table table-striped">
	<tbody>
		<?php $booleam = array('1' => 'No', '2' => 'Si'); 

			  $dias =  array('1' => 'Domingo','2' => 'Lunes','3'=>'Martes','4'=>'Miércoles','5'=>'Jueves','6'=>'Viernes','7'=>'Sábado' );
		?>

		<tr><td><?php echo __('Dia'); ?></td>
		<td>
			<?php echo h($dias[$laborable['Laborable']['dia']]); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Manana'); ?></td>
		<td>
			<?php echo h($booleam[$laborable['Laborable']['manana']]); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Tarde'); ?></td>
		<td>
			<?php echo h($booleam[$laborable['Laborable']['tarde']]); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Noche'); ?></td>
		<td>
			<?php echo h($booleam[$laborable['Laborable']['noche']]); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Creado'); ?></td>
		<td>
			<?php echo h($laborable['Laborable']['created']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Modificado'); ?></td>
		<td>
			<?php echo h($laborable['Laborable']['modified']); ?>
			&nbsp;
		</td></tr>
	
	</tbody>
	</table>
</div>
</div>