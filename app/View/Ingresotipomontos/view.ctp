<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Precio de servicio</h3>
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
				<li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Precio de servicio'), array('action' => 'index')); ?></li>             
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Precio de servicio'); ?></h2>
	<table class="table table-striped">
	<tbody>
		<tr><td><?php echo __('Tipo de ingreso'); ?></td>
		<td>
			<?php echo h($ingresotipomonto['Ingresotipo']['denominacion']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Tipo de pago'); ?></td>
		<td>
			<?php echo h($ingresotipomonto['Ingresotipopago']['denominacion']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Monto'); ?></td>
		<td>
			<?php echo h($ingresotipomonto['Ingresotipomonto']['monto']); ?>
			&nbsp;
		</td></tr>
	</tbody>
	</table>
</div>
</div>