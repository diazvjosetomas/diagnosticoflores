<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Promociones</h3>
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
				<li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Promociones'), array('action' => 'index')); ?></li>             
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">

	<table class="table table-striped">
	<tbody>
		
		<tr><td><?php echo __('Promocion'); ?></td>
		<td>
			<?php echo h($promocione['Promocione']['denominacion']); ?>
			&nbsp;
		</td></tr>
		
		<tr><td><?php echo __('Periodo'); ?></td>
		<td>
			<?php echo $this->Html->link($promocione['Periodo']['denominacion'], array('controller' => 'periodos', 'action' => 'view', $promocione['Periodo']['id'])); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Publico'); ?></td>
		<td>
			<?php echo $this->Html->link($promocione['Publico']['denominacion'], array('controller' => 'publicos', 'action' => 'view', $promocione['Publico']['id'])); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Estatus'); ?></td>
		<td>
			<?php echo h($promocione['Promocione']['status']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Creado'); ?></td>
		<td>
			<?php echo h($promocione['Promocione']['created']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Modificado'); ?></td>
		<td>
			<?php echo h($promocione['Promocione']['modified']); ?>
			&nbsp;
		</td></tr>
	
	</tbody>
	</table>
</div>
</div>