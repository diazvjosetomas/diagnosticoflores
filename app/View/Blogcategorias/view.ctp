<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i>Blog categoria</h3>
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
				<li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Blog categoria'), array('action' => 'index')); ?></li>             
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Blog categoria'); ?></h2>
	<table class="table table-striped">
	<tbody>
		<tr><td><?php echo __('Denominación'); ?></td>
		<td>
			<?php echo h($blogcategoria['Blogcategoria']['denominacion']); ?>
			&nbsp;
		</td></tr>
	</tbody>
	</table>
</div>
</div>