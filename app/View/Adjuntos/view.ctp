<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Adjunto'); ?></h2>


	<table class="table table-striped">
		<tr>
			<td>Estudio</td>
			<td>Cliente</td>
			<td>Servicio</td>
		</tr>

		<tr>
			<td>
				<?php echo h($adjunto['Adjunto']['denominacion']); ?>
			</td>
			<td>
				<?php echo $adjunto['Cliente']['nombres'].''. $adjunto['Cliente']['apellidos'] ;?>
			</td>
			<td>
				<?php echo $adjunto['Servicio']['denominacion']; ?>
			</td>
		</tr>
	</table>
</div>
</div>

<?php 
	$imgCount = 1;
	foreach ($imgadjunto as $key) {
		echo "<a data-toggle='modal' data-target='#myModal".$imgCount."' class='btn btn-primary'>";
		echo "Ver Imagen ".$imgCount;
		echo "</a>";

		$img = $this->webroot.'img/'.$key["Imgadjunto"]["ruta_imagen"];

		echo '<div id="myModal'.$imgCount.'" class="modal fade" role="dialog">
  				<div class="modal-dialog">
				    <!-- Modal content-->
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal">&times;</button>
				        <h4 class="modal-title">Vista de Imagen</h4>
				      </div>
				      <div class="modal-body">
				       <img width="570"  src="'.$img.'"> 
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				      </div>
				    </div>

				  </div>
				</div>';

	}

 ?>

 