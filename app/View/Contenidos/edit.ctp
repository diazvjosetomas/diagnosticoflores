<div class="row"><div class="col-lg-12">		<h3 class="page-header"><i class="fa fa-th"></i>Contenido<? echo '</h3>' ?>;
		
		<ol class="breadcrumb">		<li><i class="fa fa-home"></i><a href=""> Home</a></li>		<li><i class="fa fa-th"></i> Contenido </li>              
		</ol>		</div>	    </div>

<div class="row">
		     <div class="col-sm-12">
	  		   <section class="panel" style="padding: 10px;">

<div class="contenidos form">
<?php echo $this->Form->create('Contenido'); ?>
	<fieldset>
		<legend><?php echo __('Edit Contenido'); ?></legend>
<div class='row'><div class='col-sm-12'>		<?php echo $this->Form->input('id', array('label'=>'id','class'=>'form-control')); ?> 
 </div></div><div class='row'><div class='col-sm-12'>		<?php echo $this->Form->input('categoria_id', array('label'=>'categoria_id','class'=>'form-control')); ?> 
 </div></div><div class='row'><div class='col-sm-12'>		<?php echo $this->Form->input('contenido', array('label'=>'contenido','class'=>'form-control')); ?> 
 </div></div><div class='row'><div class='col-sm-12'>	<?php echo $this->Form->input('estatus', array('options'=>array('1'=>'Activo','2'=>'Inactivo'),'label'=>'estatus','class'=>'form-control')); ?> 
 </div></div>	</fieldset>

	<div class="pull-right">		
		<?php echo $this->Form->submit(__('Submit', true), array('label'=>false, 'class'=>'btn btn-primary'));?>
		<?php echo $this->Form->end(__('')); ?>
	</div>

</div>

</section>
</div>
</div>

<script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('ContenidoContenido');
    
  });
</script>
