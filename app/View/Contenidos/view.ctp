<div class="panel">
	<div class="panel-heading">
	<h2><?php echo __('Contenido'); ?></h2>		
	</div>
	



<div class="panel panel-body">
<h2><?php echo __('Contenido'); ?></h2>
	<table class="table table-striped">

	<tbody>
		
		
		<tr><td><?php echo __('Id'); ?></td>
		<td>
			<?php echo h($contenido['Contenido']['id']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Categoria'); ?></td>
		<td>
			<?php echo $this->Html->link($contenido['Categoria']['categoria'], array('controller' => 'categorias', 'action' => 'view', $contenido['Categoria']['id'])); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Contenido'); ?></td>
		<td>
			<?php echo h($contenido['Contenido']['contenido']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Estatus'); ?></td>
		<td>

			<?php 
				if ($contenido['Contenido']['estatus'] == '1') {
					$estatus = "<span style='color:green;'> <b> Active</b> </span>";
				}else{
					$estatus = "<span style='color:red;'> <b> Inactive</b> </span>";
				}

			 ?>
			<?php echo $estatus; ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Created'); ?></td>
		<td>
			<?php echo dateformat($contenido['Contenido']['created']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Modified'); ?></td>
		<td>
			<?php echo dateformat($contenido['Contenido']['modified']); ?>
			&nbsp;
		</td></tr>
	
	</tbody>
	</table>
</div>

</div>