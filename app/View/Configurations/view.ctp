<div class="configurations view">
<h2><?php echo __('Configuration'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($configuration['Configuration']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name System'); ?></dt>
		<dd>
			<?php echo h($configuration['Configuration']['name_system']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Icon'); ?></dt>
		<dd>
			<?php echo h($configuration['Configuration']['icon']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Language'); ?></dt>
		<dd>
			<?php echo $this->Html->link($configuration['Language']['language'], array('controller' => 'languages', 'action' => 'view', $configuration['Language']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Money Simbol'); ?></dt>
		<dd>
			<?php echo $this->Html->link($configuration['MoneySimbol']['coins'], array('controller' => 'money_simbols', 'action' => 'view', $configuration['MoneySimbol']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Separation Miles'); ?></dt>
		<dd>
			<?php echo h($configuration['Configuration']['separation_miles']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Separation Decimals'); ?></dt>
		<dd>
			<?php echo h($configuration['Configuration']['separation_decimals']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Country'); ?></dt>
		<dd>
			<?php echo $this->Html->link($configuration['Country']['country'], array('controller' => 'countries', 'action' => 'view', $configuration['Country']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $configuration['Configuration']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $configuration['Configuration']['id']), array(), __('Are you sure you want to delete # %s?', $configuration['Configuration']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New'), array('action' => 'add')); ?> </li>
	</ul>
</div>
