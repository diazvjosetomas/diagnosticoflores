<div class="configurations index">
	<h2><?php echo __('Configurations'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name_system'); ?></th>
			<th><?php echo $this->Paginator->sort('icon'); ?></th>
			<th><?php echo $this->Paginator->sort('language_id'); ?></th>
			<th><?php echo $this->Paginator->sort('money_simbol_id'); ?></th>
			<th><?php echo $this->Paginator->sort('separation_miles'); ?></th>
			<th><?php echo $this->Paginator->sort('separation_decimals'); ?></th>
			<th><?php echo $this->Paginator->sort('country_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($configurations as $configuration): ?>
	<tr>
		<td><?php echo h($configuration['Configuration']['id']); ?>&nbsp;</td>
		<td><?php echo h($configuration['Configuration']['name_system']); ?>&nbsp;</td>
		<td><?php echo h($configuration['Configuration']['icon']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($configuration['Language']['language'], array('controller' => 'languages', 'action' => 'view', $configuration['Language']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($configuration['MoneySimbol']['coins'], array('controller' => 'money_simbols', 'action' => 'view', $configuration['MoneySimbol']['id'])); ?>
		</td>
		<td><?php echo h($configuration['Configuration']['separation_miles']); ?>&nbsp;</td>
		<td><?php echo h($configuration['Configuration']['separation_decimals']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($configuration['Country']['country'], array('controller' => 'countries', 'action' => 'view', $configuration['Country']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Detalles'), array('action' => 'view', $configuration['Configuration']['id'])); ?>
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $configuration['Configuration']['id'])); ?>
			<?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $configuration['Configuration']['id']), array(), __('Are you sure you want to delete # %s?', $configuration['Configuration']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New'), array('action' => 'add')); ?></li>
		
	</ul>
</div>
