<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Citas</h3>
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
				<li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Citas'), array('action' => 'index')); ?></li>             
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Cita'); ?></h2>
	<table class="table table-striped">
	<tbody>
		
		<tr><td><?php echo __('Paciente'); ?></td>
		<td>
			<?php echo h($cita['Cita']['nombres']).' '.h($cita['Cita']['apellidos']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Teléfono'); ?></td>
		<td>
			<?php echo h($cita['Cita']['telefono']); ?>
			&nbsp;
		</td></tr>
		
		<tr><td><?php echo __('Correo'); ?></td>
		<td>
			<?php echo h($cita['Cita']['correo']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Fecha'); ?></td>
		<td>
			<?php echo h($cita['Cita']['fecha']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Tipo de Servicio'); ?></td>
		<td>
			<?php echo $this->Html->link($cita['Tipocita']['denominacion'], array('controller' => 'tipocitas', 'action' => 'view', $cita['Tipocita']['id'])); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Creado'); ?></td>
		<td>
			<?php echo h($cita['Cita']['created']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Modificado'); ?></td>
		<td>
			<?php echo h($cita['Cita']['modified']); ?>
			&nbsp;
		</td></tr>
	
	</tbody>
	</table>
</div>
</div>