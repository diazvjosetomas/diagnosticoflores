<style type="text/css">
    .btn-sucursal{
        background-color: #3A59C9;
    }
     .button {
   border-top: 1px solid #3a59c9;
   background: #3a59c9;
   background: -webkit-gradient(linear, left top, left bottom, from(#3a59c9), to(#3a59c9));
   background: -webkit-linear-gradient(top, #3a59c9, #3a59c9);
   background: -moz-linear-gradient(top, #3a59c9, #3a59c9);
   background: -ms-linear-gradient(top, #3a59c9, #3a59c9);
   background: -o-linear-gradient(top, #3a59c9, #3a59c9);
   padding: 8.5px 17px;
   -webkit-border-radius: 2px;
   -moz-border-radius: 2px;
   border-radius: 2px;
   -webkit-box-shadow: rgba(0,0,0,1) 0 1px 0;
   -moz-box-shadow: rgba(0,0,0,1) 0 1px 0;
   box-shadow: rgba(0,0,0,1) 0 1px 0;
   text-shadow: rgba(0,0,0,.4) 0 1px 0;
   color: white;
   font-size: 13px;
   width: 100%;
   text-decoration: none;
   vertical-align: middle;
   }
.button:hover {
   border-top-color: #3a59c9;
   background: #3a59c9;
   color:white;
  
   }
.button:active {
   border-top-color: #1b435e;
   background: #1b435e;
   }
</style>


<div class="container example">

	
	    <div class="row" id="sucursales">

	    <h3 style="text-align: center;" class="h5_back">SELECCIONA UNA SUCURSAL</h3>


	        <ul class="thumbnails">            
	            
	            <?php
	            	foreach ($sucursal as $sucursales) {
	            		echo '<div class="col-md-4"><div class="thumbnail"><img src="http://placehold.it/320x200" alt="ALT NAME" class="img-responsive" />
	                    	  <div class="caption">';
	                    echo "<h3 id='idDenominacionSucursal'>".$sucursales['Sucursales']['denominacion']."</h3>";	  
	                    echo "<p>".$sucursales['Sucursales']['email']."</p>";	  
	                    echo '<p align="center"><a onclick="seleccionaSuc('.$sucursales['Sucursales']['id'].')" href="#" class="btn btn-primary btn-sucursal">Seleccionar</a></p>';
	                    echo "</div>";
	                    echo "</div>";
	                    echo "</div>";
	            	}
	             ?>


	        </ul>
	    </div>
		
		<div id="tipocitas_div"></div>


		<?php 

			//pr($sucursal);

		 ?>
    


    <div id="horarios" class="row">

    	
        <div class="col-md-8">

        			    <div id="datosPersonales" class="row" style="display: none;">
        			    	<div class="col-md-8">
        			    	<?php echo $this->Form->create('Cita', array('url'=>'/citas/add_2', 'class'=>'')); ?>
        			    	
        			    		<h3 class="h5_back" style="text-align: center;">Datos Personales</h3>
        			    		<label>Nombres</label> <br>
        			    		<input type="text" name="data[Cita][nombres]" id="nombres" class="form-control" required><br>

        			    		<label>Apellidos</label> <br>
        			    		<input type="text" name="data[Cita][apellidos]" id="apellidos" class="form-control" required><br>

        			    		<label>Correo</label> <br>
        			    		<input type="text" name="data[Cita][correo]" id="correo" class="form-control" required><br>

        						<input type="hidden" id="diaSelect"  name="data[Cita][fecha]">
        			    		<input type="hidden" id="horaSelect" name="data[Cita][hora]">
        						<input type="hidden" id="input_tipocita"   name="data[Cita][tipocita_id]">
        						<input type="hidden" id="sucursale"  name="data[Cita][sucursale_id]">

        						<div class="row">
        							<div class="col-md-3"></div>
        							<div class="col-md-3"></div>
        							<div class="col-md-3"></div>
        							<div class="col-md-3">
        								<?php echo $this->Form->submit(__('Agendar', true), array('label'=>false, 'class'=>'btn btn-primary'));?>
        								<?php echo $this->Form->end(__('')); ?>					
        							</div>


        						</div>


                               
                            </div>


        			    		
        			    </div>







        	        <div style="display: none;text-align: center;" id="horas_disponibles" style="display: none">
        	        <h5 class="h5_back">Selecciona Un Horario?</h5>
        	        	<div class="row">
        	        		<div id='manana' class="col-md-4">
        	        			<table class="table">
        	        				<thead>
        	        					<tr>
        	        						<th>
        	        							Mañana
        	        						</th>
        	        					</tr>
        	        				</thead>
        	        				<tbody>
        	        					<?php 
        	        						$ini = false;
        	        						$cont = 1;
        	        						$hora = 9;

        	        						$totalHoras = 1;
        	        						for ($i = 1; $i <= 6 ; $i++) { 

        	        							if ($ini == false) {
        	        								$min = '00';
        	        								$ini = true;
        	        							}else{
        	        								$min = '30';
        	        								$ini = false;
        	        							}
        	        							if ($cont ==  3) {
        	        									$hora += 1;	
        	        									$cont = 1;
        	        								}	
        	        							$cont++;	

        	        							

        	        							echo "<tr>";
        	        							echo "<td style='text-align:center;'><button id='btn_".$totalHoras."' onclick='seleccionaHora(".$totalHoras.")' class='btn '>".str_pad($hora, '2','0', STR_PAD_LEFT).":".$min." AM</button></td>";
        	        							echo "</tr>";

        	        							$totalHoras++;
        	        						}
        	        					?>
        	        				</tbody>
        	        			</table>
        	        		</div>
        	        		<div id='tarde' class="col-md-4">
        	        			<table class="table">
        	        				<thead>
        	        					<tr>
        	        						<th>
        	        							Tarde
        	        						</th>
        	        					</tr>
        	        				</thead>
        	        				<tbody>
        	        					<?php 
        	        						$ini = false;
        	        						$cont = 1;
        	        						$hora = 12;
        	        						for ($i = 1; $i <= 10 ; $i++) { 

        	        							if ($ini == false) {
        	        								$min = '00';
        	        								$ini = true;
        	        							}else{
        	        								$min = '30';
        	        								$ini = false;
        	        							}
        	        							if ($cont ==  3) {
        	        									$hora += 1;        	        									$cont = 1;
        	        								}	
        	        							$cont++;	

        	        							
        	        							if ($hora == 12) {
        		        							echo "<tr>";
        		        							echo "<td style='text-align:center;'><button id='btn_".$totalHoras."' onclick='seleccionaHora(".$totalHoras.")' class='btn'>".str_pad($hora, '2','0', STR_PAD_LEFT).":".$min." PM</button></td>";
        		        							echo "</tr>";        								
        	        							}else{
        	        								echo "<tr>";
        	        								echo "<td style='text-align:center;'><button id='btn_".$totalHoras."' onclick='seleccionaHora(".$totalHoras.")' class='btn'>".str_pad($hora - 12, '2','0', STR_PAD_LEFT).":".$min." PM</button></td>";
        	        								echo "</tr>";        								
        	        							}
        	        							$totalHoras++;
        	        						}
        	        					?>
        	        				</tbody>
        	        			</table>
        	        		</div>
        	        		<div id='noche' class="col-md-4">
        	        			<table class="table">
        	        				<thead>
        	        					<tr>
        	        						<th>
        	        							Noche
        	        						</th>
        	        					</tr>
        	        				</thead>
        	        				<tbody>
        							<?php 
        	        					$ini = false;
        	        					$cont = 1;
        	        					$hora = 17;
        	        					for ($i = 1; $i <= 4 ; $i++) { 
        	      							if ($ini == false) {
        										$min = '00';
        										$ini = true;
        									}else{
        										$min = '30';
        										$ini = false;
        									}
        									if ($cont ==  3) {
        										$hora += 1;	
        	        				        	$cont = 1;
        	        				        }	
        	        				        	$cont++;	
        	   				        							
        	       							
        	       								echo "<tr>";
        	       								echo "<td style='text-align:center;'><button id='btn_".$totalHoras."'  onclick='seleccionaHora(".$totalHoras.")' class='btn'>".str_pad($hora - 12, '2','0', STR_PAD_LEFT).":".$min." PM</button></td>";
        	       								echo "</tr>";        								
        	       							
        	       							$totalHoras++;
        	       						}
        	       					?>
        	        				</tbody>
        	        			</table>
        	        		</div>
        	        	</div>
        	        </div>










            <div id="date-popover" class="popover top"
                 style="cursor: pointer; display: block; margin-left: 33%; margin-top: -50px; width: 175px;">
                <div class="arrow"></div>
                <h3 class="popover-title" style="display: none;"></h3>

                <div id="date-popover-content" class="popover-content"></div>
            </div>

            <div id="my-calendar"></div>




            <style type="text/css">
                .orange{
                    background-color: #fff0c3;
                }
                .blue{
                    background-color: #357ebd;
                }
            </style>
    

            <script type="application/javascript">
                $(document).ready(function () {
                    $("#date-popover").popover({html: true, trigger: "manual"});
                    $("#date-popover").hide();
                    $("#date-popover").click(function (e) {
                        $(this).hide();
                    });

                    //Traemos los dias feriados                
                      

                    <?php 
                        $date = '';
                        foreach ($feriados as $feriado) {
                            
                            $date .= '{"date":"'.$feriado['Feriado']['diaferiado'].'","badge":false,"title":"'.$feriado['Feriado']['denominacion'].'"},';

                        }
                        //$date = substr($date, 0, -1);

                            //  $date .= '{"date":"2016-12-17","badge":false,"title":"Example 1"},';
                        
                    ?>


                      var eventData = [


                        
                        <?php echo $date; ?>



                          //{"date":"2016-12-21","badge":false,"title":"Example 1"}
                      ];


                      console.log(eventData);



                      $("#my-calendar").zabuto_calendar({
                          legend: [
                                  
                                  {type: "block", label: "Día Feriado", classname: "orange"},                                  
                                  {type: "block", label: "Día Actual", classname: "blue"},                                  
                                ],
                          data: eventData,
                       nav_icon: { 
                          prev: '<i class="fa fa-angle-double-left" aria-hidden="true"></i>', 
                          next: '<i class="fa fa-angle-double-right" aria-hidden="true"></i>' 
                       },
                          year: <?=date('Y')?>,
                          language: 'es',
                          show_previous: false,
                          show_next: true  ,  
                          today: true,
                          
                          action: function () {
                              return myDateFunction(this.id, false);
                          },
                          action_nav: function () {
                              return myNavFunction(this.id);
                          }
                          
                          
                      });
                      
                    //Armamos el calendario
                    
                });


             

                function myDateFunction(id, fromModal) {

                    var date = $("#" + id).data("date");
                    $("#diaSelect").val(date);
                    console.log(date);
                    $.ajax({
                                url:"/Citas/laborables/"+date,
                                type:"post",
                                data:{
                                      
                                    },
                                success:function(response){ 

                                 var data = JSON.parse(response);
                                 
                                  if(data.resp == true){
                                    
                                            if (data.manana == '1' && data.tarde == '1' && data.noche == '1') {
                                                alert('Lo sentimos, este día es feriado!');
                                            }else{

                                                $(".zabuto_calendar").hide();
                                                $("#horas_disponibles").show();
                                                    if (data.manana == '1') {
                                                        $("#manana").html('Disculpe, sin horarios disponibles!')
                                                    }
                                                    if (data.tarde == '1') {
                                                        $("#tarde").html('Disculpe, sin horarios disponibles!')
                                                    }
                                                    if (data.noche == '1') {
                                                        $("#noche").html('Disculpe, sin horarios disponibles!')
                                                    }
                                            }                                       


                                  }else{
                                    
                                    console.log(data.nof+data.labmanana+data.labtarde+data.labnoche);
                                        
                                            if (data.labmanana == '1' && data.labtarde == '1' && data.labnoche == '1') {
                                                alert('Lo sentimos, este día no es laborable!');
                                            }else{

                                                if (data.labmanana == '1') {
                                                    $("#manana").html('Disculpe, sin horarios disponibles!')
                                                }
                                                if (data.labtarde == '1') {
                                                    $("#tarde").html('Disculpe, sin horarios disponibles!')
                                                }
                                                if (data.labnoche == '1') {
                                                    $("#noche").html('Disculpe, sin horarios disponibles!')
                                                }

                                                console.log('horarios'+data.horariosmanana);
                                                $("#manana").html(data.horariosmanana);
                                                $("#tarde").html(data.horariostarde);
                                                $("#noche").html(data.horariosnoche);
                                                $(".zabuto_calendar").hide();
                                                $("#horas_disponibles").show();
                                                convierteFecha(date);
                                            }    
                                      
                                  }
                                }
                            });
                    

                    var hasEvent = $("#" + id).data("hasEvent");
                    $("#date-popover").hide();
                    if (fromModal) {
                        $("#" + id + "_modal").modal("hide");
                    }
                    var date = $("#" + id).data("date");
                    var hasEvent = $("#" + id).data("hasEvent");
                    if (hasEvent && !fromModal) {
                        return false;
                    }
                    //$("#date-popover-content").html('You clicked on date ' + date);
                    //$("#date-popover").show();


                }
                //humaniza fecha
                function convierteFecha(fecha){
                	var newFecha = fecha.split("-");
                	$("#fecha").html(newFecha[2]+'/'+newFecha[1]+'/'+newFecha[0]);
                }

                function myNavFunction(id) {
                    $("#date-popover").hide();
                    var nav = $("#" + id).data("navigation");
                    var to = $("#" + id).data("to");
                    console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
                }
           
            </script>

        </div>
		<div id='detallesCita' class="col-md-4" style="border-radius: 0px 0px 0px 0px;
                                    -moz-border-radius: 0px 0px 0px 0px;
                                    -webkit-border-radius: 0px 0px 0px 0px;
                                     border: 3px solid #01B9AF;
                                     height: 85%">
			<h4 class="h5_back" style="text-align: center;">
				Mi Cita
			</h4>

			<b>Sucursal: </b> <span id="sucursal"> </span><br>
			<b>Estudio:  </b> <span id="estudio">  </span><br>
			<b>Fecha:    </b> <span id="fecha">    </span><br>
			<b>Hora:     </b> <span id="hora">     </span><br>


		</div>
		<style type="text/css">
			.border{
				background: #01b9af;
				  background-image: -webkit-linear-gradient(top, #01b9af, #01b9af);
				  background-image: -moz-linear-gradient(top, #01b9af, #01b9af);
				  background-image: -ms-linear-gradient(top, #01b9af, #01b9af);
				  background-image: -o-linear-gradient(top, #01b9af, #01b9af);
				  background-image: linear-gradient(to bottom, #01b9af, #01b9af);
				  -webkit-border-radius: 0;
				  -moz-border-radius: 0;
				  border-radius: 0px;
				  font-family: Arial;
				  color: #ffffff;
				  font-size: 16px;
				  padding: 5px 20px 10px 20px;
				  border: solid #01b9af 2px;
				  text-decoration: none;
			}

			.border:hover{
				background: #01b9af;
				  background-image: -webkit-linear-gradient(top, #01b9af, #01b9af);
				  background-image: -moz-linear-gradient(top, #01b9af, #01b9af);
				  background-image: -ms-linear-gradient(top, #01b9af, #01b9af);
				  background-image: -o-linear-gradient(top, #01b9af, #01b9af);
				  background-image: linear-gradient(to bottom, #01b9af, #01b9af);
				  -webkit-border-radius: 0;
				  -moz-border-radius: 0;
				  border-radius: 0px;
				  font-family: Arial;
				  color: #ffffff;
				  font-size: 16px;
				  padding: 5px 20px 10px 20px;
				  border: solid #01b9af 2px;
				  text-decoration: none;
			}

        	.btn {
        	  background: #3498db;
        	  background-image: -webkit-linear-gradient(top, #3498db, #2980b9);
        	  background-image: -moz-linear-gradient(top, #3498db, #2980b9);
        	  background-image: -ms-linear-gradient(top, #3498db, #2980b9);
        	  background-image: -o-linear-gradient(top, #3498db, #2980b9);
        	  background-image: linear-gradient(to bottom, #3498db, #2980b9);
        	  -webkit-border-radius: 0;
        	  -moz-border-radius: 0;
        	  border-radius: 0px;
        	  font-family: Arial;
        	  color: #ffffff;
        	  font-size: 16px;
        	  padding: 5px 20px 10px 20px;
        	  text-decoration: none;
        	  width: 100%;
        	  height: 32px;
        	  
        	}

        	.btn:hover {
        	  background: #3cb0fd;
        	  background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
        	  background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
        	  background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
        	  background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
        	  background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
        	  text-decoration: none;
        	}
        </style>        

        <div class="row" style="margin-top: 90px;">
        <div id='idVolver_2' class="col-md-2">
            <a href="" class='btn btn-primary'>
            <i class='fa fa-arrow-left'></i>
                Volver al inicio
            </a>
            
        </div>
        <div class="col-md-2"></div>
        <div class="col-md-2"></div>
        <div class="col-md-2"></div>
        <div class="col-md-2"></div>
            
        </div>
    </div>
    


        
    </div>

<!-- /container -->







<input type="hidden" name="DenominacionSucursal" id="DenominacionSucursal">
<script type="text/javascript">
	function seleccionaHora(hora){

		

		for (var i = 1; i <= 20; i++) {
			

				$("#btn_"+i).removeClass('border');				

		
		
		}

		$("#btn_"+hora).addClass('border');
		$("#horaSelect").val(hora);
		$("#hora").html(horaLegible(hora));
		$("#horas_disponibles").hide();
		$("#datosPersonales").show();



	
	}

	function horaLegible(idHora){
			  if (idHora == 1) {
			return '09:00 AM';
		}else if (idHora == 2){
			return '09:30 AM';
		}else if (idHora == 3) {
			return '10:00 AM';
		}else if (idHora == 4){
			return '10:30 AM';
		}else if (idHora == 5){
			return '11:00 AM';
		}else if (idHora == 6){
			return '11:30 AM';
		}else if (idHora == 7) {
			return '12:00 PM'
		}else if (idHora == 8) {
			return '12:30 PM'
		}else if (idHora == 9){
			return '01:00 PM';
		}else if (idHora == 10){
			return '01:30 PM';
		}else if (idHora == 11){
			return '02:00 PM';
		}else if (idHora == 12){
			return '02:30 PM';
		}else if (idHora == 13){
			return '03:00 PM';
		}else if (idHora == 14){
			return '03:30 PM';
		}else if (idHora == 15){
			return '04:00 PM';
		}else if (idHora == 16){
			return '04:30 PM';
		}else if (idHora == 17){
			return '05:00 PM';
		}else if (idHora == 18){
			return '05:30 PM';
		}else if (idHora == 19){
			return '06:00 PM';
		}else if (idHora == 20){
			return '06:30 PM';
		}

	}

	function inicia(){
		$("#horarios").hide();
	}

	function seleccionaSuc(idSuc){
			
		$("#sucursales").hide();
		$("#sucursale").val(idSuc);
		$("#sucursal").html($("#idDenominacionSucursal").text());

		    $.ajax({
		                url:"/Citas/tipocitas/"+idSuc,
		                type:"post",
		                data:{
		                      
		                    },
		                success:function(n){
		                  if(n != 0){
		                  	$("#sucursales").hide();
		                    $("#tipocitas_div").html(n);
                            $("#tipocitas_div").show();
		                  }else{
		                    console.log('no ok');
		                  }
		                }
		            });


		}

	// function selectTipoCita(id){

 //        console.log('--->'+$("#idtipocitadenominacion_"+id).text());
	// 	$("#input_tipocita").val(id);
	// 	$("#estudio").html($("#idtipocitadenominacion_"+id).text());
	// 	$("#tipocitas_div").hide();
	// 	$("#horarios").show();
 //        $("#detallesCita").show();
 //        $("#idVolver_2").show();


 //        var id =   $('.zabuto_calendar').attr('id');
 //        console.log(id);
 //        $(".zabuto_calendar").show();

	// }

	inicia();
</script>

<script type="text/javascript">
    function volver1(){
        console.log('s');
        $("#datosPersonales").show();
    }

</script>
