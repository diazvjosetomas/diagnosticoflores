<?php 


try {
		

		if (isset($feriados[0]['Feriado']['manana'])) {
			
			foreach ($feriados as $feriado) {

				echo json_encode(array('resp'=>true,
									   'manana'=>$feriado['Feriado']['manana'],
									   'tarde'=>$feriado['Feriado']['tarde'],
									   'noche'=>$feriado['Feriado']['noche'],
									   'horario'=>'si'

									   ));
			}
		}else{



			if (!isset($citas[0]['Cita']['fecha'])) {
				echo json_encode(array('resp'=>false,
									   'horarios'=>false,
									   'nof'=>'noferiado',
									   'labmanana'=>$laborables[0]['Laborable']['manana'],
									   'labtarde' =>$laborables[0]['Laborable']['tarde'],
									   'labnoche' =>$laborables[0]['Laborable']['noche']
									   ));
			}else{
				
				

					//Horarios en la mañana
					$horariosmanana  = '<table class="table">';
					$horariosmanana .= '<thead>';
					$horariosmanana .= '<tr>';
					$horariosmanana .= '<th>Mañana.</th>';
					$horariosmanana .= '</tr>';
					$horariosmanana .= '</thead>';
					$horariosmanana .= '<tbody>';
					$ini = false;
					$cont = 1;
					$hora = 9;

						$totalHoras = 1;
					for ($i = 1; $i <= 6 ; $i++) { 

							if ($ini == false) {
								$min = '00';
								$ini = true;
							}else{
								$min = '30';
								$ini = false;
							}

							if ($cont ==  3) {
									$hora += 1;	
									$cont = 1;
								}	
							$cont++;	

							

						$enc = false;
						foreach ($citas as $cita) {
							if ($cita['Cita']['hora'] == $totalHoras) {
								$enc = true;
							}else{

							}
						}


							if ($enc == false) {									
									$horariosmanana .= "<tr>";
									$horariosmanana .= "<td style='text-align:center;'><button id='btn_".$totalHoras."' onclick='seleccionaHora(".$totalHoras.")' class=' button'>".str_pad($hora, '2','0', STR_PAD_LEFT).":".$min." AM</button></td>";
									$horariosmanana .= "</tr>";								
								}	

						$totalHoras++;
					}
						$horariosmanana .='</tbody></table>';

					//Horarios en la TARDE
					$horariostarde  = '<table class="table">';
					$horariostarde .= '<thead>';
					$horariostarde .= '<tr>';
					$horariostarde .= '<th>Tarde.</th>';
					$horariostarde .= '</tr>';
					$horariostarde .= '</thead>';
					$horariostarde .= '<tbody>';
					$ini = false;
					$cont = 1;
					$hora = 12;
					for ($i = 1; $i <= 10 ; $i++) { 
						if ($ini == false) {
					        $min = '00';
					        $ini = true;
					    }else{
					        $min = '30';
					        $ini = false;
					    }
					    
					    if ($cont ==  3) {
					        $hora += 1;	
					        $cont = 1;
					    }	
						
						$cont++;	

						$enc = false;
						foreach ($citas as $cita) {
							if ($cita['Cita']['hora'] == $totalHoras) {
								$enc = true;
							}else{

							}
						}


						if ($enc == false) {
						
							    if ($hora == 12) {
								    $horariostarde .= "<tr>";
								    $horariostarde .=  "<td style='text-align:center;'><button id='btn_".$totalHoras."' onclick='seleccionaHora(".$totalHoras.")' class=' button'>".str_pad($hora, '2','0', STR_PAD_LEFT).":".$min." PM</button></td>";
								    $horariostarde .=  "</tr>";        								
							    }else{
							        $horariostarde .=  "<tr>";
							        $horariostarde .=  "<td style='text-align:center;'><button id='btn_".$totalHoras."' onclick='seleccionaHora(".$totalHoras.")' class=' button'>".str_pad($hora - 12, '2','0', STR_PAD_LEFT).":".$min." PM</button></td>";
							        $horariostarde .=  "</tr>";        
								}		        								
						}
					    
						
					    $totalHoras++;
					}
					$horariostarde .=  '</tbody></table>';


					//Horarios en la noche
					$horariosnoche  = '<table class="table">';
					$horariosnoche .= '<thead>';
					$horariosnoche .= '<tr>';
					$horariosnoche .= '<th>Noche</th>';
					$horariosnoche .= '</tr>';
					$horariosnoche .= '</thead>';
					$horariosnoche .= '<tbody>';
					$ini = false;
					$cont = 1;
					$hora = 17;
					for ($i = 1; $i <= 6 ; $i++) { 
						if ($ini == false) {
							$min = '00';
							$ini = true;
						}else{
							$min = '30';
							$ini = false;
						}
						
						if ($cont ==  3) {
							$hora += 1;	
					        $cont = 1;
					    }	
					    $cont++;	
					   	

					    $enc = false;
					    foreach ($citas as $cita) {
					    	if ($cita['Cita']['hora'] == $totalHoras) {
					    		$enc = true;
					    	}else{

					    	}
					    }



					    if ($enc == false) {
					    	
							$horariosnoche .= "<tr>";
							$horariosnoche .= "<td style='text-align:center;'><button id='btn_".$totalHoras."'  onclick='seleccionaHora(".$totalHoras.")' class=' button'>".str_pad($hora - 12, '2','0', STR_PAD_LEFT).":".$min." PM</button></td>";
						    $horariosnoche .= "</tr>";        								
						
					    }
						
												       							
					    $totalHoras++;
					}
					$horariosnoche .=  '</tbody></table>';


					echo json_encode(array('horarios'=>true,
										   'horariosmanana'=>$horariosmanana,
										   'horariostarde'=>$horariostarde,
										   'horariosnoche'=>$horariosnoche));
					
			
		}
	}	
	
} catch (Exception $e) {
	echo json_encode(array('resp'=>false,'nof'=>'noferiado'));
}




 ?>