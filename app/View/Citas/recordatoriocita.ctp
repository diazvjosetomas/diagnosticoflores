<?php 

	



	$cuerpo = '';

	foreach ($citas as $key) {

		$nombre = strtolower(ucwords($key['Cita']['nombres'])).' '.strtolower(ucwords($key['Cita']['apellidos']));
		$correo = $key['Cita']['correo'];
		$cabeceras  = "MIME-Version: 1.0\r\n";
		$cabeceras .= "Content-Transfer-Encoding: 8Bit\r\n";
		$cabeceras .= "Content-Type: text/html; charset=\"utf-8\"\r\n";
		$cabeceras .= "Reply-to: \r\n";
		$cabeceras .= "From: soporte@diagnosticoflores.com \r\n";
		$cabeceras .= "Errors-To: \r\n";

		// ----------------------------------------------------------------------
		// Cuerpo				
		$cuerpo .= "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>\r\n";
		$cuerpo .= "<html xmlns='http://www.w3.org/1999/xhtml'>\r\n";
		$cuerpo .= "<head>\r\n";
		$cuerpo .= "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />\r\n";
		$cuerpo .= "<title>Notificacion de Cita</title>\r\n";
		$cuerpo .= "<style type='text/css'> \r\n";
		$cuerpo .= "body {margin: 0; padding: 0; min-width: 100%!important;}\r\n";
		$cuerpo .= ".content {width: 100%; max-width: 600px;}</style> </head>  \r\n";
		$cuerpo .= "<style type='text/css'>";
		$cuerpo .= "			    
				    @media only screen and (min-device-width: 601px) {
				    .content {width: 800px !important;}
				    }

				    .header {padding: 40px 10px 20px 10px;}


				    .footer {padding: 20px 30px 15px 30px;}
				    .footercopy {font-family: sans-serif; font-size: 14px; color: #ffffff;}
				    .footercopy a {color: #ffffff; text-decoration: underline;}

				    @media only screen and (max-width: 550px), screen and (max-device-width: 550px) {
				    body[yahoo] .buttonwrapper {background-color: transparent!important;}
				    body[yahoo] .button a {background-color: #e05443; padding: 15px 15px 13px!important; display: block!important;}
				    }
				    </style>\r\n";
		$cuerpo .= "    <body yahoo bgcolor='#f6f8f1'>
		  
		                    <table class='content' align='center' cellpadding='0' cellspacing='0' border='0'>
		                        <tr>
		                            <td class='header' bgcolor='#01B9AF'>
		                                
		                               <table width='70' align='left' border='0' cellpadding='0' cellspacing='0'>
		                                   <tr>
		                                       <td height='70' style='padding: 0 10px 10px 0;'>
		                                           <h1 style='color:white;'>Quamtun.</h1>
		                                           <h5 style='color:white;'>Recordatorio de Cita Agendada</h5>
		                                       </td>
		                                   </tr>
		                               </table>     


		                            </td>
		                        </tr>

		                        <tr>
		                            <td class='innerpadding borderbottom'>
		                            	<br>
		                            	<br>
		                            	<br>
		                                <table width='100%'' border='0' cellspacing='0' cellpadding='0'>
		                                    <tr>
		                                        <td class='h2'>
		                                           Estimado(a), $nombre
		                                        </td>
		                                    </tr>
		                                    <tr>
		                                        <td class='bodycopy'>
		                                            Le recordamos que tiene una cita agenda para el día: ".$key['Cita']['fecha']." <br>
		                                            Estudio: ".$key['Tipocita']['denominacion']."<br>
		                                            Hora: ".devHora($key['Cita']['hora']).":".devMinuto($key['Cita']['hora'])."<br>
		                                            Sucursal: ".$key['Sucursale']['denominacion']." <br>
		                                            Direccion: ".$key['Sucursale']['direccion']." <br>
		                                            Teléfono: ".$key['Sucursale']['telefono']." <br>
		                                            Email: ".$key['Sucursale']['email']." <br>
		                                        </td>
		                                    </tr>
		                                </table>
		                            </td>
		                        </tr>
		                    </table>
		                    <script>
		                    $('#hora').html();
		                    $('#minutos').html(devMinuto(".$key['Cita']['hora']."));
		                    </scrip>
		    </body>
		</html>\r\n";

		// ----------------------------------------------------------------------
		// Envio
		// Correo, Titulo, Cuerpo, Cabecera
		mail( "$nombre <$correo>", "Cita agendada. ", $cuerpo, $cabeceras );
	}


function devHora($hora){
      if ($hora == '1') {
        return '9';
      }else if($hora == '2'){
        return '9';
      }else if($hora == '3'){
        return '10';
      }else if($hora == '4'){
        return '10';
      }else if($hora == '5'){
        return '11';
      }else if($hora == '6'){
        return '11';
      }else if($hora == '7'){
        return '12';
      }else if($hora == '8'){
        return '12';
      }else if($hora == '9'){
        return '13';
      }else if($hora == '10'){
        return '13';
      }else if($hora == '11'){
        return '14';
      }else if($hora == '12'){
        return '14';
      }else if($hora == '13'){
        return '15';
      }else if($hora == '14'){
        return '15';
      }else if($hora == '15'){
        return '16';
      }else if($hora == '16'){
        return '16';
      }else if($hora == '17'){
        return '17';
      }else if($hora == '18'){
        return '17';
      }else if($hora == '19'){
        return '18';
      }
    }

    function devMinuto($hora){
      if ($hora == '1') {
        return '00';
      }else if($hora == '2'){
        return '30';
      }else if($hora == '3'){
        return '00';
      }else if($hora == '4'){
        return '30';
      }else if($hora == '5'){
        return '00';
      }else if($hora == '6'){
        return '30';
      }else if($hora == '7'){
        return '00';
      }else if($hora == '8'){
        return '30';
      }else if($hora == '9'){
        return '00';
      }else if($hora == '10'){
        return '30';
      }else if($hora == '11'){
        return '00';
      }else if($hora == '12'){
        return '30';
      }else if($hora == '13'){
        return '00';
      }else if($hora == '14'){
        return '30';
      }else if($hora == '15'){
        return '00';
      }else if($hora == '16'){
        return '30';
      }else if($hora == '17'){
        return '00';
      }else if($hora == '18'){
        return '30';
      }else if($hora == '19'){
        return '00';
      }
    }


 ?>