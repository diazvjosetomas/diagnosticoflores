<div class="container">
<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Citas</h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
					<li><i class="fa fa-arrow-left"></i>
					<a href="<?php echo $this->Html->url('/Citas/'); ?>"> Volver a Citas </a> </li>              
		</ol>		
	</div>	    
</div>

<div class="panel">
	<div class="panel-heading panel-success">
		<h4>&nbsp;
			<span class="pull-right">
				<div class="btn-group">
				  <button type="button" class="btn btn-primary">Acción</button>
				  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
				    <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu" role="menu">
				    <li><?php echo $this->Html->link(__('Nuevo Cita'), array('action' => 'add')); ?></li>
				  </ul>
				</div>
			</span>
		</h4>
	</div>
	<div class="panel-body">
<div class="citas index">
	<table id="Cita" cellpadding="0" cellspacing="0" class="table table-striped table-rounded">
	<thead>
	<tr>
			
			<th><?php echo h('Paciente'); ?></th>
			
			<th><?php echo h('Hora'); ?></th>
			<th><?php echo h('Fecha'); ?></th>			
      <th><?php echo h('Doctor'); ?></th> 
      <th>Sucursal</th>    
			<th>Estatus</th>
			<th class="actions"><?php echo __('Acción'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($citas as $cita): ?>
	<tr>
		
		<?php  
			$asigna = 'No Asignada';
			$estBool = 0;
			$medico = 'No Asignado';
			if (isset( $asignacitas[0]['Asignacita']['cita_id'])) {	
				foreach ($asignacitas as $asignacita) {
					if ($cita['Cita']['id'] == $asignacita['Asignacita']['cita_id']) {
						if ($asignacita['Asignacita']['estatus'] == '1') {
							$asigna = 'Asignada';
							$estBool = 1;
							$idAsigna = $asignacita['Asignacita']['id'];
							$medico = ucwords($asignacita['User']['nombre'].''.$asignacita['User']['apellidos']);
						}else if($asignacita['Asignacita']['estatus'] == '0'){
							$asigna = 'No Asignada';
							$estBool = 2;
							$idAsigna = $asignacita['Asignacita']['id'];


						}
					}

				}
			}

			

		?>


		<td><?php echo ucwords(strtolower(($cita['Cita']['nombres']))).' '.ucwords(strtolower(($cita['Cita']['apellidos']))); ?>&nbsp;</td>
		
		<td  >
      
      <?=devuelvehora($cita['Cita']['hora'])?>

    </td>
		<td><?php echo ucwords(strtolower(($cita['Cita']['fecha']))); ?>&nbsp;</td>
		
		 <td><?php echo h($medico); ?>&nbsp;</td>
		 <td><?php echo h($cita['Sucursale']['denominacion']); ?></td>
     <td>
       <?php 
          if ($cita['Cita']['estatus'] == 0) {
            $estatus = 'Iniciada';
          }else if ($cita['Cita']['estatus'] == 1) {
            $estatus = 'Asistió';
          }else if ($cita['Cita']['estatus'] == 2) {
            $estatus = 'No Asistió';
          }else if ($cita['Cita']['estatus'] == 3) {
            $estatus = 'Cancelada';
          }

          echo $estatus;
        ?>
     </td>

		<td class="actions">
		
			<?php echo $this->Html->link(__('Detalles'), array('action' => 'view', $cita['Cita']['id']), array('class'=>'btn btn-sm btn-default')); ?>
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $cita['Cita']['id']), array('class'=>'btn btn-sm btn-success')); ?>
			<?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $cita['Cita']['id']), array('class'=>'btn btn-sm btn-danger'), __('¿Esta seguro que desea eliminar esta cita <b> %s </b>?', ucwords( strtolower($cita['Cita']['nombres'])))); ?>
			<?php
			if ($estBool == 1 || $estBool == 2) {
				echo $this->Html->link(ucwords($asigna), array('controller' => 'asignacitas', 'action' => 'edit2', $idAsigna) , array('class'=>'btn btn-sm btn-primary')); 
			}else{			
			 	echo $this->Html->link(ucwords($asigna), array('controller' => 'asignacitas', 'action' => 'add2', $cita['Cita']['id']) , array('class'=>'btn btn-sm btn-primary')); 
			}

		 ?>	
		</td>
	</tr>

 
<?php endforeach; ?>
	</tbody>
	</table>
</div>
</div>
</div>


<?php pr($citas) ?>


<script type="text/javascript">
    //$(document).ready(function() {
        $('#Cita').DataTable( {
            dom: 'Bfrtlip',          
            responsive: true,
	        buttons: [
	            {
	                extend: 'excel',
	                exportOptions: {
	                    columns: [0,1,2,3,4,5]
	                }
	            },
	            {
	                extend: 'pdf',
	                exportOptions: {
	                    columns: [0,1,2,3,4,5]
	                }
	            },
	            {
	                extend: 'copy',
	                exportOptions: {
	                    columns: [0,1,2,3,4,5]
	                }
	            },
	            {
	                extend: 'csv',
	                exportOptions: {
	                    columns: [0,1,2,3,4,5]
	                }
	            },
	            {
	                extend: 'print',
	                exportOptions: {
	                    columns: [0,1,2,3,4,5]
	                }
	            }



	        ],
            "language": 
            {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ".",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        } );
    //} );
</script>


<script>
	function devHora(hora){
	  if (hora == '0') {
	    return '0';
	  }
	  if (hora == '21') {
	    return '23'
	  }
	  if (hora == '1') {
	    return '9';
	  }else if(hora == '2'){
	    return '9';
	  }else if(hora == '3'){
	    return '10';
	  }else if(hora == '4'){
	    return '10';
	  }else if(hora == '5'){
	    return '11';
	  }else if(hora == '6'){
	    return '11';
	  }else if(hora == '7'){
	    return '12';
	  }else if(hora == '8'){
	    return '12';
	  }else if(hora == '9'){
	    return '13';
	  }else if(hora == '10'){
	    return '13';
	  }else if(hora == '11'){
	    return '14';
	  }else if(hora == '12'){
	    return '14';
	  }else if(hora == '13'){
	    return '15';
	  }else if(hora == '14'){
	    return '15';
	  }else if(hora == '15'){
	    return '16';
	  }else if(hora == '16'){
	    return '16';
	  }else if(hora == '17'){
	    return '17';
	  }else if(hora == '18'){
	    return '17';
	  }else if(hora == '19'){
	    return '18';
	  }else if(hora == '20'){
	    return '18';
	  }
	}

	function devMinuto(hora){
	  if (hora == '1') {
	    return '00';
	  }else if(hora == '2'){
	    return '30';
	  }else if(hora == '3'){
	    return '00';
	  }else if(hora == '4'){
	    return '30';
	  }else if(hora == '5'){
	    return '00';
	  }else if(hora == '6'){
	    return '30';
	  }else if(hora == '7'){
	    return '00';
	  }else if(hora == '8'){
	    return '30';
	  }else if(hora == '9'){
	    return '00';
	  }else if(hora == '10'){
	    return '30';
	  }else if(hora == '11'){
	    return '00';
	  }else if(hora == '12'){
	    return '30';
	  }else if(hora == '13'){
	    return '00';
	  }else if(hora == '14'){
	    return '30';
	  }else if(hora == '15'){
	    return '00';
	  }else if(hora == '16'){
	    return '30';
	  }else if(hora == '17'){
	    return '00';
	  }else if(hora == '18'){
	    return '30';
	  }else if(hora == '19'){
	    return '00';
	  }else if (hora == '20') {
	    return '30';
	  }
	}
</script>











