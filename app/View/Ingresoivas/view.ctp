<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Iva</h3>
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
				<li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Iva'), array('action' => 'index')); ?></li>             
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Iva'); ?></h2>
	<table class="table table-striped">
	<tbody>
		
		<tr><td><?php echo __('Monto'); ?></td>
		<td>
			<?php echo h($ingresoiva['Ingresoiva']['monto']); ?>
			&nbsp;
		</td></tr>
	
	</tbody>
	</table>
</div>
</div>