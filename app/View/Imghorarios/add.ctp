
<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i>Horarios</h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
                    <li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Horarios'), array('action' => 'index')); ?></li>
		</ol>		
	</div>	    
</div>


<div class="row">
		     <div class="col-sm-12">
	  		   <section class="panel" style="padding: 10px;">

<div class="imghorarios form">
<?php echo $this->Form->create('Imghorario', array('enctype'=>'multipart/form-data')); ?>
	<fieldset>
		<legend><?php echo __('Add Horarios'); ?></legend>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('titulo', array('label'=>'Titulo','class'=>'form-control')); ?> 
 </div>
</div>
<br>
	<div class='row'>
		 <div class='col-sm-12'>		
			<?php echo $this->Form->input('imagen', array('id'=>'logo','label'=>'Imagen', 'type'=>'file'));  ?>
	 	</div>
 	</div>

	</fieldset>
	<br />
	<div class="pull-right">		
		<?php echo $this->Form->submit(__('Guardar', true), array('label'=>false, 'class'=>'btn btn-primary'));?>
		<?php echo $this->Form->end(__('')); ?>
	</div>

</div>

</section>
</div>
</div>
