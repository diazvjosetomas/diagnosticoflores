
<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Categoria Citas</h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
                    <li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Categoria Citas'), array('action' => 'index')); ?></li>
		</ol>		
	</div>	    
</div>


<div class="row">
		     <div class="col-sm-12">
	  		   <section class="panel" style="padding: 10px;">

<div class="categoriacitas form">
<?php echo $this->Form->create('Categoriacita'); ?>
	<fieldset>
		<legend><?php echo __('Agregar Categoria Cita'); ?></legend>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('denominacion', array('label'=>'Denominación','class'=>'form-control')); ?> 
 </div>
</div>
	</fieldset>
	<br />
	<div class="pull-right">		
		<?php echo $this->Form->submit(__('Guardar', true), array('label'=>false, 'class'=>'btn btn-primary'));?>
		<?php echo $this->Form->end(__('')); ?>
	</div>

</div>

</section>
</div>
</div>
