<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Egresos</h3>
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
				<li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Egresos'), array('action' => 'index')); ?></li>             
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Egreso'); ?></h2>
	<table class="table table-striped">
	<tbody>
		<tr><td><?php echo __('Egresotipo'); ?></td>
		<td>
			<?php echo h($egreso['Egresotipo']['denominacion']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Fecha'); ?></td>
		<td>
			<?php echo h($egreso['Egreso']['fecha']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Descripción'); ?></td>
		<td>
			<?php echo h($egreso['Egreso']['descripcion']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Monto'); ?></td>
		<td>
			<?php echo h($egreso['Egreso']['monto']); ?>
			&nbsp;
		</td></tr>

		<tr>
			<td>Estatus</td>
			<td>
				<?php echo $egreso['Egreso']['estatus'] ? 'Pagado' : 'Pendiente' ; ?>
			</td>
		</tr>
	</tbody>
	</table>
</div>
</div>