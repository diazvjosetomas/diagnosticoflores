
<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Egresos</h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
                    <li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Egresos'), array('action' => 'index')); ?></li>
		</ol>		
	</div>	    
</div>


<div class="row">
		     <div class="col-sm-12">
	  		   <section class="panel" style="padding: 10px;">

<div class="egresos form">
<?php echo $this->Form->create('Egreso'); ?>
	<fieldset>
		<legend><?php echo __('Edit Egresos'); ?></legend>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('id', array('label'=>'id','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('egresotipo_id', array('label'=>'Categoria de egreso','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('proveedor_pago_id', array('label'=>'Proveedor de pago','class'=>'form-control', 'options'=>$proveedor_pagos)); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('fecha', array('id'=>'fecha', 'label'=>'Fecha','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('descripcion', array('label'=>'Descripción','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('concepto', array('label'=>'Concepto','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('monto', array('label'=>'Monto','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('sucursale_id', array('label'=>'Sucursal','class'=>'form-control','empty'=>'--Seleccione--')); ?> 
 </div>
</div>

<div class="row">
	<div class="col-sm-12 col-md-12 col-lg-12">
		<?php echo $this->Form->input('estatus', array('label'=>'Estatus','class'=>'form-control','options'=>array('0'=>'Pendiente', '1'=>'Pagado'))); ?>
	</div>
</div>
	</fieldset>
	<br />
	<div class="pull-right">		
		<?php echo $this->Form->submit(__('Guardar', true), array('label'=>false, 'class'=>'btn btn-primary'));?>
		<?php echo $this->Form->end(__('')); ?>
	</div>

</div>

</section>
</div>
</div>
