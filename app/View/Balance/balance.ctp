      <div class="panel">
        <div class="panel-heading" style="padding: 12px;">
          <h3>Notas </h3> </div>
      </div>
      <div class="panel">
        <div class="panel-heading panel-success">
          <h4>&nbsp;
            <span class="pull-right">
              <div class="btn-group">
                <button type="button" class="btn btn-primary">Acción</button>
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                  <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                  <li><?php echo $this->Html->link(__('Nuevo Rootnota'), array('action' => 'rootadd')); ?></li>
                </ul>
              </div>
            </span>
          </h4>
        </div>
        <div class="panel-body">
      <div class="rootnotas index">
        <table id="Rootnota" cellpadding="0" cellspacing="0" class="table table-striped table-rounded">
        <thead>
        <tr>
          
            <th><?php echo h('Titulo'); ?></th>
            <th><?php echo h('Descripcion'); ?></th>
            <th><?php echo h('Estatus'); ?></th>
            <th><?php echo h('Creada'); ?></th>
            <th><?php echo h('Modificada'); ?></th>
            <th class="actions"><?php echo __('Acción'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($rootnotas as $rootnota): ?>
        <tr>
         
          <td><?php echo h($rootnota['Rootnota']['denominacion']); ?>&nbsp;</td>
          <td><?php echo h($rootnota['Rootnota']['descripcion']); ?>&nbsp;</td>
          <td>
            <?php echo $this->Html->link($rootnota['Statu']['id'], array('controller' => 'status', 'action' => 'rootview', $rootnota['Statu']['id'])); ?>
          </td>
          <td><?php echo h($rootnota['Rootnota']['created']); ?>&nbsp;</td>
          <td><?php echo h($rootnota['Rootnota']['modified']); ?>&nbsp;</td>
          <td class="actions">
            <?php echo $this->Html->link(__('Detalles'), array('action' => 'rootview', $rootnota['Rootnota']['id']), array('class'=>'btn btn-sm btn-default')); ?>
            <?php echo $this->Html->link(__('Editar'), array('action' => 'rootedit', $rootnota['Rootnota']['id']), array('class'=>'btn btn-sm btn-success')); ?>
            <?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'rootdelete', $rootnota['Rootnota']['id']), array('class'=>'btn btn-sm btn-danger'), __('¿Quieres eliminar esta nota # %s?', $rootnota['Rootnota']['id'])); ?>
          </td>
        </tr>
      <?php endforeach; ?>
        </tbody>
        </table>
      </div>
      </div>
      </div>
      <script type="text/javascript">
          //$(document).ready(function() {
              $('#Rootnota').DataTable( {
                  dom: 'Bfrtlip',
                  buttons: [
                      'copy', 'csv', 'excel', 'pdf', 'print'
                  ],
                  "language": 
                  {
                      "sProcessing":     "Procesando...",
                      "sLengthMenu":     "Mostrar _MENU_ registros",
                      "sZeroRecords":    "No se encontraron resultados",
                      "sEmptyTable":     "Ningún dato disponible en esta tabla",
                      "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                      "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                      "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                      "sInfoPostFix":    "",
                      "sSearch":         "Buscar:",
                      "sUrl":            "",
                      "sInfoThousands":  ".",
                      "sLoadingRecords": "Cargando...",
                      "oPaginate": {
                          "sFirst":    "Primero",
                          "sLast":     "Último",
                          "sNext":     "Siguiente",
                          "sPrevious": "Anterior"
                      },
                      "oAria": {
                          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                      }
                  }
              } );
          //} );
      </script>























          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Gráficas</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <!-- bar chart -->
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Servicios Más Solicitados</h2>
                    <ul class="nav navbar-right panel_toolbox">
                          <div class="row">
                              <div class="col-md-3">
                                  <h5>Busqueda</h5>
                              </div>
                              <div class="col-md-3">
                                  Desde <input class="form-control" type="text" name="desde" id="desde">
                              </div>
                              <div class="col-md-3">
                                  Hasta: <input class="form-control" type="text" name="hasta" id="hasta">
                              </div>
                              <div class="col-md-3" style="text-align: center;">
                                  <br>            
                            <button onclick="rangoServicios()" class="btn btn-success"> Buscar</button> 
                              </div>
                          </div>
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div id="graph_bar" style="width:100%; height:280px;"></div>
                  </div>
                </div>
              </div>
              <!-- /bar charts -->

              <!-- bar charts group -->
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Empleados Con Mas Servicios</h2>
                    <ul class="nav navbar-right panel_toolbox">
                          <div class="row">
                              <div class="col-md-3">
                                  <h5>Busqueda</h5>
                              </div>
                              <div class="col-md-3">
                                  Desde <input class="form-control" type="text" name="desdeE" id="desdeE">
                              </div>
                              <div class="col-md-3">
                                  Hasta: <input class="form-control" type="text" name="hastaE" id="hastaE">
                              </div>
                              <div class="col-md-3" style="text-align: center;">
                                  <br>            
                            <button onclick="rangoEmpleados()" class="btn btn-success"> Buscar</button> 
                              </div>
                          </div>
                      
                    </ul>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content1">
                    <div id="graph_empleados" style="width:100%; height:280px;"></div>
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
              <!-- /bar charts group -->
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Ingresos Por Servicio</h2>
                    <ul class="nav navbar-right panel_toolbox">
                          <div class="row">
                              <div class="col-md-3">
                                  <h5>Busqueda</h5>
                              </div>
                              <div class="col-md-3">
                                  Desde <input class="form-control" type="text" name="desdeIng" id="desdeIng">
                              </div>
                              <div class="col-md-3">
                                  Hasta: <input class="form-control" type="text" name="hastaIng" id="hastaIng">
                              </div>
                              <div class="col-md-3" style="text-align: center;">
                                  <br>            
                              <button onclick="rangoIngresos()" class="btn btn-success"> Buscar</button> 
                              </div>
                          </div>
                      
                    </ul>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content1">
                    <div id="graph_ingresos" style="width:100%; height:280px;"></div>
                  </div>
                </div>
              </div>

              <!-- bar charts group -->

            

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Relación [ Ingresos / Egresos ] Mensuales</h2>
                    <ul class="nav navbar-right panel_toolbox">
                          <div class="row">
                              <div class="col-md-3">
                                  
                              </div>
                              <div class="col-md-6">
                                 <br>
                                 <input placeholder="Ingrese Año" class="form-control" type="text" name="anhio" id="anhio">
                              </div>
                              
                              <div class="col-md-3" style="text-align: center;">
                                  <br>            
                              <button onclick="ingre_egre_mensuales()" class="btn btn-success"> Buscar</button> 
                              </div>
                          </div>
                      
                    </ul>
                   
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <canvas id="barIngresosEgresos"></canvas>
                  </div>
                </div>
              </div>



              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Relación [ Ingresos / Egresos ] Anuales</h2>
                    <ul class="nav navbar-right panel_toolbox">
                          <div class="row">
                              <div class="col-md-3">
                                  
                              </div>
                              <div class="col-md-6">
                                 <br>
                                 <input placeholder="Ingrese Año" class="form-control" type="text" name="anhio_anual" id="anhio_anual">
                              </div>
                              
                              <div class="col-md-3" style="text-align: center;">
                                  <br>            
                              <button onclick="ingre_egre_anuales()" class="btn btn-success"> Buscar</button> 
                              </div>
                          </div>
                      
                    </ul>
                   
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <canvas id="barIngresosEgresosAnuales"></canvas>
                  </div>
                </div>
              </div>





            </div>
          </div>
      


          <script type="text/javascript">
           


           

            $("#desde").datepicker();
            $("#hasta").datepicker();
            $("#desdeE").datepicker();
            $("#hastaE").datepicker();
            $("#desdeIng").datepicker();
            $("#hastaIng").datepicker();


            function ingre_egre_mensuales(){
              $('#barIngresosEgresos').html('');

              var anhio = $("#anhio").val();

              if (anhio == '') {
                anhio = <?=date('Y')?>
              }

              $.ajax({
                  url:'/Reportes/ingre_egre_mensuales/'+anhio,
                  type:'post',
                  data: {

                  },
                  success:function(response){

                        result = JSON.parse(response);
                      
                        if ($('#barIngresosEgresos').length ){ 
                        
                            var ctx = document.getElementById("barIngresosEgresos");
                            var mybarChart = new Chart(ctx, {
                            type: 'bar',
                            data: {
                              labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
                              datasets: [{
                              label: 'Total Egresos',
                              backgroundColor: "#ff6633",
                              data: [
                                      result.E1,
                                      result.E2,
                                      result.E3,
                                      result.E4,
                                      result.E5,
                                      result.E6,
                                      result.E7,
                                      result.E8,
                                      result.E9,
                                      result.E10,
                                      result.E11,
                                      result.E12
                                      
                                    ]
                              }, {
                              label: 'Total Ingresos',
                              backgroundColor: "#4d4dff",
                              data: [
                                      result.I1, 
                                      result.I2, 
                                      result.I3, 
                                      result.I4, 
                                      result.I5, 
                                      result.I6, 
                                      result.I7, 
                                      result.I8, 
                                      result.I9, 
                                      result.I10, 
                                      result.I11, 
                                      result.I12 

                                    ]
                              }]
                            },

                            options: {
                              scales: {
                              yAxes: [{
                                ticks: {
                                beginAtZero: true
                                }
                              }]
                              }
                            }
                            });
                            
                          }
                       
                      
                    
                  }
              });




                    
            }













            function ingre_egre_anuales(){
              $('#barIngresosEgresosAnuales').html('');

              var anhio = $("#anhio_anual").val();

              if (anhio == '') {
                anhio = <?=date('Y')?>
              }

              $.ajax({
                  url:'/Reportes/ingre_egre_anuales/'+anhio,
                  type:'post',
                  data: {

                  },
                  success:function(response){

                        result = JSON.parse(response);
                      
                        if ($('#barIngresosEgresosAnuales').length ){ 
                        
                            var ctx = document.getElementById("barIngresosEgresosAnuales");
                            var mybarChart = new Chart(ctx, {
                            type: 'bar',
                            data: {
                              labels: [
                                        result.anhio1,
                                        result.anhio2,
                                        result.anhio3,
                                        result.anhio4,
                                        result.anhio5,
                                      ],
                              datasets: [{
                              label: 'Total Egresos',
                              backgroundColor: "#ff6633",
                              data: [
                                      result.E1,
                                      result.E2,
                                      result.E3,
                                      result.E4,
                                      result.E5
                                      
                                    ]
                              }, {
                              label: 'Total Ingresos',
                              backgroundColor: "#4d4dff",
                              data: [
                                      result.I1, 
                                      result.I2, 
                                      result.I3, 
                                      result.I4, 
                                      result.I5

                                    ]
                              }]
                            },

                            options: {
                              scales: {
                              yAxes: [{
                                ticks: {
                                beginAtZero: true
                                }
                              }]
                              }
                            }
                            });
                            
                          }
                       
                      
                    
                  }
              });




                    
            }







            function rangoServicios(){


              var desde = $("#desde").val();
              var hasta = $("#hasta").val();


              if (desde == '' || hasta == '') {
                alert('Debe ambas fechas para graficar la respuesta.');
                return false;
              }



            

              $.ajax({
                  url:'/Reportes/rangoservicios/'+desde+'/'+hasta,
                  type:'post',
                  data: {

                  },
                  success:function(response){
                      
                      var data = JSON.parse(response);
                      var longitud = data.length;

                      console.log(longitud);
                        $('#graph_bar').html('');
                      

                             

                            Morris.Bar({
                                element: 'graph_bar',
                                data:JSON.parse(response),
                                xkey: 'device',
                                ykeys: ['geekbench'],
                                labels: ['Cantidad de Servicios'],
                                barRatio: 0.4,
                                barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                                xLabelAngle: 35,
                                hideHover: 'auto',
                                resize: true
                            });
                      
                    
                  }
              });
            }

            function initRangoServicios(){
              var desde = 'n';
              var hasta = 'n';
              $.ajax({
                  url:'/Reportes/rangoservicios/'+desde+'/'+hasta,
                  type:'post',
                  data: {

                  },
                  success:function(response){     
                    console.log(response);
                     
                        $('#graph_bar').html('');                   

                             

                            Morris.Bar({
                                element: 'graph_bar',
                                data:JSON.parse(response),
                                xkey: 'device',
                                ykeys: ['geekbench'],
                                labels: ['Cantidad de Servicios'],
                                barRatio: 0.4,
                                barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                                xLabelAngle: 35,
                                hideHover: 'auto',
                                resize: true
                            });
                      
                    
                  }
              });

            }



            function rangoEmpleados(){


              var desde = $("#desdeE").val();
              var hasta = $("#hastaE").val();

              if (desde == '' || hasta == '') {
                alert('Debe ambas fechas para graficar la respuesta.');
                return false;
              }
            

              $.ajax({
                  url:'/Reportes/rangoempleados/'+desde+'/'+hasta,
                  type:'post',
                  data: {

                  },
                  success:function(response){
                      
                   
                        $('#graph_empleados').html('');
                            Morris.Bar({
                                element: 'graph_empleados',
                                data:JSON.parse(response),
                                xkey: 'device',
                                ykeys: ['geekbench'],
                                labels: ['Cantidad de Servicios'],
                                barRatio: 0.4,
                                barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                                xLabelAngle: 35,
                                hideHover: 'auto',
                                resize: true
                            });
                      
                    
                  }
              });
            }


        function initrangoEmpleados(){


              var desde = 'n';
              var hasta = 'n';
            
            

              $.ajax({
                  url:'/Reportes/rangoempleados/'+desde+'/'+hasta,
                  type:'post',
                  data: {

                  },
                  success:function(response){
                      
                   
                        $('#graph_empleados').html('');
                            Morris.Bar({
                                element: 'graph_empleados',
                                data:JSON.parse(response),
                                xkey: 'device',
                                ykeys: ['geekbench'],
                                labels: ['Cantidad de Servicios'],
                                barRatio: 0.4,
                                barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                                xLabelAngle: 35,
                                hideHover: 'auto',
                                resize: true
                        });
                      
                    
                  }
              });
            }



            // Rango Ingresos
            function rangoIngresos(){


              var desde = $("#desdeIng").val();
              var hasta = $("#hastaIng").val();

              if (desde == '' || hasta == '') {
                alert('Debe ambas fechas para graficar la respuesta.');
                return false;
              }
            

              $.ajax({
                  url:'/Reportes/rangoingresos/'+desde+'/'+hasta,
                  type:'post',
                  data: {

                  },
                  success:function(response){

                        console.log(response);
                      
                   
                        $('#graph_ingresos').html('');
                            Morris.Donut({
                              element: 'graph_ingresos',
                              data: JSON.parse(response),
                              colors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                              formatter: function (y) {
                              return y + " $";
                              },
                              resize: true
                            });
                      
                    
                  }
              });
            }

            // Rango Ingresos
            function initrangoIngresos(){

              var desde = 'n';
              var hasta = 'n';

            
            

              $.ajax({
                  url:'/Reportes/rangoingresos/'+desde+'/'+hasta,
                  type:'post',
                  data: {

                  },
                  success:function(response){
                      
                   
                        $('#graph_ingresos').html('');
                            Morris.Donut({
                              element: 'graph_ingresos',
                              data: JSON.parse(response),
                              colors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                              formatter: function (y) {
                              return y + " $";
                              },
                              resize: true
                            });
                      
                    
                  }
              });
            }
            




            $(document).ready(function() {
                initRangoServicios();
                initrangoEmpleados();
                initrangoIngresos();
                init_morris_charts();
                ingre_egre_mensuales();
                ingre_egre_anuales();

 


            });

          </script>            
