<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Roles</h3>
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
				<li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Roles'), array('action' => 'index')); ?></li>             
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Role'); ?></h2>
	<table class="table table-striped">
	<tbody>
		<tr><td><?php echo __('Id'); ?></td>
		<td>
			<?php echo h($role['Role']['id']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Title'); ?></td>
		<td>
			<?php echo h($role['Role']['title']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Alias'); ?></td>
		<td>
			<?php echo h($role['Role']['alias']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Created'); ?></td>
		<td>
			<?php echo h($role['Role']['created']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Modified'); ?></td>
		<td>
			<?php echo h($role['Role']['modified']); ?>
			&nbsp;
		</td></tr>
	
	</tbody>
	</table>
</div>
</div>