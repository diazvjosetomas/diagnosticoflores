<?php


class fpdfview extends FPDF{
	function Footer(){}
	function Header(){

		
	}
}
$fpdf = new fpdfview('P','mm','A4');
$fpdf->AddPage();

$fpdf->Image('img/upload/logo.png',10,10,65,19);

$fpdf->SetFont('Arial','',12);
$fpdf->Ln(1);
$fpdf->Cell(195,15, "69 X 46 y 48 #435-a Centro (999) 9.24.23.75",'',0,'R');
$fpdf->Ln(4);
$fpdf->Cell(195,15, "42 X 55 y 55-a  #355 Francisco de Montejo (999) 1.95.22.98",'',0,'R');
$fpdf->Ln(4);
$fpdf->Cell(195,15, "Mérida Yucatán. CP. 97000",'',0,'R');
$fpdf->Ln(4);
$fpdf->Cell(195,15, $datoempresas[0]['Empresa']['web'],'',0,'R');





$fpdf->Ln(35);
$fpdf->SetFont('Arial','',18);
$fpdf->Cell(0,5,"Nota de Venta",'',1,'C');



$fpdf->SetFont('Arial','',12);

$fpdf->SetTextColor('255','87','51');
$fpdf->Ln(3);
$fpdf->Cell(185,15, "FOLIO: ".ucwords(strtolower($ingreso['Ingreso']['folio'])),'',0,'R');



$fpdf->SetTextColor('0','0','0');
$fpdf->Ln(5);
$fpdf->Cell(185,15, "FECHA: ".$ingreso['Ingreso']['fecha'],'',0,'R');

$fpdf->Ln(5);
$fpdf->Cell(185,15, "N° Estudio: ".ucwords(strtolower($ingreso['Ingreso']['n_estudio'])),'',0,'R');

$fpdf->Ln(5);
$fpdf->Cell(185,15, "SUCURSAL: ".ucwords(strtolower($ingreso['Sucursale']['denominacion'])),'',0,'R');


$fpdf->Ln(5);
$fpdf->Cell(185,15, "ATENDIO: ".ucwords(strtolower($ingreso['Personale']['nombres'].' '.$ingreso['Personale']['apellidos'])),'',0,'R');


$fpdf->Ln(5);
$fpdf->Cell(185,15, "CLIENTE: ".ucwords(strtolower($ingreso['Cliente']['nombres'].' '.$ingreso['Cliente']['apellidos'])),'',0,'L');
$fpdf->Ln(5);
$fpdf->Cell(185,15, "REMITIÓ: ".ucwords(strtolower($ingreso['User']['nombre'].' '.$ingreso['User']['apellidos'])),'',0,'L');

$fpdf->Ln(18);






$fpdf->SetLineWidth(.1);

$fpdf->SetFont('Arial','',11);

$fpdf->Ln(4);
$fpdf->Cell(25,5,"CANTIDAD",'LTB',0,'C');
$fpdf->Cell(35,5,"SERVICIO",'TB',0,'C');
$fpdf->Cell(45,5,"COSTO UNITARIO",'TB',0,'C');
$fpdf->Cell(45,5,"DESC",'TB',0,'C');
$fpdf->Cell(35,5,"SUBTOTAL",'TBR',0,'C');

$fpdf->Ln(5);
$fpdf->Cell(25,5,"1",'LTB',0,'C');
$fpdf->Cell(35,5,ucwords(strtolower($ingresodetalles[0]['Ingresotipo']['denominacion'])),'TB',0,'C');
$fpdf->Cell(45,5,number_format($ingreso['Ingreso']['subtotal'],'2',',','.'),'TB',0,'C');
$fpdf->Cell(45,5,number_format($ingreso['Ingreso']['monto_excento'],'2',',','.'),'TB',0,'C');
$fpdf->Cell(35,5,number_format($ingreso['Ingreso']['subtotal'],'2',',','.'),'TBR',0,'C');


$fpdf->Ln(15);
$fpdf->Cell(185,15, "TIPO PAGO: ".ucwords(strtolower($ingreso['Ingresotipopago']['denominacion'])),'',0,'L');


$fpdf->Ln(5);
$fpdf->Cell(185,15, "SUBTOTAL: ".number_format($ingreso['Ingreso']['subtotal'],'2',',','.'),'',0,'R');
$fpdf->Ln(5);
$fpdf->Cell(185,15, "IVA: ".number_format($ingreso['Ingreso']['monto_iva'],'2',',','.'),'',0,'R');
$fpdf->Ln(8);
$fpdf->Cell(185,15, "TOTAL: ".number_format($ingreso['Ingreso']['total'],'2',',','.'),'',0,'R');



$fpdf->Ln(90);
$fpdf->Cell(185,15, "GRACIAS POR SU PREFERENCIA!",'',0,'C');
$fpdf->Ln(5);
$fpdf->Cell(185,15, "Si usted requiere de factura electrónica favor de solicitarla en ".$datoempresas[0]['Empresa']['web']." dentro de 5 días hábiles ",'',0,'C');
$fpdf->Ln(5);
$fpdf->Cell(185,15, " después de la emisión de esta nota.",'',0,'C');



$fpdf->Output('F','Detalles.pdf');



?>