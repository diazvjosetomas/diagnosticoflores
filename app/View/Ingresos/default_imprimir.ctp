<script type="text/javascript">
	window.print();
	window.close();
</script>


<div class="panel">
<div class="panel panel-body">
<h2>Ingresos 

</h2>
	<table width="100%">
	<tbody>
		
		<tr><td><?php echo __('Doctor(a)'); ?></td>
		<td>
			<?php echo ucwords(strtolower($ingreso['User']['user'])); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Personal que atendió'); ?></td>
		<td>
			<?php echo ucwords(strtolower($ingreso['Personale']['nombres'])); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Cliente'); ?></td>
		<td>
			<?php echo ucwords(strtolower($ingreso['Cliente']['nombres'])); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Folio'); ?></td>
		<td>
			<?php echo h($ingreso['Ingreso']['folio']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('N Estudio'); ?></td>
		<td>
			<?php echo h($ingreso['Ingreso']['n_estudio']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Nota'); ?></td>
		<td>
			<?php echo h($ingreso['Ingreso']['nota']); ?>
			&nbsp;
		</td></tr>

		<tr>
			<td>Monto I.V.A.</td>
			<td> <?php echo $ingreso['Ingreso']['monto_iva'] ?> </td>
		</tr>

		<tr>
			<td>Subtotal</td>
			<td> <?php echo $ingreso['Ingreso']['subtotal'] ?> </td>
		</tr>

		<tr>
			<td>Monto Total</td>
			<td> <?php echo $ingreso['Ingreso']['total'] ?> </td>
		</tr>

		<tr>
			<td>Tipo de Pago</td>
			<td> <?php echo $ingreso['Ingresotipopago']['denominacion'] ?> </td>
		</tr>

		<tr>
			<td>Sucursal</td>
			<td>
				<?php echo $ingreso['Sucursale']['denominacion']; ?>
			</td>
		</tr>

	</tbody>
	</table>
</div>
</div>

