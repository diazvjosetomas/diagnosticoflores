<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Ingresos</h3>
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
				<li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Ingresos'), array('action' => 'index')); ?></li>             
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2>Ingresos 
<span class="pull-right">	
	<a class="btn btn-primary" onclick="imprimir(<?=$id?>)">
		<i class="fa fa-print"></i>

	</a>

	<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal"><i class="fa fa-envelope"></i></button>
</span>
</h2>
	<div id="datos_a_enviar">

	<input type="hidden" id="idIngreso" name="idIngreso" value="<?=$id?>">
		
	<table class="table table-striped" >
	<tbody>
		
		<tr><td><?php echo __('Doctor(a)'); ?></td>
		<td>
			<?php echo ucwords(strtolower($ingreso['User']['user'])); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Personal que atendió'); ?></td>
		<td>
			<?php echo ucwords(strtolower($ingreso['Personale']['nombres'])); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Cliente'); ?></td>
		<td>
			<?php echo ucwords(strtolower($ingreso['Cliente']['nombres'])); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Folio'); ?></td>
		<td>
			<?php echo h($ingreso['Ingreso']['folio']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('N Estudio'); ?></td>
		<td>
			<?php echo h($ingreso['Ingreso']['n_estudio']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Nota'); ?></td>
		<td>
			<?php echo h($ingreso['Ingreso']['nota']); ?>
			&nbsp;
		</td></tr>

		<tr>
			<td>Monto I.V.A.</td>
			<td> <?php echo $ingreso['Ingreso']['monto_iva'] ?> </td>
		</tr>

		<tr>
			<td>Subtotal</td>
			<td> <?php echo $ingreso['Ingreso']['subtotal'] ?> </td>
		</tr>

		<tr>
			<td>Monto Total</td>
			<td> <?php echo $ingreso['Ingreso']['total'] ?> </td>
		</tr>

		<tr>
			<td>Tipo de Pago</td>
			<td> <?php echo $ingreso['Ingresotipopago']['denominacion'] ?> </td>
		</tr>

		<tr>
			<td>Sucursal</td>
			<td>
				<?php echo $ingreso['Sucursale']['denominacion']; ?>
			</td>
		</tr>

	</tbody>
	</table>
</div>
</div>
</div>




<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Enviar detalles de este ingreso por email</h4>
      </div>
      <div class="modal-body">
       	<div class="row">
       		<div class="col-md-6">Para: 		</div>
       		<div class="col-md-6"><input id="email" type="text" name="email" class="form-control"> 		</div>
       		
       	</div>
       	<br>
       	<div class="row">
       		
       	<div class="col-md-6">
       		Asunto:
       	</div>
       	<div class="col-md-6">
       		<input id="asunto" type="text" name="asunto" class="form-control">
       	</div>
       	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" onclick="enviarMail()">Enviar</button>
      </div>
    </div>

  </div>
</div>






<script type="text/javascript">
	function enviarMail(){

		var email = $("#email").val();
		var asunto = $("#asunto").val();

		var idIngreso = $("#idIngreso").val();

		
		

		if (email == '') {
			alert('No puede dejar el campo Para vacío...');
			$("#email").focus();
			return false;
		}

		$.ajax({
			url : '/Ingresos/sender/'+email+'/'+asunto+'/'+idIngreso,
			type: 'post',
			data: {},
			success:function(response){

				alert('Mensaje enviado con éxito!');
				$("#myModal").modal('hide');

			}
		});

	}


	function imprimir(id){
		window.open('/Ingresos/imprimir/'+id,'_self');
	}
</script>