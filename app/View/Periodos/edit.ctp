
<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Periodos</h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
                    <li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Periodos'), array('action' => 'index')); ?></li>
		</ol>		
	</div>	    
</div>


<div class="row">
		     <div class="col-sm-12">
	  		   <section class="panel" style="padding: 10px;">

<div class="periodos form">
<?php echo $this->Form->create('Periodo'); ?>
	<fieldset>
		<legend><?php echo __('Edit Periodo'); ?></legend>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('id', array('label'=>'id','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('denominacion', array('label'=>'Denominación','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<label>Día</label>
		<select class="form-control" onchange="dia(this.value)">
			<option value="Lun">Lunes</option>
			<option value="Mar">Martes</option>
			<option value="Mie">Miercoles</option>
			<option value="Jue">Jueves</option>
			<option value="Vie">Viernes</option>
			<option value="Sab">Sábado</option>
			<option value="Dom">Domingo</option>
			<option value="*">Todos los días</option>
			<option value="def">Definir día</option>
		</select>
		<?php 


			$dias = array(
						'Lun'=>'Lunes',
						'Mar'=>'Martes',
						'Mie'=>'Miercoles',
						'Jue'=>'Jueves',
						'Vie'=>'Viernes',
						'Sab'=>'Sábado',
						'Dom'=>'Domingo',
						'*'=>'Todos los días',
						'def'=>'Definir día'
				);



		  ?> 
		<br>	
		<div id="divDia">
		<?php 
			//echo $this->Form->input('dia', array('label'=>'Día Establecido','class'=>'form-control'));
		 ?>
		<input placeholder="Ingrese el número del día" type="text" name="data[Periodo][dia]" id="PeriodoDia" class="form-control">
		</div>
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php 
		$mes = array(
				'01'=>'Enero',
				'02'=>'Febrero',
				'03'=>'Marzo',
				'04'=>'Abril',
				'05'=>'Mayo',
				'06'=>'Junio',
				'07'=>'Julio',
				'08'=>'Agosto',
				'09'=>'Septiembre',
				'10'=>'Octubre',
				'11'=>'Noviembre',
				'12'=>'Diciembre',
				'*'=>'Todos los meses',

			);
		echo $this->Form->input('mes', array('label'=>'Mes','class'=>'form-control','value'=>'*','options'=>$mes)); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('anhio', array('label'=>'Año','class'=>'form-control','value'=>'*')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php 

			$hora = array(
						'01'=>'01 AM',
						'02'=>'02 AM',
						'03'=>'03 AM',
						'04'=>'04 AM',
						'05'=>'05 AM',
						'06'=>'06 AM',
						'07'=>'07 AM',
						'08'=>'08 AM',
						'09'=>'09 AM',
						'10'=>'10 AM',
						'11'=>'11 AM',
						'12'=>'12 PM',
						'13'=>'01 PM',
						'14'=>'02 PM',
						'15'=>'03 PM',
						'16'=>'04 PM',
						'17'=>'05 PM',
						'18'=>'06 PM',
						'19'=>'07 PM',
						'20'=>'08 PM',
						'21'=>'09 PM',
						'22'=>'10 PM',
						'23'=>'11 PM',
						'24'=>'12 PM',
						'*'=>'Todas las horas',
					);







		echo $this->Form->input('hora', array('label'=>'Hora','class'=>'form-control','value'=>'*','options'=>$hora)); ?> 
 </div>
</div>

	</fieldset>
	<br />
	<div class="pull-right">		
		<?php echo $this->Form->submit(__('Guardar', true), array('label'=>false, 'class'=>'btn btn-primary'));?>
		<?php echo $this->Form->end(__('')); ?>
	</div>

</div>

</section>
</div>
</div>
<script type="text/javascript">
	function dia(value){

		console.log(value);
		if (value != 'def') {
			console.log('Distinto a def');
			$("#PeriodoDia").val(value);
			$("#divDia").hide();
		}
		if (value == 'def') {
			console.log('Igual a def');
			$("#divDia").show();

		}
	}
</script>
