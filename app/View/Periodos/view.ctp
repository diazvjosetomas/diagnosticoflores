<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Periodos</h3>
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
				<li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Periodos'), array('action' => 'index')); ?></li>             
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Periodo'); ?></h2>
	<table class="table table-striped">
	<tbody>
		<tr><td><?php echo __('Id'); ?></td>
		<td>
			<?php echo h($periodo['Periodo']['id']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Denominacion'); ?></td>
		<td>
			<?php echo h($periodo['Periodo']['denominacion']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Dia'); ?></td>
		<td>
			<?php echo h($periodo['Periodo']['dia']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Mes'); ?></td>
		<td>
			<?php echo h($periodo['Periodo']['mes']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Anhio'); ?></td>
		<td>
			<?php echo h($periodo['Periodo']['anhio']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Hora'); ?></td>
		<td>
			<?php echo h($periodo['Periodo']['hora']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Minuto'); ?></td>
		<td>
			<?php echo h($periodo['Periodo']['minuto']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Created'); ?></td>
		<td>
			<?php echo h($periodo['Periodo']['created']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Modified'); ?></td>
		<td>
			<?php echo h($periodo['Periodo']['modified']); ?>
			&nbsp;
		</td></tr>
	
	</tbody>
	</table>
</div>
</div>