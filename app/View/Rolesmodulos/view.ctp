<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Rolesmodulos</h3>
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
				<li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Rolesmodulos'), array('action' => 'index')); ?></li>             
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Rolesmodulo'); ?></h2>
	<table class="table table-striped">
	<tbody>
		<tr><td><?php echo __('Id'); ?></td>
		<td>
			<?php echo h($rolesmodulo['Rolesmodulo']['id']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Role'); ?></td>
		<td>
			<?php echo $this->Html->link($rolesmodulo['Role']['title'], array('controller' => 'roles', 'action' => 'view', $rolesmodulo['Role']['id'])); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Modulo'); ?></td>
		<td>
			<?php echo $this->Html->link($rolesmodulo['Modulo']['denominacion'], array('controller' => 'modulos', 'action' => 'view', $rolesmodulo['Modulo']['id'])); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Created'); ?></td>
		<td>
			<?php echo h($rolesmodulo['Rolesmodulo']['created']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Modified'); ?></td>
		<td>
			<?php echo h($rolesmodulo['Rolesmodulo']['modified']); ?>
			&nbsp;
		</td></tr>
	
	</tbody>
	</table>
</div>
</div>