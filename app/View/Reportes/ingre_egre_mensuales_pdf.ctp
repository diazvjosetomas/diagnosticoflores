<?php 

	$arrayEgresos[] = '';
	$arrayIngresos[] = '';

	//inicializando arrays
	for ($i=1; $i <= 12 ; $i++) { 
		$arrayEgresos[$i] = 0;
		$arrayIngresos[$i]= 0;
	}



	foreach ($egresos as $key) {
		for ($i=1; $i <= 12 ; $i++) {			 
			if (date('m', strtotime($key['Egreso']['fecha'] )) == $i ) {
				$arrayEgresos[$i] += $key['Egreso']['monto'].'<br>';
			}
		}
	}


	foreach ($ingresos as $key) {
		for ($i=1; $i <= 12 ; $i++) {			 
			if (date('m', strtotime($key['Ingreso']['created'] )) == $i ) {
				$arrayIngresos[$i] += $key['Ingreso']['total'].'<br>';
			}
		}
	}




class fpdfview extends FPDF{
	function Footer(){}
	function Header(){

		
	}
}
$fpdf = new fpdfview('P','mm','A4');
$fpdf->AddPage();

$fpdf->Image('img/upload/logo.png',10,10,75,25);

$fpdf->Ln(25);

$fpdf->Ln(5);
$fpdf->SetFont('Arial','B',22);
$fpdf->Cell(0,5,'BALANCE ANUAL PARA: '.$sucursal[0]['Sucursale']['denominacion'].' / '.$anhio,'',1,'C');

$fpdf->Ln(3);
$fpdf->SetFont('Arial','',6);
$fpdf->Cell(35,5,"MES",'LTB',0,'C');
$fpdf->Cell(45,5,"INGRESO",'TB',0,'C');
$fpdf->Cell(25,5,"EGRESO",'TB',0,'C');
$fpdf->Cell(75,5,"BALANCE",'TBR',0,'C');

$arrayMeses = array('1'=>'ENERO',
					'2'=>'FEBRERO',
					'3'=>'MARZO',
					'4'=>'ABRIL',
					'5'=>'MAYO',
					'6'=>'JUNIO',
					'7'=>'JULIO',
					'8'=>'AGOSTO',
					'9'=>'SEPTIEMBRE',
					'10'=>'OCTUBRE',
					'11'=>'NOVIEMBRE',
					'12'=>'DICIEMBRE');

for ($i = 1; $i <= 12 ; $i++) { 


$fpdf->Ln(8);
$fpdf->SetFont('Arial','',6);
$fpdf->Cell(35,5,$arrayMeses[$i],'',0,'C');
$fpdf->Cell(45,5, formatmonto(  $arrayIngresos[$i]),'',0,'C');
$fpdf->Cell(25,5, formatmonto(  $arrayEgresos[$i]),'',0,'C');
$fpdf->Cell(75,5, formatmonto(  $arrayIngresos[$i] - $arrayEgresos[$i] ),'',0,'C');

}








 $fpdf->Output('F','data.pdf');


?>