<div class="container">

<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Listado de Doctores que Refieren mas Estudios</h3>
		
	
	</div>	    
</div>

<div class="panel">
	<div class="panel-heading" style="padding: 12px;">
    <div class="row">
        <div class="col-md-3">
            <?php echo __('Busqueda por Rangos '); ?>
        </div>
        <div class="col-md-3">
            Desde <input class="form-control" type="text" name="desde" id="desde">
        </div>
        <div class="col-md-3">
            Hasta: <input class="form-control" type="text" name="hasta" id="hasta">
        </div>
        <div class="col-md-3" style="text-align: center;">
            <br>            
		 	<button onclick="rango()" class="btn btn-success"> Buscar</button> 
        </div>
    </div>
    </div>
</div>





<div class="panel">


	<div class="panel-body">








<div class="proveedorPagos index">
	
	<table id="datos" cellpadding="0" cellspacing="0" class="table table-striped table-rounded">
	<thead>
	<tr>
			
			<th><?php echo h('Doctores'); ?></th>
			<th style="text-align: center;"><?php echo h('Cantidad de Remitidos'); ?></th>
			
			
	</tr>
	</thead>
	<tbody id='remitidos'>


	</tbody>
	</table>
	
	</div>
</div>
</div>
</div>

<div id='text'></div>

<script type="text/javascript">
    //$(document).ready(function() {

        $("#desde").datepicker();
        $("#hasta").datepicker();

        $('#datos').DataTable( {
           dom: 'Bfrtlip',          
            responsive: true,
            buttons: [
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: [0,1]
                    }
                },
                {
                    extend: 'pdf',
                    exportOptions: {
                        columns: [0,1]
                    }
                },
                {
                    extend: 'copy',
                    exportOptions: {
                        columns: [0,1]
                    }
                },
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: [0,1]
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [0,1]
                    }
                }



            ],
            "language": 
            {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ".",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        } );
    //} );
</script>


<style type="text/css">
    td{
        text-align: center;
    }
</style>


<script type="text/javascript">
$("#remitidos").html('');
<?php 
    foreach ($user as $key) {
        echo "$.ajax({";
        echo "url:'/Reportes/cantidadremitidos/".$key['ingresos']['user_id']."',";
        echo "type:'post',";
        echo "data:{";
        echo "},";
        echo "success:function(response){";
        echo "var data = JSON.parse(response);";
        echo "if (data.result == true){";
        echo "$('#remitidos').append('<tr><td >'+data.nombres+' '+data.apellidos+'</td><td >'+data.cantidad+'</td></tr>');}else{";
        echo "$('#remitidos').html('Ha ocurrido un error');} } });";
    } 
?>


function rango(){
    var desde = $("#desde").val();
    var hasta = $("#hasta").val();
    

    if (desde == '' || hasta == '' ) {
        alert('Colocar ambas fechas para la busqueda por rango!');
        $("#desde").focus();
        return false;

    }

    $("#remitidos").html('');

    $.ajax({
        url:'/Reportes/listadosajax/',
        type:'post',
        data: {},
        success:function(response){
            console.log(response);
            var data = JSON.parse(response);
            var longitud = data.length;
           
            for (var i = 0; i <= longitud - 1; i++) {
                    
                console.log('data'+data[i].idDoctor);
            
                //Consulta por rango de fecha por cada doctor
                var idDoctor = data[i].idDoctor;
                $.ajax({
                    url:'/Reportes/rangocantidadremitidos/'+idDoctor+'/'+desde+'/'+hasta,
                    type:'post',
                    data: {},
                    success:function(response){
                        var data2 = JSON.parse(response);
                        if (data2.result == true) {
                            $("#remitidos").append('<tr><td>'+data2.nombres+' '+data2.apellidos+' </td><td>'+data2.cantidad+'</td></tr>');
                        }
                    }
                });
                //fin de consulta por rango de fecha por cada doctor
            
            }
        }
    });
}



   
                    
</script>