

<div class="container">




<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Reportes Doctor: 
	 	<?php if (isset($ingresos[0]['User']['nombre'])): ?>
	 		<?=ucwords(strtolower($ingresos[0]['User']['nombre'].' '.$ingresos[0]['User']['apellidos'])) ?>		
	 	<?php endif ?>
	 	</h3>
		
		<ol class="breadcrumb">
					
                    <li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__(' Volver'), array('action' => 'doctores')); ?></li>
		</ol>		
	</div>	    
</div>





<?php if (!isset($ingresos[0]['Ingreso']['user_id'])): ?>
	<div class="panel">
		<div class="panel-heading panel-success">
			<h4>&nbsp;
				No tiene datos para construir un reporte
			</h4>
		</div>
	</div>
<?php endif ?>


<?php if (isset($ingresos[0]['Ingreso']['user_id'])): ?>
	
<div class="panel">

	<div class="panel-body">
			<table id="datos" cellpadding="0" cellspacing="0" class="table table-striped table-rounded">
			<thead>
			<tr>
							<th><?php echo h('Folio'); ?></th>
							<th><?php echo h('Fecha'); ?></th>
							<th><?php echo h('N° Estudio'); ?></th>
							<th><?php echo h('Cliente'); ?></th>
							<th><?php echo h('Atendio'); ?></th>
							<th><?php echo h('Servicio'); ?></th>
							<th><?php echo h('Estatus'); ?></th>
							<th><?php echo h('Notas'); ?></th>
							
			</tr>
			</thead>
			<tbody>

			<?php foreach ($ingresos as $ingreso): ?>
				<tr>					
				<td><?=$ingreso['Ingreso']['folio']?></td>
				<td><?=$ingreso['Ingreso']['created']?></td>
				<td><?=$ingreso['Ingreso']['n_estudio']?></td>
				<td><?=ucwords(strtolower( $ingreso['Cliente']['nombres'].' '.$ingreso['Cliente']['apellidos']))?></td>
				<td><?=ucwords(strtolower($ingreso['Personale']['nombres'].' '.$ingreso['Personale']['apellidos']))?></td>
				<td><?=$ingreso['Ingresodetalle'][0]['Ingresotipo']['denominacion']?></td>
				<td><?=$ingreso['Ingreso']['status']?></td>
				<td><?=$ingreso['Ingreso']['nota']?></td>
				</tr>
			<?php endforeach ?>

			</tbody>
			</table>
		</section>
	</div>

</div>


<?php endif ?>



<script type="text/javascript">
    //$(document).ready(function() {
        $('#datos').DataTable( {
            dom: 'Bfrtlip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "language": 
            {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ".",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        } );
    //} );
</script>