<div class="container">


<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Reporte Empleados</h3>
		
	</div>	    
</div>

<div class="panel">
	<div class="panel-heading" style="padding: 12px;">
		<?php echo __('Empleados'); ?>	</div>
</div>





<div class="panel">
	<div class="panel-heading panel-success">


	<div class="panel-body">








<div class="personales index">
	
	<table id="datos" cellpadding="0" cellspacing="0" class="table table-striped table-rounded">
	<thead>
	<tr>
			
			<th><?php echo h('RFC'); ?></th>
			<th><?php echo h('Nombres'); ?></th>
			<th><?php echo h('Apellidos'); ?></th>
			<th><?php echo h('Telefono'); ?></th>
			<th><?php echo h('Email'); ?></th>
			<!-- <th><?php echo h('Direccion'); ?></th>
			<th><?php echo h('Fecha Ingreso'); ?></th> -->
			
			<th class="actions"><?php echo __('Acción'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($personales as $personale): ?>
	<tr>
		
		<td><?php echo h($personale['Personale']['rfc']); ?>&nbsp;</td>
		<td><?php echo h($personale['Personale']['nombres']); ?>&nbsp;</td>
		<td><?php echo h($personale['Personale']['apellidos']); ?>&nbsp;</td>
		<td><?php echo h($personale['Personale']['telefono']); ?>&nbsp;</td>
		<td><?php echo h($personale['Personale']['email']); ?>&nbsp;</td>
		<!-- <td><?php echo h($personale['Personale']['direccion']); ?>&nbsp;</td>
		<td><?php echo h($personale['Personale']['fecha_ingreso']); ?>&nbsp;</td> -->
		
		<td class="actions">
			<?php echo $this->Html->link(__('Ver Reporte'), array('action' => 'reporteempleados', $personale['Personale']['id']), array('class'=>'btn btn-sm btn-primary')); ?>

		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	
</div>
</div>
</div>

<script type="text/javascript">
    //$(document).ready(function() {
        $('#datos').DataTable( {
            dom: 'Bfrtlip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "language": 
            {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ".",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        } );
    //} );
</script>