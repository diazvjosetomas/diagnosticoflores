

<div class="container">




<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Reporte Doctores</h3>
		
			
	</div>	    
</div>
<div class="panel">
	<div class="panel-heading" style="padding: 12px;">
		<?php echo __('Doctores'); ?>	</div>
</div>

<div class="panel">

	<div class="panel-body">
			<table id="datos" cellpadding="0" cellspacing="0" class="table table-striped table-rounded">
			<thead>
			<tr>
							<th><?php echo h('Usuario'); ?></th>
							<th><?php echo h('Nombres'); ?></th>
							
							<th><?php echo h('Email'); ?></th>
							<th><?php echo h('Nivel'); ?></th>
							<th><?php echo h('Tipo cliente'); ?></th>
							<th class="actions"><?php echo __('Actions'); ?></th>
			</tr>
			</thead>
			<tbody>
			<?php foreach ($users as $user): ?>
	<tr>
		<td><?php echo h($user['User']['user']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['nombre'].' '.$user['User']['apellidos']); ?>&nbsp;</td>
		
		<td><?php echo h($user['User']['email']); ?>&nbsp;</td>
		
		<td><?php echo $user['User']['nivel']==1?"ROOT":"CONSULTOR"; ?>&nbsp;</td>
		<td><?php echo $user['Tipocliente']['denominacion']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link('Ver Reporte',      array('action' => 'reportedoctor', $user['User']['id']), array('class'=>'btn btn-primary btn-sm', 'escapeTitle'=>false)); ?>
			
		</td>
	</tr>
<?php endforeach; ?>
			</tbody>
			</table>
		</section>
	</div>

</div>
<script type="text/javascript">
    //$(document).ready(function() {
        $('#datos').DataTable( {
            dom: 'Bfrtlip',          
            responsive: true,
	        buttons: [
	            {
	                extend: 'excel',
	                exportOptions: {
	                    columns: [0,1,2,3,4]
	                }
	            },
	            {
	                extend: 'pdf',
	                exportOptions: {
	                    columns: [0,1,2,3,4]
	                }
	            },
	            {
	                extend: 'copy',
	                exportOptions: {
	                    columns: [0,1,2,3,4]
	                }
	            },
	            {
	                extend: 'csv',
	                exportOptions: {
	                    columns: [0,1,2,3,4]
	                }
	            },
	            {
	                extend: 'print',
	                exportOptions: {
	                    columns: [0,1,2,3,4]
	                }
	            }



	        ],
            "language": 
            {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ".",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        } );
    //} );
</script>
