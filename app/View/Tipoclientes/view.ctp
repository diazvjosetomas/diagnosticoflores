<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Tipos de especialidad </h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
					<li><i class="fa fa-arrow-left"></i>
					<a href="<?=$this->Html->url('/tipoclientes/')?>"> Volver a Tipo de especialidad </a> </li>              
		</ol>		
	</div>	    
</div>

<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Tipo de especialidad'); ?></h2>
	<table class="table table-striped">

	<tbody>
		<tr><td><?php echo __('Denominación'); ?></td>
		<td>
			<?php echo h($tipocliente['Tipocliente']['denominacion']); ?>
			&nbsp;
		</td></tr>
	
	</tbody>
	</table>
</div>

</div>