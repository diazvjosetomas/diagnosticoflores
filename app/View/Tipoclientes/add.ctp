
<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Tipo de especialidad</h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href=""> Home</a></li>	
					<li><i class="fa fa-arrow-left"></i>
					<a href="<?=$this->Html->url('/Tipoclientes/')?>"> Volver a Tipo de especialidad </a> </li>              
		</ol>		
	</div>	    
</div>


<div class="row">
		     <div class="col-sm-12">
	  		   <section class="panel" style="padding: 10px;">

<div class="tipoclientes form">
<?php echo $this->Form->create('Tipocliente'); ?>
	<fieldset>
		<legend><?php echo __('Add Tipo de especialidad'); ?></legend>
<div class='row'><div class='col-sm-12'>		<?php echo $this->Form->input('denominacion', array('label'=>'Denominacion','class'=>'form-control')); ?> 
 </div></div>	</fieldset>
 	<br>
	<div class="pull-right">		
		<?php echo $this->Form->submit(__('Submit', true), array('label'=>false, 'class'=>'btn btn-primary'));?>
		<?php echo $this->Form->end(__('')); ?>
	</div>

</div>

</section>
</div>
</div>
