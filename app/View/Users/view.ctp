<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Detalles Usuarios </h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
					<li><i class="fa fa-arrow-left"></i>
					<a href="<?=$this->Html->url('/users/')?>"> Volver a Usuarios </a> </li>              
		</ol>		
	</div>	    
</div>

<div class="row">
	<div class="col-sm-10">
	  <section class="panel">
    	<div class="panel-body">
    		<dl>
						<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($user['User']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Usuario'); ?></dt>
		<dd>
			<?php echo h($user['User']['user']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nombres'); ?></dt>
		<dd>
			<?php echo h($user['User']['nombre']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Apellidos'); ?></dt>
		<dd>
			<?php echo h($user['User']['apellidos']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($user['User']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Statu'); ?></dt>
		<dd>
			<?php echo h($user['User']['statu']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nivel'); ?></dt>
		<dd>
			<?php echo h($user['User']['nivel']); ?>
			&nbsp;
		</dd>
				</dl>
			</div>
		</section>
	</div>






	