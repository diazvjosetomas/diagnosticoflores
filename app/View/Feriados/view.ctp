<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Feriados</h3>
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
				<li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Feriados'), array('action' => 'index')); ?></li>             
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Feriado'); ?></h2>
	<table class="table table-striped">
	<tbody>
		<?php $booleam = array('1' => 'No', '2' => 'Si'); ?>
		<tr><td><?php echo __('Denominacion'); ?></td>
		<td>
			<?php echo h($feriado['Feriado']['denominacion']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Diaferiado'); ?></td>
		<td>
			<?php echo h($feriado['Feriado']['diaferiado']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Manana'); ?></td>
		<td>
			<?php echo h($booleam[$feriado['Feriado']['manana']]); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Tarde'); ?></td>
		<td>
			<?php echo h($booleam[$feriado['Feriado']['tarde']]); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Noche'); ?></td>
		<td>
			<?php echo h($booleam[$feriado['Feriado']['noche']]); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Creado'); ?></td>
		<td>
			<?php echo h($feriado['Feriado']['created']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Modificado'); ?></td>
		<td>
			<?php echo h($feriado['Feriado']['modified']); ?>
			&nbsp;
		</td></tr>
	
	</tbody>
	</table>
</div>
</div>