
<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Feriados</h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
                    <li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Feriados'), array('action' => 'index')); ?></li>
		</ol>		
	</div>	    
</div>


<div class="row">
		     <div class="col-sm-12">
	  		   <section class="panel" style="padding: 10px;">

<div class="feriados form">
<?php echo $this->Form->create('Feriado'); ?>
	<fieldset>
		<legend><?php echo __('Add Feriado'); ?></legend>
<div class='row'>
<div class='col-sm-12'>
		<?php $booleam = array('1' => 'No', '2' => 'Si'); ?>
		<?php echo $this->Form->input('denominacion', array('label'=>'Denominación','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('diaferiado', array('label'=>'Día Feriado','class'=>'form-control','id'=>'FeriadoDiaferiado')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('manana', array('label'=>'Mañana','class'=>'form-control','options'=>$booleam)); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('tarde', array('label'=>'Tarde','class'=>'form-control','options'=>$booleam)); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('noche', array('label'=>'Noche','class'=>'form-control','options'=>$booleam)); ?> 
 </div>
</div>
	</fieldset>
	<br />
	<div class="pull-right">		
		<?php echo $this->Form->submit(__('Guardar', true), array('label'=>false, 'class'=>'btn btn-primary'));?>
		<?php echo $this->Form->end(__('')); ?>
	</div>

</div>

</section>
</div>
</div>
