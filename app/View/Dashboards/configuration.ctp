<!-- File: /app/View/Posts/index.ctp -->
<h1>Configuración del Sistema</h1>
<table class="table table-striped ">
<tr>
<th>Id</th>
<th>Title</th>
<th>Font Awesome</th>
<th>Action</th>
</tr>
<!-- Here is where we loop through our $posts array, printing out post info -->
<?php foreach ($dashboards as $dashboard): ?>
<tr>
<td><?=$dashboard['Dashboards']['id']; ?></td>
<td>
<?=$dashboard['Dashboards']['name']; ?>
</td>
<td><?=$dashboard['Dashboards']['font-awesome']; ?></td>

<td><?=$this->Form->postLink(
                'Delete',                
                array('action' => 'delete', $dashboard['Dashboards']['id']),
                array('class'=>'btn btn-danger','confirm' =>'Are you sure?'))?>
            <?php echo $this->Html->link('Edit', array('action' => 'edit', $dashboard['Dashboards']['id']),array('class'=>'btn btn-success'));?></td>
</tr>
<?php endforeach; ?>
</table>