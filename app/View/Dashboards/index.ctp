


        <!-- page content -->

          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Calendario de Citas</h3>
              </div>

             
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>.:: Citas ::. </h2>
                
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div id='calendar'></div>

                  </div>
                </div>
              </div>
            </div>
          </div>
   

    <!-- calendar modal -->
    <div id="CalenderModalNew?" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">New Calendar Entry</h4>
          </div>
          <div class="modal-body">
            <div id="testmodal" style="padding: 5px 20px;">
              <form id="antoform" class="form-horizontal calender" role="form">
                <div class="form-group">
                  <label class="col-sm-3 control-label">Title</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="title" name="title">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Description</label>
                  <div class="col-sm-9">
                    <textarea class="form-control" style="height:55px;" id="descr" name="descr"></textarea>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default antoclose" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary antosubmit">Save changes</button>
          </div>
        </div>
      </div>
    </div>
    <div id="CalenderModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel2">Detalles de la Cita</h4>
          </div>
          <div class="modal-body">

            <div id="testmodal2" style="padding: 5px 20px;">
              <form id="antoform2" class="form-horizontal calender" role="form">
                <div class="form-group">
                  <label class="col-sm-3 control-label">Cita: </label>
                  <div class="col-sm-9">

                    <input type="text" class="form-control" id="title2" name="title2" readonly="true">
                  </div>
                </div>
               

              </form>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default antoclose2" data-dismiss="modal">Cerrar</button>
            <button type="button" class="btn btn-primary antosubmit2">Ok</button>
          </div>
        </div>
      </div>
      </div>
   

    <div id="fc_create" data-toggle="modal" data-target="#CalenderModalNew"></div>
    <div id="fc_edit" data-toggle="modal" data-target="#CalenderModalEdit"></div>

    <script>
      $(window).load(function() {
        var date = new Date(),
            d = date.getDate(),
            m = date.getMonth(),
            y = date.getFullYear(),
            started,
            categoryClass;

          

        var calendar = $('#calendar').fullCalendar({
          defaultView: 'agendaWeek',
          monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre',
                      'Octubre', 'Noviembre', 'Diciembre'],
                  monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                  dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado'],
                  dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
                 
                  buttonText: {
                      today:    'Hoy',
                      month:    'Mes',
                      week:     'Semana',
                      day:      'Día'
          },
          header: {
            left: 'prev,next today',
            center: 'title',
            //right: 'agendaWeek'
            right: 'month,agendaWeek,agendaDay'
          },
          selectable: true,
          selectHelper: true,
          select: function(start, end, allDay) {
            $('#fc_create').click();

            started = start;
            ended = end;

            $(".antosubmit").on("click", function() {
              var title = $("#title").val();
              if (end) {
                ended = end;
              }

              categoryClass = $("#event_type").val();

              if (title) {
                calendar.fullCalendar('renderEvent', {
                    title: title,
                    start: started,
                    end: end,
                    allDay: allDay
                  },
                  true // make the event "stick"
                );
              }

              $('#title').val('');

              calendar.fullCalendar('unselect');

              $('.antoclose').click();

              return false;
            });
          },
                eventClick: function(calEvent, jsEvent, view) {
            $('#fc_edit').click();
            $('#title2').val(calEvent.title);

            categoryClass = $("#event_type").val();

            $(".antosubmit2").on("click", function() {
              calEvent.title = $("#title2").val();

              calendar.fullCalendar('updateEvent', calEvent);
              $('.antoclose2').click();
            });

            calendar.fullCalendar('unselect');
          },
          editable: false,
      

      eventColor: "#1ABB9C",



      events: [


         <?php 
      
        foreach ($citas as $cita) {
          echo "{";
          echo "title: '". ucwords(strtolower($cita['Cita']['nombres']." / ".$cita['Tipocita']['denominacion']))."',";
          echo "start: new Date(".date("Y", strtotime($cita['Cita']['fecha'])).",".(date("m", strtotime($cita['Cita']['fecha'])) - 1).", ".date("d", strtotime($cita['Cita']['fecha'])).", devHora(".$cita['Cita']['hora']."), devMinuto(".$cita['Cita']['hora'].")),";
          echo "end:   new Date(".date("Y", strtotime($cita['Cita']['fecha'])).",".(date("m", strtotime($cita['Cita']['fecha'])) - 1).", ".date("d", strtotime($cita['Cita']['fecha'])).", devHora(".$cita['Cita']['hora']."), devMinuto2(".$cita['Cita']['hora'].")),";
          echo "allDay: false,";
          echo "color: '".$cita['Sucursale']['color']."'";
          echo "},";        
        }


        foreach ($feriados  as $cita) {
          echo "{";
          echo "title: '". ucwords(strtolower($cita['Feriado']['denominacion']))."',";
          echo "start: new Date(".date("Y", strtotime($cita['Feriado']['diaferiado'])).",".(date("m", strtotime($cita['Feriado']['diaferiado'])) - 1).", ".date("d", strtotime($cita['Feriado']['diaferiado'])).", devHora(0), devMinuto(1)),";
          echo "end:   new Date(".date("Y", strtotime($cita['Feriado']['diaferiado'])).",".(date("m", strtotime($cita['Feriado']['diaferiado'])) - 1).", ".date("d", strtotime($cita['Feriado']['diaferiado'])).", devHora(23), devMinuto2(2)),";


          echo "allDay: false";
          
          echo "},";        
        }


       ?>
       

          ]
          
          
        });
      });

    function devMinuto2(hora){
      var minuto2 = devMinuto(hora);
      console.log(minuto2);
      var totalMinuto = parseInt(minuto2) + parseInt(29);
      return totalMinuto;
    }


    function devHora(hora){
      if (hora == '0') {
        return '0';
      }
      if (hora == '23') {
        return '23'
      }
      if (hora == '1') {
        return '9';
      }else if(hora == '2'){
        return '9';
      }else if(hora == '3'){
        return '10';
      }else if(hora == '4'){
        return '10';
      }else if(hora == '5'){
        return '11';
      }else if(hora == '6'){
        return '11';
      }else if(hora == '7'){
        return '12';
      }else if(hora == '8'){
        return '12';
      }else if(hora == '9'){
        return '13';
      }else if(hora == '10'){
        return '13';
      }else if(hora == '11'){
        return '14';
      }else if(hora == '12'){
        return '14';
      }else if(hora == '13'){
        return '15';
      }else if(hora == '14'){
        return '15';
      }else if(hora == '15'){
        return '16';
      }else if(hora == '16'){
        return '16';
      }else if(hora == '17'){
        return '17';
      }else if(hora == '18'){
        return '17';
      }else if(hora == '19'){
        return '18';
      }else if(hora == '20'){
        return '18';
      }else if (hora == '21') {
        return '19';
      }else if (hora == '22') {
        return '19';
      }
    }

    function devMinuto(hora){
      if (hora == '1') {
        return '00';
      }else if(hora == '2'){
        return '30';
      }else if(hora == '3'){
        return '00';
      }else if(hora == '4'){
        return '30';
      }else if(hora == '5'){
        return '00';
      }else if(hora == '6'){
        return '30';
      }else if(hora == '7'){
        return '00';
      }else if(hora == '8'){
        return '30';
      }else if(hora == '9'){
        return '00';
      }else if(hora == '10'){
        return '30';
      }else if(hora == '11'){
        return '00';
      }else if(hora == '12'){
        return '30';
      }else if(hora == '13'){
        return '00';
      }else if(hora == '14'){
        return '30';
      }else if(hora == '15'){
        return '00';
      }else if(hora == '16'){
        return '30';
      }else if(hora == '17'){
        return '00';
      }else if(hora == '18'){
        return '30';
      }else if(hora == '19'){
        return '00';
      }else if (hora == '20') {
        return '30';
      }else if (hora == '21') {
        return '00';
      }else if (hora == '22') {
        return '30';
      }
    }

  

      $('.fc-agendaWeek-button').trigger('click');
  
    </script>




<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="fa fa-th"></i> Notas Pendientes</h3>
    
      
  </div>      
</div>


<div class="panel">
  <div class="panel-heading panel-success">
    <h4>&nbsp;
      <span class="pull-right">
        <div class="btn-group">
          <button type="button" class="btn btn-primary">Acción</button>
          <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" role="menu">
            <li><?php echo $this->Html->link(__('Nueva Nota'), array('action' => 'add')); ?></li>
          </ul>
        </div>
      </span>
    </h4>
  </div>
  <div class="panel-body">
<div class="notas index">
<table id="Nota" cellpadding="0" cellspacing="0" class="table table-striped table-rounded">
  <thead>
  <tr>
      
      <th><?php echo h('Fecha Creación'); ?></th>
      <th><?php echo h('Tarea'); ?></th>
      <th><?php echo h('Fecha Limite'); ?></th>
      <th><?php echo h('Status'); ?></th>
    
      <th class="actions"><?php echo __('Acción'); ?></th>
  </tr>
  </thead>
  <tbody>
  <?php foreach ($notas as $nota): ?>
  <tr>
    
    <td><?php echo h($nota['Nota']['fecha_creado']); ?>&nbsp;</td>
    <td><?php echo h($nota['Nota']['denominacion']); ?>&nbsp;</td>
    <td><?php echo h($nota['Nota']['fecha_limite']); ?>&nbsp;</td>
    <td>
      <?php echo $this->Html->link($nota['Status']['denominacion'], array('controller' => 'status', 'action' => 'view', $nota['Status']['id'])); ?>
    </td>

    <td class="actions">
      <?php 
      // echo $this->Html->link(__('Detalles'), array('action' => 'view', $nota['Nota']['id']), array('class'=>'btn btn-sm btn-default')); 
      ?>
      <?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $nota['Nota']['id']), array('class'=>'btn btn-sm btn-success')); ?>
      <?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $nota['Nota']['id']), array('class'=>'btn btn-sm btn-danger'), __('¿Esta seguro de eliminar esta nota:  <b> %s </b>?', $nota['Nota']['denominacion'])); ?>
    </td>
  </tr>
<?php endforeach; ?>
  </tbody>
  </table>
</div>
</div>
</div>
<script type="text/javascript">
    //$(document).ready(function() {
        $('#Nota').DataTable( {
            dom: 'Bfrtlip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "language": 
            {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ".",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        } );
    //} );
</script>