


        <!-- page content -->

          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Calendario de Citas</h3>
              </div>

             
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>.:: Citas ::. </h2>
                    <!-- <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul> -->
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div id='calendar'></div>

                  </div>
                </div>
              </div>
            </div>
          </div>
   

    <!-- calendar modal -->
    <div id="CalenderModalNew?" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">New Calendar Entry</h4>
          </div>
          <div class="modal-body">
            <div id="testmodal" style="padding: 5px 20px;">
              <form id="antoform" class="form-horizontal calender" role="form">
                <div class="form-group">
                  <label class="col-sm-3 control-label">Title</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="title" name="title">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Description</label>
                  <div class="col-sm-9">
                    <textarea class="form-control" style="height:55px;" id="descr" name="descr"></textarea>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default antoclose" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary antosubmit">Save changes</button>
          </div>
        </div>
      </div>
    </div>
    <div id="CalenderModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel2">Detalles de la Cita</h4>
          </div>
          <div class="modal-body">

            <div id="testmodal2" style="padding: 5px 20px;">
              <form id="antoform2" class="form-horizontal calender" role="form">
                <div class="form-group">
                  <label class="col-sm-3 control-label">Cita: </label>
                  <div class="col-sm-9">

                    <input type="text" class="form-control" id="title2" name="title2" readonly="true">
                  </div>
                </div>
               

              </form>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default antoclose2" data-dismiss="modal">Cerrar</button>
            <button type="button" class="btn btn-primary antosubmit2">Ok</button>
          </div>
        </div>
      </div>
      </div>
   

    <div id="fc_create" data-toggle="modal" data-target="#CalenderModalNew"></div>
    <div id="fc_edit" data-toggle="modal" data-target="#CalenderModalEdit"></div>

    <script>
      $(window).load(function() {
        var date = new Date(),
            d = date.getDate(),
            m = date.getMonth(),
            y = date.getFullYear(),
            started,
            categoryClass;

          

        var calendar = $('#calendar').fullCalendar({
          monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre',
                      'Octubre', 'Noviembre', 'Diciembre'],
                  monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                  dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado'],
                  dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
                 
                  buttonText: {
                      today:    'Hoy',
                      month:    'Mes',
                      week:     'Semana',
                      day:      'Día'
          },
          header: {
            left: 'prev,next today',
            center: 'title',
            //right: 'agendaWeek'
            right: 'month,agendaWeek,agendaDay'
          },
          selectable: true,
          selectHelper: true,
          select: function(start, end, allDay) {
            $('#fc_create').click();

            started = start;
            ended = end;

            $(".antosubmit").on("click", function() {
              var title = $("#title").val();
              if (end) {
                ended = end;
              }

              categoryClass = $("#event_type").val();

              if (title) {
                calendar.fullCalendar('renderEvent', {
                    title: title,
                    start: started,
                    end: end,
                    allDay: allDay
                  },
                  true // make the event "stick"
                );
              }

              $('#title').val('');

              calendar.fullCalendar('unselect');

              $('.antoclose').click();

              return false;
            });
          },
              	eventClick: function(calEvent, jsEvent, view) {
            $('#fc_edit').click();
            $('#title2').val(calEvent.title);

            categoryClass = $("#event_type").val();

            $(".antosubmit2").on("click", function() {
              calEvent.title = $("#title2").val();

              calendar.fullCalendar('updateEvent', calEvent);
              $('.antoclose2').click();
            });

            calendar.fullCalendar('unselect');
          },
          editable: true,
           eventColor: "#1ABB9C",



    	events: [


    	   <?php 
    	
    		foreach ($citas as $cita) {
    			echo "{";
    			echo "title: '". ucwords(strtolower(" Paciente: ".$cita['Cita']['nombres']." ".$cita['Cita']['apellidos']." / Estudio: ".$cita['Tipocita']['denominacion']))."',";
    			echo "start: new Date(".date("Y", strtotime($cita['Cita']['fecha'])).",".(date("m", strtotime($cita['Cita']['fecha'])) - 1).", ".date("d", strtotime($cita['Cita']['fecha'])).", devHora(".$cita['Cita']['hora']."), devMinuto(".$cita['Cita']['hora'].")),";
    			echo "end:   new Date(".date("Y", strtotime($cita['Cita']['fecha'])).",".(date("m", strtotime($cita['Cita']['fecha'])) - 1).", ".date("d", strtotime($cita['Cita']['fecha'])).", devHora(".$cita['Cita']['hora']."), devMinuto(".$cita['Cita']['hora'].")),";
    			echo "allDay: false";
    			echo "},";    		
    		}


    	 ?>


    	 
    	 

          ]
          
          
        });
      });

		function devHora(hora){
			if (hora == '1') {
				return '9';
			}else if(hora == '2'){
				return '9';
			}else if(hora == '3'){
				return '10';
			}else if(hora == '4'){
				return '10';
			}else if(hora == '5'){
				return '11';
			}else if(hora == '6'){
				return '11';
			}else if(hora == '7'){
				return '12';
			}else if(hora == '8'){
				return '12';
			}else if(hora == '9'){
				return '13';
			}else if(hora == '10'){
				return '13';
			}else if(hora == '11'){
				return '14';
			}else if(hora == '12'){
				return '14';
			}else if(hora == '13'){
				return '15';
			}else if(hora == '14'){
				return '15';
			}else if(hora == '15'){
				return '16';
			}else if(hora == '16'){
				return '16';
			}else if(hora == '17'){
				return '17';
			}else if(hora == '18'){
				return '17';
			}else if(hora == '19'){
				return '18';
			}
		}

		function devMinuto(hora){
			if (hora == '1') {
				return '00';
			}else if(hora == '2'){
				return '30';
			}else if(hora == '3'){
				return '00';
			}else if(hora == '4'){
				return '30';
			}else if(hora == '5'){
				return '00';
			}else if(hora == '6'){
				return '30';
			}else if(hora == '7'){
				return '00';
			}else if(hora == '8'){
				return '30';
			}else if(hora == '9'){
				return '00';
			}else if(hora == '10'){
				return '30';
			}else if(hora == '11'){
				return '00';
			}else if(hora == '12'){
				return '30';
			}else if(hora == '13'){
				return '00';
			}else if(hora == '14'){
				return '30';
			}else if(hora == '15'){
				return '00';
			}else if(hora == '16'){
				return '30';
			}else if(hora == '17'){
				return '00';
			}else if(hora == '18'){
				return '30';
			}else if(hora == '19'){
				return '00';
			}
		}
    </script>




