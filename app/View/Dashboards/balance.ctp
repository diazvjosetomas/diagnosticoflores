      <div class="panel">
        <div class="panel-heading" style="padding: 12px;">
          <h3>Notas </h3> </div>
      </div>
      <div class="panel">
        <div class="panel-heading panel-success">
          <h4>&nbsp;
            <span class="pull-right">
              <div class="btn-group">
                <button type="button" class="btn btn-primary">Acción</button>
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                  <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                  <li><?php echo $this->Html->link(__('Nuevo Rootnota'), array('action' => 'rootadd')); ?></li>
                </ul>
              </div>
            </span>
          </h4>
        </div>
        <div class="panel-body">
      <div class="rootnotas index">
        <table id="Rootnota" cellpadding="0" cellspacing="0" class="table table-striped table-rounded">
        <thead>
        <tr>
          
            <th><?php echo h('Titulo'); ?></th>
            <th><?php echo h('Descripcion'); ?></th>
            <th><?php echo h('Estatus'); ?></th>
            <th><?php echo h('Creada'); ?></th>
            <th><?php echo h('Modificada'); ?></th>
            <th class="actions"><?php echo __('Acción'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($rootnotas as $rootnota): ?>
        <tr>
         
          <td><?php echo h($rootnota['Rootnota']['denominacion']); ?>&nbsp;</td>
          <td><?php echo h($rootnota['Rootnota']['descripcion']); ?>&nbsp;</td>
          <td>
            <?php echo $this->Html->link($rootnota['Statu']['id'], array('controller' => 'status', 'action' => 'rootview', $rootnota['Statu']['id'])); ?>
          </td>
          <td><?php echo h($rootnota['Rootnota']['created']); ?>&nbsp;</td>
          <td><?php echo h($rootnota['Rootnota']['modified']); ?>&nbsp;</td>
          <td class="actions">
            <?php echo $this->Html->link(__('Detalles'), array('action' => 'rootview', $rootnota['Rootnota']['id']), array('class'=>'btn btn-sm btn-default')); ?>
            <?php echo $this->Html->link(__('Editar'), array('action' => 'rootedit', $rootnota['Rootnota']['id']), array('class'=>'btn btn-sm btn-success')); ?>
            <?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'rootdelete', $rootnota['Rootnota']['id']), array('class'=>'btn btn-sm btn-danger'), __('¿Quieres eliminar esta nota # %s?', $rootnota['Rootnota']['id'])); ?>
          </td>
        </tr>
      <?php endforeach; ?>
        </tbody>
        </table>
      </div>
      </div>
      </div>
      <script type="text/javascript">
          //$(document).ready(function() {
              $('#Rootnota').DataTable( {
                  dom: 'Bfrtlip',
                  buttons: [
                      'copy', 'csv', 'excel', 'pdf', 'print'
                  ],
                  "language": 
                  {
                      "sProcessing":     "Procesando...",
                      "sLengthMenu":     "Mostrar _MENU_ registros",
                      "sZeroRecords":    "No se encontraron resultados",
                      "sEmptyTable":     "Ningún dato disponible en esta tabla",
                      "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                      "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                      "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                      "sInfoPostFix":    "",
                      "sSearch":         "Buscar:",
                      "sUrl":            "",
                      "sInfoThousands":  ".",
                      "sLoadingRecords": "Cargando...",
                      "oPaginate": {
                          "sFirst":    "Primero",
                          "sLast":     "Último",
                          "sNext":     "Siguiente",
                          "sPrevious": "Anterior"
                      },
                      "oAria": {
                          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                      }
                  }
              } );
          //} );
      </script>


          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Gráficas</h3>
              </div>

            
            </div>

            <div class="clearfix"></div>



            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Servicios Más Solicitados</h2>

                    <ul class="nav navbar-right panel_toolbox">
                          <div class="row">
                              <div class="col-md-3">
                                  <h5>Busqueda</h5>
                              </div>
                              <div class="col-md-3">
                                  Desde <input class="form-control" type="text" name="desde" id="desde">
                              </div>
                              <div class="col-md-3">
                                  Hasta: <input class="form-control" type="text" name="hasta" id="hasta">
                              </div>
                              <div class="col-md-3" style="text-align: center;">
                                  <br>            
                            <button onclick="Servicios()" class="btn btn-success"> Buscar</button> 
                              </div>
                          </div>
                      
                    </ul>
                   

                    <div id="servicios" style="height: 450px"></div>



                    <script type="text/javascript">
                                // Create the chart
                    function Servicios(){

                       var desde = $("#desde").val();
                       var hasta = $("#hasta").val();

                       

                       console.log('desde '+desde+' hasta '+hasta);
                       $.ajax({
                        url:'/Reportes/rangoservicios/'+desde+'/'+hasta,
                        type:'post',
                        
                        data:{},
                        success:function(response){

                              console.log(response);
                          
                              var datas = response.split('_');

                              var series1 = JSON.parse(datas[0]);

                              var series2 = JSON.parse(datas[1]);



                                Highcharts.chart('servicios', {
                                    chart: {
                                        type: 'column'
                                    },
                                    title: {
                                        text: ''
                                    },
                                    subtitle: {
                                        text: ''
                                    },
                                    xAxis: {
                                        type: 'category'
                                    },
                                    yAxis: {
                                        title: {
                                            text: 'Cantidad de Servicios por Sucursal'
                                        }

                                    },
                                    legend: {
                                        enabled: false
                                    },
                                    plotOptions: {
                                        series: {
                                            borderWidth: 0,
                                            dataLabels: {
                                                enabled: true,
                                                format: '{point.y:f}'
                                            }
                                        }
                                    },

                                    tooltip: {
                                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:f}</b> Servicios Totales<br/>'
                                    },

                                    series: [{
                                        name: '',
                                        colorByPoint: true,
                                        data: [
                                            

                                          series1
                                            
                                       ]
                                    }],
                                    drilldown: {
                                        series: [

                                           series2
                                       ],
                                    exporting: {
                                        buttons: {
                                            contextButton: {
                                                align: "right",
                                                enabled: true,
                                                height: 20,
                                                menuItems: undefined,
                                                onclick: undefined,
                                                symbol: "menu",
                                                symbolFill: "#666666",
                                                symbolSize: 14,
                                                symbolStroke: "#666666",
                                                symbolStrokeWidth: 1,
                                                symbolX: 12.5,
                                                symbolY: 10.5,
                                                text: 'Erportar',
                                                theme:'',
                                                verticalAlign: "top",
                                                width: 24,
                                                x: -10,
                                                y: 0
                                            }
                                        }
                                    }
                                    }
                                });

                          }
                        
                       });


                       }


                       


                     Servicios();
                </script>
                    
                </div>
              </div>
            </div>
          </div>











              <!-- bar charts group -->
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Empleados Con Mas Servicios</h2>
                    <ul class="nav navbar-right panel_toolbox">
                          <div class="row">
                              <div class="col-md-3">
                                  <h5>Busqueda</h5>
                              </div>
                              <div class="col-md-3">
                                  Desde <input class="form-control" type="text" name="desdeE" id="desdeE">
                              </div>
                              <div class="col-md-3">
                                  Hasta: <input class="form-control" type="text" name="hastaE" id="hastaE">
                              </div>
                              <div class="col-md-3" style="text-align: center;">
                                  <br>            
                            <button onclick="rangoEmpleados()" class="btn btn-success"> Buscar</button> 
                              </div>
                          </div>
                      
                    </ul>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content1">
                    <div id="graph_empleados" style="width:100%; height:450px;"></div>
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
              <!-- /bar charts group -->
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Ingresos Por Servicio</h2>
                    <ul class="nav navbar-right panel_toolbox">
                          <div class="row">
                              <div class="col-md-3">
                                  <h5>Busqueda</h5>
                              </div>
                              <div class="col-md-3">
                                  Desde <input class="form-control" type="text" name="desdeIng" id="desdeIng">
                              </div>
                              <div class="col-md-3">
                                  Hasta: <input class="form-control" type="text" name="hastaIng" id="hastaIng">
                              </div>
                              <div class="col-md-3" style="text-align: center;">
                                  <br>            
                              <button onclick="rangoIngresos()" class="btn btn-success"> Buscar</button> 
                              </div>
                          </div>
                      
                    </ul>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content1">
                    <div id="graph_ingresos" style="width:100%; height:450px;"></div>
                  </div>
                </div>
              </div>

              <!-- bar charts group -->

            

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Relación [ Ingresos / Egresos ] Mensuales</h2>
                    <ul class="nav navbar-right panel_toolbox">
                          <div class="row">
                              <div class="col-md-3">
                                 <a onclick="ingre_egre_mensuales_pdf()" target="_blank" class="btn btn-danger"> Descargar PDF </a>

                                 <!--  <button type="button" class="btn btn-success"><a id="link1" onclick="bar1()" download="ChartPng.png">Save as png</a></button> -->
                                  <button type="button" class="btn btn-success"><a id="link2" onclick="bar1()" download="ChartJpg.jpg">Save as jpg</a></button>
                              </div>

                              
                              <div class='col-md-3'>
                                  <?php echo $this->Form->input('sucursale_id', array('label'=>'Sucursal','class'=>'form-control','empty'=>'--Seleccione--')); ?> 
                               </div>
                              


                              <div class="col-md-3">
                                 <br>
                                 <input placeholder="Ingrese Año" class="form-control" type="text" name="anhio" id="anhio">
                              </div>
                              
                              <div class="col-md-3" style="text-align: center;">
                                  <br>            
                              <button onclick="ingre_egre_mensuales()" class="btn btn-success"> Buscar</button> 
                              </div>
                          </div>
                      
                    </ul>
                   
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <canvas id="barIngresosEgresos"></canvas>
                  </div>
                </div>
              </div>



              <script type="text/javascript">
                function bar1() {
                    var url_base64 = document.getElementById("barIngresosEgresos").toDataURL("image/png");
                    var url_base64jp = document.getElementById("barIngresosEgresos").toDataURL("image/jpg");

                    link1.href = url_base64;
                    link2.href=url_base64jp

                    var url = link1.href.replace(/^data:image\/[^;]/, 'data:application/octet-stream');

                }
              </script>



              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Relación [ Ingresos / Egresos ] Anuales</h2>
                    <ul class="nav navbar-right panel_toolbox">
                          <div class="row">
                              <div class="col-md-3">
                                <button type="button" class="btn btn-success"><a id="link3" onclick="bar2()" download="ChartPng.png">Save as png</a></button>
                                <button type="button" class="btn btn-success"><a id="link4" onclick="bar2()" download="ChartJpg.jpg">Save as jpg</a></button>
                                  
                              </div>
                              
                              <div class="col-md-3">
                                 <br>
                                 <input placeholder="Ingrese Año" class="form-control" type="text" name="anhio_anual" id="anhio_anual">
                              </div>
                              
                              <div class="col-md-3" style="text-align: center;">
                                  <br>            
                              <button onclick="ingre_egre_anuales()" class="btn btn-success"> Buscar</button> 
                              </div>
                          </div>
                      
                    </ul>
                   
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <canvas id="barIngresosEgresosAnuales"></canvas>
                  </div>
                </div>
              </div>

              <script type="text/javascript">
                function bar2() {
                    var url_base64 = document.getElementById("barIngresosEgresosAnuales").toDataURL("image/png");
                    var url_base64jp = document.getElementById("barIngresosEgresosAnuales").toDataURL("image/jpg");

                    link3.href = url_base64;
                    link4.href=url_base64jp

                    var url = link1.href.replace(/^data:image\/[^;]/, 'data:application/octet-stream');

                }
              </script>




            </div>
          </div>
      


          <script type="text/javascript">
     


           

            $("#desde").datepicker();
            $("#hasta").datepicker();
            $("#desdeE").datepicker();
            $("#hastaE").datepicker();
            $("#desdeIng").datepicker();
            $("#hastaIng").datepicker();


            function ingre_egre_mensuales(){
              $('#barIngresosEgresos').html('');

              var anhio = $("#anhio").val();
              var sucursale_id = $("#sucursale_id").val();
              console.log('a '+anhio+' '+sucursale_id);
              if (anhio == '') {
                anhio = <?=date('Y')?>
              }

              $.ajax({
                  url:'/Reportes/ingre_egre_mensuales/'+anhio+'/'+sucursale_id,
                  type:'post',
                  data: {

                  },
                  success:function(response){


                        


                        result = JSON.parse(response);
                        $("#barIngresosEgresos").html();
                      
                        if ($('#barIngresosEgresos').length ){ 
                        
                            var ctx = document.getElementById("barIngresosEgresos");
                            var mybarChart = new Chart(ctx, {
                            type: 'bar',
                            data: {
                              labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
                              datasets: [{
                              label: 'Total Egresos',
                              backgroundColor: "#ff6633",
                              data: [
                                      result.E1,
                                      result.E2,
                                      result.E3,
                                      result.E4,
                                      result.E5,
                                      result.E6,
                                      result.E7,
                                      result.E8,
                                      result.E9,
                                      result.E10,
                                      result.E11,
                                      result.E12
                                      
                                    ]
                              }, {
                              label: 'Total Ingresos',
                              backgroundColor: "#4d4dff",
                              data: [
                                      result.I1, 
                                      result.I2, 
                                      result.I3, 
                                      result.I4, 
                                      result.I5, 
                                      result.I6, 
                                      result.I7, 
                                      result.I8, 
                                      result.I9, 
                                      result.I10, 
                                      result.I11, 
                                      result.I12 

                                    ]
                              }, {
                              label: 'Balance',
                              backgroundColor: "#7300e6",
                              data: [
                                      result.B1, 
                                      result.B2, 
                                      result.B3, 
                                      result.B4, 
                                      result.B5, 
                                      result.B6, 
                                      result.B7, 
                                      result.B8, 
                                      result.B9, 
                                      result.B10, 
                                      result.B11, 
                                      result.B12 

                                    ]
                              }

                              ],
                                    exporting: {
                                        buttons: {
                                            contextButton: {
                                                align: "right",
                                                enabled: true,
                                                height: 20,
                                                menuItems: undefined,
                                                onclick: undefined,
                                                symbol: "menu",
                                                symbolFill: "#666666",
                                                symbolSize: 14,
                                                symbolStroke: "#666666",
                                                symbolStrokeWidth: 1,
                                                symbolX: 12.5,
                                                symbolY: 10.5,
                                                text: 'Erportar',
                                                theme:'',
                                                verticalAlign: "top",
                                                width: 24,
                                                x: -10,
                                                y: 0
                                            }
                                        }
                                    }
                            },

                            options: {
                              scales: {
                              yAxes: [{
                                ticks: {
                                beginAtZero: true
                                }
                              }]
                              }
                            }
                            });
                            
                          }
                       
                      
                    
                  }
              });




                    
            }




            function ingre_egre_mensuales_pdf(){
              $('#barIngresosEgresos').html('');

              var anhio = $("#anhio").val();
              var sucursale_id = $("#sucursale_id").val();
              console.log('a '+anhio+' '+sucursale_id);
              if (anhio == '') {
                anhio = <?=date('Y')?>
              }
             window.open('/Reportes/ingre_egre_mensuales_pdf/'+anhio+'/'+sucursale_id, '_blank');                    
            }



            function ingre_egre_anuales(){
              $('#barIngresosEgresosAnuales').html('');

              var anhio = $("#anhio_anual").val();
              

              if (anhio == '') {
                anhio = <?=date('Y')?>
              }

              $.ajax({
                  url:'/Reportes/ingre_egre_anuales/'+anhio,
                  type:'post',
                  data: {

                  },
                  success:function(response){

                        result = JSON.parse(response);
                      
                        if ($('#barIngresosEgresosAnuales').length ){ 
                        
                            var ctx = document.getElementById("barIngresosEgresosAnuales");
                            var mybarChart = new Chart(ctx, {
                            type: 'bar',
                            data: {
                              labels: [
                                        result.anhio1,
                                        result.anhio2,
                                        result.anhio3,
                                        result.anhio4,
                                        result.anhio5,
                                      ],
                              datasets: [{
                              label: 'Total Egresos',
                              backgroundColor: "#ff6633",
                              data: [
                                      result.E1,
                                      result.E2,
                                      result.E3,
                                      result.E4,
                                      result.E5
                                      
                                    ]
                              }, {
                              label: 'Total Ingresos',
                              backgroundColor: "#4d4dff",
                              data: [
                                      result.I1, 
                                      result.I2, 
                                      result.I3, 
                                      result.I4, 
                                      result.I5

                                    ]
                              }]
                            },

                            options: {
                              scales: {
                              yAxes: [{
                                ticks: {
                                beginAtZero: true
                                }
                              }]
                              }
                            }
                            });
                            
                          }
                       
                      
                    
                  }
              });




                    
            }








            function rangoEmpleados(){


              var desde = $("#desde").val();
                       var hasta = $("#hasta").val();

                       


                       $.ajax({
                        url:'/Reportes/rangoempleados/'+desde+'/'+hasta,
                        type:'post',
                        
                        data:{},
                        success:function(response){

                              console.log(response);
                          
                              var datas = response.split('_');

                              var series1 = JSON.parse(datas[0]);

                              var series2 = JSON.parse(datas[1]);



                                Highcharts.chart('graph_empleados', {
                                    chart: {
                                        type: 'column'
                                    },
                                    title: {
                                        text: ''
                                    },
                                    subtitle: {
                                        text: ''
                                    },
                                    xAxis: {
                                        type: 'category'
                                    },
                                    yAxis: {
                                        title: {
                                            text: 'Monto por Sucursal'
                                        }

                                    },
                                    legend: {
                                        enabled: false
                                    },
                                    plotOptions: {
                                        series: {
                                            borderWidth: 0,
                                            dataLabels: {
                                                enabled: true,
                                                format: '{point.y:.1f}'
                                            }
                                        }
                                    },

                                    tooltip: {
                                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> monto Total<br/>'
                                    },

                                    series: [{
                                        name: '',
                                        colorByPoint: true,
                                        data: [
                                            

                                          series1
                                            
                                       ]
                                    }],
                                    drilldown: {
                                        series: [

                                           series2
                                       ],
                                    exporting: {
                                        buttons: {
                                            contextButton: {
                                                align: "right",
                                                enabled: true,
                                                height: 20,
                                                menuItems: undefined,
                                                onclick: undefined,
                                                symbol: "menu",
                                                symbolFill: "#666666",
                                                symbolSize: 14,
                                                symbolStroke: "#666666",
                                                symbolStrokeWidth: 1,
                                                symbolX: 12.5,
                                                symbolY: 10.5,
                                                text: 'Erportar',
                                                theme:'',
                                                verticalAlign: "top",
                                                width: 24,
                                                x: -10,
                                                y: 0
                                            }
                                        }
                                    }
                                    }
                                });

                          }
                        
                       });
            }


        function initrangoEmpleados(){


              var desde = $("#desde").val();
                       var hasta = $("#hasta").val();

                       


                       $.ajax({
                        url:'/Reportes/rangoempleados',
                        type:'post',
                        
                        data:{},
                        success:function(response){

                              console.log(response);
                          
                              var datas = response.split('_');

                              var series1 = JSON.parse(datas[0]);

                              var series2 = JSON.parse(datas[1]);



                                Highcharts.chart('graph_empleados', {
                                    chart: {
                                        type: 'column'
                                    },
                                    title: {
                                        text: ''
                                    },
                                    subtitle: {
                                        text: ''
                                    },
                                    xAxis: {
                                        type: 'category'
                                    },
                                    yAxis: {
                                        title: {
                                            text: 'Monto por Sucursal'
                                        }

                                    },
                                    legend: {
                                        enabled: false
                                    },
                                    plotOptions: {
                                        series: {
                                            borderWidth: 0,
                                            dataLabels: {
                                                enabled: true,
                                                format: '{point.y:.1f}'
                                            }
                                        }
                                    },

                                    tooltip: {
                                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> monto Total<br/>'
                                    },

                                    series: [{
                                        name: '',
                                        colorByPoint: true,
                                        data: [
                                            

                                          series1
                                            
                                       ]
                                    }],
                                    drilldown: {
                                        series: [

                                           series2
                                       ],
                                    exporting: {
                                        buttons: {
                                            contextButton: {
                                                align: "right",
                                                enabled: true,
                                                height: 20,
                                                menuItems: undefined,
                                                onclick: undefined,
                                                symbol: "menu",
                                                symbolFill: "#666666",
                                                symbolSize: 14,
                                                symbolStroke: "#666666",
                                                symbolStrokeWidth: 1,
                                                symbolX: 12.5,
                                                symbolY: 10.5,
                                                text: 'Erportar',
                                                theme:'',
                                                verticalAlign: "top",
                                                width: 24,
                                                x: -10,
                                                y: 0
                                            }
                                        }
                                    }
                                    }
                                });

                          }
                        
                       });
            }



            // Rango Ingresos
            function rangoIngresos(){


              var desde = $("#desde").val();
              var hasta = $("#hasta").val();

              


              $.ajax({
               url:'/Reportes/rangoingresos',
               type:'post',
               
               data:{},
               success:function(response){

                     console.log(response);
                 
                     var datas = response.split('_');

                     var series1 = JSON.parse(datas[0]);

                     var series2 = JSON.parse(datas[1]);



                       Highcharts.chart('graph_ingresos', {
                           chart: {
                               type: 'column'
                           },
                           title: {
                               text: ''
                           },
                           subtitle: {
                               text: ''
                           },
                           xAxis: {
                               type: 'category'
                           },
                           yAxis: {
                               title: {
                                   text: 'Monto por Sucursal'
                               }

                           },
                           legend: {
                               enabled: false
                           },
                           plotOptions: {
                               series: {
                                   borderWidth: 0,
                                   dataLabels: {
                                       enabled: true,
                                       format: '{point.y:.1f}'
                                   }
                               }
                           },

                           tooltip: {
                               headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                               pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> monto Total<br/>'
                           },

                           series: [{
                               name: '',
                               colorByPoint: true,
                               data: [
                                   

                                 series1
                                   
                              ]
                           }],
                           drilldown: {
                               series: [

                                  series2
                              ],
                           exporting: {
                               buttons: {
                                   contextButton: {
                                       align: "right",
                                       enabled: true,
                                       height: 20,
                                       menuItems: undefined,
                                       onclick: undefined,
                                       symbol: "menu",
                                       symbolFill: "#666666",
                                       symbolSize: 14,
                                       symbolStroke: "#666666",
                                       symbolStrokeWidth: 1,
                                       symbolX: 12.5,
                                       symbolY: 10.5,
                                       text: '',
                                       theme:'',
                                       verticalAlign: "top",
                                       width: 24,
                                       x: -10,
                                       y: 0
                                   }
                               }
                           }
                           }
                       });

                 }
               
              });
            }

            // Rango Ingresos
            function initrangoIngresos(){

              var desde = $("#desde").val();
              var hasta = $("#hasta").val();

              


              $.ajax({
               url:'/Reportes/rangoingresos',
               type:'post',
               
               data:{},
               success:function(response){

                     console.log(response);
                 
                     var datas = response.split('_');

                     var series1 = JSON.parse(datas[0]);

                     var series2 = JSON.parse(datas[1]);



                       Highcharts.chart('graph_ingresos', {
                           chart: {
                               type: 'column'
                           },
                           title: {
                               text: ''
                           },
                           subtitle: {
                               text: ''
                           },
                           xAxis: {
                               type: 'category'
                           },
                           yAxis: {
                               title: {
                                   text: 'Monto por Sucursal'
                               }

                           },
                           legend: {
                               enabled: false
                           },
                           plotOptions: {
                               series: {
                                   borderWidth: 0,
                                   dataLabels: {
                                       enabled: true,
                                       format: '{point.y:.1f}'
                                   }
                               }
                           },

                           tooltip: {
                               headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                               pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> monto Total<br/>'
                           },

                           series: [{
                               name: '',
                               colorByPoint: true,
                               data: [
                                   

                                 series1
                                   
                              ]
                           }],
                           drilldown: {
                               series: [

                                  series2
                              ],
                           exporting: {
                               buttons: {
                                   contextButton: {
                                       align: "right",
                                       enabled: true,
                                       height: 20,
                                       menuItems: undefined,
                                       onclick: undefined,
                                       symbol: "menu",
                                       symbolFill: "#666666",
                                       symbolSize: 14,
                                       symbolStroke: "#666666",
                                       symbolStrokeWidth: 1,
                                       symbolX: 12.5,
                                       symbolY: 10.5,
                                       text: '',
                                       theme:'',
                                       verticalAlign: "top",
                                       width: 24,
                                       x: -10,
                                       y: 0
                                   }
                               }
                           }
                           }
                       });

                 }
               
              });
            }
            




            $(document).ready(function() {
                //initRangoServicios();
                initrangoEmpleados();
                initrangoIngresos();
                
                ingre_egre_mensuales();
                ingre_egre_anuales();

 


            });

          </script>            
