<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Rootnotas</h3>
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
				<li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Rootnotas'), array('action' => 'index')); ?></li>             
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Rootnota'); ?></h2>
	<table class="table table-striped">
	<tbody>
		<tr><td><?php echo __('Id'); ?></td>
		<td>
			<?php echo h($rootnota['Rootnota']['id']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Denominacion'); ?></td>
		<td>
			<?php echo h($rootnota['Rootnota']['denominacion']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Descripcion'); ?></td>
		<td>
			<?php echo h($rootnota['Rootnota']['descripcion']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Statu'); ?></td>
		<td>
			<?php echo $this->Html->link($rootnota['Statu']['id'], array('controller' => 'status', 'action' => 'view', $rootnota['Statu']['id'])); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Created'); ?></td>
		<td>
			<?php echo h($rootnota['Rootnota']['created']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Modified'); ?></td>
		<td>
			<?php echo h($rootnota['Rootnota']['modified']); ?>
			&nbsp;
		</td></tr>
	
	</tbody>
	</table>
</div>
</div>