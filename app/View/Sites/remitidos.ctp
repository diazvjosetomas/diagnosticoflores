<div class="container">
<br>
<div class="panel">
	<div class="panel-heading panel-success">
		
	</div>
	<div class="panel-body">
<div class="adjuntos index">
	<table id="Adjunto" cellpadding="0" cellspacing="0" class="table table-striped table-rounded">
	<thead>
	<tr>
			
			
			<th><?php echo h('Paciente'); ?></th>
			<th><?php echo h('Fecha'); ?></th>
			<th><?php echo h('Servicio'); ?></th>

			<th class="actions" style="text-align: center;"><?php echo __('Acción'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($adjuntos as $adjunto): ?>
	<tr>
	
		
		<td>
			<?php echo ucwords(strtolower($adjunto['Cliente']['nombres'].' '.$adjunto['Cliente']['apellidos'])); ?>
		</td>
		<td>
			<?php echo $adjunto['Adjunto']['created']; ?>
		</td>
		<td >
			<?php echo $adjunto['Ingresotipo']['denominacion']; ?>
		</td>
		
		<td class="actions"  style="text-align: center;">
			<!-- <?php echo $this->Html->link(__('Ver Radiografía'), array('action' => 'detallesremitidos', $adjunto['Adjunto']['id']), array('class'=>'btn btn-sm btn-default')); ?> -->
			<a href="#" data-toggle='modal' class="btn btn-sm btn-default" data-target='#myModal<?=$adjunto['Cliente']['id']?>'>
				Ver Radiografía
			</a>

			<div id="myModal<?=$adjunto['Cliente']['id']?>" class="modal fade" role="dialog">
			  				<div class="modal-dialog">
							    <!-- Modal content-->
							    <div class="modal-content">
							      <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal">&times;</button>
							        <h4 class="modal-title">Vista de Imagen</h4>
							      </div>
							      <div class="modal-body">
							       <img width="570"  src="/img/<?=$adjunto['Imgadjunto'][0]['ruta_imagen']?>"> 
							      </div>
							      <div class="modal-footer">
							      	<a class="btn btn-success" href="/img/<?=$adjunto['Imgadjunto'][0]['ruta_imagen']?>" download="Imagen_/img/<?=$adjunto['Imgadjunto'][0]['ruta_imagen']?>"> Descargar Imagen</a>
							        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							      </div>
							    </div>

							  </div>
							</div>
			<!-- <?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $adjunto['Adjunto']['id']), array('class'=>'btn btn-sm btn-success')); ?>
			<?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $adjunto['Adjunto']['id']), array('class'=>'btn btn-sm btn-danger'), __('Are you sure you want to delete # %s?', $adjunto['Adjunto']['id'])); ?> -->
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
</div>
</div>
</div>

