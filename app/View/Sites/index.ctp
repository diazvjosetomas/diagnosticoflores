<div class="camera_container" style="display: none;">
    <div id="camera" class="camera_wrap">
<?php 
    /*foreach ($slider as $sliders) {
        if(isset($sliders["Slider"]["carpeta_imagen"])){
           $img = $sliders["Slider"]["ruta_imagen"];
        }
        echo '<div  data-src="/img/'.$img.'"><div class="camera_caption fadeIn">';
        echo $sliders["Slider"]["contenido"].'</div></div>';        
    }*/
 ?> 
    </div>
</div>
    <img src="/img/upload/portada.jpg">
 <!--========================================================
                              CONTENT
    =========================================================-->
    <main class="center767">
        <section>
            <div class="well1">
                <div class="container">
                    <style type="text/css">
                        .border{
                            border-radius: 0px 0px 0px 0px;
                            -moz-border-radius: 0px 0px 0px 0px;
                            -webkit-border-radius: 0px 0px 0px 0px;
                            border: 1px solid #01B9AF;

                            padding-top: 15px;
                            padding-bottom: 15px;

                            height: 420px;

                            text-align: justify;
                        }

                        .h5_back{
                            background: #01B9AF;
                            padding: 2px;
                            color:white;
                        }
                    </style>
                    <a name="nosotros"></a>
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12 wow fadeInLeft" style=" text-align: justify;  animation-name: none;">
                            <h5 class="h5_back wow " style=" animation-name: none;">Nosotros</h5>
                            <br>
                            <?php 
                                echo $nosotros[0]['Contenido']['contenido'];
                             ?>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 wow fadeInUp " style="animation-name: none;">
                            <h5 class="h5_back wow " style=" animation-name: none;">Sucursales</h5>
                            <br>
                            <?php 
                                foreach($direccion as $dir){
                                    echo '<b>'.$dir['Sucursale']['denominacion'].'</b>, Dirección: '.$dir['Sucursale']['direccion'].', Teléfono: '.$dir['Sucursale']['telefono'].', Email: '.$dir['Sucursale']['email'].' <br><br>';
                                }

                                pr($direccion);
                             ?>
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12 wow fadeInRight border" style=" animation-name: none;">
                            <h5 class="h5_back">Nuestros Servicios</h5>
                            <?php 
                                foreach ($servicio as $servicios) {
                                    echo   '<div class="accordion left767 ui-accordion ui-widget ui-helper-reset" role="tablist">
                                        <div class="accordion_header ui-accordion-header ui-helper-reset ui-state-default ui-corner-all ui-accordion-icons" role="tab" id="ui-accordion-1-header-0" aria-controls="ui-accordion-1-panel-0" aria-selected="false" tabindex="0"><span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e"></span>';

                                    echo "<h6>".$servicios['Servicio']['titulo']."</h6>"; 
                                    echo "</div>";

                                    echo '<div class="accordion_cnt ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom" style="display: none;" id="ui-accordion-1-panel-0" aria-labelledby="ui-accordion-1-header-0" role="tabpanel" aria-expanded="false" aria-hidden="true">';
                                    echo "<p>".$servicios['Servicio']['denominacion']."</p>"; 
                                    echo "</div>";                                   
                                    echo "</div>";                                   
                                }
                             ?>
                            <!-- <a class="btn btn-sm" href="#">Ver Todos Los Servicios</a> -->
                        </div>
                    </div>
                </div>
            </div>
            <a name="servicio"></a>
            <div class="bg-secondary2">
                <div class="well">
                    <div class="container">
                    <center><h3 style="align-content: center;"><b>SERVICIO</b> AL CLIENTE<br></h3></center>
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-xs-12 wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;">
                                <div class="thumbnail thumbnail4">
                                    <img width="100%" height="100%" src="<?= $this->Html->templateinclude('fact.jpg', 'diagnosticoflores');?>" alt="">
                                    <div class="caption">
                                        <h5>Facturación Electrónica</h5>
                                        <a onclick="muestraModal()" class="btn btn-xs" href="#"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12 wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                                <div class="thumbnail thumbnail4">
                                    <img width="100%" height="100%" src="<?= $this->Html->templateinclude('agenda_cita.jpg', 'diagnosticoflores');?>" alt="">
                                    <div class="caption">

                                        <h5>Agenda tu Cita<br><br></h5>
                                        
                                        <a class="btn btn-xs"   id="citas_a" href="<?=$this->Html->url('/Citas/citas#citas')?>"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                                <div class="thumbnail thumbnail4">
                                    <img width="100%" height="100%" src="<?= $this->Html->templateinclude('aviso_privacidad.png', 'diagnosticoflores');?>" alt="">
                                    <div class="caption">
                                        <h5>Aviso Privacidad<br><br></h5>
                                        <a class="btn btn-xs" href="<?= $this->webroot.''.$avisoprivas[0]['Avisopriva']['nombre_archivo']   ?>"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
                if(isset($imghorarios[0]["Imghorario"]["carpeta_imagen"])){
                   $img = $this->webroot.'img/'.$imghorarios[0]["Imghorario"]["ruta_imagen"];
                }else{

                }
            ?>
            <section class="parallax well2" data-url="<?= $img; ?>" data-mobile="true">
            <center><h3 style="align-content: center; margin-top: -50px"><b>HORARIOS DE TRABAJO</b></h3></center>
                <div class="row">
                
                 <?php 
                     foreach ($horarios as $horario) {
                     ?>
                         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
                            <div class="parallax_block_wrap">
                                <div class="parallax_block">
                                    <div class="parallax_block_cnt">
                                        <?php 
                                            echo $horario['Contenido']['contenido'];
                                         ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                     }
                     ?>
                </div>
            </section>
            <a name="contacto"></a>
            <style type="text/css">
                .mailform label textarea{
                    height: 252px !important;
                }
            </style>  
            <div class="well4">
                <div class="container">
                    <h5 class=" wow fadeInRight" style="animation-name: none;">Para Mayor Información Contáctenos!</h5>
                    <div class="row">
                        <div class="mailform_wrap">
                            <form class="mailform rd-mailform" method="post" action="<?=$this->Html->url('/Sites/add/')?>">
                                <input name="form-type" value="contact" type="hidden">
                                <fieldset>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <label class="textarea mfInput" data-add-placeholder="">
                                        <textarea name="data[Contacto][mensaje]" data-constraints="@NotEmpty"></textarea>
                                        <span class="mfValidation"></span><span class="mfPlaceHolder">Tú Mensaje</span></label>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <label data-add-placeholder="" class="mfInput">
                                            <input name="data[Contacto][asunto]" data-constraints="@LettersOnly @NotEmpty" type="text">
                                        <span class="mfValidation"></span><span class="mfPlaceHolder">Asunto</span></label>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12 off_mail">
                                        <label data-add-placeholder="" class="mfInput">
                                            <input name="data[Contacto][nombres]" data-constraints="@LettersOnly @NotEmpty" type="text">
                                        <span class="mfValidation"></span><span class="mfPlaceHolder">Tú Nombre</span></label>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12 off_mail">
                                        <label data-add-placeholder="" class="mfInput">
                                            <input name="data[Contacto][email]" data-constraints="@Email @NotEmpty" type="text">
                                        <span class="mfValidation"></span><span class="mfPlaceHolder">Tú Email</span></label>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="mfControls">
                                            <?php echo $this->Form->submit(__('Enviar', true), array('label'=>false, 'class'=>'btn btn-primary mfProgress'));?>
                                            <?php echo $this->Form->end(__('')); ?>                                             
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                         </div>
                    </div>
                </div>
            </div>



<!-- 
        <img style="padding-right: 20px; padding-left: 20px; height: 300px;width: 100%;" src="<?=$this->webroot.'img/mapa.jpg' ?>"> -->


        <div id="mapa_div" style="width:100%; height:370px"></div>


        <div class="row" style="padding-left: 20px; padding-bottom: 15px;">
        <?php 
            foreach ($direccion as $direcciones) {
         ?>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <?php 
                                echo $direcciones['Contenido']['contenido'];
                             ?>
                </div>
        <?php
         }
         ?>    
        </div>

        <hr>

        <style type="text/css">
            .input_sub{
                width: 75%;
                border-left-style: none;
                border-top-style: none;
                border-right-style: none;

            }
        </style>

        <div class="row " style="padding-left: 20px; padding-bottom: 15px;padding-top: 1%;">
            <h4>Suscribirse</h4>
            <br>

            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="text-align: center;padding-top: 2px;">
                <i style="font-size: 68px;" class="fa fa-envelope-o"></i>
            </div>
            <?php echo $this->Form->create('Suscribirse', array('url'=>'/sites/addSuscriptor', 'class'=>'')); ?>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top: 24px;" >
            <label>Nombres</label>
            <input type="text" name="data[Suscribirse][nombres]" class="input_sub">
            </div>

            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top: 24px;">
            <label>Correo</label>
            <input type="text" name="data[Suscribirse][email]" class="input_sub" >
            </div>

            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="padding-top: 24px;">
             <?php echo $this->Form->submit(__('Suscribirse', true), array('label'=>false, 'class'=>'btn btn-primary'));?>
            <?php echo $this->Form->end(__('')); ?>    
            </div>
        </div>



        </section>
    </main>
    <script type="text/javascript">
        function muestraModal(){
            $("#myModalContact").modal('show');
        }
        function muestraModalOdonto(){
            $("#myModalRegistrate").modal('show');
        }
         function iniciaSesion(){
            $("#myModalSesion").modal('show');
        }
    </script>
    <style type="text/css">
        .add-form-control{
            height: 47px !important;
        }
        .modal-dialog{
            width: 60% !important;
        }

        .modal-title{
            font-size: 25px;
        }
    </style>
    <!-- Modal -->
    <div id="myModalContact" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Solicitar Factura</h4>
          </div>
          <div class="modal-body">
                <div class="categorias form">
                <?php echo $this->Form->create('Facturacione',array('url'=>'/Sites/addFactura/')); ?>
                    <fieldset>
                     <legend><?php echo __('Datos'); ?></legend>
                    <div class='row'>
                        <div class='col-sm-6'>        <?php echo $this->Form->input('razon_social', array('label'=>'Razon Social','class'=>'add-form-control form-control')); ?> 
                        </div>
                        <div class='col-sm-6'>        <?php echo $this->Form->input('email', array('label'=>'Email','class'=>'add-form-control form-control')); ?> 
                        </div>
                    </div>   
                    <div class='row'>
                        

                        <div class='col-sm-6'>        <?php echo $this->Form->input('rfc', array('label'=>'R.F.C','class'=>'add-form-control form-control')); ?> 
                        </div>

                        <div class='col-sm-6'>        <?php echo $this->Form->input('folio', array('label'=>'N° de Folio','class'=>'add-form-control form-control')); ?> 
                        </div>
                    </div>   
                     <div class='row'>
                        <div class='col-sm-12'>        <?php echo $this->Form->input('direccion', array('label'=>'Direccion','class'=>'form-control','type'=>'textarea')); ?> 
                        </div>
                    </div>   
                 </fieldset>
                </div>
          </div>
          <div class="modal-footer">
            <?php echo $this->Form->submit(__('Enviar', true), array('label'=>false, 'class'=>'btn btn-primary'));?>
            <?php echo $this->Form->end(__('')); ?>
            
          </div>
        </div>
      </div>
    </div>
    <!-- REGISTRATE -->
    <!-- Modal -->
    <div id="myModalRegistrate" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Registrate</h4>
          </div>
          <div class="modal-body">
                <div class="categorias form">
                <?php echo $this->Form->create('User',array('url'=>'/Sites/addOdontologo/','onKeypress'=>'if(event.keyCode == 13) return false;')); ?>
                    <fieldset>
                     <legend><?php echo __('Datos'); ?></legend>
                    <div class='row'>
                        <div class='col-lg-6 col-md-6 col-sm-12'><?php echo $this->Form->input('nombre', array('label'=>'Nombres ','class'=>'add-form-control form-control')); ?> 
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <?php echo $this->Form->input('apellidos', array('label'=>'Apellidos','class'=>'add-form-control form-control')); ?> 
                        </div>
                    </div>   
                    <div class="row">
                    <div class='col-lg-6 col-md-6 col-sm-12'>
                           <?php echo $this->Form->input('telefono', array('label'=>'Teléfono','class'=>'add-form-control form-control')); ?> 
                        </div>
                         <div class='col-lg-6 col-md-6 col-sm-12'><?php echo $this->Form->input('email', array('label'=>'Email','class'=>'add-form-control form-control')); ?> 
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-lg-6 col-md-6 col-sm-12'>
                        <?php echo $this->Form->input('user', array('label'=>'Usuario','class'=>'add-form-control form-control')); ?> 
                        </div>
                    
                        <div class='col-lg-6 col-md-6 col-sm-12'><?php echo $this->Form->input('tipocliente_id', array('label'=>'Tipo especialidad dental','class'=>'add-form-control form-control')); ?> 
                        </div>
                    </div>  
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <?php echo $this->Form->input('password',array('label'=>'Password','class'=>'add-form-control form-control','type'=>'password')); ?>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <?php echo $this->Form->input('pass_2',array('label'=>'Repita Password','class'=>'add-form-control form-control','type'=>'password')); ?>
                        </div>
                    </div>   
                 </fieldset>
                </div>
          </div>
          <div class="modal-footer">
            <?php echo $this->Form->submit(__('Enviar', true), array('label'=>false, 'class'=>'btn btn-primary','onclick'=>'return verificaForm()') );?>
            <?php echo $this->Form->end(__('')); ?>
            
          </div>
        </div>
        </form>
      </div>
    </div>
    <script type="text/javascript">


        function verificaForm(){

            var pass1    = $("#UserPassword").val();
            var pass2    = $("#UserPass2").val();
            var NombresApellidos = $("#UserNombre").val();
            var Correo   = $("#UserEmail").val();
            var Telefono = $("#UserTelefono").val();
            var Usuario  = $("#UserUser").val();


            if (Correo == '') {
                alert('El campo Email no puede quedar vacío');
                $("#UserCorreo").focus();
                return false;

            }

            if (Usuario == '') {
                alert('El campo Usuario no puede quedar vacío');
                $("#UserUser").focus();
                return false;

            }





            if (NombresApellidos == '') {
                alert('El campo Nombres & Apellidos no puede quedar vacío!');
                $("#UserNombresApellidos").focus();                
                return false;
            }
            
            
            if (Telefono == '') {
                alert('El campo Teléfono no puede quedar vacío');
                $("#UserTelefono").focus();
                return false;
            }
            if (pass1 == '') {
                alert('El campo Password no puede quedar vacío');
                $("#UserPass1").focus();
                return false;   
            }
            if (pass2 == '') {
                alert('El campo Password no puede quedar vacío');
                $("#UserPass2").focus();
                return false;   
            }
            if (pass1 != pass2) {
                alert('Las contraseñas no coinciden.');
                return false;
            }



            
            $.ajax({
                url:"/Sites/verificausuario/"+Usuario+"/"+Correo,
                type:"post",
                data:{},
                success:function(response){
                    var data = JSON.parse(response);
                    console.log('user '+data.user+' correo '+data.correo);
                    if (data.resp == true) {
                        if (data.user == 'existe') {
                            alert('El usuario ya se encuentra registrado');
                            return false;
                        }

                        if (data.correo == 'existe') {
                            alert('El correo ya se encuentra registrado');
                            return false;
                        }
                    }
                }
            });

            

            

      


        }
    </script>

    <script type="text/javascript">
        
    </script>

    <style type="text/css">
        .modalSession{
            width:50%;
            left: 30%;
        }
    </style>
      <!-- Modal -->
    <div id="myModalSesion" class="modal fade modalSession" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h6 class="modal-title">Inicia Session</h6>
          </div>
          <div class="modal-body">
                   <?php echo $this->Form->create('Admin', array('url'=>'/sites/login', 'class'=>'login-form')); ?>
                        <input style="font-size: 11px;" type="text" class="add-form-control form-control" placeholder="Username" autofocus name="data[Admin][user]">
                        <br>
                        <input style="font-size: 11px;" type="password" class="add-form-control form-control" placeholder="Password" name="data[Admin][password]" >
                        <span class="pull-right">
                        <?php echo $this->Html->link('Olvidaste la contraseña?',  array('controller'=>'admin', 'action'=>'recuperar'), array()); ?>
                        </span>
          </div>
          <br>
          <div class="modal-footer">
            <?php echo $this->Form->submit(__('Entrar', true), array('label'=>false, 'class'=>'btn btn-primary'));?>
            <?php echo $this->Form->end(__('')); ?>
            
          </div></form>
        </div>

      </div>
    </div>
    <div id="myModalSesion" class="modal fade" role="dialog">
        <div class="modal-dialog">    
            <div class="modal-content">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h6 class="modal-title">Entrada</h6>
                </div>
                <div class="modal-body">
                </div>         
            </div>
        </div>
    </div>