<div class="panel">
<div class="panel panel-body">
<h3><?php echo __('Ver Imagenes Adjuntas'); ?></h3>

	<a href="/Sites/remitidos" ><- Volver a Remitidos</a>

	<br>
	<br>
	<?php 
	$imgCount = 1;
	foreach ($imgadjunto as $key) {
		echo "<a data-toggle='modal' data-target='#myModal".$imgCount."' class='btn btn-primary'>";
		echo "Ver Imagen ".$imgCount;
		echo "</a>";

		$img = $this->webroot.'img/'.$key["Imgadjunto"]["ruta_imagen"];

		echo '<div id="myModal'.$imgCount.'" class="modal fade" role="dialog">
  				<div class="modal-dialog">
				    <!-- Modal content-->
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal">&times;</button>
				        <h4 class="modal-title">Vista de Imagen</h4>
				      </div>
				      <div class="modal-body">
				       <img width="570"  src="'.$img.'"> 
				      </div>
				      <div class="modal-footer">
				      	<a class="btn btn-success" href="'.$img.'" download="Imagen_'.$img.'"> Descargar Imagen</a>
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				      </div>
				    </div>

				  </div>
				</div>';
	$imgCount++;			

	}

 ?>
</div>
</div>



 