<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Permisos </h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
					<li><i class="fa fa-arrow-left"></i>
					<a href="<?=$this->Html->url('/permisos/')?>"> Volver a Permisos </a> </li>              
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Permiso'); ?></h2>
	<table class="table table-striped">

	<tbody>
		<tr><td><?php echo __('Tipo permiso'); ?></td>
		<td>
			<?php echo h($permiso['Tipopermiso']['denominacion']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Personal'); ?></td>
		<td>
			<?php echo h( $permiso['Personale']['nombres'].' '.$permiso['Personale']['apellidos']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Desde'); ?></td>
		<td>
			<?php echo h($permiso['Permiso']['desde']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Hasta'); ?></td>
		<td>
			<?php echo h($permiso['Permiso']['hasta']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Motivo'); ?></td>
		<td>
			<?php echo h($permiso['Permiso']['motivo']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Descarcar'); ?></td>
		<td>
			<a href="<?= $this->webroot.''.$permiso['Permiso']['nombre_archivo']?>">Descarga de archivo</a>&nbsp;
		</td></tr>
	</tbody>
	</table>
</div>
</div>