<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Permisos </h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href=""> Home</a></li>	
					<li><i class="fa fa-arrow-left"></i>
					<a href="<?=$this->Html->url('/permisos/')?>"> Volver a Permisos </a> </li>              
		</ol>		
	</div>	    
</div>

<div class="row">
		     <div class="col-sm-12">
	  		   <section class="panel" style="padding: 10px;">

<div class="permisos form">
<?php echo $this->Form->create('Permiso', array('enctype'=>'multipart/form-data')); ?>
	<fieldset>
		<legend><?php echo __('Emitir Permiso'); ?></legend>
<div class='row'><div class='col-sm-12'>		<?php echo $this->Form->input('tipopermiso_id', array('label'=>'Tipo Permiso','class'=>'form-control')); ?> 
 </div></div><div class='row'><div class='col-sm-12'>		<?php echo $this->Form->input('personale_id', array('label'=>'Personal','class'=>'form-control')); ?> 
 </div></div><div class='row'><div class='col-sm-12'>		<?php echo $this->Form->input('desde', array('label'=>'Desde','class'=>'form-control','id'=>'PermisoDesde')); ?> 
 </div></div><div class='row'><div class='col-sm-12'>		<?php echo $this->Form->input('hasta', array('label'=>'Hasta','class'=>'form-control','id'=>'PermisoHasta')); ?> 
 </div></div>
 <div class='row'>
	 <div class='col-sm-12'>		
	 	<?php echo $this->Form->input('motivo', array('label'=>'Motivo','class'=>'form-control')); ?> 
	 </div>
 </div>	
 <div class='row'>
	 <div class='col-sm-12'>		
	 	<?php echo $this->Form->input('contenido', array('label'=>'Archivo', 'class'=>'form-control', 'type'=>"file"));?>	
	 	<?php echo'<p for="" class="help-block text-red">* El archivo debe estar en formato .pdf</p>';?>	
	 </div>
 </div>


 </fieldset>
 	<br>
	<div class="pull-right">		
		<?php echo $this->Form->submit(__('Submit', true), array('label'=>false, 'class'=>'btn btn-primary'));?>
		<?php echo $this->Form->end(__('')); ?>
	</div>

</div>

</section>
</div>
</div>
<script type="text/javascript">
	
</script>