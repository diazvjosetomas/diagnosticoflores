<div class="container">


<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Permisos</h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
                    <li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Permisos'), array('action' => 'index')); ?></li>
		</ol>		
	</div>	    
</div>







<div class="panel">
	<div class="panel-heading panel-success">
		<h4>&nbsp;
			<span class="pull-right">
				<div class="btn-group">
				  <button type="button" class="btn btn-primary">Accion</button>
				  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
				    <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu" role="menu">
				    <li><?php echo $this->Html->link(__('Nuevo Permiso'), array('action' => 'add')); ?></li>
				    
				  </ul>
				</div>


			
			</span>
		</h4>
		
	</div>

	<div class="panel-body">








<div class="permisos index">
	
	<table id="datos" cellpadding="0" cellspacing="0" class="table table-striped table-rounded">
	<thead>
	<tr>
			
			<th><?php echo h('Tipo Permiso'); ?></th>
			<th><?php echo h('Personal'); ?></th>
			<th><?php echo h('Desde'); ?></th>
			<th><?php echo h('Hasta'); ?></th>
			<th><?php echo h('Motivo'); ?></th>
			
			<th class="actions"><?php echo __('Acción'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($permisos as $permiso): ?>
	<tr>
		
		<td>
			<?php echo $this->Html->link($permiso['Tipopermiso']['denominacion'], array('controller' => 'tipopermisos', 'action' => 'view', $permiso['Tipopermiso']['id'])); ?>
		</td>
		<td>
			<?php echo $permiso['Personale']['nombres'].' '.$permiso['Personale']['apellidos']; ?>
		</td>
		<td><?php echo h($permiso['Permiso']['desde']); ?>&nbsp;</td>
		<td><?php echo h($permiso['Permiso']['hasta']); ?>&nbsp;</td>
		<td><?php echo h($permiso['Permiso']['motivo']); ?>&nbsp;</td>
		
		<td class="actions">
			<?php echo $this->Html->link(__('Detalles'), array('action' => 'view', $permiso['Permiso']['id']), array('class'=>'btn btn-sm btn-default')); ?>
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $permiso['Permiso']['id']), array('class'=>'btn btn-sm btn-success')); ?>
			<?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $permiso['Permiso']['id']), array('class'=>'btn btn-sm btn-danger'), __('¿Esta seguro que quiere eliminar este permiso?', $permiso['Permiso']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	
	</div>
</div>
</div>
</div>

<script type="text/javascript">
    //$(document).ready(function() {
        $('#datos').DataTable( {
            dom: 'Bfrtlip',          
            responsive: true,
	        buttons: [
	            {
	                extend: 'excel',
	                exportOptions: {
	                    columns: [0,1,2,3,4]
	                }
	            },
	            {
	                extend: 'pdf',
	                exportOptions: {
	                    columns: [0,1,2,3,4]
	                }
	            },
	            {
	                extend: 'copy',
	                exportOptions: {
	                    columns: [0,1,2,3,4]
	                }
	            },
	            {
	                extend: 'csv',
	                exportOptions: {
	                    columns: [0,1,2,3,4]
	                }
	            },
	            {
	                extend: 'print',
	                exportOptions: {
	                    columns: [0,1,2,3,4]
	                }
	            }



	        ],
            "language": 
            {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ".",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        } );
    //} );
</script>