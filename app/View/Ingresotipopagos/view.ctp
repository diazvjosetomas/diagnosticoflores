<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Tipo de Pago</h3>
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
				<li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Tipo de Pago'), array('action' => 'index')); ?></li>             
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Tipo de Pago'); ?></h2>
	<table class="table table-striped">
	<tbody>
		
		<tr><td><?php echo __('Denominación'); ?></td>
		<td>
			<?php echo h($ingresotipopago['Ingresotipopago']['denominacion']); ?>
			&nbsp;
		</td></tr>

	
	</tbody>
	</table>
</div>
</div>