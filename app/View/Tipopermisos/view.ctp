<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Tipos de Permisos </h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
					<li><i class="fa fa-arrow-left"></i>
					<a href="<?=$this->Html->url('/tipopermisos/')?>"> Volver a Tipo de Permiso </a> </li>              
		</ol>		
	</div>	    
</div>


<div class="panel">
	<div class="panel-heading">
	<h2><?php echo __('Tipos de Permiso'); ?></h2>		
<div class="panel panel-body">
	<table class="table table-striped">
	<tbody>
		<tr><td><?php echo __('Denominación'); ?></td>
		<td>
			<?php echo h($tipopermiso['Tipopermiso']['denominacion']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Descripción'); ?></td>
		<td>
			<?php echo h($tipopermiso['Tipopermiso']['descripcion']); ?>
			&nbsp;
		</td></tr>
	
	</tbody>
	</table>
</div>

</div>