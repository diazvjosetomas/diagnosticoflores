<div class="container">
<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Cierre Días</h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
					<li><i class="fa fa-arrow-left"></i>
					<a href="<?php echo $this->Html->url('/Cajacierredias/'); ?>"> Volver a Cierre Días </a> </li>              
		</ol>		
	</div>	    
</div>

<div class="panel">
	<div class="panel-heading panel-success">
		<h4>&nbsp;
			<span class="pull-right">
				<div class="btn-group">
				  <button type="button" class="btn btn-primary">Acción</button>
				  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
				    <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu" role="menu">
				    <li><?php echo $this->Html->link(__('Nuevo Cierre'), array('action' => 'add')); ?></li>
				  </ul>
				</div>
			</span>
		</h4>
	</div>
	<div class="panel-body">
<div class="cajacierredias index">
	<table id="Cajacierredia" cellpadding="0" cellspacing="0" class="table table-striped table-rounded">
	<thead>
	<tr>
	        <th><?php echo h('Sucursal'); ?></th>
			<th><?php echo h('Caja'); ?></th>
			<th><?php echo h('Usuario'); ?></th>
			<th><?php echo h('Monto'); ?></th>
			<th><?php echo h('Fecha'); ?></th>
			<th>Cierre</th>
			<th class="actions"><?php echo __('Acción'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($cajacierredias as $cajacierredia): ?>
	<tr>
	    <td>
			<?php echo h($cajacierredia['Sucursale']['denominacion']); ?>
		</td>
		<td>
			<?php echo h($cajacierredia['Caja']['denominacion']); ?>
		</td>
		<td>
			<?php echo h($cajacierredia['User']['user']); ?>
		</td>
		<td><?php echo h($cajacierredia['Cajacierredia']['monto']); ?>&nbsp;</td>
		<td><?php echo h($cajacierredia['Cajacierredia']['fecha']); ?>&nbsp;</td>
		<td style="text-align: center;">
			<?php 

				$fecha = explode(' ', $cajacierredia['Cajacierredia']['created']);
				$fecha = $fecha[0];

			 ?>
			<a target="_blank" href="/Cajacierredias/planilla/<?=$cajacierredia['Sucursale']['id'].'/'.$cajacierredia['Caja']['id'].'/'.$fecha?>" class="btn btn-sm btn-success">
				Planilla <?=$cajacierredia['Cajacierredia']['created'];?>
			</a>
		</td>
		<td class="actions">
			<!-- <?php echo $this->Html->link(__('Detalles'), array('action' => 'view', $cajacierredia['Cajacierredia']['id']), array('class'=>'btn btn-sm btn-default')); ?> -->
			
			<?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $cajacierredia['Cajacierredia']['id']), array('class'=>'btn btn-sm btn-danger'), __('Are you sure you want to delete # %s?', $cajacierredia['Cajacierredia']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
</div>
</div>
</div>
<script type="text/javascript">
    //$(document).ready(function() {
        $('#Cajacierredia').DataTable( {
            dom: 'Bfrtlip',          
            responsive: true,
	        buttons: [
	            {
	                extend: 'excel',
	                exportOptions: {
	                    columns: [0,1,2,3,4,5]
	                }
	            },
	            {
	                extend: 'pdf',
	                exportOptions: {
	                    columns: [0,1,2,3,4,5]
	                }
	            },
	            {
	                extend: 'copy',
	                exportOptions: {
	                    columns: [0,1,2,3,4,5]
	                }
	            },
	            {
	                extend: 'csv',
	                exportOptions: {
	                    columns: [0,1,2,3,4,5]
	                }
	            },
	            {
	                extend: 'print',
	                exportOptions: {
	                    columns: [0,1,2,3,4,5]
	                }
	            }



	        ],
            "language": 
            {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ".",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        } );
    //} );


    function Planilla(data){
    	window.open('/Cajacierredias/planilla/'+data,'_blank');
    }
</script>