<?php
class fpdfview extends FPDF{
	function Footer(){}
	function Header(){

		
	}
}
$fpdf = new fpdfview('P','mm','A4');
$fpdf->AddPage();

$fpdf->Image('img/upload/logo.png',10,10,75,25);

$fpdf->Ln(25);

$fpdf->Ln(5);
$fpdf->SetFont('Arial','B',22);
$fpdf->Cell(0,5,'Quantum','',1,'C');

$fpdf->Ln(2);
$fpdf->SetFont('Arial','',8);
$fpdf->Cell(0,5,"Quantum / ".$sucursal[0]['Sucursale']['denominacion'].' / '.$titulo,'',1,'C');

$fpdf->Ln(3);
$fpdf->SetFont('Arial','',6);
$fpdf->Cell(25,5,"N FOLIO",'LTB',0,'C');
$fpdf->Cell(35,5,"PERSONAL",'TB',0,'C');
$fpdf->Cell(25,5,"CLIENTE",'TB',0,'C');
$fpdf->Cell(45,5,"IVA",'TB',0,'C');
$fpdf->Cell(25,5,"SUBTOTAL",'TB',0,'C');
$fpdf->Cell(0,5,"TOTAL",'TBR',1,'C');

$total_iva = 0;
$total_subtotal = 0;
$total_total = 0;


foreach ($data as $value) {
	
	$fpdf->Cell(20,5,$value['Ingreso']['folio'],'',0,'R');
	$fpdf->Cell(45,5,ucwords(strtolower($value['Personale']['nombres'].' '.$value['Personale']['apellidos'])),'',0,'C');
	$fpdf->Cell(25,5,ucwords(strtolower($value['Cliente']['nombres'].' '.$value['Cliente']['apellidos'])),'',0,'C');
	
	$fpdf->Cell(35,5,number_format($value['Ingreso']['monto_iva'],'2',',','.'),'',0,'C');
	$fpdf->Cell(35,5,number_format($value['Ingreso']['subtotal'],'2',',','.'),'',0,'C');
	$fpdf->Cell(25,5,number_format($value['Ingreso']['total'],'2',',','.'),'',0,'C');
	$fpdf->Ln(3);
	$total_iva += $value['Ingreso']['monto_iva'];
	$total_subtotal += $value['Ingreso']['subtotal'];
	$total_total += $value['Ingreso']['total'];
}

$fpdf->Ln(15);
$fpdf->SetFont('','',6);
$fpdf->Cell(25,5,"MONTOS TOTALES: ",'LTB',0,'C');
$fpdf->Cell(35,5,"::::::::::::::::::::::::::::::::::::::::::",'TB',0,'C');
$fpdf->Cell(25,5,"::::::::::::::::::::::::::::::::::::::::::",'TB',0,'C');
$fpdf->Cell(45,5,number_format($total_iva,'2',',','.'),'TB',0,'C');
$fpdf->Cell(25,5,number_format($total_subtotal,'2',',','.'),'TB',0,'C');
$fpdf->Cell(0,5,number_format($total_total,'2',',','.'),'TBR',1,'C');

 $fpdf->Output('F',$info);


?>