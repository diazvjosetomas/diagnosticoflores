<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Caja cierre dias</h3>
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
				<li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Caja cierre dias'), array('action' => 'index')); ?></li>             
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Caja cierre dias'); ?></h2>
	<table class="table table-striped">
	<tbody>
	    <tr><td><?php echo __('Sucursal'); ?></td>
		<td>
			<?php echo h($cajacierredia['Sucursale']['denominacion']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Caja'); ?></td>
		<td>
			<?php echo h($cajacierredia['Caja']['denominacion']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('User'); ?></td>
		<td>
			<?php echo h($cajacierredia['User']['user']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Monto'); ?></td>
		<td>
			<?php echo h($cajacierredia['Cajacierredia']['monto']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Fecha'); ?></td>
		<td>
			<?php echo h($cajacierredia['Cajacierredia']['created']); ?>
			&nbsp;
		</td></tr>
	
	</tbody>
	</table>
</div>
</div>