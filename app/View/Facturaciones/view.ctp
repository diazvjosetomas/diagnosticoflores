<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Facturación</h3>
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
				<li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Facturación'), array('action' => 'index')); ?></li>             
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Facturación'); ?></h2>
	<table class="table table-striped">
	<tbody>
		<tr><td><?php echo __('Razon Social'); ?></td>
		<td>
			<?php echo h($facturacione['Facturacione']['razon_social']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Email'); ?></td>
		<td>
			<?php echo h($facturacione['Facturacione']['email']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Rfc'); ?></td>
		<td>
			<?php echo h($facturacione['Facturacione']['rfc']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Dirección'); ?></td>
		<td>
			<?php echo h($facturacione['Facturacione']['direccion']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Folio'); ?></td>
		<td>
			<?php echo h($facturacione['Facturacione']['folio']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Status'); ?></td>
		<td>
			<?php 
                  if($facturacione['Facturacione']['status']==1){
                   echo "En espera";
            }else if($facturacione['Facturacione']['status']==2){
                    echo "Procesado";

            }
			 ?>
			&nbsp;
		</td></tr>
	</tbody>
	</table>
</div>
</div>