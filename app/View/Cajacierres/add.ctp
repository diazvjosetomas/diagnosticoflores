
<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Cierre Turno</h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
                    <li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Cierre de Turno'), array('action' => 'index')); ?></li>
		</ol>		
	</div>	    
</div>


<div class="row">
		     <div class="col-sm-12">
	  		   <section class="panel" style="padding: 10px;">

<div class="cajacierres form">
<?php echo $this->Form->create('Cajacierre'); ?>
	<fieldset>
		<legend><?php echo __('Add Caja cierres turno'); ?></legend>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('sucursale_id', array('label'=>'Sucursal','class'=>'form-control', 'empty'=>'--Seleccione--', "onChange"=>"selectTagRemote('".$this->Html->url('/Cajacierres/cajas')."', 'div-caja', this.value);")); ?>
 </div>
</div>
<div class='row'>
<div class='col-sm-12' id='div-caja'>
		<?php echo $this->Form->input('caja_id', array('label'=>'Caja','class'=>'form-control', 'options'=>array())); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('user_id', array('label'=>'Usuario','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('monto', array('label'=>'Monto','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('fecha', array('readonly'=>true, 'label'=>'Fecha y Hora','class'=>'form-control')); ?> 
 </div>
</div>
	</fieldset>
	<br />
	<div class="pull-right">		
		<?php echo $this->Form->submit(__('Guardar', true), array('label'=>false, 'class'=>'btn btn-primary'));?>
		<?php echo $this->Form->end(__('')); ?>
	</div>

</div>

</section>
</div>
</div>

<script type="text/javascript">
	function hora(){
	var fecha = new Date()
	var dia = fecha.getDate()
	var mes = fecha.getMonth() + 1
	var anho = fecha.getFullYear()
	var hora = fecha.getHours()
	var minuto = fecha.getMinutes()
	var segundo = fecha.getSeconds()

	if (hora >= 12) {
		var tipohora = 'PM';
	}else{
		var tipohora = 'AM';
	}

	if (hora < 10) {hora = "0" + hora}
	if (minuto < 10) {minuto = "0" + minuto}
	if (segundo < 10) {segundo = "0" + segundo}
	var horita = hora + ":" + minuto + ":" + segundo+ " " +tipohora+ " " +dia+" / "+mes+" / "+anho;
	$("#CajacierreFecha").val(horita);
	
	
	}

	hora();
</script>