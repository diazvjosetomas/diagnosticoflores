<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Tipocitas</h3>
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
				<li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Tipocitas'), array('action' => 'index')); ?></li>             
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Tipocita'); ?></h2>
	<table class="table table-striped">
	<tbody>
		<tr><td><?php echo __('Id'); ?></td>
		<td>
			<?php echo h($tipocita['Tipocita']['id']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Denominacion'); ?></td>
		<td>
			<?php echo h($tipocita['Tipocita']['denominacion']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Tiempo'); ?></td>
		<td>
			<?php echo h($tipocita['Tipocita']['tiempo']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Costo'); ?></td>
		<td>
			<?php echo h($tipocita['Tipocita']['costo']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Detalles'); ?></td>
		<td>
			<?php echo h($tipocita['Tipocita']['detalles']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Categoriacita'); ?></td>
		<td>
			<?php echo $this->Html->link($tipocita['Categoriacita']['denominacion'], array('controller' => 'categoriacitas', 'action' => 'view', $tipocita['Categoriacita']['id'])); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Carpeta Imagen'); ?></td>
		<td>
			<?php echo h($tipocita['Tipocita']['carpeta_imagen']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Nombre Imagen'); ?></td>
		<td>
			<?php echo h($tipocita['Tipocita']['nombre_imagen']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Tipo Imagen'); ?></td>
		<td>
			<?php echo h($tipocita['Tipocita']['tipo_imagen']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Ruta Imagen'); ?></td>
		<td>
			<?php echo h($tipocita['Tipocita']['ruta_imagen']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Created'); ?></td>
		<td>
			<?php echo h($tipocita['Tipocita']['created']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Modified'); ?></td>
		<td>
			<?php echo h($tipocita['Tipocita']['modified']); ?>
			&nbsp;
		</td></tr>
	
	</tbody>
	</table>
</div>
</div>