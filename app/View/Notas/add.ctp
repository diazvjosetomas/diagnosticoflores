
<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Notas</h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
                    <li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Notas'), array('action' => 'index')); ?></li>
		</ol>		
	</div>	    
</div>


<div class="row">
		     <div class="col-sm-12">
	  		   <section class="panel" style="padding: 10px;">

<div class="notas form">
<?php echo $this->Form->create('Nota'); ?>
	<fieldset>
		<legend><?php echo __('Agregar Nota'); ?></legend>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('denominacion', array('label'=>'Tarea','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('descripcion', array('label'=>'Descripción','class'=>'form-control','type'=>'textarea')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('statu_id', array('label'=>'Status','class'=>'form-control')); ?> 
 </div>
</div>
<div class="row">
	<div class="col-md-12">
		<?php echo $this->Form->input('fecha_creado', array('label'=>'Fecha Creación', 'id'=>'fecha_creado', 'class'=>'form-control')); ?>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<?php echo $this->Form->input('fecha_limite', array('label'=>'Fecha Límite', 'id'=>'fecha_limite', 'class'=>'form-control')); ?>
	</div>
</div>
	</fieldset>
	<br />
	<div class="pull-right">		
		<?php echo $this->Form->submit(__('Guardar', true), array('label'=>false, 'class'=>'btn btn-primary'));?>
		<?php echo $this->Form->end(__('')); ?>
	</div>

</div>

</section>
</div>
</div>

<script type="text/javascript">
	$("#fecha_creado").datepicker();
	$("#fecha_limite").datepicker();
</script>
