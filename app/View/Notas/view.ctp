<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Notas</h3>
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
				<li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Notas'), array('action' => 'index')); ?></li>             
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Nota'); ?></h2>
	<table class="table table-striped">
	<tbody>
		
		<tr><td><?php echo __('Denominacion'); ?></td>
		<td>
			<?php echo h($nota['Nota']['denominacion']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Descripcion'); ?></td>
		<td>
			<?php echo h($nota['Nota']['descripcion']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Estatus'); ?></td>
		<td>
			<?php echo $nota['Status']['denominacion']; ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Creada'); ?></td>
		<td>
			<?php echo h($nota['Nota']['fecha_creado']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Fecha Limite'); ?></td>
		<td>
			<?php echo h($nota['Nota']['fecha_limite']); ?>
			&nbsp;
		</td></tr>
	
	</tbody>
	</table>
</div>
</div>

