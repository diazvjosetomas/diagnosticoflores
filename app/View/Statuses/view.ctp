<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Statuses</h3>
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
				<li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__(' Volver'), array('action' => 'index')); ?></li>             
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Status'); ?></h2>
	<table class="table table-striped">
	<tbody>
		<tr><td><?php echo __('Id'); ?></td>
		<td>
			<?php echo h($status['Status']['id']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Denominacion'); ?></td>
		<td>
			<?php echo h($status['Status']['denominacion']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Creado'); ?></td>
		<td>
			<?php echo h($status['Status']['created']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Modificado'); ?></td>
		<td>
			<?php echo h($status['Status']['modified']); ?>
			&nbsp;
		</td></tr>
	
	</tbody>
	</table>
</div>
</div>