
<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Citas</h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
                    <li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Citas'), array('action' => 'index')); ?></li>
		</ol>		
	</div>	    
</div>


<div class="row">
		     <div class="col-sm-12">
	  		   <section class="panel" style="padding: 10px;">

<div class="citas form">
<?php echo $this->Form->create('Cita'); ?>
	<fieldset>
		<legend><?php echo __('Agregar Cita'); ?></legend>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('nombres', array('label'=>'Paciente','class'=>'form-control')); ?> 
 </div>
</div>
	

<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('correo', array('label'=>'Correo','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('telefono', array('label'=>'Telefono','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('fecha', array('label'=>'Fecha','class'=>'form-control','id'=>'CitaFecha')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php

		$horas = array('1'=>'09:00 AM',
					   '2'=>'09:30 AM',
					   '3'=>'10:00 AM',
					   '4'=>'10:30 AM',
					   '5'=>'11:00 AM',
					   '6'=>'11:30 AM',
					   '7'=>'12:00 PM',
					   '8'=>'12:30 PM',
					   '9'=>'01:00 PM',
					   '10'=>'01:30 PM',
					   '11'=>'02:00 PM',
					   '12'=>'02:30 PM',
					   '13'=>'03:00 PM',
					   '14'=>'03:30 PM',
					   '15'=>'04:00 PM',
					   '16'=>'04:30 PM',
					   '17'=>'05:00 PM',
					   '18'=>'05:30 PM',
					   '19'=>'06:00 PM',
					   '20'=>'06:30 PM');





		 echo $this->Form->input('hora', array('label'=>'Hora','class'=>'form-control','id'=>'CitaHora', 'options'=>$horas)); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('tipocita_id', array('label'=>'Servicios','class'=>'form-control')); ?> 
 </div>
</div>

<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('sucursale_id', array('label'=>'Sucursal','class'=>'form-control')); ?> 
 </div>
</div>
<div class="row">
	<div class="col-md-12">
		<?
		 $estatus = array("0"=>"Iniciada",
		 				  "1"=>"Asistió",
		 				  "2"=>"No Asistió",
		 				  "3"=>"Cancelada");
		 echo $this->Form->input('estatus', array('label'=>'Estatus','class'=>'form-control','id'=>'CitaEstatus', 'options'=>$estatus)); ?>
	</div>
</div>
	</fieldset>
	<br />
	<div class="pull-right">		
		<?php echo $this->Form->submit(__('Guardar', true), array('label'=>false, 'class'=>'btn btn-primary'));?>
		<?php echo $this->Form->end(__('')); ?>
	</div>

</div>

</section>
</div>
</div>
