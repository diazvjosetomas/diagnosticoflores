
<h5 class="h5_back" style="text-align: center;">
	Seleccione un tipo de cita
</h5>
<?php 
echo "<div class='row' style='padding:6px;'>";
foreach ($categoriacitas as $categoriacita) {
	echo "<div class='col-md-6'><div class='button' onclick='tipocita(".$categoriacita['Categoriacita']['id'].")' style='text-align:center;'>".$categoriacita['Categoriacita']['denominacion']."</div></div>";
}
echo "</div>";

 ?>

<br>

<div class="container" id='tipocita'>




	<div id="tipocita_id_1">
	    <div class="row">

		<?php 

			foreach ($tipocitas as $tipocita) {

				if ($tipocita['Tipocita']['categoriacita_id'] == 1) {
					
					echo '<div class="col-sm-3 col-md-3" >';
						echo '   <div class="well" style="height:390px;">';
						echo '<h5 class="text-muted" id="idTipocitaDenominacion">'.ucwords(strtolower($tipocita['Tipocita']['denominacion'])).'</h5>';

						echo "<p style='height:30%;'>".$tipocita['Tipocita']['detalles']."</p>";
						echo "<br><br><hr >";
						echo "<h6> ".$tipocita['Tipocita']['tiempo']." / ".$tipocita['Tipocita']['costo']." </h6>";
						echo "<hr>";
						echo '<p ><a onclick="selectTipoCita('.$tipocita['Tipocita']['id'].')" class="button" href="#"><i class="icon-ok"></i> Seleccionar</a></p>';
						echo '</div></div>';

				}
			}

	 	?>			
		</div>
	</div>	

	<div id="tipocita_id_2" style="display: none;">
		    <div class="row" >

			<?php 

				foreach ($tipocitas as $tipocita) {

					if ($tipocita['Tipocita']['categoriacita_id'] == 2) {
						
						echo '<div class="col-sm-3 col-md-3" >';
						echo '   <div class="well" style="height:390px;">';
						echo '<h5 class="text-muted">'.ucwords(strtolower($tipocita['Tipocita']['denominacion'])).'</h5>';

						echo "<p style='height:30%;'>".$tipocita['Tipocita']['detalles']."</p>";
						echo "<br>	<br> <hr >";
						echo "<h6> ".$tipocita['Tipocita']['tiempo']." / ".$tipocita['Tipocita']['costo']." </h6>";
						echo "<hr>";
						echo '<p ><a onclick="selectTipoCita('.$tipocita['Tipocita']['id'].')" class="button" href="#"><i class="icon-ok"></i> Seleccionar</a></p>';
						echo '</div></div>';

					}
				}

		 	?>			
			</div>
	</div>


	<div class="row" style="margin-top: 90px;">
	<div class="col-md-2">
		<!-- <a href="" class='btn btn-primary' > -->
		<!-- <i class='fa fa-arrow-left'></i>
			 Volver al inicio
		</a> -->
		
	</div>
	<div class="col-md-2"></div>
	<div class="col-md-2"></div>
	<div class="col-md-2"></div>
	<div class="col-md-2"></div>
		
	</div>
</div>
<script type="text/javascript">
	function tipocita(id){
		if (id == 1) {
			$("#tipocita_id_1").show();
			$("#tipocita_id_2").hide();
		}else{
			$("#tipocita_id_2").show();
			$("#tipocita_id_1").hide();
		}
	}

	function volver_1(){
		$("#tipocitas_div").hide();
		$("#sucursales").show();
	}

	function volver_2(){
		var id =   $('.zabuto_calendar').attr('id');
		console.log(id);
		$("#"+id).hide();
		$("#tipocitas_div").show();
		$("#detallesCita").hide();
		$("#idVolver_2").hide();
		$("#horarios").hide();
	}
</script>