
<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Citas</h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
                    <li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Citas'), array('action' => 'index')); ?></li>
		</ol>		
	</div>	    
</div>


<div class="row">
		     <div class="col-sm-12">
	  		   <section class="panel" style="padding: 10px;">

<div class="citas form">
<?php echo $this->Form->create('Cita'); ?>
	<fieldset>
		<legend><?php echo __('Agregar Cita'); ?></legend>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('nombres', array('label'=>'Nombres','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('apellidos', array('label'=>'Apellidos','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('correo', array('label'=>'Correo','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('fecha', array('label'=>'Fecha','class'=>'form-control','id'=>'CitaFecha')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('hora', array('label'=>'Hora','class'=>'form-control','id'=>'CitaHora')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('tipocita_id', array('label'=>'Tipo Cita','class'=>'form-control')); ?> 
 </div>
</div>

<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('sucursale_id', array('label'=>'Sucursal','class'=>'form-control')); ?> 
 </div>
</div>
	</fieldset>
	<br />
	<div class="pull-right">		
		<?php echo $this->Form->submit(__('Guardar', true), array('label'=>false, 'class'=>'btn btn-primary'));?>
		<?php echo $this->Form->end(__('')); ?>
	</div>

</div>

</section>
</div>
</div>
