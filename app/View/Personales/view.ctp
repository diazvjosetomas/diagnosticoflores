<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Personal </h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
					<li><i class="fa fa-arrow-left"></i>
					<a href="<?=$this->Html->url('/personales/')?>"> Volver a Personal </a> </li>              
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Personale'); ?></h2>
	<table class="table table-striped">
	<tbody>
		<tr><td><?php echo __('Rfc'); ?></td>
		<td>
			<?php echo h($personale['Personale']['rfc']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Nombres'); ?></td>
		<td>
			<?php echo h($personale['Personale']['nombres']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Apellidos'); ?></td>
		<td>
			<?php echo h($personale['Personale']['apellidos']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Telefono'); ?></td>
		<td>
			<?php echo h($personale['Personale']['telefono']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Email'); ?></td>
		<td>
			<?php echo h($personale['Personale']['email']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Direccion'); ?></td>
		<td>
			<?php echo h($personale['Personale']['direccion']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Fecha Ingreso'); ?></td>
		<td>
			<?php echo h($personale['Personale']['fecha_ingreso']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Contrato'); ?></td>
		<td>
			<a href="<?= $this->webroot.''.$personale['Personale']['nombre_archivo']?>">Descarga de archivo</a>&nbsp;
		</td></tr>
	</tbody>
	</table>
</div>
</div>