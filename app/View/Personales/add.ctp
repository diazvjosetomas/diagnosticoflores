<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Personal </h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href=""> Home</a></li>	
					<li><i class="fa fa-arrow-left"></i>
					<a href="<?=$this->Html->url('/personales/')?>"> Volver a Personal </a> </li>              
		</ol>		
	</div>	    
</div>

<div class="row">
		     <div class="col-sm-12">
	  		   <section class="panel" style="padding: 10px;">

<div class="personales form">
<?php echo $this->Form->create('Personale', array('enctype'=>'multipart/form-data')); ?>
	<fieldset>
		<legend><?php echo __('Agregar Personal'); ?></legend>
<div class='row'>
	<div class='col-sm-12'><?php echo $this->Form->input('rfc', array('label'=>'RFC','class'=>'form-control')); ?> 
    </div>
 </div>
 <div class='row'>
	<div class='col-sm-12'><?php echo $this->Form->input('sucursale_id', array('label'=>'Sucursal','class'=>'form-control')); ?> 
    </div>
 </div>
 <div class='row'><div class='col-sm-12'>		<?php echo $this->Form->input('nombres', array('label'=>'Nombres','class'=>'form-control')); ?> 
 </div></div><div class='row'><div class='col-sm-12'>		<?php echo $this->Form->input('apellidos', array('label'=>'Apellidos','class'=>'form-control')); ?> 
 </div></div><div class='row'><div class='col-sm-12'>		<?php echo $this->Form->input('telefono', array('label'=>'Telefono','class'=>'form-control')); ?> 
 </div></div><div class='row'><div class='col-sm-12'>		<?php echo $this->Form->input('email', array('label'=>'Email','class'=>'form-control')); ?> 
 </div></div><div class='row'><div class='col-sm-12'>		<?php echo $this->Form->input('direccion', array('label'=>'Dirección','class'=>'form-control')); ?> 
 </div></div>
 <div class='row'>
	 <div class='col-sm-12'>		
	 	<?php echo $this->Form->input('fecha_ingreso', array('label'=>'Fecha de Ingreso','class'=>'form-control','id'=>'PersonaleFechaIngreso')); ?> 
	 </div>
 </div>	
  <div class='row'>
	 <div class='col-sm-12'>		
	 	<?php echo $this->Form->input('contenido', array('label'=>'Archivo', 'class'=>'form-control', 'type'=>"file"));?>	
	 	<?php echo'<p for="" class="help-block text-red">* El archivo debe estar en formato .pdf</p>';?>	
	 </div>
 </div>
 </fieldset>
 	<br>
	<div class="pull-right">		
		<?php echo $this->Form->submit(__('Submit', true), array('label'=>false, 'class'=>'btn btn-primary'));?>
		<?php echo $this->Form->end(__('')); ?>
	</div>

</div>

</section>
</div>
</div>
