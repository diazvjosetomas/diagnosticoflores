

<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Video Tutoriales</h3>
		
			
	</div>	    
</div>


<div class="row">
     <div class="col-sm-12">
	  		   <section class="panel" style="padding: 10px;">

	  		   <h3>
	  		   <a href="#" data-toggle="modal" data-target="#myModalRoles">
	  		   	
	  		   <i class="fa fa-caret-right" aria-hidden="true"></i>
				Como crear roles y asignarlos a los usuarios</h3>
	  		   </a>
	  		 

			</section>
	</div>

</div>

<div class="row">
     <div class="col-sm-12">
	  		   <section class="panel" style="padding: 10px;">

	  		   <h3>
	  		   <a href="#" data-toggle="modal" data-target="#myModalNewsletter">
	  		   	
	  		   <i class="fa fa-caret-right" aria-hidden="true"></i>
				Como configurar los correos promocionales</h3>
	  		   </a>
	  		 

			</section>
	</div>

</div>


<div class="row">
     <div class="col-sm-12">
	  		   <section class="panel" style="padding: 10px;">

	  		   <h3>
	  		   <a href="#" data-toggle="modal" data-target="#myModalAdjunto">
	  		   	
	  		   <i class="fa fa-caret-right" aria-hidden="true"></i>
				Adjuntar imagenes para los doctores
</h3>
	  		   </a>
	  		 

			</section>
	</div>

</div>

</div>



<div id="myModalRoles" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Como crear roles y asignarlos a los usuarios</h4>
      </div>
      <div class="modal-body">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/_MZ_h2SxD-I" frameborder="0" allowfullscreen></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<div id="myModalNewsletter" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Como configurar los correos promocionales
</h4>
      </div>
      <div class="modal-body">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/d2yKTHtFCB0" frameborder="0" allowfullscreen></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



<div id="myModalAdjunto" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Adjuntar imagenes para los doctores
</h4>
      </div>
      <div class="modal-body">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/S2kCGeqCiSM" frameborder="0" allowfullscreen></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>