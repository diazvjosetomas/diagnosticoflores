<script src="//cdn.ckeditor.com/4.6.2/full/ckeditor.js"></script>
<div class="container">
<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Envío de Email</h3>
		
		
	</div>	    
</div>
<div class="panel">
	<div class="panel-heading" style="padding: 12px;">
		<?php echo __('Envío de Email'); ?>	</div>
</div>
<div class="panel">
	
	<div class="panel-body">
<div >
	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12">
			<input type="text" name="remitente" id="remitente" class="form-control" placeholder="Remitente">
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12">
			<input type="text" name="asunto" id="asunto" class="form-control" placeholder="Asunto">
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12">
			<input type="textarea" name="body" id="body" class="form-control" >
		</div>
	</div>
	<br>

	

	<div class="row pull-right">
		<button onclick="enviamail()" class="btn btn-success">
				<i class="fa fa-envelope-o"></i>
				Enviar Mail
		</button>
	</div>

	<script type="text/javascript">
		function enviamail(){
			var remitente = $("#remitente").val();

			if (remitente == '') {
				alert('No puede dejar el remitente vacío.');
				return false;
			}

			var body = $("#body").val();

			

			var asunto = $("#asunto").val();




			$.ajax({
				url: '/Email/enviamail/'+remitente+'/'+body+'/'+asunto,
				type: 'post',
				data: {},
				success:function(response){

					alert('Mensaje enviado con éxito!');

					window.location.reload();

				}

			});
		}
	</script>

	
</div>
</div>
</div>

<script>
            CKEDITOR.replace( 'body' );
        </script>
