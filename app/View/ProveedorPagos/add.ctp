<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Proveedor de Pagos </h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href=""> Home</a></li>	
					<li><i class="fa fa-arrow-left"></i>
					<a href="<?=$this->Html->url('/ProveedorPagos/')?>"> Volver a Proveedor de Pagos </a> </li>              
		</ol>		
	</div>	    
</div>


<div class="row">
		     <div class="col-sm-12">
	  		   <section class="panel" style="padding: 10px;">

<div class="proveedorPagos form">
<?php echo $this->Form->create('ProveedorPago'); ?>
	<fieldset>
		<legend><?php echo __('Añadir Proveedor de Pago'); ?></legend>
<div class='row'><div class='col-sm-12'>		<?php echo $this->Form->input('denominacion', array('label'=>'Denominacion','class'=>'form-control')); ?> 
 </div></div>	</fieldset>
 	<br>
	<div class="pull-right">		
		<?php echo $this->Form->submit(__('Submit', true), array('label'=>false, 'class'=>'btn btn-primary'));?>
		<?php echo $this->Form->end(__('')); ?>
	</div>

</div>

</section>
</div>
</div>
