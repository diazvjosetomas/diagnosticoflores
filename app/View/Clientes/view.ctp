<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Clientes</h3>
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
				<li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Clientes'), array('action' => 'index')); ?></li>             
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Cliente'); ?></h2>
	<table class="table table-striped">
	<tbody>
		<tr><td><?php echo __('Nombres'); ?></td>
		<td>
			<?php echo h($cliente['Cliente']['nombres']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Apellidos'); ?></td>
		<td>
			<?php echo h($cliente['Cliente']['apellidos']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Telefono'); ?></td>
		<td>
			<?php echo h($cliente['Cliente']['telefono']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Email'); ?></td>
		<td>
			<?php echo h($cliente['Cliente']['email']); ?>
			&nbsp;
		</td></tr>
	</tbody>
	</table>
</div>
</div>