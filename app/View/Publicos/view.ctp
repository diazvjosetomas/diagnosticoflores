<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Publicos</h3>
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
				<li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Publicos'), array('action' => 'index')); ?></li>             
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Publico'); ?></h2>
	<table class="table table-striped">
	<tbody>
		<tr><td><?php echo __('Id'); ?></td>
		<td>
			<?php echo h($publico['Publico']['id']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Denominacion'); ?></td>
		<td>
			<?php echo h($publico['Publico']['denominacion']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Tabla'); ?></td>
		<td>
			<?php echo h($publico['Publico']['tabla']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Campo'); ?></td>
		<td>
			<?php echo h($publico['Publico']['campo']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Created'); ?></td>
		<td>
			<?php echo h($publico['Publico']['created']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Modified'); ?></td>
		<td>
			<?php echo h($publico['Publico']['modified']); ?>
			&nbsp;
		</td></tr>
	
	</tbody>
	</table>
</div>
</div>