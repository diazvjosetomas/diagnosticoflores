<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Imgadjuntos</h3>
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
				<li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Imgadjuntos'), array('action' => 'index')); ?></li>             
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Imgadjunto'); ?></h2>
	<table class="table table-striped">
	<tbody>
		<tr><td><?php echo __('Id'); ?></td>
		<td>
			<?php echo h($imgadjunto['Imgadjunto']['id']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Adjunto'); ?></td>
		<td>
			<?php echo $this->Html->link($imgadjunto['Adjunto']['denominacion'], array('controller' => 'adjuntos', 'action' => 'view', $imgadjunto['Adjunto']['id'])); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Carpeta Imagen'); ?></td>
		<td>
			<?php echo h($imgadjunto['Imgadjunto']['carpeta_imagen']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Nombre Imagen'); ?></td>
		<td>
			<?php echo h($imgadjunto['Imgadjunto']['nombre_imagen']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Tipo Imagen'); ?></td>
		<td>
			<?php echo h($imgadjunto['Imgadjunto']['tipo_imagen']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Ruta Imagen'); ?></td>
		<td>
			<?php echo h($imgadjunto['Imgadjunto']['ruta_imagen']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Created'); ?></td>
		<td>
			<?php echo h($imgadjunto['Imgadjunto']['created']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Modified'); ?></td>
		<td>
			<?php echo h($imgadjunto['Imgadjunto']['modified']); ?>
			&nbsp;
		</td></tr>
	
	</tbody>
	</table>
</div>
</div>