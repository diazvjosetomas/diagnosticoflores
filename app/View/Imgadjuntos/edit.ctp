
<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Imgadjuntos</h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
                    <li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Imgadjuntos'), array('action' => 'index')); ?></li>
		</ol>		
	</div>	    
</div>


<div class="row">
		     <div class="col-sm-12">
	  		   <section class="panel" style="padding: 10px;">

<div class="imgadjuntos form">
<?php echo $this->Form->create('Imgadjunto'); ?>
	<fieldset>
		<legend><?php echo __('Edit Imgadjunto'); ?></legend>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('id', array('label'=>'id','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('adjunto_id', array('label'=>'adjunto_id','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('carpeta_imagen', array('label'=>'carpeta_imagen','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('nombre_imagen', array('label'=>'nombre_imagen','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('tipo_imagen', array('label'=>'tipo_imagen','class'=>'form-control')); ?> 
 </div>
</div>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('ruta_imagen', array('label'=>'ruta_imagen','class'=>'form-control')); ?> 
 </div>
</div>
	</fieldset>
	<br />
	<div class="pull-right">		
		<?php echo $this->Form->submit(__('Guardar', true), array('label'=>false, 'class'=>'btn btn-primary'));?>
		<?php echo $this->Form->end(__('')); ?>
	</div>

</div>

</section>
</div>
</div>
