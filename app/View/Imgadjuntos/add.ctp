
<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Imgadjuntos</h3>
		
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
                    <li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__(' Volver'), array('controller'=>'Adjuntos','action' => 'index')); ?></li>
		</ol>		
	</div>	    
</div>


<div class="row">
		     <div class="col-sm-12">
	  		   <section class="panel" style="padding: 10px;">

<div class="imgadjuntos form">
<?php echo $this->Form->create('Imgadjunto', array('enctype'=>'multipart/form-data')); ?>
	<fieldset>
		<legend><?php echo __('Adjuntar Imagen'); ?></legend>
<div class='row'>
<div class='col-sm-12'>
		<?php echo $this->Form->input('adjunto_id', array('label'=>'Denominacion','class'=>'form-control')); ?> 
 </div>
</div>

<div class='row'>
		 <div class='col-sm-12'>		
			<?php echo $this->Form->input('imagen', array('id'=>'logo','label'=>'Imagen', 'type'=>'file'));  ?>
	 	</div>
</div>




	</fieldset>
	<br />
	<div class="pull-right">		
		<?php echo $this->Form->submit(__('Guardar', true), array('label'=>false, 'class'=>'btn btn-primary'));?>
		<?php echo $this->Form->end(__('')); ?>
	</div>

</div>

</section>
</div>
</div>
