<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Notificaciones</h3>
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
				<li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Notificaciones'), array('action' => 'index')); ?></li>             
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Notificacione'); ?></h2>
	<table class="table table-striped">
	<tbody>
		<tr><td><?php echo __('Id'); ?></td>
		<td>
			<?php echo h($notificacione['Notificacione']['id']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Modulo'); ?></td>
		<td>
			<?php echo h($notificacione['Notificacione']['modulo']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Nombretabla'); ?></td>
		<td>
			<?php echo h($notificacione['Notificacione']['nombretabla']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Registros'); ?></td>
		<td>
			<?php echo h($notificacione['Notificacione']['registros']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Url'); ?></td>
		<td>
			<?php echo h($notificacione['Notificacione']['url']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Created'); ?></td>
		<td>
			<?php echo h($notificacione['Notificacione']['created']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Modified'); ?></td>
		<td>
			<?php echo h($notificacione['Notificacione']['modified']); ?>
			&nbsp;
		</td></tr>
	
	</tbody>
	</table>
</div>
</div>