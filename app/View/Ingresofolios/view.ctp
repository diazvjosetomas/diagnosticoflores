<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-th"></i> Ingreso folios</h3>
		<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="<?php echo $this->Html->url('/Dashboards/'); ?>"> Home</a></li>	
				<li><i class="fa fa-arrow-left"></i><?php echo $this->Html->link(__('Volver a Ingreso folios'), array('action' => 'index')); ?></li>             
		</ol>		
	</div>	    
</div>
<div class="panel">
<div class="panel panel-body">
<h2><?php echo __('Ingreso folios'); ?></h2>
	<table class="table table-striped">
	<tbody>
		<tr><td><?php echo __('Sucursale'); ?></td>
		<td>
			<?php echo h($ingresofolio['Sucursale']['denominacion']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Secuencia'); ?></td> 
		<td>
			<?php echo h($ingresofolio['Ingresofolio']['secuencia']); ?>
			&nbsp;
		</td></tr>
		<tr><td><?php echo __('Último'); ?></td>
		<td>
			<?php echo h($ingresofolio['Ingresofolio']['ultimo']); ?>
			&nbsp;
		</td></tr>
	</tbody>
	</table>
</div>
</div>