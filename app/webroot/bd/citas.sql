-- ---
-- Globals
-- ---

-- SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
-- SET FOREIGN_KEY_CHECKS=0;

-- ---
-- Table 'citas'
-- 
-- ---

DROP TABLE IF EXISTS `citas`;
		
CREATE TABLE `citas` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `tipo_cita_id` INTEGER NOT NULL,
  `nombres_apellidos` VARCHAR(250) NOT NULL,
  `email` VARCHAR(250) NULL,
  `mensaje` VARCHAR(2050) NOT NULL,
  `telefono` VARCHAR(60) NOT NULL,
  `fecha` DATE NULL,
  `hora` VARCHAR(9) NOT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'tipo_cita'
-- 
-- ---

DROP TABLE IF EXISTS `tipo_cita`;
		
CREATE TABLE `tipo_cita` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `categoria_id` INTEGER NOT NULL,
  `titulo_cita` VARCHAR(120) NOT NULL,
  `tiempo_estudio` VARCHAR(60) NULL,
  `costo` VARCHAR(60) NULL,
  `detalles_cita` VARCHAR(512) NOT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'categoria'
-- 
-- ---

DROP TABLE IF EXISTS `categoria`;
		
CREATE TABLE `categoria` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `categoria` VARCHAR(256) NOT NULL,
  `estatus` INTEGER NOT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Foreign Keys 
-- ---

ALTER TABLE `citas` ADD FOREIGN KEY (tipo_cita_id) REFERENCES `tipo_cita` (`id`);
ALTER TABLE `tipo_cita` ADD FOREIGN KEY (categoria_id) REFERENCES `categoria` (`id`);

-- ---
-- Table Properties
-- ---

-- ALTER TABLE `citas` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `tipo_cita` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `categoria` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ---
-- Test Data
-- ---

-- INSERT INTO `citas` (`id`,`tipo_cita_id`,`nombres_apellidos`,`email`,`mensaje`,`telefono`,`fecha`,`hora`) VALUES
-- ('','','','','','','','');
-- INSERT INTO `tipo_cita` (`id`,`categoria_id`,`titulo_cita`,`tiempo_estudio`,`costo`,`detalles_cita`) VALUES
-- ('','','','','','');
-- INSERT INTO `categoria` (`id`,`categoria`,`estatus`) VALUES
-- ('','','');